/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.clemson.asam.asam.AsamPackage
 * @generated
 */
public interface AsamFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  AsamFactory eINSTANCE = edu.clemson.asam.asam.impl.AsamFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Library</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Library</em>'.
   * @generated
   */
  AsamLibrary createAsamLibrary();

  /**
   * Returns a new object of class '<em>Subclause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Subclause</em>'.
   * @generated
   */
  AsamSubclause createAsamSubclause();

  /**
   * Returns a new object of class '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract</em>'.
   * @generated
   */
  Contract createContract();

  /**
   * Returns a new object of class '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement</em>'.
   * @generated
   */
  AsamStatement createAsamStatement();

  /**
   * Returns a new object of class '<em>Internal Failure Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Internal Failure Statement</em>'.
   * @generated
   */
  InternalFailureStatement createInternalFailureStatement();

  /**
   * Returns a new object of class '<em>Safety Constraint Handles Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Safety Constraint Handles Statement</em>'.
   * @generated
   */
  SafetyConstraintHandlesStatement createSafetyConstraintHandlesStatement();

  /**
   * Returns a new object of class '<em>Safety Constraint Prevents Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Safety Constraint Prevents Statement</em>'.
   * @generated
   */
  SafetyConstraintPreventsStatement createSafetyConstraintPreventsStatement();

  /**
   * Returns a new object of class '<em>Error Propagation Rule Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Error Propagation Rule Statement</em>'.
   * @generated
   */
  ErrorPropagationRuleStatement createErrorPropagationRuleStatement();

  /**
   * Returns a new object of class '<em>Type Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Statement</em>'.
   * @generated
   */
  TypeStatement createTypeStatement();

  /**
   * Returns a new object of class '<em>Errors List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Errors List</em>'.
   * @generated
   */
  ErrorsList createErrorsList();

  /**
   * Returns a new object of class '<em>Errors List No Probability</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Errors List No Probability</em>'.
   * @generated
   */
  ErrorsListNoProbability createErrorsListNoProbability();

  /**
   * Returns a new object of class '<em>Ports List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ports List</em>'.
   * @generated
   */
  PortsList createPortsList();

  /**
   * Returns a new object of class '<em>Errs Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Errs Statement</em>'.
   * @generated
   */
  ErrsStatement createErrsStatement();

  /**
   * Returns a new object of class '<em>Error Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Error Statement</em>'.
   * @generated
   */
  ErrorStatement createErrorStatement();

  /**
   * Returns a new object of class '<em>Error Type Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Error Type Statement</em>'.
   * @generated
   */
  ErrorTypeStatement createErrorTypeStatement();

  /**
   * Returns a new object of class '<em>Unsafe Control Action Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unsafe Control Action Statement</em>'.
   * @generated
   */
  UnsafeControlActionStatement createUnsafeControlActionStatement();

  /**
   * Returns a new object of class '<em>Causes Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Causes Statement</em>'.
   * @generated
   */
  CausesStatement createCausesStatement();

  /**
   * Returns a new object of class '<em>General Cause Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>General Cause Statement</em>'.
   * @generated
   */
  GeneralCauseStatement createGeneralCauseStatement();

  /**
   * Returns a new object of class '<em>Specific Cause Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Specific Cause Statement</em>'.
   * @generated
   */
  SpecificCauseStatement createSpecificCauseStatement();

  /**
   * Returns a new object of class '<em>Safety Constraint Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Safety Constraint Statement</em>'.
   * @generated
   */
  SafetyConstraintStatement createSafetyConstraintStatement();

  /**
   * Returns a new object of class '<em>Guarantee Ids</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Guarantee Ids</em>'.
   * @generated
   */
  GuaranteeIds createGuaranteeIds();

  /**
   * Returns a new object of class '<em>Error</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Error</em>'.
   * @generated
   */
  Error createError();

  /**
   * Returns a new object of class '<em>Error No Probability</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Error No Probability</em>'.
   * @generated
   */
  ErrorNoProbability createErrorNoProbability();

  /**
   * Returns a new object of class '<em>Contract Library</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract Library</em>'.
   * @generated
   */
  AsamContractLibrary createAsamContractLibrary();

  /**
   * Returns a new object of class '<em>Contract Subclause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract Subclause</em>'.
   * @generated
   */
  AsamContractSubclause createAsamContractSubclause();

  /**
   * Returns a new object of class '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Contract</em>'.
   * @generated
   */
  AsamContract createAsamContract();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  AsamPackage getAsamPackage();

} //AsamFactory
