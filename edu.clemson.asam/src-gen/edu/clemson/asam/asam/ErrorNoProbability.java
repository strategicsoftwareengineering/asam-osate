/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error No Probability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.ErrorNoProbability#getError <em>Error</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getErrorNoProbability()
 * @model
 * @generated
 */
public interface ErrorNoProbability extends EObject
{
  /**
   * Returns the value of the '<em><b>Error</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Error</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Error</em>' attribute.
   * @see #setError(String)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorNoProbability_Error()
   * @model
   * @generated
   */
  String getError();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorNoProbability#getError <em>Error</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Error</em>' attribute.
   * @see #getError()
   * @generated
   */
  void setError(String value);

} // ErrorNoProbability
