/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.ErrorTypeStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error Type Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorTypeStatementImpl#getErrorType <em>Error Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ErrorTypeStatementImpl extends MinimalEObjectImpl.Container implements ErrorTypeStatement
{
  /**
   * The cached value of the '{@link #getErrorType() <em>Error Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getErrorType()
   * @generated
   * @ordered
   */
  protected edu.clemson.asam.asam.Error errorType;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ErrorTypeStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.ERROR_TYPE_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.asam.asam.Error getErrorType()
  {
    return errorType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetErrorType(edu.clemson.asam.asam.Error newErrorType, NotificationChain msgs)
  {
    edu.clemson.asam.asam.Error oldErrorType = errorType;
    errorType = newErrorType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE, oldErrorType, newErrorType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setErrorType(edu.clemson.asam.asam.Error newErrorType)
  {
    if (newErrorType != errorType)
    {
      NotificationChain msgs = null;
      if (errorType != null)
        msgs = ((InternalEObject)errorType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE, null, msgs);
      if (newErrorType != null)
        msgs = ((InternalEObject)newErrorType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE, null, msgs);
      msgs = basicSetErrorType(newErrorType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE, newErrorType, newErrorType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE:
        return basicSetErrorType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE:
        return getErrorType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE:
        setErrorType((edu.clemson.asam.asam.Error)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE:
        setErrorType((edu.clemson.asam.asam.Error)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_TYPE_STATEMENT__ERROR_TYPE:
        return errorType != null;
    }
    return super.eIsSet(featureID);
  }

} //ErrorTypeStatementImpl
