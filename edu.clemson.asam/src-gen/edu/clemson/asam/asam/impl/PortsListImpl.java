/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.PortsList;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ports List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.PortsListImpl#getFirstPort <em>First Port</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.PortsListImpl#getRestPorts <em>Rest Ports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortsListImpl extends MinimalEObjectImpl.Container implements PortsList
{
  /**
   * The default value of the '{@link #getFirstPort() <em>First Port</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirstPort()
   * @generated
   * @ordered
   */
  protected static final String FIRST_PORT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFirstPort() <em>First Port</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirstPort()
   * @generated
   * @ordered
   */
  protected String firstPort = FIRST_PORT_EDEFAULT;

  /**
   * The cached value of the '{@link #getRestPorts() <em>Rest Ports</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRestPorts()
   * @generated
   * @ordered
   */
  protected EList<String> restPorts;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortsListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.PORTS_LIST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFirstPort()
  {
    return firstPort;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFirstPort(String newFirstPort)
  {
    String oldFirstPort = firstPort;
    firstPort = newFirstPort;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.PORTS_LIST__FIRST_PORT, oldFirstPort, firstPort));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getRestPorts()
  {
    if (restPorts == null)
    {
      restPorts = new EDataTypeEList<String>(String.class, this, AsamPackage.PORTS_LIST__REST_PORTS);
    }
    return restPorts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.PORTS_LIST__FIRST_PORT:
        return getFirstPort();
      case AsamPackage.PORTS_LIST__REST_PORTS:
        return getRestPorts();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.PORTS_LIST__FIRST_PORT:
        setFirstPort((String)newValue);
        return;
      case AsamPackage.PORTS_LIST__REST_PORTS:
        getRestPorts().clear();
        getRestPorts().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.PORTS_LIST__FIRST_PORT:
        setFirstPort(FIRST_PORT_EDEFAULT);
        return;
      case AsamPackage.PORTS_LIST__REST_PORTS:
        getRestPorts().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.PORTS_LIST__FIRST_PORT:
        return FIRST_PORT_EDEFAULT == null ? firstPort != null : !FIRST_PORT_EDEFAULT.equals(firstPort);
      case AsamPackage.PORTS_LIST__REST_PORTS:
        return restPorts != null && !restPorts.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (firstPort: ");
    result.append(firstPort);
    result.append(", restPorts: ");
    result.append(restPorts);
    result.append(')');
    return result.toString();
  }

} //PortsListImpl
