/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.ErrorsListNoProbability;
import edu.clemson.asam.asam.GuaranteeIds;
import edu.clemson.asam.asam.PortsList;
import edu.clemson.asam.asam.SafetyConstraintHandlesStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Safety Constraint Handles Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl#getSafetyConstraintId <em>Safety Constraint Id</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl#getSafetyConstraintDescription <em>Safety Constraint Description</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl#getErrors <em>Errors</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl#getPorts <em>Ports</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SafetyConstraintHandlesStatementImpl extends AsamStatementImpl implements SafetyConstraintHandlesStatement
{
  /**
   * The default value of the '{@link #getSafetyConstraintId() <em>Safety Constraint Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSafetyConstraintId()
   * @generated
   * @ordered
   */
  protected static final String SAFETY_CONSTRAINT_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSafetyConstraintId() <em>Safety Constraint Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSafetyConstraintId()
   * @generated
   * @ordered
   */
  protected String safetyConstraintId = SAFETY_CONSTRAINT_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getSafetyConstraintDescription() <em>Safety Constraint Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSafetyConstraintDescription()
   * @generated
   * @ordered
   */
  protected static final String SAFETY_CONSTRAINT_DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSafetyConstraintDescription() <em>Safety Constraint Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSafetyConstraintDescription()
   * @generated
   * @ordered
   */
  protected String safetyConstraintDescription = SAFETY_CONSTRAINT_DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getErrors() <em>Errors</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getErrors()
   * @generated
   * @ordered
   */
  protected ErrorsListNoProbability errors;

  /**
   * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPorts()
   * @generated
   * @ordered
   */
  protected PortsList ports;

  /**
   * The cached value of the '{@link #getMatchingXagreeStatements() <em>Matching Xagree Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMatchingXagreeStatements()
   * @generated
   * @ordered
   */
  protected GuaranteeIds matchingXagreeStatements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SafetyConstraintHandlesStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.SAFETY_CONSTRAINT_HANDLES_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSafetyConstraintId()
  {
    return safetyConstraintId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSafetyConstraintId(String newSafetyConstraintId)
  {
    String oldSafetyConstraintId = safetyConstraintId;
    safetyConstraintId = newSafetyConstraintId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID, oldSafetyConstraintId, safetyConstraintId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSafetyConstraintDescription()
  {
    return safetyConstraintDescription;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSafetyConstraintDescription(String newSafetyConstraintDescription)
  {
    String oldSafetyConstraintDescription = safetyConstraintDescription;
    safetyConstraintDescription = newSafetyConstraintDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION, oldSafetyConstraintDescription, safetyConstraintDescription));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorsListNoProbability getErrors()
  {
    return errors;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetErrors(ErrorsListNoProbability newErrors, NotificationChain msgs)
  {
    ErrorsListNoProbability oldErrors = errors;
    errors = newErrors;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS, oldErrors, newErrors);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setErrors(ErrorsListNoProbability newErrors)
  {
    if (newErrors != errors)
    {
      NotificationChain msgs = null;
      if (errors != null)
        msgs = ((InternalEObject)errors).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS, null, msgs);
      if (newErrors != null)
        msgs = ((InternalEObject)newErrors).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS, null, msgs);
      msgs = basicSetErrors(newErrors, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS, newErrors, newErrors));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortsList getPorts()
  {
    return ports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPorts(PortsList newPorts, NotificationChain msgs)
  {
    PortsList oldPorts = ports;
    ports = newPorts;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS, oldPorts, newPorts);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPorts(PortsList newPorts)
  {
    if (newPorts != ports)
    {
      NotificationChain msgs = null;
      if (ports != null)
        msgs = ((InternalEObject)ports).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS, null, msgs);
      if (newPorts != null)
        msgs = ((InternalEObject)newPorts).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS, null, msgs);
      msgs = basicSetPorts(newPorts, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS, newPorts, newPorts));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuaranteeIds getMatchingXagreeStatements()
  {
    return matchingXagreeStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMatchingXagreeStatements(GuaranteeIds newMatchingXagreeStatements, NotificationChain msgs)
  {
    GuaranteeIds oldMatchingXagreeStatements = matchingXagreeStatements;
    matchingXagreeStatements = newMatchingXagreeStatements;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS, oldMatchingXagreeStatements, newMatchingXagreeStatements);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMatchingXagreeStatements(GuaranteeIds newMatchingXagreeStatements)
  {
    if (newMatchingXagreeStatements != matchingXagreeStatements)
    {
      NotificationChain msgs = null;
      if (matchingXagreeStatements != null)
        msgs = ((InternalEObject)matchingXagreeStatements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS, null, msgs);
      if (newMatchingXagreeStatements != null)
        msgs = ((InternalEObject)newMatchingXagreeStatements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS, null, msgs);
      msgs = basicSetMatchingXagreeStatements(newMatchingXagreeStatements, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS, newMatchingXagreeStatements, newMatchingXagreeStatements));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS:
        return basicSetErrors(null, msgs);
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS:
        return basicSetPorts(null, msgs);
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        return basicSetMatchingXagreeStatements(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID:
        return getSafetyConstraintId();
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION:
        return getSafetyConstraintDescription();
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS:
        return getErrors();
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS:
        return getPorts();
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        return getMatchingXagreeStatements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID:
        setSafetyConstraintId((String)newValue);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION:
        setSafetyConstraintDescription((String)newValue);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS:
        setErrors((ErrorsListNoProbability)newValue);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS:
        setPorts((PortsList)newValue);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        setMatchingXagreeStatements((GuaranteeIds)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID:
        setSafetyConstraintId(SAFETY_CONSTRAINT_ID_EDEFAULT);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION:
        setSafetyConstraintDescription(SAFETY_CONSTRAINT_DESCRIPTION_EDEFAULT);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS:
        setErrors((ErrorsListNoProbability)null);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS:
        setPorts((PortsList)null);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        setMatchingXagreeStatements((GuaranteeIds)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID:
        return SAFETY_CONSTRAINT_ID_EDEFAULT == null ? safetyConstraintId != null : !SAFETY_CONSTRAINT_ID_EDEFAULT.equals(safetyConstraintId);
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION:
        return SAFETY_CONSTRAINT_DESCRIPTION_EDEFAULT == null ? safetyConstraintDescription != null : !SAFETY_CONSTRAINT_DESCRIPTION_EDEFAULT.equals(safetyConstraintDescription);
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS:
        return errors != null;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS:
        return ports != null;
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        return matchingXagreeStatements != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (safetyConstraintId: ");
    result.append(safetyConstraintId);
    result.append(", safetyConstraintDescription: ");
    result.append(safetyConstraintDescription);
    result.append(')');
    return result.toString();
  }

} //SafetyConstraintHandlesStatementImpl
