/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.ErrorPropagationRuleStatement;
import edu.clemson.asam.asam.ErrorsList;
import edu.clemson.asam.asam.ErrorsListNoProbability;
import edu.clemson.asam.asam.PortsList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error Propagation Rule Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl#getInErrorsList <em>In Errors List</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl#getInPortsLists <em>In Ports Lists</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl#getOutErrorsList <em>Out Errors List</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl#getOutPortsLists <em>Out Ports Lists</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ErrorPropagationRuleStatementImpl extends AsamStatementImpl implements ErrorPropagationRuleStatement
{
  /**
   * The cached value of the '{@link #getInErrorsList() <em>In Errors List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInErrorsList()
   * @generated
   * @ordered
   */
  protected ErrorsListNoProbability inErrorsList;

  /**
   * The cached value of the '{@link #getInPortsLists() <em>In Ports Lists</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInPortsLists()
   * @generated
   * @ordered
   */
  protected PortsList inPortsLists;

  /**
   * The cached value of the '{@link #getOutErrorsList() <em>Out Errors List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutErrorsList()
   * @generated
   * @ordered
   */
  protected ErrorsList outErrorsList;

  /**
   * The cached value of the '{@link #getOutPortsLists() <em>Out Ports Lists</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutPortsLists()
   * @generated
   * @ordered
   */
  protected PortsList outPortsLists;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ErrorPropagationRuleStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.ERROR_PROPAGATION_RULE_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorsListNoProbability getInErrorsList()
  {
    return inErrorsList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInErrorsList(ErrorsListNoProbability newInErrorsList, NotificationChain msgs)
  {
    ErrorsListNoProbability oldInErrorsList = inErrorsList;
    inErrorsList = newInErrorsList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST, oldInErrorsList, newInErrorsList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInErrorsList(ErrorsListNoProbability newInErrorsList)
  {
    if (newInErrorsList != inErrorsList)
    {
      NotificationChain msgs = null;
      if (inErrorsList != null)
        msgs = ((InternalEObject)inErrorsList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST, null, msgs);
      if (newInErrorsList != null)
        msgs = ((InternalEObject)newInErrorsList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST, null, msgs);
      msgs = basicSetInErrorsList(newInErrorsList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST, newInErrorsList, newInErrorsList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortsList getInPortsLists()
  {
    return inPortsLists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInPortsLists(PortsList newInPortsLists, NotificationChain msgs)
  {
    PortsList oldInPortsLists = inPortsLists;
    inPortsLists = newInPortsLists;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS, oldInPortsLists, newInPortsLists);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInPortsLists(PortsList newInPortsLists)
  {
    if (newInPortsLists != inPortsLists)
    {
      NotificationChain msgs = null;
      if (inPortsLists != null)
        msgs = ((InternalEObject)inPortsLists).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS, null, msgs);
      if (newInPortsLists != null)
        msgs = ((InternalEObject)newInPortsLists).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS, null, msgs);
      msgs = basicSetInPortsLists(newInPortsLists, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS, newInPortsLists, newInPortsLists));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorsList getOutErrorsList()
  {
    return outErrorsList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOutErrorsList(ErrorsList newOutErrorsList, NotificationChain msgs)
  {
    ErrorsList oldOutErrorsList = outErrorsList;
    outErrorsList = newOutErrorsList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST, oldOutErrorsList, newOutErrorsList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOutErrorsList(ErrorsList newOutErrorsList)
  {
    if (newOutErrorsList != outErrorsList)
    {
      NotificationChain msgs = null;
      if (outErrorsList != null)
        msgs = ((InternalEObject)outErrorsList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST, null, msgs);
      if (newOutErrorsList != null)
        msgs = ((InternalEObject)newOutErrorsList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST, null, msgs);
      msgs = basicSetOutErrorsList(newOutErrorsList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST, newOutErrorsList, newOutErrorsList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortsList getOutPortsLists()
  {
    return outPortsLists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOutPortsLists(PortsList newOutPortsLists, NotificationChain msgs)
  {
    PortsList oldOutPortsLists = outPortsLists;
    outPortsLists = newOutPortsLists;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS, oldOutPortsLists, newOutPortsLists);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOutPortsLists(PortsList newOutPortsLists)
  {
    if (newOutPortsLists != outPortsLists)
    {
      NotificationChain msgs = null;
      if (outPortsLists != null)
        msgs = ((InternalEObject)outPortsLists).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS, null, msgs);
      if (newOutPortsLists != null)
        msgs = ((InternalEObject)newOutPortsLists).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS, null, msgs);
      msgs = basicSetOutPortsLists(newOutPortsLists, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS, newOutPortsLists, newOutPortsLists));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST:
        return basicSetInErrorsList(null, msgs);
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS:
        return basicSetInPortsLists(null, msgs);
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST:
        return basicSetOutErrorsList(null, msgs);
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS:
        return basicSetOutPortsLists(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST:
        return getInErrorsList();
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS:
        return getInPortsLists();
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST:
        return getOutErrorsList();
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS:
        return getOutPortsLists();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST:
        setInErrorsList((ErrorsListNoProbability)newValue);
        return;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS:
        setInPortsLists((PortsList)newValue);
        return;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST:
        setOutErrorsList((ErrorsList)newValue);
        return;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS:
        setOutPortsLists((PortsList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST:
        setInErrorsList((ErrorsListNoProbability)null);
        return;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS:
        setInPortsLists((PortsList)null);
        return;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST:
        setOutErrorsList((ErrorsList)null);
        return;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS:
        setOutPortsLists((PortsList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST:
        return inErrorsList != null;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS:
        return inPortsLists != null;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST:
        return outErrorsList != null;
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS:
        return outPortsLists != null;
    }
    return super.eIsSet(featureID);
  }

} //ErrorPropagationRuleStatementImpl
