/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.CausesStatement;
import edu.clemson.asam.asam.GeneralCauseStatement;
import edu.clemson.asam.asam.SpecificCauseStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Causes Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.CausesStatementImpl#getGeneral <em>General</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.CausesStatementImpl#getSpecific <em>Specific</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CausesStatementImpl extends MinimalEObjectImpl.Container implements CausesStatement
{
  /**
   * The cached value of the '{@link #getGeneral() <em>General</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGeneral()
   * @generated
   * @ordered
   */
  protected GeneralCauseStatement general;

  /**
   * The cached value of the '{@link #getSpecific() <em>Specific</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpecific()
   * @generated
   * @ordered
   */
  protected SpecificCauseStatement specific;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CausesStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.CAUSES_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GeneralCauseStatement getGeneral()
  {
    return general;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGeneral(GeneralCauseStatement newGeneral, NotificationChain msgs)
  {
    GeneralCauseStatement oldGeneral = general;
    general = newGeneral;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.CAUSES_STATEMENT__GENERAL, oldGeneral, newGeneral);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGeneral(GeneralCauseStatement newGeneral)
  {
    if (newGeneral != general)
    {
      NotificationChain msgs = null;
      if (general != null)
        msgs = ((InternalEObject)general).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.CAUSES_STATEMENT__GENERAL, null, msgs);
      if (newGeneral != null)
        msgs = ((InternalEObject)newGeneral).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.CAUSES_STATEMENT__GENERAL, null, msgs);
      msgs = basicSetGeneral(newGeneral, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.CAUSES_STATEMENT__GENERAL, newGeneral, newGeneral));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecificCauseStatement getSpecific()
  {
    return specific;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSpecific(SpecificCauseStatement newSpecific, NotificationChain msgs)
  {
    SpecificCauseStatement oldSpecific = specific;
    specific = newSpecific;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.CAUSES_STATEMENT__SPECIFIC, oldSpecific, newSpecific);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSpecific(SpecificCauseStatement newSpecific)
  {
    if (newSpecific != specific)
    {
      NotificationChain msgs = null;
      if (specific != null)
        msgs = ((InternalEObject)specific).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.CAUSES_STATEMENT__SPECIFIC, null, msgs);
      if (newSpecific != null)
        msgs = ((InternalEObject)newSpecific).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.CAUSES_STATEMENT__SPECIFIC, null, msgs);
      msgs = basicSetSpecific(newSpecific, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.CAUSES_STATEMENT__SPECIFIC, newSpecific, newSpecific));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.CAUSES_STATEMENT__GENERAL:
        return basicSetGeneral(null, msgs);
      case AsamPackage.CAUSES_STATEMENT__SPECIFIC:
        return basicSetSpecific(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.CAUSES_STATEMENT__GENERAL:
        return getGeneral();
      case AsamPackage.CAUSES_STATEMENT__SPECIFIC:
        return getSpecific();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.CAUSES_STATEMENT__GENERAL:
        setGeneral((GeneralCauseStatement)newValue);
        return;
      case AsamPackage.CAUSES_STATEMENT__SPECIFIC:
        setSpecific((SpecificCauseStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.CAUSES_STATEMENT__GENERAL:
        setGeneral((GeneralCauseStatement)null);
        return;
      case AsamPackage.CAUSES_STATEMENT__SPECIFIC:
        setSpecific((SpecificCauseStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.CAUSES_STATEMENT__GENERAL:
        return general != null;
      case AsamPackage.CAUSES_STATEMENT__SPECIFIC:
        return specific != null;
    }
    return super.eIsSet(featureID);
  }

} //CausesStatementImpl
