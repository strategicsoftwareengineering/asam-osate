/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.ErrorsList;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Errors List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorsListImpl#getFirstError <em>First Error</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorsListImpl#getRestErrors <em>Rest Errors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ErrorsListImpl extends MinimalEObjectImpl.Container implements ErrorsList
{
  /**
   * The cached value of the '{@link #getFirstError() <em>First Error</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirstError()
   * @generated
   * @ordered
   */
  protected edu.clemson.asam.asam.Error firstError;

  /**
   * The cached value of the '{@link #getRestErrors() <em>Rest Errors</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRestErrors()
   * @generated
   * @ordered
   */
  protected EList<edu.clemson.asam.asam.Error> restErrors;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ErrorsListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.ERRORS_LIST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.asam.asam.Error getFirstError()
  {
    return firstError;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFirstError(edu.clemson.asam.asam.Error newFirstError, NotificationChain msgs)
  {
    edu.clemson.asam.asam.Error oldFirstError = firstError;
    firstError = newFirstError;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERRORS_LIST__FIRST_ERROR, oldFirstError, newFirstError);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFirstError(edu.clemson.asam.asam.Error newFirstError)
  {
    if (newFirstError != firstError)
    {
      NotificationChain msgs = null;
      if (firstError != null)
        msgs = ((InternalEObject)firstError).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERRORS_LIST__FIRST_ERROR, null, msgs);
      if (newFirstError != null)
        msgs = ((InternalEObject)newFirstError).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERRORS_LIST__FIRST_ERROR, null, msgs);
      msgs = basicSetFirstError(newFirstError, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERRORS_LIST__FIRST_ERROR, newFirstError, newFirstError));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<edu.clemson.asam.asam.Error> getRestErrors()
  {
    if (restErrors == null)
    {
      restErrors = new EObjectContainmentEList<edu.clemson.asam.asam.Error>(edu.clemson.asam.asam.Error.class, this, AsamPackage.ERRORS_LIST__REST_ERRORS);
    }
    return restErrors;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.ERRORS_LIST__FIRST_ERROR:
        return basicSetFirstError(null, msgs);
      case AsamPackage.ERRORS_LIST__REST_ERRORS:
        return ((InternalEList<?>)getRestErrors()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.ERRORS_LIST__FIRST_ERROR:
        return getFirstError();
      case AsamPackage.ERRORS_LIST__REST_ERRORS:
        return getRestErrors();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.ERRORS_LIST__FIRST_ERROR:
        setFirstError((edu.clemson.asam.asam.Error)newValue);
        return;
      case AsamPackage.ERRORS_LIST__REST_ERRORS:
        getRestErrors().clear();
        getRestErrors().addAll((Collection<? extends edu.clemson.asam.asam.Error>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERRORS_LIST__FIRST_ERROR:
        setFirstError((edu.clemson.asam.asam.Error)null);
        return;
      case AsamPackage.ERRORS_LIST__REST_ERRORS:
        getRestErrors().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERRORS_LIST__FIRST_ERROR:
        return firstError != null;
      case AsamPackage.ERRORS_LIST__REST_ERRORS:
        return restErrors != null && !restErrors.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ErrorsListImpl
