/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamContract;
import edu.clemson.asam.asam.AsamContractLibrary;
import edu.clemson.asam.asam.AsamContractSubclause;
import edu.clemson.asam.asam.AsamFactory;
import edu.clemson.asam.asam.AsamLibrary;
import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.AsamStatement;
import edu.clemson.asam.asam.AsamSubclause;
import edu.clemson.asam.asam.CausesStatement;
import edu.clemson.asam.asam.Contract;
import edu.clemson.asam.asam.ErrorNoProbability;
import edu.clemson.asam.asam.ErrorPropagationRuleStatement;
import edu.clemson.asam.asam.ErrorStatement;
import edu.clemson.asam.asam.ErrorTypeStatement;
import edu.clemson.asam.asam.ErrorsList;
import edu.clemson.asam.asam.ErrorsListNoProbability;
import edu.clemson.asam.asam.ErrsStatement;
import edu.clemson.asam.asam.GeneralCauseStatement;
import edu.clemson.asam.asam.GuaranteeIds;
import edu.clemson.asam.asam.InternalFailureStatement;
import edu.clemson.asam.asam.PortsList;
import edu.clemson.asam.asam.SafetyConstraintHandlesStatement;
import edu.clemson.asam.asam.SafetyConstraintPreventsStatement;
import edu.clemson.asam.asam.SafetyConstraintStatement;
import edu.clemson.asam.asam.SpecificCauseStatement;
import edu.clemson.asam.asam.TypeStatement;
import edu.clemson.asam.asam.UnsafeControlActionStatement;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AsamPackageImpl extends EPackageImpl implements AsamPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass asamLibraryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass asamSubclauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass contractEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass asamStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass internalFailureStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass safetyConstraintHandlesStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass safetyConstraintPreventsStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorPropagationRuleStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorsListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorsListNoProbabilityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portsListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errsStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorTypeStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unsafeControlActionStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass causesStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass generalCauseStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass specificCauseStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass safetyConstraintStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass guaranteeIdsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorNoProbabilityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass asamContractLibraryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass asamContractSubclauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass asamContractEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see edu.clemson.asam.asam.AsamPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private AsamPackageImpl()
  {
    super(eNS_URI, AsamFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link AsamPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static AsamPackage init()
  {
    if (isInited) return (AsamPackage)EPackage.Registry.INSTANCE.getEPackage(AsamPackage.eNS_URI);

    // Obtain or create and register package
    AsamPackageImpl theAsamPackage = (AsamPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AsamPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AsamPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    Aadl2Package.eINSTANCE.eClass();

    // Create package meta-data objects
    theAsamPackage.createPackageContents();

    // Initialize created meta-data
    theAsamPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theAsamPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(AsamPackage.eNS_URI, theAsamPackage);
    return theAsamPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAsamLibrary()
  {
    return asamLibraryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAsamSubclause()
  {
    return asamSubclauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getContract()
  {
    return contractEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAsamStatement()
  {
    return asamStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInternalFailureStatement()
  {
    return internalFailureStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInternalFailureStatement_InternalFailureId()
  {
    return (EAttribute)internalFailureStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInternalFailureStatement_InternalFailureDescription()
  {
    return (EAttribute)internalFailureStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInternalFailureStatement_Errors()
  {
    return (EReference)internalFailureStatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInternalFailureStatement_Ports()
  {
    return (EReference)internalFailureStatementEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSafetyConstraintHandlesStatement()
  {
    return safetyConstraintHandlesStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSafetyConstraintHandlesStatement_SafetyConstraintId()
  {
    return (EAttribute)safetyConstraintHandlesStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSafetyConstraintHandlesStatement_SafetyConstraintDescription()
  {
    return (EAttribute)safetyConstraintHandlesStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSafetyConstraintHandlesStatement_Errors()
  {
    return (EReference)safetyConstraintHandlesStatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSafetyConstraintHandlesStatement_Ports()
  {
    return (EReference)safetyConstraintHandlesStatementEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSafetyConstraintHandlesStatement_MatchingXagreeStatements()
  {
    return (EReference)safetyConstraintHandlesStatementEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSafetyConstraintPreventsStatement()
  {
    return safetyConstraintPreventsStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSafetyConstraintPreventsStatement_SafetyConstraintId()
  {
    return (EAttribute)safetyConstraintPreventsStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSafetyConstraintPreventsStatement_SafetyConstraintDescription()
  {
    return (EAttribute)safetyConstraintPreventsStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSafetyConstraintPreventsStatement_Errors()
  {
    return (EReference)safetyConstraintPreventsStatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSafetyConstraintPreventsStatement_Ports()
  {
    return (EReference)safetyConstraintPreventsStatementEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSafetyConstraintPreventsStatement_MatchingXagreeStatements()
  {
    return (EReference)safetyConstraintPreventsStatementEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrorPropagationRuleStatement()
  {
    return errorPropagationRuleStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorPropagationRuleStatement_InErrorsList()
  {
    return (EReference)errorPropagationRuleStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorPropagationRuleStatement_InPortsLists()
  {
    return (EReference)errorPropagationRuleStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorPropagationRuleStatement_OutErrorsList()
  {
    return (EReference)errorPropagationRuleStatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorPropagationRuleStatement_OutPortsLists()
  {
    return (EReference)errorPropagationRuleStatementEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeStatement()
  {
    return typeStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTypeStatement_Type()
  {
    return (EAttribute)typeStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrorsList()
  {
    return errorsListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorsList_FirstError()
  {
    return (EReference)errorsListEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorsList_RestErrors()
  {
    return (EReference)errorsListEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrorsListNoProbability()
  {
    return errorsListNoProbabilityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorsListNoProbability_FirstError()
  {
    return (EReference)errorsListNoProbabilityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorsListNoProbability_RestErrors()
  {
    return (EReference)errorsListNoProbabilityEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortsList()
  {
    return portsListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPortsList_FirstPort()
  {
    return (EAttribute)portsListEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPortsList_RestPorts()
  {
    return (EAttribute)portsListEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrsStatement()
  {
    return errsStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrsStatement_FirstError()
  {
    return (EReference)errsStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrsStatement_RestErrors()
  {
    return (EReference)errsStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrorStatement()
  {
    return errorStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorStatement_Type()
  {
    return (EReference)errorStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorStatement_Uca()
  {
    return (EReference)errorStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorStatement_Cause()
  {
    return (EReference)errorStatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorStatement_Sc()
  {
    return (EReference)errorStatementEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrorTypeStatement()
  {
    return errorTypeStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getErrorTypeStatement_ErrorType()
  {
    return (EReference)errorTypeStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnsafeControlActionStatement()
  {
    return unsafeControlActionStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUnsafeControlActionStatement_Id()
  {
    return (EAttribute)unsafeControlActionStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUnsafeControlActionStatement_Description()
  {
    return (EAttribute)unsafeControlActionStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCausesStatement()
  {
    return causesStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCausesStatement_General()
  {
    return (EReference)causesStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCausesStatement_Specific()
  {
    return (EReference)causesStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGeneralCauseStatement()
  {
    return generalCauseStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGeneralCauseStatement_Description()
  {
    return (EAttribute)generalCauseStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSpecificCauseStatement()
  {
    return specificCauseStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSpecificCauseStatement_Description()
  {
    return (EAttribute)specificCauseStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSafetyConstraintStatement()
  {
    return safetyConstraintStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSafetyConstraintStatement_Id()
  {
    return (EAttribute)safetyConstraintStatementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSafetyConstraintStatement_Description()
  {
    return (EAttribute)safetyConstraintStatementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSafetyConstraintStatement_MatchingXagreeStatements()
  {
    return (EReference)safetyConstraintStatementEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGuaranteeIds()
  {
    return guaranteeIdsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGuaranteeIds_First()
  {
    return (EAttribute)guaranteeIdsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGuaranteeIds_Rest()
  {
    return (EAttribute)guaranteeIdsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getError()
  {
    return errorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getError_Error()
  {
    return (EAttribute)errorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getError_SeverityLevel()
  {
    return (EAttribute)errorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getError_Probability()
  {
    return (EAttribute)errorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrorNoProbability()
  {
    return errorNoProbabilityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getErrorNoProbability_Error()
  {
    return (EAttribute)errorNoProbabilityEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAsamContractLibrary()
  {
    return asamContractLibraryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAsamContractLibrary_Contract()
  {
    return (EReference)asamContractLibraryEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAsamContractSubclause()
  {
    return asamContractSubclauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAsamContractSubclause_Contract()
  {
    return (EReference)asamContractSubclauseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAsamContract()
  {
    return asamContractEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAsamContract_Statement()
  {
    return (EReference)asamContractEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamFactory getAsamFactory()
  {
    return (AsamFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    asamLibraryEClass = createEClass(ASAM_LIBRARY);

    asamSubclauseEClass = createEClass(ASAM_SUBCLAUSE);

    contractEClass = createEClass(CONTRACT);

    asamStatementEClass = createEClass(ASAM_STATEMENT);

    internalFailureStatementEClass = createEClass(INTERNAL_FAILURE_STATEMENT);
    createEAttribute(internalFailureStatementEClass, INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID);
    createEAttribute(internalFailureStatementEClass, INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION);
    createEReference(internalFailureStatementEClass, INTERNAL_FAILURE_STATEMENT__ERRORS);
    createEReference(internalFailureStatementEClass, INTERNAL_FAILURE_STATEMENT__PORTS);

    safetyConstraintHandlesStatementEClass = createEClass(SAFETY_CONSTRAINT_HANDLES_STATEMENT);
    createEAttribute(safetyConstraintHandlesStatementEClass, SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID);
    createEAttribute(safetyConstraintHandlesStatementEClass, SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION);
    createEReference(safetyConstraintHandlesStatementEClass, SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS);
    createEReference(safetyConstraintHandlesStatementEClass, SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS);
    createEReference(safetyConstraintHandlesStatementEClass, SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS);

    safetyConstraintPreventsStatementEClass = createEClass(SAFETY_CONSTRAINT_PREVENTS_STATEMENT);
    createEAttribute(safetyConstraintPreventsStatementEClass, SAFETY_CONSTRAINT_PREVENTS_STATEMENT__SAFETY_CONSTRAINT_ID);
    createEAttribute(safetyConstraintPreventsStatementEClass, SAFETY_CONSTRAINT_PREVENTS_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION);
    createEReference(safetyConstraintPreventsStatementEClass, SAFETY_CONSTRAINT_PREVENTS_STATEMENT__ERRORS);
    createEReference(safetyConstraintPreventsStatementEClass, SAFETY_CONSTRAINT_PREVENTS_STATEMENT__PORTS);
    createEReference(safetyConstraintPreventsStatementEClass, SAFETY_CONSTRAINT_PREVENTS_STATEMENT__MATCHING_XAGREE_STATEMENTS);

    errorPropagationRuleStatementEClass = createEClass(ERROR_PROPAGATION_RULE_STATEMENT);
    createEReference(errorPropagationRuleStatementEClass, ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST);
    createEReference(errorPropagationRuleStatementEClass, ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS);
    createEReference(errorPropagationRuleStatementEClass, ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST);
    createEReference(errorPropagationRuleStatementEClass, ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS);

    typeStatementEClass = createEClass(TYPE_STATEMENT);
    createEAttribute(typeStatementEClass, TYPE_STATEMENT__TYPE);

    errorsListEClass = createEClass(ERRORS_LIST);
    createEReference(errorsListEClass, ERRORS_LIST__FIRST_ERROR);
    createEReference(errorsListEClass, ERRORS_LIST__REST_ERRORS);

    errorsListNoProbabilityEClass = createEClass(ERRORS_LIST_NO_PROBABILITY);
    createEReference(errorsListNoProbabilityEClass, ERRORS_LIST_NO_PROBABILITY__FIRST_ERROR);
    createEReference(errorsListNoProbabilityEClass, ERRORS_LIST_NO_PROBABILITY__REST_ERRORS);

    portsListEClass = createEClass(PORTS_LIST);
    createEAttribute(portsListEClass, PORTS_LIST__FIRST_PORT);
    createEAttribute(portsListEClass, PORTS_LIST__REST_PORTS);

    errsStatementEClass = createEClass(ERRS_STATEMENT);
    createEReference(errsStatementEClass, ERRS_STATEMENT__FIRST_ERROR);
    createEReference(errsStatementEClass, ERRS_STATEMENT__REST_ERRORS);

    errorStatementEClass = createEClass(ERROR_STATEMENT);
    createEReference(errorStatementEClass, ERROR_STATEMENT__TYPE);
    createEReference(errorStatementEClass, ERROR_STATEMENT__UCA);
    createEReference(errorStatementEClass, ERROR_STATEMENT__CAUSE);
    createEReference(errorStatementEClass, ERROR_STATEMENT__SC);

    errorTypeStatementEClass = createEClass(ERROR_TYPE_STATEMENT);
    createEReference(errorTypeStatementEClass, ERROR_TYPE_STATEMENT__ERROR_TYPE);

    unsafeControlActionStatementEClass = createEClass(UNSAFE_CONTROL_ACTION_STATEMENT);
    createEAttribute(unsafeControlActionStatementEClass, UNSAFE_CONTROL_ACTION_STATEMENT__ID);
    createEAttribute(unsafeControlActionStatementEClass, UNSAFE_CONTROL_ACTION_STATEMENT__DESCRIPTION);

    causesStatementEClass = createEClass(CAUSES_STATEMENT);
    createEReference(causesStatementEClass, CAUSES_STATEMENT__GENERAL);
    createEReference(causesStatementEClass, CAUSES_STATEMENT__SPECIFIC);

    generalCauseStatementEClass = createEClass(GENERAL_CAUSE_STATEMENT);
    createEAttribute(generalCauseStatementEClass, GENERAL_CAUSE_STATEMENT__DESCRIPTION);

    specificCauseStatementEClass = createEClass(SPECIFIC_CAUSE_STATEMENT);
    createEAttribute(specificCauseStatementEClass, SPECIFIC_CAUSE_STATEMENT__DESCRIPTION);

    safetyConstraintStatementEClass = createEClass(SAFETY_CONSTRAINT_STATEMENT);
    createEAttribute(safetyConstraintStatementEClass, SAFETY_CONSTRAINT_STATEMENT__ID);
    createEAttribute(safetyConstraintStatementEClass, SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION);
    createEReference(safetyConstraintStatementEClass, SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS);

    guaranteeIdsEClass = createEClass(GUARANTEE_IDS);
    createEAttribute(guaranteeIdsEClass, GUARANTEE_IDS__FIRST);
    createEAttribute(guaranteeIdsEClass, GUARANTEE_IDS__REST);

    errorEClass = createEClass(ERROR);
    createEAttribute(errorEClass, ERROR__ERROR);
    createEAttribute(errorEClass, ERROR__SEVERITY_LEVEL);
    createEAttribute(errorEClass, ERROR__PROBABILITY);

    errorNoProbabilityEClass = createEClass(ERROR_NO_PROBABILITY);
    createEAttribute(errorNoProbabilityEClass, ERROR_NO_PROBABILITY__ERROR);

    asamContractLibraryEClass = createEClass(ASAM_CONTRACT_LIBRARY);
    createEReference(asamContractLibraryEClass, ASAM_CONTRACT_LIBRARY__CONTRACT);

    asamContractSubclauseEClass = createEClass(ASAM_CONTRACT_SUBCLAUSE);
    createEReference(asamContractSubclauseEClass, ASAM_CONTRACT_SUBCLAUSE__CONTRACT);

    asamContractEClass = createEClass(ASAM_CONTRACT);
    createEReference(asamContractEClass, ASAM_CONTRACT__STATEMENT);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    Aadl2Package theAadl2Package = (Aadl2Package)EPackage.Registry.INSTANCE.getEPackage(Aadl2Package.eNS_URI);
    EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    asamLibraryEClass.getESuperTypes().add(theAadl2Package.getAnnexLibrary());
    asamSubclauseEClass.getESuperTypes().add(theAadl2Package.getAnnexSubclause());
    internalFailureStatementEClass.getESuperTypes().add(this.getAsamStatement());
    safetyConstraintHandlesStatementEClass.getESuperTypes().add(this.getAsamStatement());
    safetyConstraintPreventsStatementEClass.getESuperTypes().add(this.getAsamStatement());
    errorPropagationRuleStatementEClass.getESuperTypes().add(this.getAsamStatement());
    typeStatementEClass.getESuperTypes().add(this.getAsamStatement());
    errsStatementEClass.getESuperTypes().add(this.getAsamStatement());
    asamContractLibraryEClass.getESuperTypes().add(this.getAsamLibrary());
    asamContractSubclauseEClass.getESuperTypes().add(this.getAsamSubclause());
    asamContractEClass.getESuperTypes().add(this.getContract());

    // Initialize classes and features; add operations and parameters
    initEClass(asamLibraryEClass, AsamLibrary.class, "AsamLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(asamSubclauseEClass, AsamSubclause.class, "AsamSubclause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(contractEClass, Contract.class, "Contract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(asamStatementEClass, AsamStatement.class, "AsamStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(internalFailureStatementEClass, InternalFailureStatement.class, "InternalFailureStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getInternalFailureStatement_InternalFailureId(), theEcorePackage.getEString(), "internalFailureId", null, 0, 1, InternalFailureStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getInternalFailureStatement_InternalFailureDescription(), theEcorePackage.getEString(), "internalFailureDescription", null, 0, 1, InternalFailureStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getInternalFailureStatement_Errors(), this.getErrorsList(), null, "errors", null, 0, 1, InternalFailureStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getInternalFailureStatement_Ports(), this.getPortsList(), null, "ports", null, 0, 1, InternalFailureStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(safetyConstraintHandlesStatementEClass, SafetyConstraintHandlesStatement.class, "SafetyConstraintHandlesStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSafetyConstraintHandlesStatement_SafetyConstraintId(), theEcorePackage.getEString(), "safetyConstraintId", null, 0, 1, SafetyConstraintHandlesStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSafetyConstraintHandlesStatement_SafetyConstraintDescription(), theEcorePackage.getEString(), "safetyConstraintDescription", null, 0, 1, SafetyConstraintHandlesStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSafetyConstraintHandlesStatement_Errors(), this.getErrorsListNoProbability(), null, "errors", null, 0, 1, SafetyConstraintHandlesStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSafetyConstraintHandlesStatement_Ports(), this.getPortsList(), null, "ports", null, 0, 1, SafetyConstraintHandlesStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSafetyConstraintHandlesStatement_MatchingXagreeStatements(), this.getGuaranteeIds(), null, "matchingXagreeStatements", null, 0, 1, SafetyConstraintHandlesStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(safetyConstraintPreventsStatementEClass, SafetyConstraintPreventsStatement.class, "SafetyConstraintPreventsStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSafetyConstraintPreventsStatement_SafetyConstraintId(), theEcorePackage.getEString(), "safetyConstraintId", null, 0, 1, SafetyConstraintPreventsStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSafetyConstraintPreventsStatement_SafetyConstraintDescription(), theEcorePackage.getEString(), "safetyConstraintDescription", null, 0, 1, SafetyConstraintPreventsStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSafetyConstraintPreventsStatement_Errors(), this.getErrorsListNoProbability(), null, "errors", null, 0, 1, SafetyConstraintPreventsStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSafetyConstraintPreventsStatement_Ports(), this.getPortsList(), null, "ports", null, 0, 1, SafetyConstraintPreventsStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSafetyConstraintPreventsStatement_MatchingXagreeStatements(), this.getGuaranteeIds(), null, "matchingXagreeStatements", null, 0, 1, SafetyConstraintPreventsStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errorPropagationRuleStatementEClass, ErrorPropagationRuleStatement.class, "ErrorPropagationRuleStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getErrorPropagationRuleStatement_InErrorsList(), this.getErrorsListNoProbability(), null, "inErrorsList", null, 0, 1, ErrorPropagationRuleStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorPropagationRuleStatement_InPortsLists(), this.getPortsList(), null, "inPortsLists", null, 0, 1, ErrorPropagationRuleStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorPropagationRuleStatement_OutErrorsList(), this.getErrorsList(), null, "outErrorsList", null, 0, 1, ErrorPropagationRuleStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorPropagationRuleStatement_OutPortsLists(), this.getPortsList(), null, "outPortsLists", null, 0, 1, ErrorPropagationRuleStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(typeStatementEClass, TypeStatement.class, "TypeStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTypeStatement_Type(), theEcorePackage.getEString(), "type", null, 0, 1, TypeStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errorsListEClass, ErrorsList.class, "ErrorsList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getErrorsList_FirstError(), this.getError(), null, "firstError", null, 0, 1, ErrorsList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorsList_RestErrors(), this.getError(), null, "restErrors", null, 0, -1, ErrorsList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errorsListNoProbabilityEClass, ErrorsListNoProbability.class, "ErrorsListNoProbability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getErrorsListNoProbability_FirstError(), this.getErrorNoProbability(), null, "firstError", null, 0, 1, ErrorsListNoProbability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorsListNoProbability_RestErrors(), this.getErrorNoProbability(), null, "restErrors", null, 0, -1, ErrorsListNoProbability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(portsListEClass, PortsList.class, "PortsList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getPortsList_FirstPort(), theEcorePackage.getEString(), "firstPort", null, 0, 1, PortsList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPortsList_RestPorts(), theEcorePackage.getEString(), "restPorts", null, 0, -1, PortsList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errsStatementEClass, ErrsStatement.class, "ErrsStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getErrsStatement_FirstError(), this.getErrorStatement(), null, "firstError", null, 0, 1, ErrsStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrsStatement_RestErrors(), this.getErrorStatement(), null, "restErrors", null, 0, -1, ErrsStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errorStatementEClass, ErrorStatement.class, "ErrorStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getErrorStatement_Type(), this.getErrorTypeStatement(), null, "type", null, 0, 1, ErrorStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorStatement_Uca(), this.getUnsafeControlActionStatement(), null, "uca", null, 0, 1, ErrorStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorStatement_Cause(), this.getCausesStatement(), null, "cause", null, 0, 1, ErrorStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getErrorStatement_Sc(), this.getSafetyConstraintStatement(), null, "sc", null, 0, 1, ErrorStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errorTypeStatementEClass, ErrorTypeStatement.class, "ErrorTypeStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getErrorTypeStatement_ErrorType(), this.getError(), null, "errorType", null, 0, 1, ErrorTypeStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unsafeControlActionStatementEClass, UnsafeControlActionStatement.class, "UnsafeControlActionStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getUnsafeControlActionStatement_Id(), theEcorePackage.getEString(), "id", null, 0, 1, UnsafeControlActionStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getUnsafeControlActionStatement_Description(), theEcorePackage.getEString(), "description", null, 0, 1, UnsafeControlActionStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(causesStatementEClass, CausesStatement.class, "CausesStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCausesStatement_General(), this.getGeneralCauseStatement(), null, "general", null, 0, 1, CausesStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCausesStatement_Specific(), this.getSpecificCauseStatement(), null, "specific", null, 0, 1, CausesStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(generalCauseStatementEClass, GeneralCauseStatement.class, "GeneralCauseStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGeneralCauseStatement_Description(), theEcorePackage.getEString(), "description", null, 0, 1, GeneralCauseStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(specificCauseStatementEClass, SpecificCauseStatement.class, "SpecificCauseStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSpecificCauseStatement_Description(), theEcorePackage.getEString(), "description", null, 0, 1, SpecificCauseStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(safetyConstraintStatementEClass, SafetyConstraintStatement.class, "SafetyConstraintStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSafetyConstraintStatement_Id(), theEcorePackage.getEString(), "id", null, 0, 1, SafetyConstraintStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSafetyConstraintStatement_Description(), theEcorePackage.getEString(), "description", null, 0, 1, SafetyConstraintStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSafetyConstraintStatement_MatchingXagreeStatements(), this.getGuaranteeIds(), null, "matchingXagreeStatements", null, 0, 1, SafetyConstraintStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(guaranteeIdsEClass, GuaranteeIds.class, "GuaranteeIds", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGuaranteeIds_First(), theEcorePackage.getEString(), "first", null, 0, 1, GuaranteeIds.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getGuaranteeIds_Rest(), theEcorePackage.getEString(), "rest", null, 0, -1, GuaranteeIds.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errorEClass, edu.clemson.asam.asam.Error.class, "Error", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getError_Error(), theEcorePackage.getEString(), "error", null, 0, 1, edu.clemson.asam.asam.Error.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getError_SeverityLevel(), theEcorePackage.getEString(), "severityLevel", null, 0, 1, edu.clemson.asam.asam.Error.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getError_Probability(), theEcorePackage.getEString(), "probability", null, 0, 1, edu.clemson.asam.asam.Error.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(errorNoProbabilityEClass, ErrorNoProbability.class, "ErrorNoProbability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getErrorNoProbability_Error(), theEcorePackage.getEString(), "error", null, 0, 1, ErrorNoProbability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(asamContractLibraryEClass, AsamContractLibrary.class, "AsamContractLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAsamContractLibrary_Contract(), this.getContract(), null, "contract", null, 0, 1, AsamContractLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(asamContractSubclauseEClass, AsamContractSubclause.class, "AsamContractSubclause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAsamContractSubclause_Contract(), this.getContract(), null, "contract", null, 0, 1, AsamContractSubclause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(asamContractEClass, AsamContract.class, "AsamContract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAsamContract_Statement(), this.getAsamStatement(), null, "statement", null, 0, -1, AsamContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //AsamPackageImpl
