/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.CausesStatement;
import edu.clemson.asam.asam.ErrorStatement;
import edu.clemson.asam.asam.ErrorTypeStatement;
import edu.clemson.asam.asam.SafetyConstraintStatement;
import edu.clemson.asam.asam.UnsafeControlActionStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorStatementImpl#getType <em>Type</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorStatementImpl#getUca <em>Uca</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorStatementImpl#getCause <em>Cause</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorStatementImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ErrorStatementImpl extends MinimalEObjectImpl.Container implements ErrorStatement
{
  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected ErrorTypeStatement type;

  /**
   * The cached value of the '{@link #getUca() <em>Uca</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUca()
   * @generated
   * @ordered
   */
  protected UnsafeControlActionStatement uca;

  /**
   * The cached value of the '{@link #getCause() <em>Cause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCause()
   * @generated
   * @ordered
   */
  protected CausesStatement cause;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected SafetyConstraintStatement sc;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ErrorStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.ERROR_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorTypeStatement getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(ErrorTypeStatement newType, NotificationChain msgs)
  {
    ErrorTypeStatement oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(ErrorTypeStatement newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnsafeControlActionStatement getUca()
  {
    return uca;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUca(UnsafeControlActionStatement newUca, NotificationChain msgs)
  {
    UnsafeControlActionStatement oldUca = uca;
    uca = newUca;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__UCA, oldUca, newUca);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUca(UnsafeControlActionStatement newUca)
  {
    if (newUca != uca)
    {
      NotificationChain msgs = null;
      if (uca != null)
        msgs = ((InternalEObject)uca).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__UCA, null, msgs);
      if (newUca != null)
        msgs = ((InternalEObject)newUca).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__UCA, null, msgs);
      msgs = basicSetUca(newUca, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__UCA, newUca, newUca));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CausesStatement getCause()
  {
    return cause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCause(CausesStatement newCause, NotificationChain msgs)
  {
    CausesStatement oldCause = cause;
    cause = newCause;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__CAUSE, oldCause, newCause);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCause(CausesStatement newCause)
  {
    if (newCause != cause)
    {
      NotificationChain msgs = null;
      if (cause != null)
        msgs = ((InternalEObject)cause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__CAUSE, null, msgs);
      if (newCause != null)
        msgs = ((InternalEObject)newCause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__CAUSE, null, msgs);
      msgs = basicSetCause(newCause, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__CAUSE, newCause, newCause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SafetyConstraintStatement getSc()
  {
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSc(SafetyConstraintStatement newSc, NotificationChain msgs)
  {
    SafetyConstraintStatement oldSc = sc;
    sc = newSc;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__SC, oldSc, newSc);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSc(SafetyConstraintStatement newSc)
  {
    if (newSc != sc)
    {
      NotificationChain msgs = null;
      if (sc != null)
        msgs = ((InternalEObject)sc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__SC, null, msgs);
      if (newSc != null)
        msgs = ((InternalEObject)newSc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.ERROR_STATEMENT__SC, null, msgs);
      msgs = basicSetSc(newSc, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR_STATEMENT__SC, newSc, newSc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_STATEMENT__TYPE:
        return basicSetType(null, msgs);
      case AsamPackage.ERROR_STATEMENT__UCA:
        return basicSetUca(null, msgs);
      case AsamPackage.ERROR_STATEMENT__CAUSE:
        return basicSetCause(null, msgs);
      case AsamPackage.ERROR_STATEMENT__SC:
        return basicSetSc(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_STATEMENT__TYPE:
        return getType();
      case AsamPackage.ERROR_STATEMENT__UCA:
        return getUca();
      case AsamPackage.ERROR_STATEMENT__CAUSE:
        return getCause();
      case AsamPackage.ERROR_STATEMENT__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_STATEMENT__TYPE:
        setType((ErrorTypeStatement)newValue);
        return;
      case AsamPackage.ERROR_STATEMENT__UCA:
        setUca((UnsafeControlActionStatement)newValue);
        return;
      case AsamPackage.ERROR_STATEMENT__CAUSE:
        setCause((CausesStatement)newValue);
        return;
      case AsamPackage.ERROR_STATEMENT__SC:
        setSc((SafetyConstraintStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_STATEMENT__TYPE:
        setType((ErrorTypeStatement)null);
        return;
      case AsamPackage.ERROR_STATEMENT__UCA:
        setUca((UnsafeControlActionStatement)null);
        return;
      case AsamPackage.ERROR_STATEMENT__CAUSE:
        setCause((CausesStatement)null);
        return;
      case AsamPackage.ERROR_STATEMENT__SC:
        setSc((SafetyConstraintStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR_STATEMENT__TYPE:
        return type != null;
      case AsamPackage.ERROR_STATEMENT__UCA:
        return uca != null;
      case AsamPackage.ERROR_STATEMENT__CAUSE:
        return cause != null;
      case AsamPackage.ERROR_STATEMENT__SC:
        return sc != null;
    }
    return super.eIsSet(featureID);
  }

} //ErrorStatementImpl
