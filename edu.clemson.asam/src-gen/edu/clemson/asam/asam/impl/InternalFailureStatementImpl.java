/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.ErrorsList;
import edu.clemson.asam.asam.InternalFailureStatement;
import edu.clemson.asam.asam.PortsList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Internal Failure Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.InternalFailureStatementImpl#getInternalFailureId <em>Internal Failure Id</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.InternalFailureStatementImpl#getInternalFailureDescription <em>Internal Failure Description</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.InternalFailureStatementImpl#getErrors <em>Errors</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.InternalFailureStatementImpl#getPorts <em>Ports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InternalFailureStatementImpl extends AsamStatementImpl implements InternalFailureStatement
{
  /**
   * The default value of the '{@link #getInternalFailureId() <em>Internal Failure Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInternalFailureId()
   * @generated
   * @ordered
   */
  protected static final String INTERNAL_FAILURE_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getInternalFailureId() <em>Internal Failure Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInternalFailureId()
   * @generated
   * @ordered
   */
  protected String internalFailureId = INTERNAL_FAILURE_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getInternalFailureDescription() <em>Internal Failure Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInternalFailureDescription()
   * @generated
   * @ordered
   */
  protected static final String INTERNAL_FAILURE_DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getInternalFailureDescription() <em>Internal Failure Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInternalFailureDescription()
   * @generated
   * @ordered
   */
  protected String internalFailureDescription = INTERNAL_FAILURE_DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getErrors() <em>Errors</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getErrors()
   * @generated
   * @ordered
   */
  protected ErrorsList errors;

  /**
   * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPorts()
   * @generated
   * @ordered
   */
  protected PortsList ports;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InternalFailureStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.INTERNAL_FAILURE_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getInternalFailureId()
  {
    return internalFailureId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInternalFailureId(String newInternalFailureId)
  {
    String oldInternalFailureId = internalFailureId;
    internalFailureId = newInternalFailureId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID, oldInternalFailureId, internalFailureId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getInternalFailureDescription()
  {
    return internalFailureDescription;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInternalFailureDescription(String newInternalFailureDescription)
  {
    String oldInternalFailureDescription = internalFailureDescription;
    internalFailureDescription = newInternalFailureDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION, oldInternalFailureDescription, internalFailureDescription));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorsList getErrors()
  {
    return errors;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetErrors(ErrorsList newErrors, NotificationChain msgs)
  {
    ErrorsList oldErrors = errors;
    errors = newErrors;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS, oldErrors, newErrors);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setErrors(ErrorsList newErrors)
  {
    if (newErrors != errors)
    {
      NotificationChain msgs = null;
      if (errors != null)
        msgs = ((InternalEObject)errors).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS, null, msgs);
      if (newErrors != null)
        msgs = ((InternalEObject)newErrors).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS, null, msgs);
      msgs = basicSetErrors(newErrors, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS, newErrors, newErrors));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortsList getPorts()
  {
    return ports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPorts(PortsList newPorts, NotificationChain msgs)
  {
    PortsList oldPorts = ports;
    ports = newPorts;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS, oldPorts, newPorts);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPorts(PortsList newPorts)
  {
    if (newPorts != ports)
    {
      NotificationChain msgs = null;
      if (ports != null)
        msgs = ((InternalEObject)ports).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS, null, msgs);
      if (newPorts != null)
        msgs = ((InternalEObject)newPorts).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS, null, msgs);
      msgs = basicSetPorts(newPorts, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS, newPorts, newPorts));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS:
        return basicSetErrors(null, msgs);
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS:
        return basicSetPorts(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID:
        return getInternalFailureId();
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION:
        return getInternalFailureDescription();
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS:
        return getErrors();
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS:
        return getPorts();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID:
        setInternalFailureId((String)newValue);
        return;
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION:
        setInternalFailureDescription((String)newValue);
        return;
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS:
        setErrors((ErrorsList)newValue);
        return;
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS:
        setPorts((PortsList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID:
        setInternalFailureId(INTERNAL_FAILURE_ID_EDEFAULT);
        return;
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION:
        setInternalFailureDescription(INTERNAL_FAILURE_DESCRIPTION_EDEFAULT);
        return;
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS:
        setErrors((ErrorsList)null);
        return;
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS:
        setPorts((PortsList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID:
        return INTERNAL_FAILURE_ID_EDEFAULT == null ? internalFailureId != null : !INTERNAL_FAILURE_ID_EDEFAULT.equals(internalFailureId);
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION:
        return INTERNAL_FAILURE_DESCRIPTION_EDEFAULT == null ? internalFailureDescription != null : !INTERNAL_FAILURE_DESCRIPTION_EDEFAULT.equals(internalFailureDescription);
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__ERRORS:
        return errors != null;
      case AsamPackage.INTERNAL_FAILURE_STATEMENT__PORTS:
        return ports != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (internalFailureId: ");
    result.append(internalFailureId);
    result.append(", internalFailureDescription: ");
    result.append(internalFailureDescription);
    result.append(')');
    return result.toString();
  }

} //InternalFailureStatementImpl
