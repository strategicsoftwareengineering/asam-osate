/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.GuaranteeIds;
import edu.clemson.asam.asam.SafetyConstraintStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Safety Constraint Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintStatementImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintStatementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.SafetyConstraintStatementImpl#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SafetyConstraintStatementImpl extends MinimalEObjectImpl.Container implements SafetyConstraintStatement
{
  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getMatchingXagreeStatements() <em>Matching Xagree Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMatchingXagreeStatements()
   * @generated
   * @ordered
   */
  protected GuaranteeIds matchingXagreeStatements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SafetyConstraintStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.SAFETY_CONSTRAINT_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId()
  {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId)
  {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_STATEMENT__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuaranteeIds getMatchingXagreeStatements()
  {
    return matchingXagreeStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMatchingXagreeStatements(GuaranteeIds newMatchingXagreeStatements, NotificationChain msgs)
  {
    GuaranteeIds oldMatchingXagreeStatements = matchingXagreeStatements;
    matchingXagreeStatements = newMatchingXagreeStatements;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS, oldMatchingXagreeStatements, newMatchingXagreeStatements);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMatchingXagreeStatements(GuaranteeIds newMatchingXagreeStatements)
  {
    if (newMatchingXagreeStatements != matchingXagreeStatements)
    {
      NotificationChain msgs = null;
      if (matchingXagreeStatements != null)
        msgs = ((InternalEObject)matchingXagreeStatements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS, null, msgs);
      if (newMatchingXagreeStatements != null)
        msgs = ((InternalEObject)newMatchingXagreeStatements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS, null, msgs);
      msgs = basicSetMatchingXagreeStatements(newMatchingXagreeStatements, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS, newMatchingXagreeStatements, newMatchingXagreeStatements));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        return basicSetMatchingXagreeStatements(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__ID:
        return getId();
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION:
        return getDescription();
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        return getMatchingXagreeStatements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__ID:
        setId((String)newValue);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        setMatchingXagreeStatements((GuaranteeIds)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__ID:
        setId(ID_EDEFAULT);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        setMatchingXagreeStatements((GuaranteeIds)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS:
        return matchingXagreeStatements != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", description: ");
    result.append(description);
    result.append(')');
    return result.toString();
  }

} //SafetyConstraintStatementImpl
