/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorImpl#getError <em>Error</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorImpl#getSeverityLevel <em>Severity Level</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.impl.ErrorImpl#getProbability <em>Probability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ErrorImpl extends MinimalEObjectImpl.Container implements edu.clemson.asam.asam.Error
{
  /**
   * The default value of the '{@link #getError() <em>Error</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getError()
   * @generated
   * @ordered
   */
  protected static final String ERROR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getError() <em>Error</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getError()
   * @generated
   * @ordered
   */
  protected String error = ERROR_EDEFAULT;

  /**
   * The default value of the '{@link #getSeverityLevel() <em>Severity Level</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSeverityLevel()
   * @generated
   * @ordered
   */
  protected static final String SEVERITY_LEVEL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSeverityLevel() <em>Severity Level</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSeverityLevel()
   * @generated
   * @ordered
   */
  protected String severityLevel = SEVERITY_LEVEL_EDEFAULT;

  /**
   * The default value of the '{@link #getProbability() <em>Probability</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProbability()
   * @generated
   * @ordered
   */
  protected static final String PROBABILITY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getProbability() <em>Probability</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProbability()
   * @generated
   * @ordered
   */
  protected String probability = PROBABILITY_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ErrorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.ERROR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getError()
  {
    return error;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setError(String newError)
  {
    String oldError = error;
    error = newError;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR__ERROR, oldError, error));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSeverityLevel()
  {
    return severityLevel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSeverityLevel(String newSeverityLevel)
  {
    String oldSeverityLevel = severityLevel;
    severityLevel = newSeverityLevel;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR__SEVERITY_LEVEL, oldSeverityLevel, severityLevel));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getProbability()
  {
    return probability;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProbability(String newProbability)
  {
    String oldProbability = probability;
    probability = newProbability;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AsamPackage.ERROR__PROBABILITY, oldProbability, probability));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR__ERROR:
        return getError();
      case AsamPackage.ERROR__SEVERITY_LEVEL:
        return getSeverityLevel();
      case AsamPackage.ERROR__PROBABILITY:
        return getProbability();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR__ERROR:
        setError((String)newValue);
        return;
      case AsamPackage.ERROR__SEVERITY_LEVEL:
        setSeverityLevel((String)newValue);
        return;
      case AsamPackage.ERROR__PROBABILITY:
        setProbability((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR__ERROR:
        setError(ERROR_EDEFAULT);
        return;
      case AsamPackage.ERROR__SEVERITY_LEVEL:
        setSeverityLevel(SEVERITY_LEVEL_EDEFAULT);
        return;
      case AsamPackage.ERROR__PROBABILITY:
        setProbability(PROBABILITY_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AsamPackage.ERROR__ERROR:
        return ERROR_EDEFAULT == null ? error != null : !ERROR_EDEFAULT.equals(error);
      case AsamPackage.ERROR__SEVERITY_LEVEL:
        return SEVERITY_LEVEL_EDEFAULT == null ? severityLevel != null : !SEVERITY_LEVEL_EDEFAULT.equals(severityLevel);
      case AsamPackage.ERROR__PROBABILITY:
        return PROBABILITY_EDEFAULT == null ? probability != null : !PROBABILITY_EDEFAULT.equals(probability);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (error: ");
    result.append(error);
    result.append(", severityLevel: ");
    result.append(severityLevel);
    result.append(", probability: ");
    result.append(probability);
    result.append(')');
    return result.toString();
  }

} //ErrorImpl
