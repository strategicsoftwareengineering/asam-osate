/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamLibrary;
import edu.clemson.asam.asam.AsamPackage;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.AnnexLibraryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AsamLibraryImpl extends AnnexLibraryImpl implements AsamLibrary
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AsamLibraryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AsamPackage.Literals.ASAM_LIBRARY;
  }

} //AsamLibraryImpl
