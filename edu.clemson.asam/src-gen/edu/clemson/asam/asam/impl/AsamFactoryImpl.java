/**
 */
package edu.clemson.asam.asam.impl;

import edu.clemson.asam.asam.AsamContract;
import edu.clemson.asam.asam.AsamContractLibrary;
import edu.clemson.asam.asam.AsamContractSubclause;
import edu.clemson.asam.asam.AsamFactory;
import edu.clemson.asam.asam.AsamLibrary;
import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.AsamStatement;
import edu.clemson.asam.asam.AsamSubclause;
import edu.clemson.asam.asam.CausesStatement;
import edu.clemson.asam.asam.Contract;
import edu.clemson.asam.asam.ErrorNoProbability;
import edu.clemson.asam.asam.ErrorPropagationRuleStatement;
import edu.clemson.asam.asam.ErrorStatement;
import edu.clemson.asam.asam.ErrorTypeStatement;
import edu.clemson.asam.asam.ErrorsList;
import edu.clemson.asam.asam.ErrorsListNoProbability;
import edu.clemson.asam.asam.ErrsStatement;
import edu.clemson.asam.asam.GeneralCauseStatement;
import edu.clemson.asam.asam.GuaranteeIds;
import edu.clemson.asam.asam.InternalFailureStatement;
import edu.clemson.asam.asam.PortsList;
import edu.clemson.asam.asam.SafetyConstraintHandlesStatement;
import edu.clemson.asam.asam.SafetyConstraintPreventsStatement;
import edu.clemson.asam.asam.SafetyConstraintStatement;
import edu.clemson.asam.asam.SpecificCauseStatement;
import edu.clemson.asam.asam.TypeStatement;
import edu.clemson.asam.asam.UnsafeControlActionStatement;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AsamFactoryImpl extends EFactoryImpl implements AsamFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static AsamFactory init()
  {
    try
    {
      AsamFactory theAsamFactory = (AsamFactory)EPackage.Registry.INSTANCE.getEFactory(AsamPackage.eNS_URI);
      if (theAsamFactory != null)
      {
        return theAsamFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new AsamFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case AsamPackage.ASAM_LIBRARY: return createAsamLibrary();
      case AsamPackage.ASAM_SUBCLAUSE: return createAsamSubclause();
      case AsamPackage.CONTRACT: return createContract();
      case AsamPackage.ASAM_STATEMENT: return createAsamStatement();
      case AsamPackage.INTERNAL_FAILURE_STATEMENT: return createInternalFailureStatement();
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT: return createSafetyConstraintHandlesStatement();
      case AsamPackage.SAFETY_CONSTRAINT_PREVENTS_STATEMENT: return createSafetyConstraintPreventsStatement();
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT: return createErrorPropagationRuleStatement();
      case AsamPackage.TYPE_STATEMENT: return createTypeStatement();
      case AsamPackage.ERRORS_LIST: return createErrorsList();
      case AsamPackage.ERRORS_LIST_NO_PROBABILITY: return createErrorsListNoProbability();
      case AsamPackage.PORTS_LIST: return createPortsList();
      case AsamPackage.ERRS_STATEMENT: return createErrsStatement();
      case AsamPackage.ERROR_STATEMENT: return createErrorStatement();
      case AsamPackage.ERROR_TYPE_STATEMENT: return createErrorTypeStatement();
      case AsamPackage.UNSAFE_CONTROL_ACTION_STATEMENT: return createUnsafeControlActionStatement();
      case AsamPackage.CAUSES_STATEMENT: return createCausesStatement();
      case AsamPackage.GENERAL_CAUSE_STATEMENT: return createGeneralCauseStatement();
      case AsamPackage.SPECIFIC_CAUSE_STATEMENT: return createSpecificCauseStatement();
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT: return createSafetyConstraintStatement();
      case AsamPackage.GUARANTEE_IDS: return createGuaranteeIds();
      case AsamPackage.ERROR: return createError();
      case AsamPackage.ERROR_NO_PROBABILITY: return createErrorNoProbability();
      case AsamPackage.ASAM_CONTRACT_LIBRARY: return createAsamContractLibrary();
      case AsamPackage.ASAM_CONTRACT_SUBCLAUSE: return createAsamContractSubclause();
      case AsamPackage.ASAM_CONTRACT: return createAsamContract();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamLibrary createAsamLibrary()
  {
    AsamLibraryImpl asamLibrary = new AsamLibraryImpl();
    return asamLibrary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamSubclause createAsamSubclause()
  {
    AsamSubclauseImpl asamSubclause = new AsamSubclauseImpl();
    return asamSubclause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Contract createContract()
  {
    ContractImpl contract = new ContractImpl();
    return contract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamStatement createAsamStatement()
  {
    AsamStatementImpl asamStatement = new AsamStatementImpl();
    return asamStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InternalFailureStatement createInternalFailureStatement()
  {
    InternalFailureStatementImpl internalFailureStatement = new InternalFailureStatementImpl();
    return internalFailureStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SafetyConstraintHandlesStatement createSafetyConstraintHandlesStatement()
  {
    SafetyConstraintHandlesStatementImpl safetyConstraintHandlesStatement = new SafetyConstraintHandlesStatementImpl();
    return safetyConstraintHandlesStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SafetyConstraintPreventsStatement createSafetyConstraintPreventsStatement()
  {
    SafetyConstraintPreventsStatementImpl safetyConstraintPreventsStatement = new SafetyConstraintPreventsStatementImpl();
    return safetyConstraintPreventsStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorPropagationRuleStatement createErrorPropagationRuleStatement()
  {
    ErrorPropagationRuleStatementImpl errorPropagationRuleStatement = new ErrorPropagationRuleStatementImpl();
    return errorPropagationRuleStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeStatement createTypeStatement()
  {
    TypeStatementImpl typeStatement = new TypeStatementImpl();
    return typeStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorsList createErrorsList()
  {
    ErrorsListImpl errorsList = new ErrorsListImpl();
    return errorsList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorsListNoProbability createErrorsListNoProbability()
  {
    ErrorsListNoProbabilityImpl errorsListNoProbability = new ErrorsListNoProbabilityImpl();
    return errorsListNoProbability;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortsList createPortsList()
  {
    PortsListImpl portsList = new PortsListImpl();
    return portsList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrsStatement createErrsStatement()
  {
    ErrsStatementImpl errsStatement = new ErrsStatementImpl();
    return errsStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorStatement createErrorStatement()
  {
    ErrorStatementImpl errorStatement = new ErrorStatementImpl();
    return errorStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorTypeStatement createErrorTypeStatement()
  {
    ErrorTypeStatementImpl errorTypeStatement = new ErrorTypeStatementImpl();
    return errorTypeStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnsafeControlActionStatement createUnsafeControlActionStatement()
  {
    UnsafeControlActionStatementImpl unsafeControlActionStatement = new UnsafeControlActionStatementImpl();
    return unsafeControlActionStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CausesStatement createCausesStatement()
  {
    CausesStatementImpl causesStatement = new CausesStatementImpl();
    return causesStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GeneralCauseStatement createGeneralCauseStatement()
  {
    GeneralCauseStatementImpl generalCauseStatement = new GeneralCauseStatementImpl();
    return generalCauseStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecificCauseStatement createSpecificCauseStatement()
  {
    SpecificCauseStatementImpl specificCauseStatement = new SpecificCauseStatementImpl();
    return specificCauseStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SafetyConstraintStatement createSafetyConstraintStatement()
  {
    SafetyConstraintStatementImpl safetyConstraintStatement = new SafetyConstraintStatementImpl();
    return safetyConstraintStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuaranteeIds createGuaranteeIds()
  {
    GuaranteeIdsImpl guaranteeIds = new GuaranteeIdsImpl();
    return guaranteeIds;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public edu.clemson.asam.asam.Error createError()
  {
    ErrorImpl error = new ErrorImpl();
    return error;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorNoProbability createErrorNoProbability()
  {
    ErrorNoProbabilityImpl errorNoProbability = new ErrorNoProbabilityImpl();
    return errorNoProbability;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamContractLibrary createAsamContractLibrary()
  {
    AsamContractLibraryImpl asamContractLibrary = new AsamContractLibraryImpl();
    return asamContractLibrary;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamContractSubclause createAsamContractSubclause()
  {
    AsamContractSubclauseImpl asamContractSubclause = new AsamContractSubclauseImpl();
    return asamContractSubclause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamContract createAsamContract()
  {
    AsamContractImpl asamContract = new AsamContractImpl();
    return asamContract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamPackage getAsamPackage()
  {
    return (AsamPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static AsamPackage getPackage()
  {
    return AsamPackage.eINSTANCE;
  }

} //AsamFactoryImpl
