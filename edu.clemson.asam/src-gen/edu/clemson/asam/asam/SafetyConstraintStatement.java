/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Safety Constraint Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintStatement#getId <em>Id</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintStatement#getDescription <em>Description</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintStatement#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintStatement()
 * @model
 * @generated
 */
public interface SafetyConstraintStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintStatement_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintStatement#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintStatement_Description()
   * @model
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintStatement#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

  /**
   * Returns the value of the '<em><b>Matching Xagree Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Matching Xagree Statements</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Matching Xagree Statements</em>' containment reference.
   * @see #setMatchingXagreeStatements(GuaranteeIds)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintStatement_MatchingXagreeStatements()
   * @model containment="true"
   * @generated
   */
  GuaranteeIds getMatchingXagreeStatements();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintStatement#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Matching Xagree Statements</em>' containment reference.
   * @see #getMatchingXagreeStatements()
   * @generated
   */
  void setMatchingXagreeStatements(GuaranteeIds value);

} // SafetyConstraintStatement
