/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Errs Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.ErrsStatement#getFirstError <em>First Error</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrsStatement#getRestErrors <em>Rest Errors</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getErrsStatement()
 * @model
 * @generated
 */
public interface ErrsStatement extends AsamStatement
{
  /**
   * Returns the value of the '<em><b>First Error</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First Error</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First Error</em>' containment reference.
   * @see #setFirstError(ErrorStatement)
   * @see edu.clemson.asam.asam.AsamPackage#getErrsStatement_FirstError()
   * @model containment="true"
   * @generated
   */
  ErrorStatement getFirstError();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrsStatement#getFirstError <em>First Error</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First Error</em>' containment reference.
   * @see #getFirstError()
   * @generated
   */
  void setFirstError(ErrorStatement value);

  /**
   * Returns the value of the '<em><b>Rest Errors</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.asam.asam.ErrorStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest Errors</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest Errors</em>' containment reference list.
   * @see edu.clemson.asam.asam.AsamPackage#getErrsStatement_RestErrors()
   * @model containment="true"
   * @generated
   */
  EList<ErrorStatement> getRestErrors();

} // ErrsStatement
