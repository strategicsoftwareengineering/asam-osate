/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error Type Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.ErrorTypeStatement#getErrorType <em>Error Type</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getErrorTypeStatement()
 * @model
 * @generated
 */
public interface ErrorTypeStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Error Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Error Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Error Type</em>' containment reference.
   * @see #setErrorType(edu.clemson.asam.asam.Error)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorTypeStatement_ErrorType()
   * @model containment="true"
   * @generated
   */
  edu.clemson.asam.asam.Error getErrorType();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorTypeStatement#getErrorType <em>Error Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Error Type</em>' containment reference.
   * @see #getErrorType()
   * @generated
   */
  void setErrorType(edu.clemson.asam.asam.Error value);

} // ErrorTypeStatement
