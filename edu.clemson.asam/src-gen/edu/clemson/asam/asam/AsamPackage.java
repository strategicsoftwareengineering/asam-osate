/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.osate.aadl2.Aadl2Package;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.clemson.asam.asam.AsamFactory
 * @model kind="package"
 * @generated
 */
public interface AsamPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "asam";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.clemson.edu/ASAM";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "asam";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  AsamPackage eINSTANCE = edu.clemson.asam.asam.impl.AsamPackageImpl.init();

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.AsamLibraryImpl <em>Library</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.AsamLibraryImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamLibrary()
   * @generated
   */
  int ASAM_LIBRARY = 0;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_LIBRARY__OWNED_ELEMENT = Aadl2Package.ANNEX_LIBRARY__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_LIBRARY__OWNED_COMMENT = Aadl2Package.ANNEX_LIBRARY__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_LIBRARY__NAME = Aadl2Package.ANNEX_LIBRARY__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_LIBRARY__QUALIFIED_NAME = Aadl2Package.ANNEX_LIBRARY__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_LIBRARY__OWNED_PROPERTY_ASSOCIATION = Aadl2Package.ANNEX_LIBRARY__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The number of structural features of the '<em>Library</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_LIBRARY_FEATURE_COUNT = Aadl2Package.ANNEX_LIBRARY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.AsamSubclauseImpl <em>Subclause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.AsamSubclauseImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamSubclause()
   * @generated
   */
  int ASAM_SUBCLAUSE = 1;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_SUBCLAUSE__OWNED_ELEMENT = Aadl2Package.ANNEX_SUBCLAUSE__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_SUBCLAUSE__OWNED_COMMENT = Aadl2Package.ANNEX_SUBCLAUSE__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_SUBCLAUSE__NAME = Aadl2Package.ANNEX_SUBCLAUSE__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_SUBCLAUSE__QUALIFIED_NAME = Aadl2Package.ANNEX_SUBCLAUSE__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION = Aadl2Package.ANNEX_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The feature id for the '<em><b>In Mode</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_SUBCLAUSE__IN_MODE = Aadl2Package.ANNEX_SUBCLAUSE__IN_MODE;

  /**
   * The number of structural features of the '<em>Subclause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_SUBCLAUSE_FEATURE_COUNT = Aadl2Package.ANNEX_SUBCLAUSE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ContractImpl <em>Contract</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ContractImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getContract()
   * @generated
   */
  int CONTRACT = 2;

  /**
   * The number of structural features of the '<em>Contract</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTRACT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.AsamStatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.AsamStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamStatement()
   * @generated
   */
  int ASAM_STATEMENT = 3;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.InternalFailureStatementImpl <em>Internal Failure Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.InternalFailureStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getInternalFailureStatement()
   * @generated
   */
  int INTERNAL_FAILURE_STATEMENT = 4;

  /**
   * The feature id for the '<em><b>Internal Failure Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID = ASAM_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Internal Failure Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION = ASAM_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Errors</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERNAL_FAILURE_STATEMENT__ERRORS = ASAM_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERNAL_FAILURE_STATEMENT__PORTS = ASAM_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Internal Failure Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERNAL_FAILURE_STATEMENT_FEATURE_COUNT = ASAM_STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl <em>Safety Constraint Handles Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSafetyConstraintHandlesStatement()
   * @generated
   */
  int SAFETY_CONSTRAINT_HANDLES_STATEMENT = 5;

  /**
   * The feature id for the '<em><b>Safety Constraint Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID = ASAM_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Safety Constraint Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION = ASAM_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Errors</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS = ASAM_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS = ASAM_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Matching Xagree Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS = ASAM_STATEMENT_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Safety Constraint Handles Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_HANDLES_STATEMENT_FEATURE_COUNT = ASAM_STATEMENT_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.SafetyConstraintPreventsStatementImpl <em>Safety Constraint Prevents Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.SafetyConstraintPreventsStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSafetyConstraintPreventsStatement()
   * @generated
   */
  int SAFETY_CONSTRAINT_PREVENTS_STATEMENT = 6;

  /**
   * The feature id for the '<em><b>Safety Constraint Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_PREVENTS_STATEMENT__SAFETY_CONSTRAINT_ID = ASAM_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Safety Constraint Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_PREVENTS_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION = ASAM_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Errors</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_PREVENTS_STATEMENT__ERRORS = ASAM_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_PREVENTS_STATEMENT__PORTS = ASAM_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Matching Xagree Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_PREVENTS_STATEMENT__MATCHING_XAGREE_STATEMENTS = ASAM_STATEMENT_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Safety Constraint Prevents Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_PREVENTS_STATEMENT_FEATURE_COUNT = ASAM_STATEMENT_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl <em>Error Propagation Rule Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorPropagationRuleStatement()
   * @generated
   */
  int ERROR_PROPAGATION_RULE_STATEMENT = 7;

  /**
   * The feature id for the '<em><b>In Errors List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST = ASAM_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>In Ports Lists</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS = ASAM_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Out Errors List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST = ASAM_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Out Ports Lists</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS = ASAM_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Error Propagation Rule Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_PROPAGATION_RULE_STATEMENT_FEATURE_COUNT = ASAM_STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.TypeStatementImpl <em>Type Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.TypeStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getTypeStatement()
   * @generated
   */
  int TYPE_STATEMENT = 8;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_STATEMENT__TYPE = ASAM_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_STATEMENT_FEATURE_COUNT = ASAM_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrorsListImpl <em>Errors List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrorsListImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorsList()
   * @generated
   */
  int ERRORS_LIST = 9;

  /**
   * The feature id for the '<em><b>First Error</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRORS_LIST__FIRST_ERROR = 0;

  /**
   * The feature id for the '<em><b>Rest Errors</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRORS_LIST__REST_ERRORS = 1;

  /**
   * The number of structural features of the '<em>Errors List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRORS_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrorsListNoProbabilityImpl <em>Errors List No Probability</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrorsListNoProbabilityImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorsListNoProbability()
   * @generated
   */
  int ERRORS_LIST_NO_PROBABILITY = 10;

  /**
   * The feature id for the '<em><b>First Error</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRORS_LIST_NO_PROBABILITY__FIRST_ERROR = 0;

  /**
   * The feature id for the '<em><b>Rest Errors</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRORS_LIST_NO_PROBABILITY__REST_ERRORS = 1;

  /**
   * The number of structural features of the '<em>Errors List No Probability</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRORS_LIST_NO_PROBABILITY_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.PortsListImpl <em>Ports List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.PortsListImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getPortsList()
   * @generated
   */
  int PORTS_LIST = 11;

  /**
   * The feature id for the '<em><b>First Port</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORTS_LIST__FIRST_PORT = 0;

  /**
   * The feature id for the '<em><b>Rest Ports</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORTS_LIST__REST_PORTS = 1;

  /**
   * The number of structural features of the '<em>Ports List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORTS_LIST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrsStatementImpl <em>Errs Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrsStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrsStatement()
   * @generated
   */
  int ERRS_STATEMENT = 12;

  /**
   * The feature id for the '<em><b>First Error</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRS_STATEMENT__FIRST_ERROR = ASAM_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Rest Errors</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRS_STATEMENT__REST_ERRORS = ASAM_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Errs Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERRS_STATEMENT_FEATURE_COUNT = ASAM_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrorStatementImpl <em>Error Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrorStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorStatement()
   * @generated
   */
  int ERROR_STATEMENT = 13;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_STATEMENT__TYPE = 0;

  /**
   * The feature id for the '<em><b>Uca</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_STATEMENT__UCA = 1;

  /**
   * The feature id for the '<em><b>Cause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_STATEMENT__CAUSE = 2;

  /**
   * The feature id for the '<em><b>Sc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_STATEMENT__SC = 3;

  /**
   * The number of structural features of the '<em>Error Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_STATEMENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrorTypeStatementImpl <em>Error Type Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrorTypeStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorTypeStatement()
   * @generated
   */
  int ERROR_TYPE_STATEMENT = 14;

  /**
   * The feature id for the '<em><b>Error Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_TYPE_STATEMENT__ERROR_TYPE = 0;

  /**
   * The number of structural features of the '<em>Error Type Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_TYPE_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.UnsafeControlActionStatementImpl <em>Unsafe Control Action Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.UnsafeControlActionStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getUnsafeControlActionStatement()
   * @generated
   */
  int UNSAFE_CONTROL_ACTION_STATEMENT = 15;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNSAFE_CONTROL_ACTION_STATEMENT__ID = 0;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNSAFE_CONTROL_ACTION_STATEMENT__DESCRIPTION = 1;

  /**
   * The number of structural features of the '<em>Unsafe Control Action Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNSAFE_CONTROL_ACTION_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.CausesStatementImpl <em>Causes Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.CausesStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getCausesStatement()
   * @generated
   */
  int CAUSES_STATEMENT = 16;

  /**
   * The feature id for the '<em><b>General</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAUSES_STATEMENT__GENERAL = 0;

  /**
   * The feature id for the '<em><b>Specific</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAUSES_STATEMENT__SPECIFIC = 1;

  /**
   * The number of structural features of the '<em>Causes Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAUSES_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.GeneralCauseStatementImpl <em>General Cause Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.GeneralCauseStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getGeneralCauseStatement()
   * @generated
   */
  int GENERAL_CAUSE_STATEMENT = 17;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERAL_CAUSE_STATEMENT__DESCRIPTION = 0;

  /**
   * The number of structural features of the '<em>General Cause Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERAL_CAUSE_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.SpecificCauseStatementImpl <em>Specific Cause Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.SpecificCauseStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSpecificCauseStatement()
   * @generated
   */
  int SPECIFIC_CAUSE_STATEMENT = 18;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECIFIC_CAUSE_STATEMENT__DESCRIPTION = 0;

  /**
   * The number of structural features of the '<em>Specific Cause Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SPECIFIC_CAUSE_STATEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.SafetyConstraintStatementImpl <em>Safety Constraint Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.SafetyConstraintStatementImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSafetyConstraintStatement()
   * @generated
   */
  int SAFETY_CONSTRAINT_STATEMENT = 19;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_STATEMENT__ID = 0;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION = 1;

  /**
   * The feature id for the '<em><b>Matching Xagree Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS = 2;

  /**
   * The number of structural features of the '<em>Safety Constraint Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SAFETY_CONSTRAINT_STATEMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.GuaranteeIdsImpl <em>Guarantee Ids</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.GuaranteeIdsImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getGuaranteeIds()
   * @generated
   */
  int GUARANTEE_IDS = 20;

  /**
   * The feature id for the '<em><b>First</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARANTEE_IDS__FIRST = 0;

  /**
   * The feature id for the '<em><b>Rest</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARANTEE_IDS__REST = 1;

  /**
   * The number of structural features of the '<em>Guarantee Ids</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARANTEE_IDS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrorImpl <em>Error</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrorImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getError()
   * @generated
   */
  int ERROR = 21;

  /**
   * The feature id for the '<em><b>Error</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR__ERROR = 0;

  /**
   * The feature id for the '<em><b>Severity Level</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR__SEVERITY_LEVEL = 1;

  /**
   * The feature id for the '<em><b>Probability</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR__PROBABILITY = 2;

  /**
   * The number of structural features of the '<em>Error</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.ErrorNoProbabilityImpl <em>Error No Probability</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.ErrorNoProbabilityImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorNoProbability()
   * @generated
   */
  int ERROR_NO_PROBABILITY = 22;

  /**
   * The feature id for the '<em><b>Error</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_NO_PROBABILITY__ERROR = 0;

  /**
   * The number of structural features of the '<em>Error No Probability</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_NO_PROBABILITY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.AsamContractLibraryImpl <em>Contract Library</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.AsamContractLibraryImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamContractLibrary()
   * @generated
   */
  int ASAM_CONTRACT_LIBRARY = 23;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_LIBRARY__OWNED_ELEMENT = ASAM_LIBRARY__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_LIBRARY__OWNED_COMMENT = ASAM_LIBRARY__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_LIBRARY__NAME = ASAM_LIBRARY__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_LIBRARY__QUALIFIED_NAME = ASAM_LIBRARY__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_LIBRARY__OWNED_PROPERTY_ASSOCIATION = ASAM_LIBRARY__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The feature id for the '<em><b>Contract</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_LIBRARY__CONTRACT = ASAM_LIBRARY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Contract Library</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_LIBRARY_FEATURE_COUNT = ASAM_LIBRARY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.AsamContractSubclauseImpl <em>Contract Subclause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.AsamContractSubclauseImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamContractSubclause()
   * @generated
   */
  int ASAM_CONTRACT_SUBCLAUSE = 24;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE__OWNED_ELEMENT = ASAM_SUBCLAUSE__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE__OWNED_COMMENT = ASAM_SUBCLAUSE__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE__NAME = ASAM_SUBCLAUSE__NAME;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE__QUALIFIED_NAME = ASAM_SUBCLAUSE__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Owned Property Association</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION = ASAM_SUBCLAUSE__OWNED_PROPERTY_ASSOCIATION;

  /**
   * The feature id for the '<em><b>In Mode</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE__IN_MODE = ASAM_SUBCLAUSE__IN_MODE;

  /**
   * The feature id for the '<em><b>Contract</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE__CONTRACT = ASAM_SUBCLAUSE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Contract Subclause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_SUBCLAUSE_FEATURE_COUNT = ASAM_SUBCLAUSE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link edu.clemson.asam.asam.impl.AsamContractImpl <em>Contract</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see edu.clemson.asam.asam.impl.AsamContractImpl
   * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamContract()
   * @generated
   */
  int ASAM_CONTRACT = 25;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT__STATEMENT = CONTRACT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Contract</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASAM_CONTRACT_FEATURE_COUNT = CONTRACT_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.AsamLibrary <em>Library</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Library</em>'.
   * @see edu.clemson.asam.asam.AsamLibrary
   * @generated
   */
  EClass getAsamLibrary();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.AsamSubclause <em>Subclause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subclause</em>'.
   * @see edu.clemson.asam.asam.AsamSubclause
   * @generated
   */
  EClass getAsamSubclause();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.Contract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract</em>'.
   * @see edu.clemson.asam.asam.Contract
   * @generated
   */
  EClass getContract();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.AsamStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see edu.clemson.asam.asam.AsamStatement
   * @generated
   */
  EClass getAsamStatement();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.InternalFailureStatement <em>Internal Failure Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Internal Failure Statement</em>'.
   * @see edu.clemson.asam.asam.InternalFailureStatement
   * @generated
   */
  EClass getInternalFailureStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureId <em>Internal Failure Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Internal Failure Id</em>'.
   * @see edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureId()
   * @see #getInternalFailureStatement()
   * @generated
   */
  EAttribute getInternalFailureStatement_InternalFailureId();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureDescription <em>Internal Failure Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Internal Failure Description</em>'.
   * @see edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureDescription()
   * @see #getInternalFailureStatement()
   * @generated
   */
  EAttribute getInternalFailureStatement_InternalFailureDescription();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.InternalFailureStatement#getErrors <em>Errors</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Errors</em>'.
   * @see edu.clemson.asam.asam.InternalFailureStatement#getErrors()
   * @see #getInternalFailureStatement()
   * @generated
   */
  EReference getInternalFailureStatement_Errors();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.InternalFailureStatement#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ports</em>'.
   * @see edu.clemson.asam.asam.InternalFailureStatement#getPorts()
   * @see #getInternalFailureStatement()
   * @generated
   */
  EReference getInternalFailureStatement_Ports();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.SafetyConstraintHandlesStatement <em>Safety Constraint Handles Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Safety Constraint Handles Statement</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintHandlesStatement
   * @generated
   */
  EClass getSafetyConstraintHandlesStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getSafetyConstraintId <em>Safety Constraint Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Safety Constraint Id</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getSafetyConstraintId()
   * @see #getSafetyConstraintHandlesStatement()
   * @generated
   */
  EAttribute getSafetyConstraintHandlesStatement_SafetyConstraintId();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getSafetyConstraintDescription <em>Safety Constraint Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Safety Constraint Description</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getSafetyConstraintDescription()
   * @see #getSafetyConstraintHandlesStatement()
   * @generated
   */
  EAttribute getSafetyConstraintHandlesStatement_SafetyConstraintDescription();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getErrors <em>Errors</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Errors</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getErrors()
   * @see #getSafetyConstraintHandlesStatement()
   * @generated
   */
  EReference getSafetyConstraintHandlesStatement_Errors();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ports</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getPorts()
   * @see #getSafetyConstraintHandlesStatement()
   * @generated
   */
  EReference getSafetyConstraintHandlesStatement_Ports();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Matching Xagree Statements</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintHandlesStatement#getMatchingXagreeStatements()
   * @see #getSafetyConstraintHandlesStatement()
   * @generated
   */
  EReference getSafetyConstraintHandlesStatement_MatchingXagreeStatements();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement <em>Safety Constraint Prevents Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Safety Constraint Prevents Statement</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintPreventsStatement
   * @generated
   */
  EClass getSafetyConstraintPreventsStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintId <em>Safety Constraint Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Safety Constraint Id</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintId()
   * @see #getSafetyConstraintPreventsStatement()
   * @generated
   */
  EAttribute getSafetyConstraintPreventsStatement_SafetyConstraintId();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintDescription <em>Safety Constraint Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Safety Constraint Description</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintDescription()
   * @see #getSafetyConstraintPreventsStatement()
   * @generated
   */
  EAttribute getSafetyConstraintPreventsStatement_SafetyConstraintDescription();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getErrors <em>Errors</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Errors</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getErrors()
   * @see #getSafetyConstraintPreventsStatement()
   * @generated
   */
  EReference getSafetyConstraintPreventsStatement_Errors();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ports</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getPorts()
   * @see #getSafetyConstraintPreventsStatement()
   * @generated
   */
  EReference getSafetyConstraintPreventsStatement_Ports();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Matching Xagree Statements</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getMatchingXagreeStatements()
   * @see #getSafetyConstraintPreventsStatement()
   * @generated
   */
  EReference getSafetyConstraintPreventsStatement_MatchingXagreeStatements();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement <em>Error Propagation Rule Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Error Propagation Rule Statement</em>'.
   * @see edu.clemson.asam.asam.ErrorPropagationRuleStatement
   * @generated
   */
  EClass getErrorPropagationRuleStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInErrorsList <em>In Errors List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>In Errors List</em>'.
   * @see edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInErrorsList()
   * @see #getErrorPropagationRuleStatement()
   * @generated
   */
  EReference getErrorPropagationRuleStatement_InErrorsList();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInPortsLists <em>In Ports Lists</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>In Ports Lists</em>'.
   * @see edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInPortsLists()
   * @see #getErrorPropagationRuleStatement()
   * @generated
   */
  EReference getErrorPropagationRuleStatement_InPortsLists();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutErrorsList <em>Out Errors List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Out Errors List</em>'.
   * @see edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutErrorsList()
   * @see #getErrorPropagationRuleStatement()
   * @generated
   */
  EReference getErrorPropagationRuleStatement_OutErrorsList();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutPortsLists <em>Out Ports Lists</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Out Ports Lists</em>'.
   * @see edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutPortsLists()
   * @see #getErrorPropagationRuleStatement()
   * @generated
   */
  EReference getErrorPropagationRuleStatement_OutPortsLists();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.TypeStatement <em>Type Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Statement</em>'.
   * @see edu.clemson.asam.asam.TypeStatement
   * @generated
   */
  EClass getTypeStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.TypeStatement#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see edu.clemson.asam.asam.TypeStatement#getType()
   * @see #getTypeStatement()
   * @generated
   */
  EAttribute getTypeStatement_Type();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.ErrorsList <em>Errors List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Errors List</em>'.
   * @see edu.clemson.asam.asam.ErrorsList
   * @generated
   */
  EClass getErrorsList();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorsList#getFirstError <em>First Error</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>First Error</em>'.
   * @see edu.clemson.asam.asam.ErrorsList#getFirstError()
   * @see #getErrorsList()
   * @generated
   */
  EReference getErrorsList_FirstError();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.asam.asam.ErrorsList#getRestErrors <em>Rest Errors</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest Errors</em>'.
   * @see edu.clemson.asam.asam.ErrorsList#getRestErrors()
   * @see #getErrorsList()
   * @generated
   */
  EReference getErrorsList_RestErrors();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.ErrorsListNoProbability <em>Errors List No Probability</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Errors List No Probability</em>'.
   * @see edu.clemson.asam.asam.ErrorsListNoProbability
   * @generated
   */
  EClass getErrorsListNoProbability();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorsListNoProbability#getFirstError <em>First Error</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>First Error</em>'.
   * @see edu.clemson.asam.asam.ErrorsListNoProbability#getFirstError()
   * @see #getErrorsListNoProbability()
   * @generated
   */
  EReference getErrorsListNoProbability_FirstError();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.asam.asam.ErrorsListNoProbability#getRestErrors <em>Rest Errors</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest Errors</em>'.
   * @see edu.clemson.asam.asam.ErrorsListNoProbability#getRestErrors()
   * @see #getErrorsListNoProbability()
   * @generated
   */
  EReference getErrorsListNoProbability_RestErrors();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.PortsList <em>Ports List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ports List</em>'.
   * @see edu.clemson.asam.asam.PortsList
   * @generated
   */
  EClass getPortsList();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.PortsList#getFirstPort <em>First Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>First Port</em>'.
   * @see edu.clemson.asam.asam.PortsList#getFirstPort()
   * @see #getPortsList()
   * @generated
   */
  EAttribute getPortsList_FirstPort();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.asam.asam.PortsList#getRestPorts <em>Rest Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Rest Ports</em>'.
   * @see edu.clemson.asam.asam.PortsList#getRestPorts()
   * @see #getPortsList()
   * @generated
   */
  EAttribute getPortsList_RestPorts();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.ErrsStatement <em>Errs Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Errs Statement</em>'.
   * @see edu.clemson.asam.asam.ErrsStatement
   * @generated
   */
  EClass getErrsStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrsStatement#getFirstError <em>First Error</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>First Error</em>'.
   * @see edu.clemson.asam.asam.ErrsStatement#getFirstError()
   * @see #getErrsStatement()
   * @generated
   */
  EReference getErrsStatement_FirstError();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.asam.asam.ErrsStatement#getRestErrors <em>Rest Errors</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rest Errors</em>'.
   * @see edu.clemson.asam.asam.ErrsStatement#getRestErrors()
   * @see #getErrsStatement()
   * @generated
   */
  EReference getErrsStatement_RestErrors();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.ErrorStatement <em>Error Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Error Statement</em>'.
   * @see edu.clemson.asam.asam.ErrorStatement
   * @generated
   */
  EClass getErrorStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorStatement#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see edu.clemson.asam.asam.ErrorStatement#getType()
   * @see #getErrorStatement()
   * @generated
   */
  EReference getErrorStatement_Type();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorStatement#getUca <em>Uca</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Uca</em>'.
   * @see edu.clemson.asam.asam.ErrorStatement#getUca()
   * @see #getErrorStatement()
   * @generated
   */
  EReference getErrorStatement_Uca();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorStatement#getCause <em>Cause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cause</em>'.
   * @see edu.clemson.asam.asam.ErrorStatement#getCause()
   * @see #getErrorStatement()
   * @generated
   */
  EReference getErrorStatement_Cause();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorStatement#getSc <em>Sc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sc</em>'.
   * @see edu.clemson.asam.asam.ErrorStatement#getSc()
   * @see #getErrorStatement()
   * @generated
   */
  EReference getErrorStatement_Sc();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.ErrorTypeStatement <em>Error Type Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Error Type Statement</em>'.
   * @see edu.clemson.asam.asam.ErrorTypeStatement
   * @generated
   */
  EClass getErrorTypeStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.ErrorTypeStatement#getErrorType <em>Error Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Error Type</em>'.
   * @see edu.clemson.asam.asam.ErrorTypeStatement#getErrorType()
   * @see #getErrorTypeStatement()
   * @generated
   */
  EReference getErrorTypeStatement_ErrorType();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.UnsafeControlActionStatement <em>Unsafe Control Action Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unsafe Control Action Statement</em>'.
   * @see edu.clemson.asam.asam.UnsafeControlActionStatement
   * @generated
   */
  EClass getUnsafeControlActionStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.UnsafeControlActionStatement#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see edu.clemson.asam.asam.UnsafeControlActionStatement#getId()
   * @see #getUnsafeControlActionStatement()
   * @generated
   */
  EAttribute getUnsafeControlActionStatement_Id();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.UnsafeControlActionStatement#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see edu.clemson.asam.asam.UnsafeControlActionStatement#getDescription()
   * @see #getUnsafeControlActionStatement()
   * @generated
   */
  EAttribute getUnsafeControlActionStatement_Description();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.CausesStatement <em>Causes Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Causes Statement</em>'.
   * @see edu.clemson.asam.asam.CausesStatement
   * @generated
   */
  EClass getCausesStatement();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.CausesStatement#getGeneral <em>General</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>General</em>'.
   * @see edu.clemson.asam.asam.CausesStatement#getGeneral()
   * @see #getCausesStatement()
   * @generated
   */
  EReference getCausesStatement_General();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.CausesStatement#getSpecific <em>Specific</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Specific</em>'.
   * @see edu.clemson.asam.asam.CausesStatement#getSpecific()
   * @see #getCausesStatement()
   * @generated
   */
  EReference getCausesStatement_Specific();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.GeneralCauseStatement <em>General Cause Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>General Cause Statement</em>'.
   * @see edu.clemson.asam.asam.GeneralCauseStatement
   * @generated
   */
  EClass getGeneralCauseStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.GeneralCauseStatement#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see edu.clemson.asam.asam.GeneralCauseStatement#getDescription()
   * @see #getGeneralCauseStatement()
   * @generated
   */
  EAttribute getGeneralCauseStatement_Description();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.SpecificCauseStatement <em>Specific Cause Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Specific Cause Statement</em>'.
   * @see edu.clemson.asam.asam.SpecificCauseStatement
   * @generated
   */
  EClass getSpecificCauseStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.SpecificCauseStatement#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see edu.clemson.asam.asam.SpecificCauseStatement#getDescription()
   * @see #getSpecificCauseStatement()
   * @generated
   */
  EAttribute getSpecificCauseStatement_Description();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.SafetyConstraintStatement <em>Safety Constraint Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Safety Constraint Statement</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintStatement
   * @generated
   */
  EClass getSafetyConstraintStatement();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.SafetyConstraintStatement#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintStatement#getId()
   * @see #getSafetyConstraintStatement()
   * @generated
   */
  EAttribute getSafetyConstraintStatement_Id();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.SafetyConstraintStatement#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintStatement#getDescription()
   * @see #getSafetyConstraintStatement()
   * @generated
   */
  EAttribute getSafetyConstraintStatement_Description();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.SafetyConstraintStatement#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Matching Xagree Statements</em>'.
   * @see edu.clemson.asam.asam.SafetyConstraintStatement#getMatchingXagreeStatements()
   * @see #getSafetyConstraintStatement()
   * @generated
   */
  EReference getSafetyConstraintStatement_MatchingXagreeStatements();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.GuaranteeIds <em>Guarantee Ids</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guarantee Ids</em>'.
   * @see edu.clemson.asam.asam.GuaranteeIds
   * @generated
   */
  EClass getGuaranteeIds();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.GuaranteeIds#getFirst <em>First</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>First</em>'.
   * @see edu.clemson.asam.asam.GuaranteeIds#getFirst()
   * @see #getGuaranteeIds()
   * @generated
   */
  EAttribute getGuaranteeIds_First();

  /**
   * Returns the meta object for the attribute list '{@link edu.clemson.asam.asam.GuaranteeIds#getRest <em>Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Rest</em>'.
   * @see edu.clemson.asam.asam.GuaranteeIds#getRest()
   * @see #getGuaranteeIds()
   * @generated
   */
  EAttribute getGuaranteeIds_Rest();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.Error <em>Error</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Error</em>'.
   * @see edu.clemson.asam.asam.Error
   * @generated
   */
  EClass getError();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.Error#getError <em>Error</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Error</em>'.
   * @see edu.clemson.asam.asam.Error#getError()
   * @see #getError()
   * @generated
   */
  EAttribute getError_Error();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.Error#getSeverityLevel <em>Severity Level</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Severity Level</em>'.
   * @see edu.clemson.asam.asam.Error#getSeverityLevel()
   * @see #getError()
   * @generated
   */
  EAttribute getError_SeverityLevel();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.Error#getProbability <em>Probability</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Probability</em>'.
   * @see edu.clemson.asam.asam.Error#getProbability()
   * @see #getError()
   * @generated
   */
  EAttribute getError_Probability();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.ErrorNoProbability <em>Error No Probability</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Error No Probability</em>'.
   * @see edu.clemson.asam.asam.ErrorNoProbability
   * @generated
   */
  EClass getErrorNoProbability();

  /**
   * Returns the meta object for the attribute '{@link edu.clemson.asam.asam.ErrorNoProbability#getError <em>Error</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Error</em>'.
   * @see edu.clemson.asam.asam.ErrorNoProbability#getError()
   * @see #getErrorNoProbability()
   * @generated
   */
  EAttribute getErrorNoProbability_Error();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.AsamContractLibrary <em>Contract Library</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract Library</em>'.
   * @see edu.clemson.asam.asam.AsamContractLibrary
   * @generated
   */
  EClass getAsamContractLibrary();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.AsamContractLibrary#getContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Contract</em>'.
   * @see edu.clemson.asam.asam.AsamContractLibrary#getContract()
   * @see #getAsamContractLibrary()
   * @generated
   */
  EReference getAsamContractLibrary_Contract();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.AsamContractSubclause <em>Contract Subclause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract Subclause</em>'.
   * @see edu.clemson.asam.asam.AsamContractSubclause
   * @generated
   */
  EClass getAsamContractSubclause();

  /**
   * Returns the meta object for the containment reference '{@link edu.clemson.asam.asam.AsamContractSubclause#getContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Contract</em>'.
   * @see edu.clemson.asam.asam.AsamContractSubclause#getContract()
   * @see #getAsamContractSubclause()
   * @generated
   */
  EReference getAsamContractSubclause_Contract();

  /**
   * Returns the meta object for class '{@link edu.clemson.asam.asam.AsamContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contract</em>'.
   * @see edu.clemson.asam.asam.AsamContract
   * @generated
   */
  EClass getAsamContract();

  /**
   * Returns the meta object for the containment reference list '{@link edu.clemson.asam.asam.AsamContract#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statement</em>'.
   * @see edu.clemson.asam.asam.AsamContract#getStatement()
   * @see #getAsamContract()
   * @generated
   */
  EReference getAsamContract_Statement();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  AsamFactory getAsamFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.AsamLibraryImpl <em>Library</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.AsamLibraryImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamLibrary()
     * @generated
     */
    EClass ASAM_LIBRARY = eINSTANCE.getAsamLibrary();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.AsamSubclauseImpl <em>Subclause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.AsamSubclauseImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamSubclause()
     * @generated
     */
    EClass ASAM_SUBCLAUSE = eINSTANCE.getAsamSubclause();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ContractImpl <em>Contract</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ContractImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getContract()
     * @generated
     */
    EClass CONTRACT = eINSTANCE.getContract();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.AsamStatementImpl <em>Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.AsamStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamStatement()
     * @generated
     */
    EClass ASAM_STATEMENT = eINSTANCE.getAsamStatement();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.InternalFailureStatementImpl <em>Internal Failure Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.InternalFailureStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getInternalFailureStatement()
     * @generated
     */
    EClass INTERNAL_FAILURE_STATEMENT = eINSTANCE.getInternalFailureStatement();

    /**
     * The meta object literal for the '<em><b>Internal Failure Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_ID = eINSTANCE.getInternalFailureStatement_InternalFailureId();

    /**
     * The meta object literal for the '<em><b>Internal Failure Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTERNAL_FAILURE_STATEMENT__INTERNAL_FAILURE_DESCRIPTION = eINSTANCE.getInternalFailureStatement_InternalFailureDescription();

    /**
     * The meta object literal for the '<em><b>Errors</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERNAL_FAILURE_STATEMENT__ERRORS = eINSTANCE.getInternalFailureStatement_Errors();

    /**
     * The meta object literal for the '<em><b>Ports</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERNAL_FAILURE_STATEMENT__PORTS = eINSTANCE.getInternalFailureStatement_Ports();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl <em>Safety Constraint Handles Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.SafetyConstraintHandlesStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSafetyConstraintHandlesStatement()
     * @generated
     */
    EClass SAFETY_CONSTRAINT_HANDLES_STATEMENT = eINSTANCE.getSafetyConstraintHandlesStatement();

    /**
     * The meta object literal for the '<em><b>Safety Constraint Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_ID = eINSTANCE.getSafetyConstraintHandlesStatement_SafetyConstraintId();

    /**
     * The meta object literal for the '<em><b>Safety Constraint Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SAFETY_CONSTRAINT_HANDLES_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION = eINSTANCE.getSafetyConstraintHandlesStatement_SafetyConstraintDescription();

    /**
     * The meta object literal for the '<em><b>Errors</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SAFETY_CONSTRAINT_HANDLES_STATEMENT__ERRORS = eINSTANCE.getSafetyConstraintHandlesStatement_Errors();

    /**
     * The meta object literal for the '<em><b>Ports</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SAFETY_CONSTRAINT_HANDLES_STATEMENT__PORTS = eINSTANCE.getSafetyConstraintHandlesStatement_Ports();

    /**
     * The meta object literal for the '<em><b>Matching Xagree Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SAFETY_CONSTRAINT_HANDLES_STATEMENT__MATCHING_XAGREE_STATEMENTS = eINSTANCE.getSafetyConstraintHandlesStatement_MatchingXagreeStatements();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.SafetyConstraintPreventsStatementImpl <em>Safety Constraint Prevents Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.SafetyConstraintPreventsStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSafetyConstraintPreventsStatement()
     * @generated
     */
    EClass SAFETY_CONSTRAINT_PREVENTS_STATEMENT = eINSTANCE.getSafetyConstraintPreventsStatement();

    /**
     * The meta object literal for the '<em><b>Safety Constraint Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SAFETY_CONSTRAINT_PREVENTS_STATEMENT__SAFETY_CONSTRAINT_ID = eINSTANCE.getSafetyConstraintPreventsStatement_SafetyConstraintId();

    /**
     * The meta object literal for the '<em><b>Safety Constraint Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SAFETY_CONSTRAINT_PREVENTS_STATEMENT__SAFETY_CONSTRAINT_DESCRIPTION = eINSTANCE.getSafetyConstraintPreventsStatement_SafetyConstraintDescription();

    /**
     * The meta object literal for the '<em><b>Errors</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SAFETY_CONSTRAINT_PREVENTS_STATEMENT__ERRORS = eINSTANCE.getSafetyConstraintPreventsStatement_Errors();

    /**
     * The meta object literal for the '<em><b>Ports</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SAFETY_CONSTRAINT_PREVENTS_STATEMENT__PORTS = eINSTANCE.getSafetyConstraintPreventsStatement_Ports();

    /**
     * The meta object literal for the '<em><b>Matching Xagree Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SAFETY_CONSTRAINT_PREVENTS_STATEMENT__MATCHING_XAGREE_STATEMENTS = eINSTANCE.getSafetyConstraintPreventsStatement_MatchingXagreeStatements();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl <em>Error Propagation Rule Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrorPropagationRuleStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorPropagationRuleStatement()
     * @generated
     */
    EClass ERROR_PROPAGATION_RULE_STATEMENT = eINSTANCE.getErrorPropagationRuleStatement();

    /**
     * The meta object literal for the '<em><b>In Errors List</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_PROPAGATION_RULE_STATEMENT__IN_ERRORS_LIST = eINSTANCE.getErrorPropagationRuleStatement_InErrorsList();

    /**
     * The meta object literal for the '<em><b>In Ports Lists</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_PROPAGATION_RULE_STATEMENT__IN_PORTS_LISTS = eINSTANCE.getErrorPropagationRuleStatement_InPortsLists();

    /**
     * The meta object literal for the '<em><b>Out Errors List</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_PROPAGATION_RULE_STATEMENT__OUT_ERRORS_LIST = eINSTANCE.getErrorPropagationRuleStatement_OutErrorsList();

    /**
     * The meta object literal for the '<em><b>Out Ports Lists</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_PROPAGATION_RULE_STATEMENT__OUT_PORTS_LISTS = eINSTANCE.getErrorPropagationRuleStatement_OutPortsLists();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.TypeStatementImpl <em>Type Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.TypeStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getTypeStatement()
     * @generated
     */
    EClass TYPE_STATEMENT = eINSTANCE.getTypeStatement();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TYPE_STATEMENT__TYPE = eINSTANCE.getTypeStatement_Type();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrorsListImpl <em>Errors List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrorsListImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorsList()
     * @generated
     */
    EClass ERRORS_LIST = eINSTANCE.getErrorsList();

    /**
     * The meta object literal for the '<em><b>First Error</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERRORS_LIST__FIRST_ERROR = eINSTANCE.getErrorsList_FirstError();

    /**
     * The meta object literal for the '<em><b>Rest Errors</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERRORS_LIST__REST_ERRORS = eINSTANCE.getErrorsList_RestErrors();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrorsListNoProbabilityImpl <em>Errors List No Probability</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrorsListNoProbabilityImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorsListNoProbability()
     * @generated
     */
    EClass ERRORS_LIST_NO_PROBABILITY = eINSTANCE.getErrorsListNoProbability();

    /**
     * The meta object literal for the '<em><b>First Error</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERRORS_LIST_NO_PROBABILITY__FIRST_ERROR = eINSTANCE.getErrorsListNoProbability_FirstError();

    /**
     * The meta object literal for the '<em><b>Rest Errors</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERRORS_LIST_NO_PROBABILITY__REST_ERRORS = eINSTANCE.getErrorsListNoProbability_RestErrors();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.PortsListImpl <em>Ports List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.PortsListImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getPortsList()
     * @generated
     */
    EClass PORTS_LIST = eINSTANCE.getPortsList();

    /**
     * The meta object literal for the '<em><b>First Port</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PORTS_LIST__FIRST_PORT = eINSTANCE.getPortsList_FirstPort();

    /**
     * The meta object literal for the '<em><b>Rest Ports</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PORTS_LIST__REST_PORTS = eINSTANCE.getPortsList_RestPorts();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrsStatementImpl <em>Errs Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrsStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrsStatement()
     * @generated
     */
    EClass ERRS_STATEMENT = eINSTANCE.getErrsStatement();

    /**
     * The meta object literal for the '<em><b>First Error</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERRS_STATEMENT__FIRST_ERROR = eINSTANCE.getErrsStatement_FirstError();

    /**
     * The meta object literal for the '<em><b>Rest Errors</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERRS_STATEMENT__REST_ERRORS = eINSTANCE.getErrsStatement_RestErrors();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrorStatementImpl <em>Error Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrorStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorStatement()
     * @generated
     */
    EClass ERROR_STATEMENT = eINSTANCE.getErrorStatement();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_STATEMENT__TYPE = eINSTANCE.getErrorStatement_Type();

    /**
     * The meta object literal for the '<em><b>Uca</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_STATEMENT__UCA = eINSTANCE.getErrorStatement_Uca();

    /**
     * The meta object literal for the '<em><b>Cause</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_STATEMENT__CAUSE = eINSTANCE.getErrorStatement_Cause();

    /**
     * The meta object literal for the '<em><b>Sc</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_STATEMENT__SC = eINSTANCE.getErrorStatement_Sc();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrorTypeStatementImpl <em>Error Type Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrorTypeStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorTypeStatement()
     * @generated
     */
    EClass ERROR_TYPE_STATEMENT = eINSTANCE.getErrorTypeStatement();

    /**
     * The meta object literal for the '<em><b>Error Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ERROR_TYPE_STATEMENT__ERROR_TYPE = eINSTANCE.getErrorTypeStatement_ErrorType();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.UnsafeControlActionStatementImpl <em>Unsafe Control Action Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.UnsafeControlActionStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getUnsafeControlActionStatement()
     * @generated
     */
    EClass UNSAFE_CONTROL_ACTION_STATEMENT = eINSTANCE.getUnsafeControlActionStatement();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNSAFE_CONTROL_ACTION_STATEMENT__ID = eINSTANCE.getUnsafeControlActionStatement_Id();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNSAFE_CONTROL_ACTION_STATEMENT__DESCRIPTION = eINSTANCE.getUnsafeControlActionStatement_Description();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.CausesStatementImpl <em>Causes Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.CausesStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getCausesStatement()
     * @generated
     */
    EClass CAUSES_STATEMENT = eINSTANCE.getCausesStatement();

    /**
     * The meta object literal for the '<em><b>General</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CAUSES_STATEMENT__GENERAL = eINSTANCE.getCausesStatement_General();

    /**
     * The meta object literal for the '<em><b>Specific</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CAUSES_STATEMENT__SPECIFIC = eINSTANCE.getCausesStatement_Specific();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.GeneralCauseStatementImpl <em>General Cause Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.GeneralCauseStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getGeneralCauseStatement()
     * @generated
     */
    EClass GENERAL_CAUSE_STATEMENT = eINSTANCE.getGeneralCauseStatement();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GENERAL_CAUSE_STATEMENT__DESCRIPTION = eINSTANCE.getGeneralCauseStatement_Description();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.SpecificCauseStatementImpl <em>Specific Cause Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.SpecificCauseStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSpecificCauseStatement()
     * @generated
     */
    EClass SPECIFIC_CAUSE_STATEMENT = eINSTANCE.getSpecificCauseStatement();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SPECIFIC_CAUSE_STATEMENT__DESCRIPTION = eINSTANCE.getSpecificCauseStatement_Description();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.SafetyConstraintStatementImpl <em>Safety Constraint Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.SafetyConstraintStatementImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getSafetyConstraintStatement()
     * @generated
     */
    EClass SAFETY_CONSTRAINT_STATEMENT = eINSTANCE.getSafetyConstraintStatement();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SAFETY_CONSTRAINT_STATEMENT__ID = eINSTANCE.getSafetyConstraintStatement_Id();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SAFETY_CONSTRAINT_STATEMENT__DESCRIPTION = eINSTANCE.getSafetyConstraintStatement_Description();

    /**
     * The meta object literal for the '<em><b>Matching Xagree Statements</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SAFETY_CONSTRAINT_STATEMENT__MATCHING_XAGREE_STATEMENTS = eINSTANCE.getSafetyConstraintStatement_MatchingXagreeStatements();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.GuaranteeIdsImpl <em>Guarantee Ids</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.GuaranteeIdsImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getGuaranteeIds()
     * @generated
     */
    EClass GUARANTEE_IDS = eINSTANCE.getGuaranteeIds();

    /**
     * The meta object literal for the '<em><b>First</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GUARANTEE_IDS__FIRST = eINSTANCE.getGuaranteeIds_First();

    /**
     * The meta object literal for the '<em><b>Rest</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GUARANTEE_IDS__REST = eINSTANCE.getGuaranteeIds_Rest();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrorImpl <em>Error</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrorImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getError()
     * @generated
     */
    EClass ERROR = eINSTANCE.getError();

    /**
     * The meta object literal for the '<em><b>Error</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ERROR__ERROR = eINSTANCE.getError_Error();

    /**
     * The meta object literal for the '<em><b>Severity Level</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ERROR__SEVERITY_LEVEL = eINSTANCE.getError_SeverityLevel();

    /**
     * The meta object literal for the '<em><b>Probability</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ERROR__PROBABILITY = eINSTANCE.getError_Probability();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.ErrorNoProbabilityImpl <em>Error No Probability</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.ErrorNoProbabilityImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getErrorNoProbability()
     * @generated
     */
    EClass ERROR_NO_PROBABILITY = eINSTANCE.getErrorNoProbability();

    /**
     * The meta object literal for the '<em><b>Error</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ERROR_NO_PROBABILITY__ERROR = eINSTANCE.getErrorNoProbability_Error();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.AsamContractLibraryImpl <em>Contract Library</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.AsamContractLibraryImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamContractLibrary()
     * @generated
     */
    EClass ASAM_CONTRACT_LIBRARY = eINSTANCE.getAsamContractLibrary();

    /**
     * The meta object literal for the '<em><b>Contract</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASAM_CONTRACT_LIBRARY__CONTRACT = eINSTANCE.getAsamContractLibrary_Contract();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.AsamContractSubclauseImpl <em>Contract Subclause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.AsamContractSubclauseImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamContractSubclause()
     * @generated
     */
    EClass ASAM_CONTRACT_SUBCLAUSE = eINSTANCE.getAsamContractSubclause();

    /**
     * The meta object literal for the '<em><b>Contract</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASAM_CONTRACT_SUBCLAUSE__CONTRACT = eINSTANCE.getAsamContractSubclause_Contract();

    /**
     * The meta object literal for the '{@link edu.clemson.asam.asam.impl.AsamContractImpl <em>Contract</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see edu.clemson.asam.asam.impl.AsamContractImpl
     * @see edu.clemson.asam.asam.impl.AsamPackageImpl#getAsamContract()
     * @generated
     */
    EClass ASAM_CONTRACT = eINSTANCE.getAsamContract();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASAM_CONTRACT__STATEMENT = eINSTANCE.getAsamContract_Statement();

  }

} //AsamPackage
