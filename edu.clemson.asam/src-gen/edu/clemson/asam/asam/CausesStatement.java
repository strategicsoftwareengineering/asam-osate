/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Causes Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.CausesStatement#getGeneral <em>General</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.CausesStatement#getSpecific <em>Specific</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getCausesStatement()
 * @model
 * @generated
 */
public interface CausesStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>General</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>General</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>General</em>' containment reference.
   * @see #setGeneral(GeneralCauseStatement)
   * @see edu.clemson.asam.asam.AsamPackage#getCausesStatement_General()
   * @model containment="true"
   * @generated
   */
  GeneralCauseStatement getGeneral();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.CausesStatement#getGeneral <em>General</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>General</em>' containment reference.
   * @see #getGeneral()
   * @generated
   */
  void setGeneral(GeneralCauseStatement value);

  /**
   * Returns the value of the '<em><b>Specific</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Specific</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Specific</em>' containment reference.
   * @see #setSpecific(SpecificCauseStatement)
   * @see edu.clemson.asam.asam.AsamPackage#getCausesStatement_Specific()
   * @model containment="true"
   * @generated
   */
  SpecificCauseStatement getSpecific();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.CausesStatement#getSpecific <em>Specific</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Specific</em>' containment reference.
   * @see #getSpecific()
   * @generated
   */
  void setSpecific(SpecificCauseStatement value);

} // CausesStatement
