/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ports List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.PortsList#getFirstPort <em>First Port</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.PortsList#getRestPorts <em>Rest Ports</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getPortsList()
 * @model
 * @generated
 */
public interface PortsList extends EObject
{
  /**
   * Returns the value of the '<em><b>First Port</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First Port</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First Port</em>' attribute.
   * @see #setFirstPort(String)
   * @see edu.clemson.asam.asam.AsamPackage#getPortsList_FirstPort()
   * @model
   * @generated
   */
  String getFirstPort();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.PortsList#getFirstPort <em>First Port</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First Port</em>' attribute.
   * @see #getFirstPort()
   * @generated
   */
  void setFirstPort(String value);

  /**
   * Returns the value of the '<em><b>Rest Ports</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest Ports</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest Ports</em>' attribute list.
   * @see edu.clemson.asam.asam.AsamPackage#getPortsList_RestPorts()
   * @model unique="false"
   * @generated
   */
  EList<String> getRestPorts();

} // PortsList
