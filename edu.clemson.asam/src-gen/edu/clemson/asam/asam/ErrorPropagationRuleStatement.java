/**
 */
package edu.clemson.asam.asam;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error Propagation Rule Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInErrorsList <em>In Errors List</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInPortsLists <em>In Ports Lists</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutErrorsList <em>Out Errors List</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutPortsLists <em>Out Ports Lists</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getErrorPropagationRuleStatement()
 * @model
 * @generated
 */
public interface ErrorPropagationRuleStatement extends AsamStatement
{
  /**
   * Returns the value of the '<em><b>In Errors List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>In Errors List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>In Errors List</em>' containment reference.
   * @see #setInErrorsList(ErrorsListNoProbability)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorPropagationRuleStatement_InErrorsList()
   * @model containment="true"
   * @generated
   */
  ErrorsListNoProbability getInErrorsList();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInErrorsList <em>In Errors List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>In Errors List</em>' containment reference.
   * @see #getInErrorsList()
   * @generated
   */
  void setInErrorsList(ErrorsListNoProbability value);

  /**
   * Returns the value of the '<em><b>In Ports Lists</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>In Ports Lists</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>In Ports Lists</em>' containment reference.
   * @see #setInPortsLists(PortsList)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorPropagationRuleStatement_InPortsLists()
   * @model containment="true"
   * @generated
   */
  PortsList getInPortsLists();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getInPortsLists <em>In Ports Lists</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>In Ports Lists</em>' containment reference.
   * @see #getInPortsLists()
   * @generated
   */
  void setInPortsLists(PortsList value);

  /**
   * Returns the value of the '<em><b>Out Errors List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Out Errors List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Out Errors List</em>' containment reference.
   * @see #setOutErrorsList(ErrorsList)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorPropagationRuleStatement_OutErrorsList()
   * @model containment="true"
   * @generated
   */
  ErrorsList getOutErrorsList();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutErrorsList <em>Out Errors List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Out Errors List</em>' containment reference.
   * @see #getOutErrorsList()
   * @generated
   */
  void setOutErrorsList(ErrorsList value);

  /**
   * Returns the value of the '<em><b>Out Ports Lists</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Out Ports Lists</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Out Ports Lists</em>' containment reference.
   * @see #setOutPortsLists(PortsList)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorPropagationRuleStatement_OutPortsLists()
   * @model containment="true"
   * @generated
   */
  PortsList getOutPortsLists();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement#getOutPortsLists <em>Out Ports Lists</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Out Ports Lists</em>' containment reference.
   * @see #getOutPortsLists()
   * @generated
   */
  void setOutPortsLists(PortsList value);

} // ErrorPropagationRuleStatement
