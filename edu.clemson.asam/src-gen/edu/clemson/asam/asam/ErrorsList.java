/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Errors List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.ErrorsList#getFirstError <em>First Error</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrorsList#getRestErrors <em>Rest Errors</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getErrorsList()
 * @model
 * @generated
 */
public interface ErrorsList extends EObject
{
  /**
   * Returns the value of the '<em><b>First Error</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First Error</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First Error</em>' containment reference.
   * @see #setFirstError(edu.clemson.asam.asam.Error)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorsList_FirstError()
   * @model containment="true"
   * @generated
   */
  edu.clemson.asam.asam.Error getFirstError();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorsList#getFirstError <em>First Error</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First Error</em>' containment reference.
   * @see #getFirstError()
   * @generated
   */
  void setFirstError(edu.clemson.asam.asam.Error value);

  /**
   * Returns the value of the '<em><b>Rest Errors</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.asam.asam.Error}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest Errors</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest Errors</em>' containment reference list.
   * @see edu.clemson.asam.asam.AsamPackage#getErrorsList_RestErrors()
   * @model containment="true"
   * @generated
   */
  EList<edu.clemson.asam.asam.Error> getRestErrors();

} // ErrorsList
