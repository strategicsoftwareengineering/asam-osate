/**
 */
package edu.clemson.asam.asam;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Internal Failure Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureId <em>Internal Failure Id</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureDescription <em>Internal Failure Description</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.InternalFailureStatement#getErrors <em>Errors</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.InternalFailureStatement#getPorts <em>Ports</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getInternalFailureStatement()
 * @model
 * @generated
 */
public interface InternalFailureStatement extends AsamStatement
{
  /**
   * Returns the value of the '<em><b>Internal Failure Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Internal Failure Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Internal Failure Id</em>' attribute.
   * @see #setInternalFailureId(String)
   * @see edu.clemson.asam.asam.AsamPackage#getInternalFailureStatement_InternalFailureId()
   * @model
   * @generated
   */
  String getInternalFailureId();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureId <em>Internal Failure Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Internal Failure Id</em>' attribute.
   * @see #getInternalFailureId()
   * @generated
   */
  void setInternalFailureId(String value);

  /**
   * Returns the value of the '<em><b>Internal Failure Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Internal Failure Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Internal Failure Description</em>' attribute.
   * @see #setInternalFailureDescription(String)
   * @see edu.clemson.asam.asam.AsamPackage#getInternalFailureStatement_InternalFailureDescription()
   * @model
   * @generated
   */
  String getInternalFailureDescription();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.InternalFailureStatement#getInternalFailureDescription <em>Internal Failure Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Internal Failure Description</em>' attribute.
   * @see #getInternalFailureDescription()
   * @generated
   */
  void setInternalFailureDescription(String value);

  /**
   * Returns the value of the '<em><b>Errors</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Errors</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Errors</em>' containment reference.
   * @see #setErrors(ErrorsList)
   * @see edu.clemson.asam.asam.AsamPackage#getInternalFailureStatement_Errors()
   * @model containment="true"
   * @generated
   */
  ErrorsList getErrors();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.InternalFailureStatement#getErrors <em>Errors</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Errors</em>' containment reference.
   * @see #getErrors()
   * @generated
   */
  void setErrors(ErrorsList value);

  /**
   * Returns the value of the '<em><b>Ports</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ports</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ports</em>' containment reference.
   * @see #setPorts(PortsList)
   * @see edu.clemson.asam.asam.AsamPackage#getInternalFailureStatement_Ports()
   * @model containment="true"
   * @generated
   */
  PortsList getPorts();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.InternalFailureStatement#getPorts <em>Ports</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ports</em>' containment reference.
   * @see #getPorts()
   * @generated
   */
  void setPorts(PortsList value);

} // InternalFailureStatement
