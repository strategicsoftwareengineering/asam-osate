/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexSubclause;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subclause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.asam.asam.AsamPackage#getAsamSubclause()
 * @model
 * @generated
 */
public interface AsamSubclause extends EObject, AnnexSubclause
{
} // AsamSubclause
