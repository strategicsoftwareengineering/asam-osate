/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.ErrorStatement#getType <em>Type</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrorStatement#getUca <em>Uca</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrorStatement#getCause <em>Cause</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.ErrorStatement#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getErrorStatement()
 * @model
 * @generated
 */
public interface ErrorStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(ErrorTypeStatement)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorStatement_Type()
   * @model containment="true"
   * @generated
   */
  ErrorTypeStatement getType();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorStatement#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(ErrorTypeStatement value);

  /**
   * Returns the value of the '<em><b>Uca</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uca</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uca</em>' containment reference.
   * @see #setUca(UnsafeControlActionStatement)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorStatement_Uca()
   * @model containment="true"
   * @generated
   */
  UnsafeControlActionStatement getUca();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorStatement#getUca <em>Uca</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uca</em>' containment reference.
   * @see #getUca()
   * @generated
   */
  void setUca(UnsafeControlActionStatement value);

  /**
   * Returns the value of the '<em><b>Cause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cause</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cause</em>' containment reference.
   * @see #setCause(CausesStatement)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorStatement_Cause()
   * @model containment="true"
   * @generated
   */
  CausesStatement getCause();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorStatement#getCause <em>Cause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cause</em>' containment reference.
   * @see #getCause()
   * @generated
   */
  void setCause(CausesStatement value);

  /**
   * Returns the value of the '<em><b>Sc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' containment reference.
   * @see #setSc(SafetyConstraintStatement)
   * @see edu.clemson.asam.asam.AsamPackage#getErrorStatement_Sc()
   * @model containment="true"
   * @generated
   */
  SafetyConstraintStatement getSc();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.ErrorStatement#getSc <em>Sc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sc</em>' containment reference.
   * @see #getSc()
   * @generated
   */
  void setSc(SafetyConstraintStatement value);

} // ErrorStatement
