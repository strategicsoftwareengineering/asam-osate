/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contract</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.asam.asam.AsamPackage#getContract()
 * @model
 * @generated
 */
public interface Contract extends EObject
{
} // Contract
