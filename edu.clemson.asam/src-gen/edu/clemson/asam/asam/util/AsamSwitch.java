/**
 */
package edu.clemson.asam.asam.util;

import edu.clemson.asam.asam.AsamContract;
import edu.clemson.asam.asam.AsamContractLibrary;
import edu.clemson.asam.asam.AsamContractSubclause;
import edu.clemson.asam.asam.AsamLibrary;
import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.AsamStatement;
import edu.clemson.asam.asam.AsamSubclause;
import edu.clemson.asam.asam.CausesStatement;
import edu.clemson.asam.asam.Contract;
import edu.clemson.asam.asam.ErrorNoProbability;
import edu.clemson.asam.asam.ErrorPropagationRuleStatement;
import edu.clemson.asam.asam.ErrorStatement;
import edu.clemson.asam.asam.ErrorTypeStatement;
import edu.clemson.asam.asam.ErrorsList;
import edu.clemson.asam.asam.ErrorsListNoProbability;
import edu.clemson.asam.asam.ErrsStatement;
import edu.clemson.asam.asam.GeneralCauseStatement;
import edu.clemson.asam.asam.GuaranteeIds;
import edu.clemson.asam.asam.InternalFailureStatement;
import edu.clemson.asam.asam.PortsList;
import edu.clemson.asam.asam.SafetyConstraintHandlesStatement;
import edu.clemson.asam.asam.SafetyConstraintPreventsStatement;
import edu.clemson.asam.asam.SafetyConstraintStatement;
import edu.clemson.asam.asam.SpecificCauseStatement;
import edu.clemson.asam.asam.TypeStatement;
import edu.clemson.asam.asam.UnsafeControlActionStatement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Element;
import org.osate.aadl2.ModalElement;
import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.clemson.asam.asam.AsamPackage
 * @generated
 */
public class AsamSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static AsamPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = AsamPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case AsamPackage.ASAM_LIBRARY:
      {
        AsamLibrary asamLibrary = (AsamLibrary)theEObject;
        T result = caseAsamLibrary(asamLibrary);
        if (result == null) result = caseAnnexLibrary(asamLibrary);
        if (result == null) result = caseNamedElement(asamLibrary);
        if (result == null) result = caseElement(asamLibrary);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ASAM_SUBCLAUSE:
      {
        AsamSubclause asamSubclause = (AsamSubclause)theEObject;
        T result = caseAsamSubclause(asamSubclause);
        if (result == null) result = caseAnnexSubclause(asamSubclause);
        if (result == null) result = caseModalElement(asamSubclause);
        if (result == null) result = caseNamedElement(asamSubclause);
        if (result == null) result = caseElement(asamSubclause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.CONTRACT:
      {
        Contract contract = (Contract)theEObject;
        T result = caseContract(contract);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ASAM_STATEMENT:
      {
        AsamStatement asamStatement = (AsamStatement)theEObject;
        T result = caseAsamStatement(asamStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.INTERNAL_FAILURE_STATEMENT:
      {
        InternalFailureStatement internalFailureStatement = (InternalFailureStatement)theEObject;
        T result = caseInternalFailureStatement(internalFailureStatement);
        if (result == null) result = caseAsamStatement(internalFailureStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.SAFETY_CONSTRAINT_HANDLES_STATEMENT:
      {
        SafetyConstraintHandlesStatement safetyConstraintHandlesStatement = (SafetyConstraintHandlesStatement)theEObject;
        T result = caseSafetyConstraintHandlesStatement(safetyConstraintHandlesStatement);
        if (result == null) result = caseAsamStatement(safetyConstraintHandlesStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.SAFETY_CONSTRAINT_PREVENTS_STATEMENT:
      {
        SafetyConstraintPreventsStatement safetyConstraintPreventsStatement = (SafetyConstraintPreventsStatement)theEObject;
        T result = caseSafetyConstraintPreventsStatement(safetyConstraintPreventsStatement);
        if (result == null) result = caseAsamStatement(safetyConstraintPreventsStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERROR_PROPAGATION_RULE_STATEMENT:
      {
        ErrorPropagationRuleStatement errorPropagationRuleStatement = (ErrorPropagationRuleStatement)theEObject;
        T result = caseErrorPropagationRuleStatement(errorPropagationRuleStatement);
        if (result == null) result = caseAsamStatement(errorPropagationRuleStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.TYPE_STATEMENT:
      {
        TypeStatement typeStatement = (TypeStatement)theEObject;
        T result = caseTypeStatement(typeStatement);
        if (result == null) result = caseAsamStatement(typeStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERRORS_LIST:
      {
        ErrorsList errorsList = (ErrorsList)theEObject;
        T result = caseErrorsList(errorsList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERRORS_LIST_NO_PROBABILITY:
      {
        ErrorsListNoProbability errorsListNoProbability = (ErrorsListNoProbability)theEObject;
        T result = caseErrorsListNoProbability(errorsListNoProbability);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.PORTS_LIST:
      {
        PortsList portsList = (PortsList)theEObject;
        T result = casePortsList(portsList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERRS_STATEMENT:
      {
        ErrsStatement errsStatement = (ErrsStatement)theEObject;
        T result = caseErrsStatement(errsStatement);
        if (result == null) result = caseAsamStatement(errsStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERROR_STATEMENT:
      {
        ErrorStatement errorStatement = (ErrorStatement)theEObject;
        T result = caseErrorStatement(errorStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERROR_TYPE_STATEMENT:
      {
        ErrorTypeStatement errorTypeStatement = (ErrorTypeStatement)theEObject;
        T result = caseErrorTypeStatement(errorTypeStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.UNSAFE_CONTROL_ACTION_STATEMENT:
      {
        UnsafeControlActionStatement unsafeControlActionStatement = (UnsafeControlActionStatement)theEObject;
        T result = caseUnsafeControlActionStatement(unsafeControlActionStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.CAUSES_STATEMENT:
      {
        CausesStatement causesStatement = (CausesStatement)theEObject;
        T result = caseCausesStatement(causesStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.GENERAL_CAUSE_STATEMENT:
      {
        GeneralCauseStatement generalCauseStatement = (GeneralCauseStatement)theEObject;
        T result = caseGeneralCauseStatement(generalCauseStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.SPECIFIC_CAUSE_STATEMENT:
      {
        SpecificCauseStatement specificCauseStatement = (SpecificCauseStatement)theEObject;
        T result = caseSpecificCauseStatement(specificCauseStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.SAFETY_CONSTRAINT_STATEMENT:
      {
        SafetyConstraintStatement safetyConstraintStatement = (SafetyConstraintStatement)theEObject;
        T result = caseSafetyConstraintStatement(safetyConstraintStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.GUARANTEE_IDS:
      {
        GuaranteeIds guaranteeIds = (GuaranteeIds)theEObject;
        T result = caseGuaranteeIds(guaranteeIds);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERROR:
      {
        edu.clemson.asam.asam.Error error = (edu.clemson.asam.asam.Error)theEObject;
        T result = caseError(error);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ERROR_NO_PROBABILITY:
      {
        ErrorNoProbability errorNoProbability = (ErrorNoProbability)theEObject;
        T result = caseErrorNoProbability(errorNoProbability);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ASAM_CONTRACT_LIBRARY:
      {
        AsamContractLibrary asamContractLibrary = (AsamContractLibrary)theEObject;
        T result = caseAsamContractLibrary(asamContractLibrary);
        if (result == null) result = caseAsamLibrary(asamContractLibrary);
        if (result == null) result = caseAnnexLibrary(asamContractLibrary);
        if (result == null) result = caseNamedElement(asamContractLibrary);
        if (result == null) result = caseElement(asamContractLibrary);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ASAM_CONTRACT_SUBCLAUSE:
      {
        AsamContractSubclause asamContractSubclause = (AsamContractSubclause)theEObject;
        T result = caseAsamContractSubclause(asamContractSubclause);
        if (result == null) result = caseAsamSubclause(asamContractSubclause);
        if (result == null) result = caseAnnexSubclause(asamContractSubclause);
        if (result == null) result = caseModalElement(asamContractSubclause);
        if (result == null) result = caseNamedElement(asamContractSubclause);
        if (result == null) result = caseElement(asamContractSubclause);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case AsamPackage.ASAM_CONTRACT:
      {
        AsamContract asamContract = (AsamContract)theEObject;
        T result = caseAsamContract(asamContract);
        if (result == null) result = caseContract(asamContract);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Library</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Library</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAsamLibrary(AsamLibrary object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Subclause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Subclause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAsamSubclause(AsamSubclause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseContract(Contract object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAsamStatement(AsamStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Internal Failure Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Internal Failure Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInternalFailureStatement(InternalFailureStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Safety Constraint Handles Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Safety Constraint Handles Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSafetyConstraintHandlesStatement(SafetyConstraintHandlesStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Safety Constraint Prevents Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Safety Constraint Prevents Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSafetyConstraintPreventsStatement(SafetyConstraintPreventsStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Error Propagation Rule Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Error Propagation Rule Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrorPropagationRuleStatement(ErrorPropagationRuleStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeStatement(TypeStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Errors List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Errors List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrorsList(ErrorsList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Errors List No Probability</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Errors List No Probability</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrorsListNoProbability(ErrorsListNoProbability object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ports List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ports List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortsList(PortsList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Errs Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Errs Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrsStatement(ErrsStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Error Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Error Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrorStatement(ErrorStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Error Type Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Error Type Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrorTypeStatement(ErrorTypeStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unsafe Control Action Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unsafe Control Action Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnsafeControlActionStatement(UnsafeControlActionStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Causes Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Causes Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCausesStatement(CausesStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>General Cause Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>General Cause Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGeneralCauseStatement(GeneralCauseStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Specific Cause Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Specific Cause Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSpecificCauseStatement(SpecificCauseStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Safety Constraint Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Safety Constraint Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSafetyConstraintStatement(SafetyConstraintStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Guarantee Ids</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Guarantee Ids</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGuaranteeIds(GuaranteeIds object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Error</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Error</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseError(edu.clemson.asam.asam.Error object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Error No Probability</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Error No Probability</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrorNoProbability(ErrorNoProbability object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract Library</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract Library</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAsamContractLibrary(AsamContractLibrary object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract Subclause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract Subclause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAsamContractSubclause(AsamContractSubclause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAsamContract(AsamContract object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElement(Element object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamedElement(NamedElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Annex Library</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Annex Library</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnnexLibrary(AnnexLibrary object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Modal Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Modal Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModalElement(ModalElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Annex Subclause</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Annex Subclause</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnnexSubclause(AnnexSubclause object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //AsamSwitch
