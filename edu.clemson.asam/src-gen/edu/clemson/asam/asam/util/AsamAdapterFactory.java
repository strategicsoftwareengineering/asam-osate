/**
 */
package edu.clemson.asam.asam.util;

import edu.clemson.asam.asam.AsamContract;
import edu.clemson.asam.asam.AsamContractLibrary;
import edu.clemson.asam.asam.AsamContractSubclause;
import edu.clemson.asam.asam.AsamLibrary;
import edu.clemson.asam.asam.AsamPackage;
import edu.clemson.asam.asam.AsamStatement;
import edu.clemson.asam.asam.AsamSubclause;
import edu.clemson.asam.asam.CausesStatement;
import edu.clemson.asam.asam.Contract;
import edu.clemson.asam.asam.ErrorNoProbability;
import edu.clemson.asam.asam.ErrorPropagationRuleStatement;
import edu.clemson.asam.asam.ErrorStatement;
import edu.clemson.asam.asam.ErrorTypeStatement;
import edu.clemson.asam.asam.ErrorsList;
import edu.clemson.asam.asam.ErrorsListNoProbability;
import edu.clemson.asam.asam.ErrsStatement;
import edu.clemson.asam.asam.GeneralCauseStatement;
import edu.clemson.asam.asam.GuaranteeIds;
import edu.clemson.asam.asam.InternalFailureStatement;
import edu.clemson.asam.asam.PortsList;
import edu.clemson.asam.asam.SafetyConstraintHandlesStatement;
import edu.clemson.asam.asam.SafetyConstraintPreventsStatement;
import edu.clemson.asam.asam.SafetyConstraintStatement;
import edu.clemson.asam.asam.SpecificCauseStatement;
import edu.clemson.asam.asam.TypeStatement;
import edu.clemson.asam.asam.UnsafeControlActionStatement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Element;
import org.osate.aadl2.ModalElement;
import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see edu.clemson.asam.asam.AsamPackage
 * @generated
 */
public class AsamAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static AsamPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AsamAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = AsamPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AsamSwitch<Adapter> modelSwitch =
    new AsamSwitch<Adapter>()
    {
      @Override
      public Adapter caseAsamLibrary(AsamLibrary object)
      {
        return createAsamLibraryAdapter();
      }
      @Override
      public Adapter caseAsamSubclause(AsamSubclause object)
      {
        return createAsamSubclauseAdapter();
      }
      @Override
      public Adapter caseContract(Contract object)
      {
        return createContractAdapter();
      }
      @Override
      public Adapter caseAsamStatement(AsamStatement object)
      {
        return createAsamStatementAdapter();
      }
      @Override
      public Adapter caseInternalFailureStatement(InternalFailureStatement object)
      {
        return createInternalFailureStatementAdapter();
      }
      @Override
      public Adapter caseSafetyConstraintHandlesStatement(SafetyConstraintHandlesStatement object)
      {
        return createSafetyConstraintHandlesStatementAdapter();
      }
      @Override
      public Adapter caseSafetyConstraintPreventsStatement(SafetyConstraintPreventsStatement object)
      {
        return createSafetyConstraintPreventsStatementAdapter();
      }
      @Override
      public Adapter caseErrorPropagationRuleStatement(ErrorPropagationRuleStatement object)
      {
        return createErrorPropagationRuleStatementAdapter();
      }
      @Override
      public Adapter caseTypeStatement(TypeStatement object)
      {
        return createTypeStatementAdapter();
      }
      @Override
      public Adapter caseErrorsList(ErrorsList object)
      {
        return createErrorsListAdapter();
      }
      @Override
      public Adapter caseErrorsListNoProbability(ErrorsListNoProbability object)
      {
        return createErrorsListNoProbabilityAdapter();
      }
      @Override
      public Adapter casePortsList(PortsList object)
      {
        return createPortsListAdapter();
      }
      @Override
      public Adapter caseErrsStatement(ErrsStatement object)
      {
        return createErrsStatementAdapter();
      }
      @Override
      public Adapter caseErrorStatement(ErrorStatement object)
      {
        return createErrorStatementAdapter();
      }
      @Override
      public Adapter caseErrorTypeStatement(ErrorTypeStatement object)
      {
        return createErrorTypeStatementAdapter();
      }
      @Override
      public Adapter caseUnsafeControlActionStatement(UnsafeControlActionStatement object)
      {
        return createUnsafeControlActionStatementAdapter();
      }
      @Override
      public Adapter caseCausesStatement(CausesStatement object)
      {
        return createCausesStatementAdapter();
      }
      @Override
      public Adapter caseGeneralCauseStatement(GeneralCauseStatement object)
      {
        return createGeneralCauseStatementAdapter();
      }
      @Override
      public Adapter caseSpecificCauseStatement(SpecificCauseStatement object)
      {
        return createSpecificCauseStatementAdapter();
      }
      @Override
      public Adapter caseSafetyConstraintStatement(SafetyConstraintStatement object)
      {
        return createSafetyConstraintStatementAdapter();
      }
      @Override
      public Adapter caseGuaranteeIds(GuaranteeIds object)
      {
        return createGuaranteeIdsAdapter();
      }
      @Override
      public Adapter caseError(edu.clemson.asam.asam.Error object)
      {
        return createErrorAdapter();
      }
      @Override
      public Adapter caseErrorNoProbability(ErrorNoProbability object)
      {
        return createErrorNoProbabilityAdapter();
      }
      @Override
      public Adapter caseAsamContractLibrary(AsamContractLibrary object)
      {
        return createAsamContractLibraryAdapter();
      }
      @Override
      public Adapter caseAsamContractSubclause(AsamContractSubclause object)
      {
        return createAsamContractSubclauseAdapter();
      }
      @Override
      public Adapter caseAsamContract(AsamContract object)
      {
        return createAsamContractAdapter();
      }
      @Override
      public Adapter caseElement(Element object)
      {
        return createElementAdapter();
      }
      @Override
      public Adapter caseNamedElement(NamedElement object)
      {
        return createNamedElementAdapter();
      }
      @Override
      public Adapter caseAnnexLibrary(AnnexLibrary object)
      {
        return createAnnexLibraryAdapter();
      }
      @Override
      public Adapter caseModalElement(ModalElement object)
      {
        return createModalElementAdapter();
      }
      @Override
      public Adapter caseAnnexSubclause(AnnexSubclause object)
      {
        return createAnnexSubclauseAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.AsamLibrary <em>Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.AsamLibrary
   * @generated
   */
  public Adapter createAsamLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.AsamSubclause <em>Subclause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.AsamSubclause
   * @generated
   */
  public Adapter createAsamSubclauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.Contract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.Contract
   * @generated
   */
  public Adapter createContractAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.AsamStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.AsamStatement
   * @generated
   */
  public Adapter createAsamStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.InternalFailureStatement <em>Internal Failure Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.InternalFailureStatement
   * @generated
   */
  public Adapter createInternalFailureStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.SafetyConstraintHandlesStatement <em>Safety Constraint Handles Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.SafetyConstraintHandlesStatement
   * @generated
   */
  public Adapter createSafetyConstraintHandlesStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement <em>Safety Constraint Prevents Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.SafetyConstraintPreventsStatement
   * @generated
   */
  public Adapter createSafetyConstraintPreventsStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.ErrorPropagationRuleStatement <em>Error Propagation Rule Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.ErrorPropagationRuleStatement
   * @generated
   */
  public Adapter createErrorPropagationRuleStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.TypeStatement <em>Type Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.TypeStatement
   * @generated
   */
  public Adapter createTypeStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.ErrorsList <em>Errors List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.ErrorsList
   * @generated
   */
  public Adapter createErrorsListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.ErrorsListNoProbability <em>Errors List No Probability</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.ErrorsListNoProbability
   * @generated
   */
  public Adapter createErrorsListNoProbabilityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.PortsList <em>Ports List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.PortsList
   * @generated
   */
  public Adapter createPortsListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.ErrsStatement <em>Errs Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.ErrsStatement
   * @generated
   */
  public Adapter createErrsStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.ErrorStatement <em>Error Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.ErrorStatement
   * @generated
   */
  public Adapter createErrorStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.ErrorTypeStatement <em>Error Type Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.ErrorTypeStatement
   * @generated
   */
  public Adapter createErrorTypeStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.UnsafeControlActionStatement <em>Unsafe Control Action Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.UnsafeControlActionStatement
   * @generated
   */
  public Adapter createUnsafeControlActionStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.CausesStatement <em>Causes Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.CausesStatement
   * @generated
   */
  public Adapter createCausesStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.GeneralCauseStatement <em>General Cause Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.GeneralCauseStatement
   * @generated
   */
  public Adapter createGeneralCauseStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.SpecificCauseStatement <em>Specific Cause Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.SpecificCauseStatement
   * @generated
   */
  public Adapter createSpecificCauseStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.SafetyConstraintStatement <em>Safety Constraint Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.SafetyConstraintStatement
   * @generated
   */
  public Adapter createSafetyConstraintStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.GuaranteeIds <em>Guarantee Ids</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.GuaranteeIds
   * @generated
   */
  public Adapter createGuaranteeIdsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.Error <em>Error</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.Error
   * @generated
   */
  public Adapter createErrorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.ErrorNoProbability <em>Error No Probability</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.ErrorNoProbability
   * @generated
   */
  public Adapter createErrorNoProbabilityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.AsamContractLibrary <em>Contract Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.AsamContractLibrary
   * @generated
   */
  public Adapter createAsamContractLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.AsamContractSubclause <em>Contract Subclause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.AsamContractSubclause
   * @generated
   */
  public Adapter createAsamContractSubclauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link edu.clemson.asam.asam.AsamContract <em>Contract</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see edu.clemson.asam.asam.AsamContract
   * @generated
   */
  public Adapter createAsamContractAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.Element <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.Element
   * @generated
   */
  public Adapter createElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.NamedElement <em>Named Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.NamedElement
   * @generated
   */
  public Adapter createNamedElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.AnnexLibrary <em>Annex Library</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.AnnexLibrary
   * @generated
   */
  public Adapter createAnnexLibraryAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.ModalElement <em>Modal Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.ModalElement
   * @generated
   */
  public Adapter createModalElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.osate.aadl2.AnnexSubclause <em>Annex Subclause</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.osate.aadl2.AnnexSubclause
   * @generated
   */
  public Adapter createAnnexSubclauseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //AsamAdapterFactory
