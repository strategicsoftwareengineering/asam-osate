/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contract</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.AsamContract#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getAsamContract()
 * @model
 * @generated
 */
public interface AsamContract extends Contract
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.asam.asam.AsamStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference list.
   * @see edu.clemson.asam.asam.AsamPackage#getAsamContract_Statement()
   * @model containment="true"
   * @generated
   */
  EList<AsamStatement> getStatement();

} // AsamContract
