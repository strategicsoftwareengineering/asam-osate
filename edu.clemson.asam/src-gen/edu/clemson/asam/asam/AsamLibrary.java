/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexLibrary;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Library</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.asam.asam.AsamPackage#getAsamLibrary()
 * @model
 * @generated
 */
public interface AsamLibrary extends EObject, AnnexLibrary
{
} // AsamLibrary
