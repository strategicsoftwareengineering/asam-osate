/**
 */
package edu.clemson.asam.asam;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Safety Constraint Prevents Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintId <em>Safety Constraint Id</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintDescription <em>Safety Constraint Description</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getErrors <em>Errors</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getPorts <em>Ports</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintPreventsStatement()
 * @model
 * @generated
 */
public interface SafetyConstraintPreventsStatement extends AsamStatement
{
  /**
   * Returns the value of the '<em><b>Safety Constraint Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Safety Constraint Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Safety Constraint Id</em>' attribute.
   * @see #setSafetyConstraintId(String)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintPreventsStatement_SafetyConstraintId()
   * @model
   * @generated
   */
  String getSafetyConstraintId();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintId <em>Safety Constraint Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Safety Constraint Id</em>' attribute.
   * @see #getSafetyConstraintId()
   * @generated
   */
  void setSafetyConstraintId(String value);

  /**
   * Returns the value of the '<em><b>Safety Constraint Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Safety Constraint Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Safety Constraint Description</em>' attribute.
   * @see #setSafetyConstraintDescription(String)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintPreventsStatement_SafetyConstraintDescription()
   * @model
   * @generated
   */
  String getSafetyConstraintDescription();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getSafetyConstraintDescription <em>Safety Constraint Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Safety Constraint Description</em>' attribute.
   * @see #getSafetyConstraintDescription()
   * @generated
   */
  void setSafetyConstraintDescription(String value);

  /**
   * Returns the value of the '<em><b>Errors</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Errors</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Errors</em>' containment reference.
   * @see #setErrors(ErrorsListNoProbability)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintPreventsStatement_Errors()
   * @model containment="true"
   * @generated
   */
  ErrorsListNoProbability getErrors();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getErrors <em>Errors</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Errors</em>' containment reference.
   * @see #getErrors()
   * @generated
   */
  void setErrors(ErrorsListNoProbability value);

  /**
   * Returns the value of the '<em><b>Ports</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ports</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ports</em>' containment reference.
   * @see #setPorts(PortsList)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintPreventsStatement_Ports()
   * @model containment="true"
   * @generated
   */
  PortsList getPorts();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getPorts <em>Ports</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ports</em>' containment reference.
   * @see #getPorts()
   * @generated
   */
  void setPorts(PortsList value);

  /**
   * Returns the value of the '<em><b>Matching Xagree Statements</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Matching Xagree Statements</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Matching Xagree Statements</em>' containment reference.
   * @see #setMatchingXagreeStatements(GuaranteeIds)
   * @see edu.clemson.asam.asam.AsamPackage#getSafetyConstraintPreventsStatement_MatchingXagreeStatements()
   * @model containment="true"
   * @generated
   */
  GuaranteeIds getMatchingXagreeStatements();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.SafetyConstraintPreventsStatement#getMatchingXagreeStatements <em>Matching Xagree Statements</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Matching Xagree Statements</em>' containment reference.
   * @see #getMatchingXagreeStatements()
   * @generated
   */
  void setMatchingXagreeStatements(GuaranteeIds value);

} // SafetyConstraintPreventsStatement
