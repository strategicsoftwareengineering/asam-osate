/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.Error#getError <em>Error</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.Error#getSeverityLevel <em>Severity Level</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.Error#getProbability <em>Probability</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getError()
 * @model
 * @generated
 */
public interface Error extends EObject
{
  /**
   * Returns the value of the '<em><b>Error</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Error</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Error</em>' attribute.
   * @see #setError(String)
   * @see edu.clemson.asam.asam.AsamPackage#getError_Error()
   * @model
   * @generated
   */
  String getError();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.Error#getError <em>Error</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Error</em>' attribute.
   * @see #getError()
   * @generated
   */
  void setError(String value);

  /**
   * Returns the value of the '<em><b>Severity Level</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Severity Level</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Severity Level</em>' attribute.
   * @see #setSeverityLevel(String)
   * @see edu.clemson.asam.asam.AsamPackage#getError_SeverityLevel()
   * @model
   * @generated
   */
  String getSeverityLevel();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.Error#getSeverityLevel <em>Severity Level</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Severity Level</em>' attribute.
   * @see #getSeverityLevel()
   * @generated
   */
  void setSeverityLevel(String value);

  /**
   * Returns the value of the '<em><b>Probability</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Probability</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Probability</em>' attribute.
   * @see #setProbability(String)
   * @see edu.clemson.asam.asam.AsamPackage#getError_Probability()
   * @model
   * @generated
   */
  String getProbability();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.Error#getProbability <em>Probability</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Probability</em>' attribute.
   * @see #getProbability()
   * @generated
   */
  void setProbability(String value);

} // Error
