/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.asam.asam.AsamPackage#getAsamStatement()
 * @model
 * @generated
 */
public interface AsamStatement extends EObject
{
} // AsamStatement
