/**
 */
package edu.clemson.asam.asam;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.TypeStatement#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getTypeStatement()
 * @model
 * @generated
 */
public interface TypeStatement extends AsamStatement
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see edu.clemson.asam.asam.AsamPackage#getTypeStatement_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.TypeStatement#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // TypeStatement
