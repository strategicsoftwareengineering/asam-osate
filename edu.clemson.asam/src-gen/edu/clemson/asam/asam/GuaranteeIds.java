/**
 */
package edu.clemson.asam.asam;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guarantee Ids</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.asam.asam.GuaranteeIds#getFirst <em>First</em>}</li>
 *   <li>{@link edu.clemson.asam.asam.GuaranteeIds#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see edu.clemson.asam.asam.AsamPackage#getGuaranteeIds()
 * @model
 * @generated
 */
public interface GuaranteeIds extends EObject
{
  /**
   * Returns the value of the '<em><b>First</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First</em>' attribute.
   * @see #setFirst(String)
   * @see edu.clemson.asam.asam.AsamPackage#getGuaranteeIds_First()
   * @model
   * @generated
   */
  String getFirst();

  /**
   * Sets the value of the '{@link edu.clemson.asam.asam.GuaranteeIds#getFirst <em>First</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>First</em>' attribute.
   * @see #getFirst()
   * @generated
   */
  void setFirst(String value);

  /**
   * Returns the value of the '<em><b>Rest</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rest</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rest</em>' attribute list.
   * @see edu.clemson.asam.asam.AsamPackage#getGuaranteeIds_Rest()
   * @model unique="false"
   * @generated
   */
  EList<String> getRest();

} // GuaranteeIds
