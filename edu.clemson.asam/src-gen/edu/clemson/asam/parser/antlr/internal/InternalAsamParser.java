package edu.clemson.asam.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import edu.clemson.asam.services.AsamGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalAsamParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AsymmetricApproximateValue", "AsymmetricReplicatesError", "AsymmetricServiceOmission", "SymmetricApproximateValue", "SymmetricReplicatesError", "SymmetricServiceOmission", "TransientServiceOmission", "BoundedOmissionInterval", "EarlyServiceTermination", "AsymmetricItemOmission", "LateServiceTermination", "UndetectableValueError", "SymmetricItemOmission", "AsymmetricExactValue", "DetectableValueError", "SequenceTimingError", "SymmetricExactValue", "AsymmetricOmission", "BoundedValueChange", "SequenceCommission", "SequenceValueError", "ServiceTimingError", "TimingRelatedError", "Controlled_process", "EarlyServiceStart", "ServiceCommission", "ServiceValueError", "SymmetricOmission", "ValueRelatedError", "AsymmetricTiming", "ConcurrencyError", "LateServiceStart", "OutOfCalibration", "ReplicationError", "SequenceOmission", "AsymmetricValue", "ItemTimingError", "ServiceOmission", "SymmetricTiming", "DelayedService", "ItemCommission", "ItemValueError", "SymmetricValue", "WriteWriteRace", "EarlyDelivery", "RaceCondition", "ReadWriteRace", "Catastrophic", "EarlyService", "ItemOmission", "LateDelivery", "ServiceError", "OutOfBounds", "AboveRange", "BelowRange", "MutExError", "Negligible", "OutOfOrder", "OutOfRange", "RateJitter", "Starvation", "StuckValue", "Classifier", "Constraint", "Controller", "Reference", "Critical", "Deadlock", "HighRate", "Marginal", "Specific", "Actuator", "Constant", "Internal", "Prevents", "Verified", "General", "LowRate", "Applies", "Binding", "Compute", "Failure", "Handles", "Causes_1", "Safety", "Sensor", "Delta", "False", "Modes", "Errs", "True", "Type_1", "PlusSignEqualsSignGreaterThanSign", "P", "UCA", "FullStopFullStop", "ColonColon", "EqualsSignGreaterThanSign", "SC", "By", "In", "On", "To", "LeftParenthesis", "RightParenthesis", "Asterisk", "PlusSign", "Comma", "HyphenMinus", "FullStop", "DigitZero", "DigitOne", "Colon", "Semicolon", "LeftSquareBracket", "RightSquareBracket", "LeftCurlyBracket", "RightCurlyBracket", "RULE_INTEGER_LIT", "RULE_FLOAT", "RULE_SL_COMMENT", "RULE_DIGIT", "RULE_EXPONENT", "RULE_INT_EXPONENT", "RULE_REAL_LIT", "RULE_BASED_INTEGER", "RULE_EXTENDED_DIGIT", "RULE_STRING", "RULE_ID", "RULE_WS"
    };
    public static final int EqualsSignGreaterThanSign=101;
    public static final int Internal=77;
    public static final int ValueRelatedError=32;
    public static final int OutOfOrder=61;
    public static final int False=91;
    public static final int StuckValue=65;
    public static final int Verified=79;
    public static final int LateServiceTermination=14;
    public static final int PlusSignEqualsSignGreaterThanSign=96;
    public static final int LeftParenthesis=107;
    public static final int AsymmetricReplicatesError=5;
    public static final int BoundedOmissionInterval=11;
    public static final int ConcurrencyError=34;
    public static final int Failure=85;
    public static final int TimingRelatedError=26;
    public static final int Controlled_process=27;
    public static final int ReplicationError=37;
    public static final int DetectableValueError=18;
    public static final int RULE_ID=132;
    public static final int Marginal=73;
    public static final int OutOfCalibration=36;
    public static final int AsymmetricValue=39;
    public static final int RULE_DIGIT=125;
    public static final int Causes_1=87;
    public static final int BelowRange=58;
    public static final int ColonColon=100;
    public static final int ItemOmission=53;
    public static final int PlusSign=110;
    public static final int LeftSquareBracket=118;
    public static final int SequenceValueError=24;
    public static final int ReadWriteRace=50;
    public static final int LateDelivery=54;
    public static final int LowRate=81;
    public static final int SymmetricExactValue=20;
    public static final int In=104;
    public static final int RULE_REAL_LIT=128;
    public static final int EarlyServiceStart=28;
    public static final int Classifier=66;
    public static final int AsymmetricItemOmission=13;
    public static final int DelayedService=43;
    public static final int Negligible=60;
    public static final int P=97;
    public static final int SC=102;
    public static final int Comma=111;
    public static final int HyphenMinus=112;
    public static final int SequenceTimingError=19;
    public static final int SymmetricTiming=42;
    public static final int Prevents=78;
    public static final int RightCurlyBracket=121;
    public static final int OutOfRange=62;
    public static final int Modes=92;
    public static final int FullStop=113;
    public static final int AboveRange=57;
    public static final int Reference=69;
    public static final int ItemTimingError=40;
    public static final int Controller=68;
    public static final int Deadlock=71;
    public static final int Semicolon=117;
    public static final int UCA=98;
    public static final int RULE_EXPONENT=126;
    public static final int ServiceTimingError=25;
    public static final int Catastrophic=51;
    public static final int Delta=90;
    public static final int By=103;
    public static final int RULE_EXTENDED_DIGIT=130;
    public static final int RULE_FLOAT=123;
    public static final int AsymmetricApproximateValue=4;
    public static final int Constraint=67;
    public static final int True=94;
    public static final int Critical=70;
    public static final int AsymmetricOmission=21;
    public static final int AsymmetricTiming=33;
    public static final int RULE_INT_EXPONENT=127;
    public static final int SymmetricServiceOmission=9;
    public static final int EarlyServiceTermination=12;
    public static final int SequenceCommission=23;
    public static final int FullStopFullStop=99;
    public static final int RateJitter=63;
    public static final int To=106;
    public static final int Applies=82;
    public static final int RULE_BASED_INTEGER=129;
    public static final int SequenceOmission=38;
    public static final int RightSquareBracket=119;
    public static final int WriteWriteRace=47;
    public static final int Binding=83;
    public static final int ServiceError=55;
    public static final int SymmetricApproximateValue=7;
    public static final int Starvation=64;
    public static final int HighRate=72;
    public static final int Handles=86;
    public static final int RightParenthesis=108;
    public static final int LateServiceStart=35;
    public static final int DigitZero=114;
    public static final int BoundedValueChange=22;
    public static final int Type_1=95;
    public static final int AsymmetricExactValue=17;
    public static final int General=80;
    public static final int ServiceOmission=41;
    public static final int UndetectableValueError=15;
    public static final int RULE_INTEGER_LIT=122;
    public static final int Sensor=89;
    public static final int Constant=76;
    public static final int RULE_STRING=131;
    public static final int SymmetricOmission=31;
    public static final int SymmetricReplicatesError=8;
    public static final int TransientServiceOmission=10;
    public static final int RULE_SL_COMMENT=124;
    public static final int Safety=88;
    public static final int Colon=116;
    public static final int EOF=-1;
    public static final int Asterisk=109;
    public static final int ServiceCommission=29;
    public static final int Actuator=75;
    public static final int ServiceValueError=30;
    public static final int RaceCondition=49;
    public static final int RULE_WS=133;
    public static final int DigitOne=115;
    public static final int LeftCurlyBracket=120;
    public static final int ItemCommission=44;
    public static final int ItemValueError=45;
    public static final int OutOfBounds=56;
    public static final int Specific=74;
    public static final int EarlyDelivery=48;
    public static final int Errs=93;
    public static final int SymmetricItemOmission=16;
    public static final int MutExError=59;
    public static final int EarlyService=52;
    public static final int Compute=84;
    public static final int SymmetricValue=46;
    public static final int AsymmetricServiceOmission=6;
    public static final int On=105;

    // delegates
    // delegators


        public InternalAsamParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAsamParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAsamParser.tokenNames; }
    public String getGrammarFileName() { return "InternalAsamParser.g"; }




    	private AsamGrammarAccess grammarAccess;
    	 	
    	public InternalAsamParser(TokenStream input, AsamGrammarAccess grammarAccess) {
    		this(input);
    		this.grammarAccess = grammarAccess;
    		registerRules(grammarAccess.getGrammar());
    	}
    	
    	@Override
    	protected String getFirstRuleName() {
    		return "AnnexLibrary";	
    	} 
    	   	   	
    	@Override
    	protected AsamGrammarAccess getGrammarAccess() {
    		return grammarAccess;
    	}



    // $ANTLR start "entryRuleAnnexLibrary"
    // InternalAsamParser.g:61:1: entryRuleAnnexLibrary returns [EObject current=null] : iv_ruleAnnexLibrary= ruleAnnexLibrary EOF ;
    public final EObject entryRuleAnnexLibrary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnexLibrary = null;


        try {
            // InternalAsamParser.g:62:2: (iv_ruleAnnexLibrary= ruleAnnexLibrary EOF )
            // InternalAsamParser.g:63:2: iv_ruleAnnexLibrary= ruleAnnexLibrary EOF
            {
             newCompositeNode(grammarAccess.getAnnexLibraryRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAnnexLibrary=ruleAnnexLibrary();

            state._fsp--;

             current =iv_ruleAnnexLibrary; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnexLibrary"


    // $ANTLR start "ruleAnnexLibrary"
    // InternalAsamParser.g:70:1: ruleAnnexLibrary returns [EObject current=null] : this_AsamLibrary_0= ruleAsamLibrary ;
    public final EObject ruleAnnexLibrary() throws RecognitionException {
        EObject current = null;

        EObject this_AsamLibrary_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:73:28: (this_AsamLibrary_0= ruleAsamLibrary )
            // InternalAsamParser.g:75:5: this_AsamLibrary_0= ruleAsamLibrary
            {
             
                    newCompositeNode(grammarAccess.getAnnexLibraryAccess().getAsamLibraryParserRuleCall()); 
                
            pushFollow(FollowSets000.FOLLOW_2);
            this_AsamLibrary_0=ruleAsamLibrary();

            state._fsp--;


                    current = this_AsamLibrary_0;
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnexLibrary"


    // $ANTLR start "entryRuleAsamLibrary"
    // InternalAsamParser.g:93:1: entryRuleAsamLibrary returns [EObject current=null] : iv_ruleAsamLibrary= ruleAsamLibrary EOF ;
    public final EObject entryRuleAsamLibrary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAsamLibrary = null;


        try {
            // InternalAsamParser.g:94:2: (iv_ruleAsamLibrary= ruleAsamLibrary EOF )
            // InternalAsamParser.g:95:2: iv_ruleAsamLibrary= ruleAsamLibrary EOF
            {
             newCompositeNode(grammarAccess.getAsamLibraryRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAsamLibrary=ruleAsamLibrary();

            state._fsp--;

             current =iv_ruleAsamLibrary; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAsamLibrary"


    // $ANTLR start "ruleAsamLibrary"
    // InternalAsamParser.g:102:1: ruleAsamLibrary returns [EObject current=null] : ( () ( (lv_contract_1_0= ruleAsamContract ) ) ) ;
    public final EObject ruleAsamLibrary() throws RecognitionException {
        EObject current = null;

        EObject lv_contract_1_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:105:28: ( ( () ( (lv_contract_1_0= ruleAsamContract ) ) ) )
            // InternalAsamParser.g:106:1: ( () ( (lv_contract_1_0= ruleAsamContract ) ) )
            {
            // InternalAsamParser.g:106:1: ( () ( (lv_contract_1_0= ruleAsamContract ) ) )
            // InternalAsamParser.g:106:2: () ( (lv_contract_1_0= ruleAsamContract ) )
            {
            // InternalAsamParser.g:106:2: ()
            // InternalAsamParser.g:107:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAsamLibraryAccess().getAsamContractLibraryAction_0(),
                        current);
                

            }

            // InternalAsamParser.g:112:2: ( (lv_contract_1_0= ruleAsamContract ) )
            // InternalAsamParser.g:113:1: (lv_contract_1_0= ruleAsamContract )
            {
            // InternalAsamParser.g:113:1: (lv_contract_1_0= ruleAsamContract )
            // InternalAsamParser.g:114:3: lv_contract_1_0= ruleAsamContract
            {
             
            	        newCompositeNode(grammarAccess.getAsamLibraryAccess().getContractAsamContractParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_contract_1_0=ruleAsamContract();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAsamLibraryRule());
            	        }
                   		set(
                   			current, 
                   			"contract",
                    		lv_contract_1_0, 
                    		"edu.clemson.asam.Asam.AsamContract");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAsamLibrary"


    // $ANTLR start "entryRuleAsamSubclause"
    // InternalAsamParser.g:138:1: entryRuleAsamSubclause returns [EObject current=null] : iv_ruleAsamSubclause= ruleAsamSubclause EOF ;
    public final EObject entryRuleAsamSubclause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAsamSubclause = null;


        try {
            // InternalAsamParser.g:139:2: (iv_ruleAsamSubclause= ruleAsamSubclause EOF )
            // InternalAsamParser.g:140:2: iv_ruleAsamSubclause= ruleAsamSubclause EOF
            {
             newCompositeNode(grammarAccess.getAsamSubclauseRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAsamSubclause=ruleAsamSubclause();

            state._fsp--;

             current =iv_ruleAsamSubclause; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAsamSubclause"


    // $ANTLR start "ruleAsamSubclause"
    // InternalAsamParser.g:147:1: ruleAsamSubclause returns [EObject current=null] : ( () ( (lv_contract_1_0= ruleAsamContract ) ) ) ;
    public final EObject ruleAsamSubclause() throws RecognitionException {
        EObject current = null;

        EObject lv_contract_1_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:150:28: ( ( () ( (lv_contract_1_0= ruleAsamContract ) ) ) )
            // InternalAsamParser.g:151:1: ( () ( (lv_contract_1_0= ruleAsamContract ) ) )
            {
            // InternalAsamParser.g:151:1: ( () ( (lv_contract_1_0= ruleAsamContract ) ) )
            // InternalAsamParser.g:151:2: () ( (lv_contract_1_0= ruleAsamContract ) )
            {
            // InternalAsamParser.g:151:2: ()
            // InternalAsamParser.g:152:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAsamSubclauseAccess().getAsamContractSubclauseAction_0(),
                        current);
                

            }

            // InternalAsamParser.g:157:2: ( (lv_contract_1_0= ruleAsamContract ) )
            // InternalAsamParser.g:158:1: (lv_contract_1_0= ruleAsamContract )
            {
            // InternalAsamParser.g:158:1: (lv_contract_1_0= ruleAsamContract )
            // InternalAsamParser.g:159:3: lv_contract_1_0= ruleAsamContract
            {
             
            	        newCompositeNode(grammarAccess.getAsamSubclauseAccess().getContractAsamContractParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_contract_1_0=ruleAsamContract();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAsamSubclauseRule());
            	        }
                   		set(
                   			current, 
                   			"contract",
                    		lv_contract_1_0, 
                    		"edu.clemson.asam.Asam.AsamContract");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAsamSubclause"


    // $ANTLR start "entryRuleAsamContract"
    // InternalAsamParser.g:183:1: entryRuleAsamContract returns [EObject current=null] : iv_ruleAsamContract= ruleAsamContract EOF ;
    public final EObject entryRuleAsamContract() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAsamContract = null;


        try {
            // InternalAsamParser.g:184:2: (iv_ruleAsamContract= ruleAsamContract EOF )
            // InternalAsamParser.g:185:2: iv_ruleAsamContract= ruleAsamContract EOF
            {
             newCompositeNode(grammarAccess.getAsamContractRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAsamContract=ruleAsamContract();

            state._fsp--;

             current =iv_ruleAsamContract; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAsamContract"


    // $ANTLR start "ruleAsamContract"
    // InternalAsamParser.g:192:1: ruleAsamContract returns [EObject current=null] : ( () ( (lv_statement_1_0= ruleAsamStatement ) )* ) ;
    public final EObject ruleAsamContract() throws RecognitionException {
        EObject current = null;

        EObject lv_statement_1_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:195:28: ( ( () ( (lv_statement_1_0= ruleAsamStatement ) )* ) )
            // InternalAsamParser.g:196:1: ( () ( (lv_statement_1_0= ruleAsamStatement ) )* )
            {
            // InternalAsamParser.g:196:1: ( () ( (lv_statement_1_0= ruleAsamStatement ) )* )
            // InternalAsamParser.g:196:2: () ( (lv_statement_1_0= ruleAsamStatement ) )*
            {
            // InternalAsamParser.g:196:2: ()
            // InternalAsamParser.g:197:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAsamContractAccess().getAsamContractAction_0(),
                        current);
                

            }

            // InternalAsamParser.g:202:2: ( (lv_statement_1_0= ruleAsamStatement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==Internal||LA1_0==Safety||LA1_0==Errs||LA1_0==Type_1||LA1_0==LeftSquareBracket) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalAsamParser.g:203:1: (lv_statement_1_0= ruleAsamStatement )
            	    {
            	    // InternalAsamParser.g:203:1: (lv_statement_1_0= ruleAsamStatement )
            	    // InternalAsamParser.g:204:3: lv_statement_1_0= ruleAsamStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAsamContractAccess().getStatementAsamStatementParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_statement_1_0=ruleAsamStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAsamContractRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"statement",
            	            		lv_statement_1_0, 
            	            		"edu.clemson.asam.Asam.AsamStatement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAsamContract"


    // $ANTLR start "entryRuleAsamStatement"
    // InternalAsamParser.g:228:1: entryRuleAsamStatement returns [EObject current=null] : iv_ruleAsamStatement= ruleAsamStatement EOF ;
    public final EObject entryRuleAsamStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAsamStatement = null;


        try {
            // InternalAsamParser.g:229:2: (iv_ruleAsamStatement= ruleAsamStatement EOF )
            // InternalAsamParser.g:230:2: iv_ruleAsamStatement= ruleAsamStatement EOF
            {
             newCompositeNode(grammarAccess.getAsamStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAsamStatement=ruleAsamStatement();

            state._fsp--;

             current =iv_ruleAsamStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAsamStatement"


    // $ANTLR start "ruleAsamStatement"
    // InternalAsamParser.g:237:1: ruleAsamStatement returns [EObject current=null] : (this_ErrsStatement_0= ruleErrsStatement | this_InternalFailureStatement_1= ruleInternalFailureStatement | this_SafetyConstraintHandlesStatement_2= ruleSafetyConstraintHandlesStatement | this_SafetyConstraintPreventsStatement_3= ruleSafetyConstraintPreventsStatement | this_ErrorPropagationRuleStatement_4= ruleErrorPropagationRuleStatement | this_TypeStatement_5= ruleTypeStatement ) ;
    public final EObject ruleAsamStatement() throws RecognitionException {
        EObject current = null;

        EObject this_ErrsStatement_0 = null;

        EObject this_InternalFailureStatement_1 = null;

        EObject this_SafetyConstraintHandlesStatement_2 = null;

        EObject this_SafetyConstraintPreventsStatement_3 = null;

        EObject this_ErrorPropagationRuleStatement_4 = null;

        EObject this_TypeStatement_5 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:240:28: ( (this_ErrsStatement_0= ruleErrsStatement | this_InternalFailureStatement_1= ruleInternalFailureStatement | this_SafetyConstraintHandlesStatement_2= ruleSafetyConstraintHandlesStatement | this_SafetyConstraintPreventsStatement_3= ruleSafetyConstraintPreventsStatement | this_ErrorPropagationRuleStatement_4= ruleErrorPropagationRuleStatement | this_TypeStatement_5= ruleTypeStatement ) )
            // InternalAsamParser.g:241:1: (this_ErrsStatement_0= ruleErrsStatement | this_InternalFailureStatement_1= ruleInternalFailureStatement | this_SafetyConstraintHandlesStatement_2= ruleSafetyConstraintHandlesStatement | this_SafetyConstraintPreventsStatement_3= ruleSafetyConstraintPreventsStatement | this_ErrorPropagationRuleStatement_4= ruleErrorPropagationRuleStatement | this_TypeStatement_5= ruleTypeStatement )
            {
            // InternalAsamParser.g:241:1: (this_ErrsStatement_0= ruleErrsStatement | this_InternalFailureStatement_1= ruleInternalFailureStatement | this_SafetyConstraintHandlesStatement_2= ruleSafetyConstraintHandlesStatement | this_SafetyConstraintPreventsStatement_3= ruleSafetyConstraintPreventsStatement | this_ErrorPropagationRuleStatement_4= ruleErrorPropagationRuleStatement | this_TypeStatement_5= ruleTypeStatement )
            int alt2=6;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // InternalAsamParser.g:242:5: this_ErrsStatement_0= ruleErrsStatement
                    {
                     
                            newCompositeNode(grammarAccess.getAsamStatementAccess().getErrsStatementParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ErrsStatement_0=ruleErrsStatement();

                    state._fsp--;


                            current = this_ErrsStatement_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:252:5: this_InternalFailureStatement_1= ruleInternalFailureStatement
                    {
                     
                            newCompositeNode(grammarAccess.getAsamStatementAccess().getInternalFailureStatementParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_InternalFailureStatement_1=ruleInternalFailureStatement();

                    state._fsp--;


                            current = this_InternalFailureStatement_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:262:5: this_SafetyConstraintHandlesStatement_2= ruleSafetyConstraintHandlesStatement
                    {
                     
                            newCompositeNode(grammarAccess.getAsamStatementAccess().getSafetyConstraintHandlesStatementParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SafetyConstraintHandlesStatement_2=ruleSafetyConstraintHandlesStatement();

                    state._fsp--;


                            current = this_SafetyConstraintHandlesStatement_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalAsamParser.g:272:5: this_SafetyConstraintPreventsStatement_3= ruleSafetyConstraintPreventsStatement
                    {
                     
                            newCompositeNode(grammarAccess.getAsamStatementAccess().getSafetyConstraintPreventsStatementParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SafetyConstraintPreventsStatement_3=ruleSafetyConstraintPreventsStatement();

                    state._fsp--;


                            current = this_SafetyConstraintPreventsStatement_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalAsamParser.g:282:5: this_ErrorPropagationRuleStatement_4= ruleErrorPropagationRuleStatement
                    {
                     
                            newCompositeNode(grammarAccess.getAsamStatementAccess().getErrorPropagationRuleStatementParserRuleCall_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ErrorPropagationRuleStatement_4=ruleErrorPropagationRuleStatement();

                    state._fsp--;


                            current = this_ErrorPropagationRuleStatement_4;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalAsamParser.g:292:5: this_TypeStatement_5= ruleTypeStatement
                    {
                     
                            newCompositeNode(grammarAccess.getAsamStatementAccess().getTypeStatementParserRuleCall_5()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TypeStatement_5=ruleTypeStatement();

                    state._fsp--;


                            current = this_TypeStatement_5;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAsamStatement"


    // $ANTLR start "entryRuleInternalFailureStatement"
    // InternalAsamParser.g:308:1: entryRuleInternalFailureStatement returns [EObject current=null] : iv_ruleInternalFailureStatement= ruleInternalFailureStatement EOF ;
    public final EObject entryRuleInternalFailureStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInternalFailureStatement = null;


        try {
            // InternalAsamParser.g:309:2: (iv_ruleInternalFailureStatement= ruleInternalFailureStatement EOF )
            // InternalAsamParser.g:310:2: iv_ruleInternalFailureStatement= ruleInternalFailureStatement EOF
            {
             newCompositeNode(grammarAccess.getInternalFailureStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInternalFailureStatement=ruleInternalFailureStatement();

            state._fsp--;

             current =iv_ruleInternalFailureStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInternalFailureStatement"


    // $ANTLR start "ruleInternalFailureStatement"
    // InternalAsamParser.g:317:1: ruleInternalFailureStatement returns [EObject current=null] : (otherlv_0= Internal otherlv_1= Failure ( (lv_internalFailureId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_internalFailureDescription_4_0= RULE_STRING ) ) otherlv_5= Causes_1 otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsList ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Semicolon ) ;
    public final EObject ruleInternalFailureStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_internalFailureId_2_0=null;
        Token otherlv_3=null;
        Token lv_internalFailureDescription_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        EObject lv_errors_7_0 = null;

        EObject lv_ports_11_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:320:28: ( (otherlv_0= Internal otherlv_1= Failure ( (lv_internalFailureId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_internalFailureDescription_4_0= RULE_STRING ) ) otherlv_5= Causes_1 otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsList ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Semicolon ) )
            // InternalAsamParser.g:321:1: (otherlv_0= Internal otherlv_1= Failure ( (lv_internalFailureId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_internalFailureDescription_4_0= RULE_STRING ) ) otherlv_5= Causes_1 otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsList ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Semicolon )
            {
            // InternalAsamParser.g:321:1: (otherlv_0= Internal otherlv_1= Failure ( (lv_internalFailureId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_internalFailureDescription_4_0= RULE_STRING ) ) otherlv_5= Causes_1 otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsList ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Semicolon )
            // InternalAsamParser.g:322:2: otherlv_0= Internal otherlv_1= Failure ( (lv_internalFailureId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_internalFailureDescription_4_0= RULE_STRING ) ) otherlv_5= Causes_1 otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsList ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Semicolon
            {
            otherlv_0=(Token)match(input,Internal,FollowSets000.FOLLOW_4); 

                	newLeafNode(otherlv_0, grammarAccess.getInternalFailureStatementAccess().getInternalKeyword_0());
                
            otherlv_1=(Token)match(input,Failure,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getInternalFailureStatementAccess().getFailureKeyword_1());
                
            // InternalAsamParser.g:331:1: ( (lv_internalFailureId_2_0= RULE_ID ) )
            // InternalAsamParser.g:332:1: (lv_internalFailureId_2_0= RULE_ID )
            {
            // InternalAsamParser.g:332:1: (lv_internalFailureId_2_0= RULE_ID )
            // InternalAsamParser.g:333:3: lv_internalFailureId_2_0= RULE_ID
            {
            lv_internalFailureId_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_6); 

            			newLeafNode(lv_internalFailureId_2_0, grammarAccess.getInternalFailureStatementAccess().getInternalFailureIdIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInternalFailureStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"internalFailureId",
                    		lv_internalFailureId_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,Colon,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_3, grammarAccess.getInternalFailureStatementAccess().getColonKeyword_3());
                
            // InternalAsamParser.g:354:1: ( (lv_internalFailureDescription_4_0= RULE_STRING ) )
            // InternalAsamParser.g:355:1: (lv_internalFailureDescription_4_0= RULE_STRING )
            {
            // InternalAsamParser.g:355:1: (lv_internalFailureDescription_4_0= RULE_STRING )
            // InternalAsamParser.g:356:3: lv_internalFailureDescription_4_0= RULE_STRING
            {
            lv_internalFailureDescription_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_8); 

            			newLeafNode(lv_internalFailureDescription_4_0, grammarAccess.getInternalFailureStatementAccess().getInternalFailureDescriptionSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInternalFailureStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"internalFailureDescription",
                    		lv_internalFailureDescription_4_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }

            otherlv_5=(Token)match(input,Causes_1,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_5, grammarAccess.getInternalFailureStatementAccess().getCausesKeyword_5());
                
            otherlv_6=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_6, grammarAccess.getInternalFailureStatementAccess().getLeftSquareBracketKeyword_6());
                
            // InternalAsamParser.g:382:1: ( (lv_errors_7_0= ruleErrorsList ) )
            // InternalAsamParser.g:383:1: (lv_errors_7_0= ruleErrorsList )
            {
            // InternalAsamParser.g:383:1: (lv_errors_7_0= ruleErrorsList )
            // InternalAsamParser.g:384:3: lv_errors_7_0= ruleErrorsList
            {
             
            	        newCompositeNode(grammarAccess.getInternalFailureStatementAccess().getErrorsErrorsListParserRuleCall_7_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_errors_7_0=ruleErrorsList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInternalFailureStatementRule());
            	        }
                   		set(
                   			current, 
                   			"errors",
                    		lv_errors_7_0, 
                    		"edu.clemson.asam.Asam.ErrorsList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_8=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_12); 

                	newLeafNode(otherlv_8, grammarAccess.getInternalFailureStatementAccess().getRightSquareBracketKeyword_8());
                
            otherlv_9=(Token)match(input,On,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_9, grammarAccess.getInternalFailureStatementAccess().getOnKeyword_9());
                
            otherlv_10=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_10, grammarAccess.getInternalFailureStatementAccess().getLeftSquareBracketKeyword_10());
                
            // InternalAsamParser.g:415:1: ( (lv_ports_11_0= rulePortsList ) )
            // InternalAsamParser.g:416:1: (lv_ports_11_0= rulePortsList )
            {
            // InternalAsamParser.g:416:1: (lv_ports_11_0= rulePortsList )
            // InternalAsamParser.g:417:3: lv_ports_11_0= rulePortsList
            {
             
            	        newCompositeNode(grammarAccess.getInternalFailureStatementAccess().getPortsPortsListParserRuleCall_11_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_ports_11_0=rulePortsList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInternalFailureStatementRule());
            	        }
                   		set(
                   			current, 
                   			"ports",
                    		lv_ports_11_0, 
                    		"edu.clemson.asam.Asam.PortsList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_12=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_13); 

                	newLeafNode(otherlv_12, grammarAccess.getInternalFailureStatementAccess().getRightSquareBracketKeyword_12());
                
            otherlv_13=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_13, grammarAccess.getInternalFailureStatementAccess().getSemicolonKeyword_13());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInternalFailureStatement"


    // $ANTLR start "entryRuleSafetyConstraintHandlesStatement"
    // InternalAsamParser.g:451:1: entryRuleSafetyConstraintHandlesStatement returns [EObject current=null] : iv_ruleSafetyConstraintHandlesStatement= ruleSafetyConstraintHandlesStatement EOF ;
    public final EObject entryRuleSafetyConstraintHandlesStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSafetyConstraintHandlesStatement = null;


        try {
            // InternalAsamParser.g:452:2: (iv_ruleSafetyConstraintHandlesStatement= ruleSafetyConstraintHandlesStatement EOF )
            // InternalAsamParser.g:453:2: iv_ruleSafetyConstraintHandlesStatement= ruleSafetyConstraintHandlesStatement EOF
            {
             newCompositeNode(grammarAccess.getSafetyConstraintHandlesStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSafetyConstraintHandlesStatement=ruleSafetyConstraintHandlesStatement();

            state._fsp--;

             current =iv_ruleSafetyConstraintHandlesStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSafetyConstraintHandlesStatement"


    // $ANTLR start "ruleSafetyConstraintHandlesStatement"
    // InternalAsamParser.g:460:1: ruleSafetyConstraintHandlesStatement returns [EObject current=null] : (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Handles otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon ) ;
    public final EObject ruleSafetyConstraintHandlesStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_safetyConstraintId_2_0=null;
        Token otherlv_3=null;
        Token lv_safetyConstraintDescription_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_errors_7_0 = null;

        EObject lv_ports_11_0 = null;

        EObject lv_matchingXagreeStatements_16_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:463:28: ( (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Handles otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon ) )
            // InternalAsamParser.g:464:1: (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Handles otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon )
            {
            // InternalAsamParser.g:464:1: (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Handles otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon )
            // InternalAsamParser.g:465:2: otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Handles otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon
            {
            otherlv_0=(Token)match(input,Safety,FollowSets000.FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getSafetyConstraintHandlesStatementAccess().getSafetyKeyword_0());
                
            otherlv_1=(Token)match(input,Constraint,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getSafetyConstraintHandlesStatementAccess().getConstraintKeyword_1());
                
            // InternalAsamParser.g:474:1: ( (lv_safetyConstraintId_2_0= RULE_ID ) )
            // InternalAsamParser.g:475:1: (lv_safetyConstraintId_2_0= RULE_ID )
            {
            // InternalAsamParser.g:475:1: (lv_safetyConstraintId_2_0= RULE_ID )
            // InternalAsamParser.g:476:3: lv_safetyConstraintId_2_0= RULE_ID
            {
            lv_safetyConstraintId_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_6); 

            			newLeafNode(lv_safetyConstraintId_2_0, grammarAccess.getSafetyConstraintHandlesStatementAccess().getSafetyConstraintIdIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSafetyConstraintHandlesStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"safetyConstraintId",
                    		lv_safetyConstraintId_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,Colon,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_3, grammarAccess.getSafetyConstraintHandlesStatementAccess().getColonKeyword_3());
                
            // InternalAsamParser.g:497:1: ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) )
            // InternalAsamParser.g:498:1: (lv_safetyConstraintDescription_4_0= RULE_STRING )
            {
            // InternalAsamParser.g:498:1: (lv_safetyConstraintDescription_4_0= RULE_STRING )
            // InternalAsamParser.g:499:3: lv_safetyConstraintDescription_4_0= RULE_STRING
            {
            lv_safetyConstraintDescription_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_15); 

            			newLeafNode(lv_safetyConstraintDescription_4_0, grammarAccess.getSafetyConstraintHandlesStatementAccess().getSafetyConstraintDescriptionSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSafetyConstraintHandlesStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"safetyConstraintDescription",
                    		lv_safetyConstraintDescription_4_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }

            otherlv_5=(Token)match(input,Handles,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_5, grammarAccess.getSafetyConstraintHandlesStatementAccess().getHandlesKeyword_5());
                
            otherlv_6=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_6, grammarAccess.getSafetyConstraintHandlesStatementAccess().getLeftSquareBracketKeyword_6());
                
            // InternalAsamParser.g:525:1: ( (lv_errors_7_0= ruleErrorsListNoProbability ) )
            // InternalAsamParser.g:526:1: (lv_errors_7_0= ruleErrorsListNoProbability )
            {
            // InternalAsamParser.g:526:1: (lv_errors_7_0= ruleErrorsListNoProbability )
            // InternalAsamParser.g:527:3: lv_errors_7_0= ruleErrorsListNoProbability
            {
             
            	        newCompositeNode(grammarAccess.getSafetyConstraintHandlesStatementAccess().getErrorsErrorsListNoProbabilityParserRuleCall_7_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_errors_7_0=ruleErrorsListNoProbability();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSafetyConstraintHandlesStatementRule());
            	        }
                   		set(
                   			current, 
                   			"errors",
                    		lv_errors_7_0, 
                    		"edu.clemson.asam.Asam.ErrorsListNoProbability");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_8=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_12); 

                	newLeafNode(otherlv_8, grammarAccess.getSafetyConstraintHandlesStatementAccess().getRightSquareBracketKeyword_8());
                
            otherlv_9=(Token)match(input,On,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_9, grammarAccess.getSafetyConstraintHandlesStatementAccess().getOnKeyword_9());
                
            otherlv_10=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_10, grammarAccess.getSafetyConstraintHandlesStatementAccess().getLeftSquareBracketKeyword_10());
                
            // InternalAsamParser.g:558:1: ( (lv_ports_11_0= rulePortsList ) )
            // InternalAsamParser.g:559:1: (lv_ports_11_0= rulePortsList )
            {
            // InternalAsamParser.g:559:1: (lv_ports_11_0= rulePortsList )
            // InternalAsamParser.g:560:3: lv_ports_11_0= rulePortsList
            {
             
            	        newCompositeNode(grammarAccess.getSafetyConstraintHandlesStatementAccess().getPortsPortsListParserRuleCall_11_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_ports_11_0=rulePortsList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSafetyConstraintHandlesStatementRule());
            	        }
                   		set(
                   			current, 
                   			"ports",
                    		lv_ports_11_0, 
                    		"edu.clemson.asam.Asam.PortsList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_12=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_16); 

                	newLeafNode(otherlv_12, grammarAccess.getSafetyConstraintHandlesStatementAccess().getRightSquareBracketKeyword_12());
                
            otherlv_13=(Token)match(input,Verified,FollowSets000.FOLLOW_17); 

                	newLeafNode(otherlv_13, grammarAccess.getSafetyConstraintHandlesStatementAccess().getVerifiedKeyword_13());
                
            otherlv_14=(Token)match(input,By,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_14, grammarAccess.getSafetyConstraintHandlesStatementAccess().getByKeyword_14());
                
            otherlv_15=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_15, grammarAccess.getSafetyConstraintHandlesStatementAccess().getLeftSquareBracketKeyword_15());
                
            // InternalAsamParser.g:596:1: ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) )
            // InternalAsamParser.g:597:1: (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds )
            {
            // InternalAsamParser.g:597:1: (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds )
            // InternalAsamParser.g:598:3: lv_matchingXagreeStatements_16_0= ruleGuaranteeIds
            {
             
            	        newCompositeNode(grammarAccess.getSafetyConstraintHandlesStatementAccess().getMatchingXagreeStatementsGuaranteeIdsParserRuleCall_16_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_matchingXagreeStatements_16_0=ruleGuaranteeIds();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSafetyConstraintHandlesStatementRule());
            	        }
                   		set(
                   			current, 
                   			"matchingXagreeStatements",
                    		lv_matchingXagreeStatements_16_0, 
                    		"edu.clemson.asam.Asam.GuaranteeIds");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_17=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_13); 

                	newLeafNode(otherlv_17, grammarAccess.getSafetyConstraintHandlesStatementAccess().getRightSquareBracketKeyword_17());
                
            otherlv_18=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_18, grammarAccess.getSafetyConstraintHandlesStatementAccess().getSemicolonKeyword_18());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSafetyConstraintHandlesStatement"


    // $ANTLR start "entryRuleSafetyConstraintPreventsStatement"
    // InternalAsamParser.g:632:1: entryRuleSafetyConstraintPreventsStatement returns [EObject current=null] : iv_ruleSafetyConstraintPreventsStatement= ruleSafetyConstraintPreventsStatement EOF ;
    public final EObject entryRuleSafetyConstraintPreventsStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSafetyConstraintPreventsStatement = null;


        try {
            // InternalAsamParser.g:633:2: (iv_ruleSafetyConstraintPreventsStatement= ruleSafetyConstraintPreventsStatement EOF )
            // InternalAsamParser.g:634:2: iv_ruleSafetyConstraintPreventsStatement= ruleSafetyConstraintPreventsStatement EOF
            {
             newCompositeNode(grammarAccess.getSafetyConstraintPreventsStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSafetyConstraintPreventsStatement=ruleSafetyConstraintPreventsStatement();

            state._fsp--;

             current =iv_ruleSafetyConstraintPreventsStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSafetyConstraintPreventsStatement"


    // $ANTLR start "ruleSafetyConstraintPreventsStatement"
    // InternalAsamParser.g:641:1: ruleSafetyConstraintPreventsStatement returns [EObject current=null] : (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Prevents otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon ) ;
    public final EObject ruleSafetyConstraintPreventsStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_safetyConstraintId_2_0=null;
        Token otherlv_3=null;
        Token lv_safetyConstraintDescription_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_errors_7_0 = null;

        EObject lv_ports_11_0 = null;

        EObject lv_matchingXagreeStatements_16_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:644:28: ( (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Prevents otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon ) )
            // InternalAsamParser.g:645:1: (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Prevents otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon )
            {
            // InternalAsamParser.g:645:1: (otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Prevents otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon )
            // InternalAsamParser.g:646:2: otherlv_0= Safety otherlv_1= Constraint ( (lv_safetyConstraintId_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) ) otherlv_5= Prevents otherlv_6= LeftSquareBracket ( (lv_errors_7_0= ruleErrorsListNoProbability ) ) otherlv_8= RightSquareBracket otherlv_9= On otherlv_10= LeftSquareBracket ( (lv_ports_11_0= rulePortsList ) ) otherlv_12= RightSquareBracket otherlv_13= Verified otherlv_14= By otherlv_15= LeftSquareBracket ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) ) otherlv_17= RightSquareBracket otherlv_18= Semicolon
            {
            otherlv_0=(Token)match(input,Safety,FollowSets000.FOLLOW_14); 

                	newLeafNode(otherlv_0, grammarAccess.getSafetyConstraintPreventsStatementAccess().getSafetyKeyword_0());
                
            otherlv_1=(Token)match(input,Constraint,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getSafetyConstraintPreventsStatementAccess().getConstraintKeyword_1());
                
            // InternalAsamParser.g:655:1: ( (lv_safetyConstraintId_2_0= RULE_ID ) )
            // InternalAsamParser.g:656:1: (lv_safetyConstraintId_2_0= RULE_ID )
            {
            // InternalAsamParser.g:656:1: (lv_safetyConstraintId_2_0= RULE_ID )
            // InternalAsamParser.g:657:3: lv_safetyConstraintId_2_0= RULE_ID
            {
            lv_safetyConstraintId_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_6); 

            			newLeafNode(lv_safetyConstraintId_2_0, grammarAccess.getSafetyConstraintPreventsStatementAccess().getSafetyConstraintIdIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSafetyConstraintPreventsStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"safetyConstraintId",
                    		lv_safetyConstraintId_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,Colon,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_3, grammarAccess.getSafetyConstraintPreventsStatementAccess().getColonKeyword_3());
                
            // InternalAsamParser.g:678:1: ( (lv_safetyConstraintDescription_4_0= RULE_STRING ) )
            // InternalAsamParser.g:679:1: (lv_safetyConstraintDescription_4_0= RULE_STRING )
            {
            // InternalAsamParser.g:679:1: (lv_safetyConstraintDescription_4_0= RULE_STRING )
            // InternalAsamParser.g:680:3: lv_safetyConstraintDescription_4_0= RULE_STRING
            {
            lv_safetyConstraintDescription_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_18); 

            			newLeafNode(lv_safetyConstraintDescription_4_0, grammarAccess.getSafetyConstraintPreventsStatementAccess().getSafetyConstraintDescriptionSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSafetyConstraintPreventsStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"safetyConstraintDescription",
                    		lv_safetyConstraintDescription_4_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }

            otherlv_5=(Token)match(input,Prevents,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_5, grammarAccess.getSafetyConstraintPreventsStatementAccess().getPreventsKeyword_5());
                
            otherlv_6=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_6, grammarAccess.getSafetyConstraintPreventsStatementAccess().getLeftSquareBracketKeyword_6());
                
            // InternalAsamParser.g:706:1: ( (lv_errors_7_0= ruleErrorsListNoProbability ) )
            // InternalAsamParser.g:707:1: (lv_errors_7_0= ruleErrorsListNoProbability )
            {
            // InternalAsamParser.g:707:1: (lv_errors_7_0= ruleErrorsListNoProbability )
            // InternalAsamParser.g:708:3: lv_errors_7_0= ruleErrorsListNoProbability
            {
             
            	        newCompositeNode(grammarAccess.getSafetyConstraintPreventsStatementAccess().getErrorsErrorsListNoProbabilityParserRuleCall_7_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_errors_7_0=ruleErrorsListNoProbability();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSafetyConstraintPreventsStatementRule());
            	        }
                   		set(
                   			current, 
                   			"errors",
                    		lv_errors_7_0, 
                    		"edu.clemson.asam.Asam.ErrorsListNoProbability");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_8=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_12); 

                	newLeafNode(otherlv_8, grammarAccess.getSafetyConstraintPreventsStatementAccess().getRightSquareBracketKeyword_8());
                
            otherlv_9=(Token)match(input,On,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_9, grammarAccess.getSafetyConstraintPreventsStatementAccess().getOnKeyword_9());
                
            otherlv_10=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_10, grammarAccess.getSafetyConstraintPreventsStatementAccess().getLeftSquareBracketKeyword_10());
                
            // InternalAsamParser.g:739:1: ( (lv_ports_11_0= rulePortsList ) )
            // InternalAsamParser.g:740:1: (lv_ports_11_0= rulePortsList )
            {
            // InternalAsamParser.g:740:1: (lv_ports_11_0= rulePortsList )
            // InternalAsamParser.g:741:3: lv_ports_11_0= rulePortsList
            {
             
            	        newCompositeNode(grammarAccess.getSafetyConstraintPreventsStatementAccess().getPortsPortsListParserRuleCall_11_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_ports_11_0=rulePortsList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSafetyConstraintPreventsStatementRule());
            	        }
                   		set(
                   			current, 
                   			"ports",
                    		lv_ports_11_0, 
                    		"edu.clemson.asam.Asam.PortsList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_12=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_16); 

                	newLeafNode(otherlv_12, grammarAccess.getSafetyConstraintPreventsStatementAccess().getRightSquareBracketKeyword_12());
                
            otherlv_13=(Token)match(input,Verified,FollowSets000.FOLLOW_17); 

                	newLeafNode(otherlv_13, grammarAccess.getSafetyConstraintPreventsStatementAccess().getVerifiedKeyword_13());
                
            otherlv_14=(Token)match(input,By,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_14, grammarAccess.getSafetyConstraintPreventsStatementAccess().getByKeyword_14());
                
            otherlv_15=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_15, grammarAccess.getSafetyConstraintPreventsStatementAccess().getLeftSquareBracketKeyword_15());
                
            // InternalAsamParser.g:777:1: ( (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds ) )
            // InternalAsamParser.g:778:1: (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds )
            {
            // InternalAsamParser.g:778:1: (lv_matchingXagreeStatements_16_0= ruleGuaranteeIds )
            // InternalAsamParser.g:779:3: lv_matchingXagreeStatements_16_0= ruleGuaranteeIds
            {
             
            	        newCompositeNode(grammarAccess.getSafetyConstraintPreventsStatementAccess().getMatchingXagreeStatementsGuaranteeIdsParserRuleCall_16_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_matchingXagreeStatements_16_0=ruleGuaranteeIds();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSafetyConstraintPreventsStatementRule());
            	        }
                   		set(
                   			current, 
                   			"matchingXagreeStatements",
                    		lv_matchingXagreeStatements_16_0, 
                    		"edu.clemson.asam.Asam.GuaranteeIds");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_17=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_13); 

                	newLeafNode(otherlv_17, grammarAccess.getSafetyConstraintPreventsStatementAccess().getRightSquareBracketKeyword_17());
                
            otherlv_18=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_18, grammarAccess.getSafetyConstraintPreventsStatementAccess().getSemicolonKeyword_18());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSafetyConstraintPreventsStatement"


    // $ANTLR start "entryRuleErrorPropagationRuleStatement"
    // InternalAsamParser.g:813:1: entryRuleErrorPropagationRuleStatement returns [EObject current=null] : iv_ruleErrorPropagationRuleStatement= ruleErrorPropagationRuleStatement EOF ;
    public final EObject entryRuleErrorPropagationRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleErrorPropagationRuleStatement = null;


        try {
            // InternalAsamParser.g:814:2: (iv_ruleErrorPropagationRuleStatement= ruleErrorPropagationRuleStatement EOF )
            // InternalAsamParser.g:815:2: iv_ruleErrorPropagationRuleStatement= ruleErrorPropagationRuleStatement EOF
            {
             newCompositeNode(grammarAccess.getErrorPropagationRuleStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleErrorPropagationRuleStatement=ruleErrorPropagationRuleStatement();

            state._fsp--;

             current =iv_ruleErrorPropagationRuleStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleErrorPropagationRuleStatement"


    // $ANTLR start "ruleErrorPropagationRuleStatement"
    // InternalAsamParser.g:822:1: ruleErrorPropagationRuleStatement returns [EObject current=null] : (otherlv_0= LeftSquareBracket ( (lv_inErrorsList_1_0= ruleErrorsListNoProbability ) ) otherlv_2= RightSquareBracket otherlv_3= On otherlv_4= LeftSquareBracket ( (lv_inPortsLists_5_0= rulePortsList ) ) otherlv_6= RightSquareBracket otherlv_7= Causes_1 otherlv_8= LeftSquareBracket ( (lv_outErrorsList_9_0= ruleErrorsList ) ) otherlv_10= RightSquareBracket otherlv_11= On otherlv_12= LeftSquareBracket ( (lv_outPortsLists_13_0= rulePortsList ) ) otherlv_14= RightSquareBracket otherlv_15= Semicolon ) ;
    public final EObject ruleErrorPropagationRuleStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_inErrorsList_1_0 = null;

        EObject lv_inPortsLists_5_0 = null;

        EObject lv_outErrorsList_9_0 = null;

        EObject lv_outPortsLists_13_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:825:28: ( (otherlv_0= LeftSquareBracket ( (lv_inErrorsList_1_0= ruleErrorsListNoProbability ) ) otherlv_2= RightSquareBracket otherlv_3= On otherlv_4= LeftSquareBracket ( (lv_inPortsLists_5_0= rulePortsList ) ) otherlv_6= RightSquareBracket otherlv_7= Causes_1 otherlv_8= LeftSquareBracket ( (lv_outErrorsList_9_0= ruleErrorsList ) ) otherlv_10= RightSquareBracket otherlv_11= On otherlv_12= LeftSquareBracket ( (lv_outPortsLists_13_0= rulePortsList ) ) otherlv_14= RightSquareBracket otherlv_15= Semicolon ) )
            // InternalAsamParser.g:826:1: (otherlv_0= LeftSquareBracket ( (lv_inErrorsList_1_0= ruleErrorsListNoProbability ) ) otherlv_2= RightSquareBracket otherlv_3= On otherlv_4= LeftSquareBracket ( (lv_inPortsLists_5_0= rulePortsList ) ) otherlv_6= RightSquareBracket otherlv_7= Causes_1 otherlv_8= LeftSquareBracket ( (lv_outErrorsList_9_0= ruleErrorsList ) ) otherlv_10= RightSquareBracket otherlv_11= On otherlv_12= LeftSquareBracket ( (lv_outPortsLists_13_0= rulePortsList ) ) otherlv_14= RightSquareBracket otherlv_15= Semicolon )
            {
            // InternalAsamParser.g:826:1: (otherlv_0= LeftSquareBracket ( (lv_inErrorsList_1_0= ruleErrorsListNoProbability ) ) otherlv_2= RightSquareBracket otherlv_3= On otherlv_4= LeftSquareBracket ( (lv_inPortsLists_5_0= rulePortsList ) ) otherlv_6= RightSquareBracket otherlv_7= Causes_1 otherlv_8= LeftSquareBracket ( (lv_outErrorsList_9_0= ruleErrorsList ) ) otherlv_10= RightSquareBracket otherlv_11= On otherlv_12= LeftSquareBracket ( (lv_outPortsLists_13_0= rulePortsList ) ) otherlv_14= RightSquareBracket otherlv_15= Semicolon )
            // InternalAsamParser.g:827:2: otherlv_0= LeftSquareBracket ( (lv_inErrorsList_1_0= ruleErrorsListNoProbability ) ) otherlv_2= RightSquareBracket otherlv_3= On otherlv_4= LeftSquareBracket ( (lv_inPortsLists_5_0= rulePortsList ) ) otherlv_6= RightSquareBracket otherlv_7= Causes_1 otherlv_8= LeftSquareBracket ( (lv_outErrorsList_9_0= ruleErrorsList ) ) otherlv_10= RightSquareBracket otherlv_11= On otherlv_12= LeftSquareBracket ( (lv_outPortsLists_13_0= rulePortsList ) ) otherlv_14= RightSquareBracket otherlv_15= Semicolon
            {
            otherlv_0=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getErrorPropagationRuleStatementAccess().getLeftSquareBracketKeyword_0());
                
            // InternalAsamParser.g:831:1: ( (lv_inErrorsList_1_0= ruleErrorsListNoProbability ) )
            // InternalAsamParser.g:832:1: (lv_inErrorsList_1_0= ruleErrorsListNoProbability )
            {
            // InternalAsamParser.g:832:1: (lv_inErrorsList_1_0= ruleErrorsListNoProbability )
            // InternalAsamParser.g:833:3: lv_inErrorsList_1_0= ruleErrorsListNoProbability
            {
             
            	        newCompositeNode(grammarAccess.getErrorPropagationRuleStatementAccess().getInErrorsListErrorsListNoProbabilityParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_inErrorsList_1_0=ruleErrorsListNoProbability();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorPropagationRuleStatementRule());
            	        }
                   		set(
                   			current, 
                   			"inErrorsList",
                    		lv_inErrorsList_1_0, 
                    		"edu.clemson.asam.Asam.ErrorsListNoProbability");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_12); 

                	newLeafNode(otherlv_2, grammarAccess.getErrorPropagationRuleStatementAccess().getRightSquareBracketKeyword_2());
                
            otherlv_3=(Token)match(input,On,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_3, grammarAccess.getErrorPropagationRuleStatementAccess().getOnKeyword_3());
                
            otherlv_4=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_4, grammarAccess.getErrorPropagationRuleStatementAccess().getLeftSquareBracketKeyword_4());
                
            // InternalAsamParser.g:864:1: ( (lv_inPortsLists_5_0= rulePortsList ) )
            // InternalAsamParser.g:865:1: (lv_inPortsLists_5_0= rulePortsList )
            {
            // InternalAsamParser.g:865:1: (lv_inPortsLists_5_0= rulePortsList )
            // InternalAsamParser.g:866:3: lv_inPortsLists_5_0= rulePortsList
            {
             
            	        newCompositeNode(grammarAccess.getErrorPropagationRuleStatementAccess().getInPortsListsPortsListParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_inPortsLists_5_0=rulePortsList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorPropagationRuleStatementRule());
            	        }
                   		set(
                   			current, 
                   			"inPortsLists",
                    		lv_inPortsLists_5_0, 
                    		"edu.clemson.asam.Asam.PortsList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_8); 

                	newLeafNode(otherlv_6, grammarAccess.getErrorPropagationRuleStatementAccess().getRightSquareBracketKeyword_6());
                
            otherlv_7=(Token)match(input,Causes_1,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_7, grammarAccess.getErrorPropagationRuleStatementAccess().getCausesKeyword_7());
                
            otherlv_8=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_8, grammarAccess.getErrorPropagationRuleStatementAccess().getLeftSquareBracketKeyword_8());
                
            // InternalAsamParser.g:897:1: ( (lv_outErrorsList_9_0= ruleErrorsList ) )
            // InternalAsamParser.g:898:1: (lv_outErrorsList_9_0= ruleErrorsList )
            {
            // InternalAsamParser.g:898:1: (lv_outErrorsList_9_0= ruleErrorsList )
            // InternalAsamParser.g:899:3: lv_outErrorsList_9_0= ruleErrorsList
            {
             
            	        newCompositeNode(grammarAccess.getErrorPropagationRuleStatementAccess().getOutErrorsListErrorsListParserRuleCall_9_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_outErrorsList_9_0=ruleErrorsList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorPropagationRuleStatementRule());
            	        }
                   		set(
                   			current, 
                   			"outErrorsList",
                    		lv_outErrorsList_9_0, 
                    		"edu.clemson.asam.Asam.ErrorsList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_10=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_12); 

                	newLeafNode(otherlv_10, grammarAccess.getErrorPropagationRuleStatementAccess().getRightSquareBracketKeyword_10());
                
            otherlv_11=(Token)match(input,On,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_11, grammarAccess.getErrorPropagationRuleStatementAccess().getOnKeyword_11());
                
            otherlv_12=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_12, grammarAccess.getErrorPropagationRuleStatementAccess().getLeftSquareBracketKeyword_12());
                
            // InternalAsamParser.g:930:1: ( (lv_outPortsLists_13_0= rulePortsList ) )
            // InternalAsamParser.g:931:1: (lv_outPortsLists_13_0= rulePortsList )
            {
            // InternalAsamParser.g:931:1: (lv_outPortsLists_13_0= rulePortsList )
            // InternalAsamParser.g:932:3: lv_outPortsLists_13_0= rulePortsList
            {
             
            	        newCompositeNode(grammarAccess.getErrorPropagationRuleStatementAccess().getOutPortsListsPortsListParserRuleCall_13_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_outPortsLists_13_0=rulePortsList();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorPropagationRuleStatementRule());
            	        }
                   		set(
                   			current, 
                   			"outPortsLists",
                    		lv_outPortsLists_13_0, 
                    		"edu.clemson.asam.Asam.PortsList");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_14=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_13); 

                	newLeafNode(otherlv_14, grammarAccess.getErrorPropagationRuleStatementAccess().getRightSquareBracketKeyword_14());
                
            otherlv_15=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_15, grammarAccess.getErrorPropagationRuleStatementAccess().getSemicolonKeyword_15());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleErrorPropagationRuleStatement"


    // $ANTLR start "entryRuleTypeStatement"
    // InternalAsamParser.g:966:1: entryRuleTypeStatement returns [EObject current=null] : iv_ruleTypeStatement= ruleTypeStatement EOF ;
    public final EObject entryRuleTypeStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeStatement = null;


        try {
            // InternalAsamParser.g:967:2: (iv_ruleTypeStatement= ruleTypeStatement EOF )
            // InternalAsamParser.g:968:2: iv_ruleTypeStatement= ruleTypeStatement EOF
            {
             newCompositeNode(grammarAccess.getTypeStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTypeStatement=ruleTypeStatement();

            state._fsp--;

             current =iv_ruleTypeStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeStatement"


    // $ANTLR start "ruleTypeStatement"
    // InternalAsamParser.g:975:1: ruleTypeStatement returns [EObject current=null] : (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) ) ) otherlv_3= Semicolon ) ;
    public final EObject ruleTypeStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_type_2_1=null;
        Token lv_type_2_2=null;
        Token lv_type_2_3=null;
        Token lv_type_2_4=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:978:28: ( (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) ) ) otherlv_3= Semicolon ) )
            // InternalAsamParser.g:979:1: (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) ) ) otherlv_3= Semicolon )
            {
            // InternalAsamParser.g:979:1: (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) ) ) otherlv_3= Semicolon )
            // InternalAsamParser.g:980:2: otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) ) ) otherlv_3= Semicolon
            {
            otherlv_0=(Token)match(input,Type_1,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getTypeStatementAccess().getTypeKeyword_0());
                
            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_20); 

                	newLeafNode(otherlv_1, grammarAccess.getTypeStatementAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalAsamParser.g:989:1: ( ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) ) )
            // InternalAsamParser.g:990:1: ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) )
            {
            // InternalAsamParser.g:990:1: ( (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator ) )
            // InternalAsamParser.g:991:1: (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator )
            {
            // InternalAsamParser.g:991:1: (lv_type_2_1= Sensor | lv_type_2_2= Controller | lv_type_2_3= Controlled_process | lv_type_2_4= Actuator )
            int alt3=4;
            switch ( input.LA(1) ) {
            case Sensor:
                {
                alt3=1;
                }
                break;
            case Controller:
                {
                alt3=2;
                }
                break;
            case Controlled_process:
                {
                alt3=3;
                }
                break;
            case Actuator:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalAsamParser.g:992:3: lv_type_2_1= Sensor
                    {
                    lv_type_2_1=(Token)match(input,Sensor,FollowSets000.FOLLOW_13); 

                            newLeafNode(lv_type_2_1, grammarAccess.getTypeStatementAccess().getTypeSensorKeyword_2_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTypeStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_2_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:1005:8: lv_type_2_2= Controller
                    {
                    lv_type_2_2=(Token)match(input,Controller,FollowSets000.FOLLOW_13); 

                            newLeafNode(lv_type_2_2, grammarAccess.getTypeStatementAccess().getTypeControllerKeyword_2_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTypeStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_2_2, null);
                    	    

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:1018:8: lv_type_2_3= Controlled_process
                    {
                    lv_type_2_3=(Token)match(input,Controlled_process,FollowSets000.FOLLOW_13); 

                            newLeafNode(lv_type_2_3, grammarAccess.getTypeStatementAccess().getTypeControlled_processKeyword_2_0_2());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTypeStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_2_3, null);
                    	    

                    }
                    break;
                case 4 :
                    // InternalAsamParser.g:1031:8: lv_type_2_4= Actuator
                    {
                    lv_type_2_4=(Token)match(input,Actuator,FollowSets000.FOLLOW_13); 

                            newLeafNode(lv_type_2_4, grammarAccess.getTypeStatementAccess().getTypeActuatorKeyword_2_0_3());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTypeStatementRule());
                    	        }
                           		setWithLastConsumed(current, "type", lv_type_2_4, null);
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_3=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getTypeStatementAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeStatement"


    // $ANTLR start "entryRuleErrorsList"
    // InternalAsamParser.g:1060:1: entryRuleErrorsList returns [EObject current=null] : iv_ruleErrorsList= ruleErrorsList EOF ;
    public final EObject entryRuleErrorsList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleErrorsList = null;


        try {
            // InternalAsamParser.g:1061:2: (iv_ruleErrorsList= ruleErrorsList EOF )
            // InternalAsamParser.g:1062:2: iv_ruleErrorsList= ruleErrorsList EOF
            {
             newCompositeNode(grammarAccess.getErrorsListRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleErrorsList=ruleErrorsList();

            state._fsp--;

             current =iv_ruleErrorsList; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleErrorsList"


    // $ANTLR start "ruleErrorsList"
    // InternalAsamParser.g:1069:1: ruleErrorsList returns [EObject current=null] : ( ( (lv_firstError_0_0= ruleError ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleError ) ) )* ) ;
    public final EObject ruleErrorsList() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_firstError_0_0 = null;

        EObject lv_restErrors_2_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:1072:28: ( ( ( (lv_firstError_0_0= ruleError ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleError ) ) )* ) )
            // InternalAsamParser.g:1073:1: ( ( (lv_firstError_0_0= ruleError ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleError ) ) )* )
            {
            // InternalAsamParser.g:1073:1: ( ( (lv_firstError_0_0= ruleError ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleError ) ) )* )
            // InternalAsamParser.g:1073:2: ( (lv_firstError_0_0= ruleError ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleError ) ) )*
            {
            // InternalAsamParser.g:1073:2: ( (lv_firstError_0_0= ruleError ) )
            // InternalAsamParser.g:1074:1: (lv_firstError_0_0= ruleError )
            {
            // InternalAsamParser.g:1074:1: (lv_firstError_0_0= ruleError )
            // InternalAsamParser.g:1075:3: lv_firstError_0_0= ruleError
            {
             
            	        newCompositeNode(grammarAccess.getErrorsListAccess().getFirstErrorErrorParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_21);
            lv_firstError_0_0=ruleError();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorsListRule());
            	        }
                   		set(
                   			current, 
                   			"firstError",
                    		lv_firstError_0_0, 
                    		"edu.clemson.asam.Asam.Error");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:1091:2: (otherlv_1= Comma ( (lv_restErrors_2_0= ruleError ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==Comma) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalAsamParser.g:1092:2: otherlv_1= Comma ( (lv_restErrors_2_0= ruleError ) )
            	    {
            	    otherlv_1=(Token)match(input,Comma,FollowSets000.FOLLOW_10); 

            	        	newLeafNode(otherlv_1, grammarAccess.getErrorsListAccess().getCommaKeyword_1_0());
            	        
            	    // InternalAsamParser.g:1096:1: ( (lv_restErrors_2_0= ruleError ) )
            	    // InternalAsamParser.g:1097:1: (lv_restErrors_2_0= ruleError )
            	    {
            	    // InternalAsamParser.g:1097:1: (lv_restErrors_2_0= ruleError )
            	    // InternalAsamParser.g:1098:3: lv_restErrors_2_0= ruleError
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getErrorsListAccess().getRestErrorsErrorParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_21);
            	    lv_restErrors_2_0=ruleError();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getErrorsListRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"restErrors",
            	            		lv_restErrors_2_0, 
            	            		"edu.clemson.asam.Asam.Error");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleErrorsList"


    // $ANTLR start "entryRuleErrorsListNoProbability"
    // InternalAsamParser.g:1122:1: entryRuleErrorsListNoProbability returns [EObject current=null] : iv_ruleErrorsListNoProbability= ruleErrorsListNoProbability EOF ;
    public final EObject entryRuleErrorsListNoProbability() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleErrorsListNoProbability = null;


        try {
            // InternalAsamParser.g:1123:2: (iv_ruleErrorsListNoProbability= ruleErrorsListNoProbability EOF )
            // InternalAsamParser.g:1124:2: iv_ruleErrorsListNoProbability= ruleErrorsListNoProbability EOF
            {
             newCompositeNode(grammarAccess.getErrorsListNoProbabilityRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleErrorsListNoProbability=ruleErrorsListNoProbability();

            state._fsp--;

             current =iv_ruleErrorsListNoProbability; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleErrorsListNoProbability"


    // $ANTLR start "ruleErrorsListNoProbability"
    // InternalAsamParser.g:1131:1: ruleErrorsListNoProbability returns [EObject current=null] : ( ( (lv_firstError_0_0= ruleErrorNoProbability ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleErrorNoProbability ) ) )* ) ;
    public final EObject ruleErrorsListNoProbability() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_firstError_0_0 = null;

        EObject lv_restErrors_2_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:1134:28: ( ( ( (lv_firstError_0_0= ruleErrorNoProbability ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleErrorNoProbability ) ) )* ) )
            // InternalAsamParser.g:1135:1: ( ( (lv_firstError_0_0= ruleErrorNoProbability ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleErrorNoProbability ) ) )* )
            {
            // InternalAsamParser.g:1135:1: ( ( (lv_firstError_0_0= ruleErrorNoProbability ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleErrorNoProbability ) ) )* )
            // InternalAsamParser.g:1135:2: ( (lv_firstError_0_0= ruleErrorNoProbability ) ) (otherlv_1= Comma ( (lv_restErrors_2_0= ruleErrorNoProbability ) ) )*
            {
            // InternalAsamParser.g:1135:2: ( (lv_firstError_0_0= ruleErrorNoProbability ) )
            // InternalAsamParser.g:1136:1: (lv_firstError_0_0= ruleErrorNoProbability )
            {
            // InternalAsamParser.g:1136:1: (lv_firstError_0_0= ruleErrorNoProbability )
            // InternalAsamParser.g:1137:3: lv_firstError_0_0= ruleErrorNoProbability
            {
             
            	        newCompositeNode(grammarAccess.getErrorsListNoProbabilityAccess().getFirstErrorErrorNoProbabilityParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_21);
            lv_firstError_0_0=ruleErrorNoProbability();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorsListNoProbabilityRule());
            	        }
                   		set(
                   			current, 
                   			"firstError",
                    		lv_firstError_0_0, 
                    		"edu.clemson.asam.Asam.ErrorNoProbability");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:1153:2: (otherlv_1= Comma ( (lv_restErrors_2_0= ruleErrorNoProbability ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==Comma) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalAsamParser.g:1154:2: otherlv_1= Comma ( (lv_restErrors_2_0= ruleErrorNoProbability ) )
            	    {
            	    otherlv_1=(Token)match(input,Comma,FollowSets000.FOLLOW_10); 

            	        	newLeafNode(otherlv_1, grammarAccess.getErrorsListNoProbabilityAccess().getCommaKeyword_1_0());
            	        
            	    // InternalAsamParser.g:1158:1: ( (lv_restErrors_2_0= ruleErrorNoProbability ) )
            	    // InternalAsamParser.g:1159:1: (lv_restErrors_2_0= ruleErrorNoProbability )
            	    {
            	    // InternalAsamParser.g:1159:1: (lv_restErrors_2_0= ruleErrorNoProbability )
            	    // InternalAsamParser.g:1160:3: lv_restErrors_2_0= ruleErrorNoProbability
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getErrorsListNoProbabilityAccess().getRestErrorsErrorNoProbabilityParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_21);
            	    lv_restErrors_2_0=ruleErrorNoProbability();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getErrorsListNoProbabilityRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"restErrors",
            	            		lv_restErrors_2_0, 
            	            		"edu.clemson.asam.Asam.ErrorNoProbability");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleErrorsListNoProbability"


    // $ANTLR start "entryRulePortsList"
    // InternalAsamParser.g:1184:1: entryRulePortsList returns [EObject current=null] : iv_rulePortsList= rulePortsList EOF ;
    public final EObject entryRulePortsList() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePortsList = null;


        try {
            // InternalAsamParser.g:1185:2: (iv_rulePortsList= rulePortsList EOF )
            // InternalAsamParser.g:1186:2: iv_rulePortsList= rulePortsList EOF
            {
             newCompositeNode(grammarAccess.getPortsListRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePortsList=rulePortsList();

            state._fsp--;

             current =iv_rulePortsList; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePortsList"


    // $ANTLR start "rulePortsList"
    // InternalAsamParser.g:1193:1: rulePortsList returns [EObject current=null] : ( ( (lv_firstPort_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_restPorts_2_0= RULE_ID ) ) )* ) ;
    public final EObject rulePortsList() throws RecognitionException {
        EObject current = null;

        Token lv_firstPort_0_0=null;
        Token otherlv_1=null;
        Token lv_restPorts_2_0=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:1196:28: ( ( ( (lv_firstPort_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_restPorts_2_0= RULE_ID ) ) )* ) )
            // InternalAsamParser.g:1197:1: ( ( (lv_firstPort_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_restPorts_2_0= RULE_ID ) ) )* )
            {
            // InternalAsamParser.g:1197:1: ( ( (lv_firstPort_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_restPorts_2_0= RULE_ID ) ) )* )
            // InternalAsamParser.g:1197:2: ( (lv_firstPort_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_restPorts_2_0= RULE_ID ) ) )*
            {
            // InternalAsamParser.g:1197:2: ( (lv_firstPort_0_0= RULE_ID ) )
            // InternalAsamParser.g:1198:1: (lv_firstPort_0_0= RULE_ID )
            {
            // InternalAsamParser.g:1198:1: (lv_firstPort_0_0= RULE_ID )
            // InternalAsamParser.g:1199:3: lv_firstPort_0_0= RULE_ID
            {
            lv_firstPort_0_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); 

            			newLeafNode(lv_firstPort_0_0, grammarAccess.getPortsListAccess().getFirstPortIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPortsListRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"firstPort",
                    		lv_firstPort_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            // InternalAsamParser.g:1215:2: (otherlv_1= Comma ( (lv_restPorts_2_0= RULE_ID ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==Comma) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalAsamParser.g:1216:2: otherlv_1= Comma ( (lv_restPorts_2_0= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,Comma,FollowSets000.FOLLOW_5); 

            	        	newLeafNode(otherlv_1, grammarAccess.getPortsListAccess().getCommaKeyword_1_0());
            	        
            	    // InternalAsamParser.g:1220:1: ( (lv_restPorts_2_0= RULE_ID ) )
            	    // InternalAsamParser.g:1221:1: (lv_restPorts_2_0= RULE_ID )
            	    {
            	    // InternalAsamParser.g:1221:1: (lv_restPorts_2_0= RULE_ID )
            	    // InternalAsamParser.g:1222:3: lv_restPorts_2_0= RULE_ID
            	    {
            	    lv_restPorts_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); 

            	    			newLeafNode(lv_restPorts_2_0, grammarAccess.getPortsListAccess().getRestPortsIDTerminalRuleCall_1_1_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getPortsListRule());
            	    	        }
            	           		addWithLastConsumed(
            	           			current, 
            	           			"restPorts",
            	            		lv_restPorts_2_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePortsList"


    // $ANTLR start "entryRuleErrsStatement"
    // InternalAsamParser.g:1246:1: entryRuleErrsStatement returns [EObject current=null] : iv_ruleErrsStatement= ruleErrsStatement EOF ;
    public final EObject entryRuleErrsStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleErrsStatement = null;


        try {
            // InternalAsamParser.g:1247:2: (iv_ruleErrsStatement= ruleErrsStatement EOF )
            // InternalAsamParser.g:1248:2: iv_ruleErrsStatement= ruleErrsStatement EOF
            {
             newCompositeNode(grammarAccess.getErrsStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleErrsStatement=ruleErrsStatement();

            state._fsp--;

             current =iv_ruleErrsStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleErrsStatement"


    // $ANTLR start "ruleErrsStatement"
    // InternalAsamParser.g:1255:1: ruleErrsStatement returns [EObject current=null] : ( () otherlv_1= Errs otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftSquareBracket ( (lv_firstError_4_0= ruleErrorStatement ) ) (otherlv_5= Comma ( (lv_restErrors_6_0= ruleErrorStatement ) ) )* otherlv_7= RightSquareBracket ) ;
    public final EObject ruleErrsStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_firstError_4_0 = null;

        EObject lv_restErrors_6_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:1258:28: ( ( () otherlv_1= Errs otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftSquareBracket ( (lv_firstError_4_0= ruleErrorStatement ) ) (otherlv_5= Comma ( (lv_restErrors_6_0= ruleErrorStatement ) ) )* otherlv_7= RightSquareBracket ) )
            // InternalAsamParser.g:1259:1: ( () otherlv_1= Errs otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftSquareBracket ( (lv_firstError_4_0= ruleErrorStatement ) ) (otherlv_5= Comma ( (lv_restErrors_6_0= ruleErrorStatement ) ) )* otherlv_7= RightSquareBracket )
            {
            // InternalAsamParser.g:1259:1: ( () otherlv_1= Errs otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftSquareBracket ( (lv_firstError_4_0= ruleErrorStatement ) ) (otherlv_5= Comma ( (lv_restErrors_6_0= ruleErrorStatement ) ) )* otherlv_7= RightSquareBracket )
            // InternalAsamParser.g:1259:2: () otherlv_1= Errs otherlv_2= EqualsSignGreaterThanSign otherlv_3= LeftSquareBracket ( (lv_firstError_4_0= ruleErrorStatement ) ) (otherlv_5= Comma ( (lv_restErrors_6_0= ruleErrorStatement ) ) )* otherlv_7= RightSquareBracket
            {
            // InternalAsamParser.g:1259:2: ()
            // InternalAsamParser.g:1260:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getErrsStatementAccess().getErrsStatementAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,Errs,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_1, grammarAccess.getErrsStatementAccess().getErrsKeyword_1());
                
            otherlv_2=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_2, grammarAccess.getErrsStatementAccess().getEqualsSignGreaterThanSignKeyword_2());
                
            otherlv_3=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_22); 

                	newLeafNode(otherlv_3, grammarAccess.getErrsStatementAccess().getLeftSquareBracketKeyword_3());
                
            // InternalAsamParser.g:1280:1: ( (lv_firstError_4_0= ruleErrorStatement ) )
            // InternalAsamParser.g:1281:1: (lv_firstError_4_0= ruleErrorStatement )
            {
            // InternalAsamParser.g:1281:1: (lv_firstError_4_0= ruleErrorStatement )
            // InternalAsamParser.g:1282:3: lv_firstError_4_0= ruleErrorStatement
            {
             
            	        newCompositeNode(grammarAccess.getErrsStatementAccess().getFirstErrorErrorStatementParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_23);
            lv_firstError_4_0=ruleErrorStatement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrsStatementRule());
            	        }
                   		set(
                   			current, 
                   			"firstError",
                    		lv_firstError_4_0, 
                    		"edu.clemson.asam.Asam.ErrorStatement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:1298:2: (otherlv_5= Comma ( (lv_restErrors_6_0= ruleErrorStatement ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==Comma) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalAsamParser.g:1299:2: otherlv_5= Comma ( (lv_restErrors_6_0= ruleErrorStatement ) )
            	    {
            	    otherlv_5=(Token)match(input,Comma,FollowSets000.FOLLOW_22); 

            	        	newLeafNode(otherlv_5, grammarAccess.getErrsStatementAccess().getCommaKeyword_5_0());
            	        
            	    // InternalAsamParser.g:1303:1: ( (lv_restErrors_6_0= ruleErrorStatement ) )
            	    // InternalAsamParser.g:1304:1: (lv_restErrors_6_0= ruleErrorStatement )
            	    {
            	    // InternalAsamParser.g:1304:1: (lv_restErrors_6_0= ruleErrorStatement )
            	    // InternalAsamParser.g:1305:3: lv_restErrors_6_0= ruleErrorStatement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getErrsStatementAccess().getRestErrorsErrorStatementParserRuleCall_5_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_23);
            	    lv_restErrors_6_0=ruleErrorStatement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getErrsStatementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"restErrors",
            	            		lv_restErrors_6_0, 
            	            		"edu.clemson.asam.Asam.ErrorStatement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_7=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getErrsStatementAccess().getRightSquareBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleErrsStatement"


    // $ANTLR start "entryRuleErrorStatement"
    // InternalAsamParser.g:1334:1: entryRuleErrorStatement returns [EObject current=null] : iv_ruleErrorStatement= ruleErrorStatement EOF ;
    public final EObject entryRuleErrorStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleErrorStatement = null;


        try {
            // InternalAsamParser.g:1335:2: (iv_ruleErrorStatement= ruleErrorStatement EOF )
            // InternalAsamParser.g:1336:2: iv_ruleErrorStatement= ruleErrorStatement EOF
            {
             newCompositeNode(grammarAccess.getErrorStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleErrorStatement=ruleErrorStatement();

            state._fsp--;

             current =iv_ruleErrorStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleErrorStatement"


    // $ANTLR start "ruleErrorStatement"
    // InternalAsamParser.g:1343:1: ruleErrorStatement returns [EObject current=null] : (otherlv_0= LeftCurlyBracket ( (lv_type_1_0= ruleErrorTypeStatement ) ) otherlv_2= Comma ( (lv_uca_3_0= ruleUnsafeControlActionStatement ) ) otherlv_4= Comma ( (lv_cause_5_0= ruleCausesStatement ) ) otherlv_6= Comma ( (lv_sc_7_0= ruleSafetyConstraintStatement ) ) otherlv_8= RightCurlyBracket ) ;
    public final EObject ruleErrorStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_type_1_0 = null;

        EObject lv_uca_3_0 = null;

        EObject lv_cause_5_0 = null;

        EObject lv_sc_7_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:1346:28: ( (otherlv_0= LeftCurlyBracket ( (lv_type_1_0= ruleErrorTypeStatement ) ) otherlv_2= Comma ( (lv_uca_3_0= ruleUnsafeControlActionStatement ) ) otherlv_4= Comma ( (lv_cause_5_0= ruleCausesStatement ) ) otherlv_6= Comma ( (lv_sc_7_0= ruleSafetyConstraintStatement ) ) otherlv_8= RightCurlyBracket ) )
            // InternalAsamParser.g:1347:1: (otherlv_0= LeftCurlyBracket ( (lv_type_1_0= ruleErrorTypeStatement ) ) otherlv_2= Comma ( (lv_uca_3_0= ruleUnsafeControlActionStatement ) ) otherlv_4= Comma ( (lv_cause_5_0= ruleCausesStatement ) ) otherlv_6= Comma ( (lv_sc_7_0= ruleSafetyConstraintStatement ) ) otherlv_8= RightCurlyBracket )
            {
            // InternalAsamParser.g:1347:1: (otherlv_0= LeftCurlyBracket ( (lv_type_1_0= ruleErrorTypeStatement ) ) otherlv_2= Comma ( (lv_uca_3_0= ruleUnsafeControlActionStatement ) ) otherlv_4= Comma ( (lv_cause_5_0= ruleCausesStatement ) ) otherlv_6= Comma ( (lv_sc_7_0= ruleSafetyConstraintStatement ) ) otherlv_8= RightCurlyBracket )
            // InternalAsamParser.g:1348:2: otherlv_0= LeftCurlyBracket ( (lv_type_1_0= ruleErrorTypeStatement ) ) otherlv_2= Comma ( (lv_uca_3_0= ruleUnsafeControlActionStatement ) ) otherlv_4= Comma ( (lv_cause_5_0= ruleCausesStatement ) ) otherlv_6= Comma ( (lv_sc_7_0= ruleSafetyConstraintStatement ) ) otherlv_8= RightCurlyBracket
            {
            otherlv_0=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_24); 

                	newLeafNode(otherlv_0, grammarAccess.getErrorStatementAccess().getLeftCurlyBracketKeyword_0());
                
            // InternalAsamParser.g:1352:1: ( (lv_type_1_0= ruleErrorTypeStatement ) )
            // InternalAsamParser.g:1353:1: (lv_type_1_0= ruleErrorTypeStatement )
            {
            // InternalAsamParser.g:1353:1: (lv_type_1_0= ruleErrorTypeStatement )
            // InternalAsamParser.g:1354:3: lv_type_1_0= ruleErrorTypeStatement
            {
             
            	        newCompositeNode(grammarAccess.getErrorStatementAccess().getTypeErrorTypeStatementParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_25);
            lv_type_1_0=ruleErrorTypeStatement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorStatementRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_1_0, 
                    		"edu.clemson.asam.Asam.ErrorTypeStatement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,Comma,FollowSets000.FOLLOW_26); 

                	newLeafNode(otherlv_2, grammarAccess.getErrorStatementAccess().getCommaKeyword_2());
                
            // InternalAsamParser.g:1375:1: ( (lv_uca_3_0= ruleUnsafeControlActionStatement ) )
            // InternalAsamParser.g:1376:1: (lv_uca_3_0= ruleUnsafeControlActionStatement )
            {
            // InternalAsamParser.g:1376:1: (lv_uca_3_0= ruleUnsafeControlActionStatement )
            // InternalAsamParser.g:1377:3: lv_uca_3_0= ruleUnsafeControlActionStatement
            {
             
            	        newCompositeNode(grammarAccess.getErrorStatementAccess().getUcaUnsafeControlActionStatementParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_25);
            lv_uca_3_0=ruleUnsafeControlActionStatement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorStatementRule());
            	        }
                   		set(
                   			current, 
                   			"uca",
                    		lv_uca_3_0, 
                    		"edu.clemson.asam.Asam.UnsafeControlActionStatement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,Comma,FollowSets000.FOLLOW_8); 

                	newLeafNode(otherlv_4, grammarAccess.getErrorStatementAccess().getCommaKeyword_4());
                
            // InternalAsamParser.g:1398:1: ( (lv_cause_5_0= ruleCausesStatement ) )
            // InternalAsamParser.g:1399:1: (lv_cause_5_0= ruleCausesStatement )
            {
            // InternalAsamParser.g:1399:1: (lv_cause_5_0= ruleCausesStatement )
            // InternalAsamParser.g:1400:3: lv_cause_5_0= ruleCausesStatement
            {
             
            	        newCompositeNode(grammarAccess.getErrorStatementAccess().getCauseCausesStatementParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_25);
            lv_cause_5_0=ruleCausesStatement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorStatementRule());
            	        }
                   		set(
                   			current, 
                   			"cause",
                    		lv_cause_5_0, 
                    		"edu.clemson.asam.Asam.CausesStatement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,Comma,FollowSets000.FOLLOW_27); 

                	newLeafNode(otherlv_6, grammarAccess.getErrorStatementAccess().getCommaKeyword_6());
                
            // InternalAsamParser.g:1421:1: ( (lv_sc_7_0= ruleSafetyConstraintStatement ) )
            // InternalAsamParser.g:1422:1: (lv_sc_7_0= ruleSafetyConstraintStatement )
            {
            // InternalAsamParser.g:1422:1: (lv_sc_7_0= ruleSafetyConstraintStatement )
            // InternalAsamParser.g:1423:3: lv_sc_7_0= ruleSafetyConstraintStatement
            {
             
            	        newCompositeNode(grammarAccess.getErrorStatementAccess().getScSafetyConstraintStatementParserRuleCall_7_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_28);
            lv_sc_7_0=ruleSafetyConstraintStatement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorStatementRule());
            	        }
                   		set(
                   			current, 
                   			"sc",
                    		lv_sc_7_0, 
                    		"edu.clemson.asam.Asam.SafetyConstraintStatement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_8=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_8, grammarAccess.getErrorStatementAccess().getRightCurlyBracketKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleErrorStatement"


    // $ANTLR start "entryRuleErrorTypeStatement"
    // InternalAsamParser.g:1452:1: entryRuleErrorTypeStatement returns [EObject current=null] : iv_ruleErrorTypeStatement= ruleErrorTypeStatement EOF ;
    public final EObject entryRuleErrorTypeStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleErrorTypeStatement = null;


        try {
            // InternalAsamParser.g:1453:2: (iv_ruleErrorTypeStatement= ruleErrorTypeStatement EOF )
            // InternalAsamParser.g:1454:2: iv_ruleErrorTypeStatement= ruleErrorTypeStatement EOF
            {
             newCompositeNode(grammarAccess.getErrorTypeStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleErrorTypeStatement=ruleErrorTypeStatement();

            state._fsp--;

             current =iv_ruleErrorTypeStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleErrorTypeStatement"


    // $ANTLR start "ruleErrorTypeStatement"
    // InternalAsamParser.g:1461:1: ruleErrorTypeStatement returns [EObject current=null] : (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( (lv_errorType_2_0= ruleError ) ) ) ;
    public final EObject ruleErrorTypeStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_errorType_2_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:1464:28: ( (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( (lv_errorType_2_0= ruleError ) ) ) )
            // InternalAsamParser.g:1465:1: (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( (lv_errorType_2_0= ruleError ) ) )
            {
            // InternalAsamParser.g:1465:1: (otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( (lv_errorType_2_0= ruleError ) ) )
            // InternalAsamParser.g:1466:2: otherlv_0= Type_1 otherlv_1= EqualsSignGreaterThanSign ( (lv_errorType_2_0= ruleError ) )
            {
            otherlv_0=(Token)match(input,Type_1,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getErrorTypeStatementAccess().getTypeKeyword_0());
                
            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_1, grammarAccess.getErrorTypeStatementAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalAsamParser.g:1475:1: ( (lv_errorType_2_0= ruleError ) )
            // InternalAsamParser.g:1476:1: (lv_errorType_2_0= ruleError )
            {
            // InternalAsamParser.g:1476:1: (lv_errorType_2_0= ruleError )
            // InternalAsamParser.g:1477:3: lv_errorType_2_0= ruleError
            {
             
            	        newCompositeNode(grammarAccess.getErrorTypeStatementAccess().getErrorTypeErrorParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_errorType_2_0=ruleError();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getErrorTypeStatementRule());
            	        }
                   		set(
                   			current, 
                   			"errorType",
                    		lv_errorType_2_0, 
                    		"edu.clemson.asam.Asam.Error");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleErrorTypeStatement"


    // $ANTLR start "entryRuleUnsafeControlActionStatement"
    // InternalAsamParser.g:1501:1: entryRuleUnsafeControlActionStatement returns [EObject current=null] : iv_ruleUnsafeControlActionStatement= ruleUnsafeControlActionStatement EOF ;
    public final EObject entryRuleUnsafeControlActionStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnsafeControlActionStatement = null;


        try {
            // InternalAsamParser.g:1502:2: (iv_ruleUnsafeControlActionStatement= ruleUnsafeControlActionStatement EOF )
            // InternalAsamParser.g:1503:2: iv_ruleUnsafeControlActionStatement= ruleUnsafeControlActionStatement EOF
            {
             newCompositeNode(grammarAccess.getUnsafeControlActionStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleUnsafeControlActionStatement=ruleUnsafeControlActionStatement();

            state._fsp--;

             current =iv_ruleUnsafeControlActionStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnsafeControlActionStatement"


    // $ANTLR start "ruleUnsafeControlActionStatement"
    // InternalAsamParser.g:1510:1: ruleUnsafeControlActionStatement returns [EObject current=null] : (otherlv_0= UCA otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) ) ;
    public final EObject ruleUnsafeControlActionStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_id_2_0=null;
        Token otherlv_3=null;
        Token lv_description_4_0=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:1513:28: ( (otherlv_0= UCA otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) ) )
            // InternalAsamParser.g:1514:1: (otherlv_0= UCA otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) )
            {
            // InternalAsamParser.g:1514:1: (otherlv_0= UCA otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) )
            // InternalAsamParser.g:1515:2: otherlv_0= UCA otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,UCA,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getUnsafeControlActionStatementAccess().getUCAKeyword_0());
                
            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getUnsafeControlActionStatementAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalAsamParser.g:1524:1: ( (lv_id_2_0= RULE_ID ) )
            // InternalAsamParser.g:1525:1: (lv_id_2_0= RULE_ID )
            {
            // InternalAsamParser.g:1525:1: (lv_id_2_0= RULE_ID )
            // InternalAsamParser.g:1526:3: lv_id_2_0= RULE_ID
            {
            lv_id_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_6); 

            			newLeafNode(lv_id_2_0, grammarAccess.getUnsafeControlActionStatementAccess().getIdIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUnsafeControlActionStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,Colon,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_3, grammarAccess.getUnsafeControlActionStatementAccess().getColonKeyword_3());
                
            // InternalAsamParser.g:1547:1: ( (lv_description_4_0= RULE_STRING ) )
            // InternalAsamParser.g:1548:1: (lv_description_4_0= RULE_STRING )
            {
            // InternalAsamParser.g:1548:1: (lv_description_4_0= RULE_STRING )
            // InternalAsamParser.g:1549:3: lv_description_4_0= RULE_STRING
            {
            lv_description_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_description_4_0, grammarAccess.getUnsafeControlActionStatementAccess().getDescriptionSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUnsafeControlActionStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"description",
                    		lv_description_4_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnsafeControlActionStatement"


    // $ANTLR start "entryRuleCausesStatement"
    // InternalAsamParser.g:1573:1: entryRuleCausesStatement returns [EObject current=null] : iv_ruleCausesStatement= ruleCausesStatement EOF ;
    public final EObject entryRuleCausesStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCausesStatement = null;


        try {
            // InternalAsamParser.g:1574:2: (iv_ruleCausesStatement= ruleCausesStatement EOF )
            // InternalAsamParser.g:1575:2: iv_ruleCausesStatement= ruleCausesStatement EOF
            {
             newCompositeNode(grammarAccess.getCausesStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCausesStatement=ruleCausesStatement();

            state._fsp--;

             current =iv_ruleCausesStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCausesStatement"


    // $ANTLR start "ruleCausesStatement"
    // InternalAsamParser.g:1582:1: ruleCausesStatement returns [EObject current=null] : (otherlv_0= Causes_1 otherlv_1= EqualsSignGreaterThanSign otherlv_2= LeftCurlyBracket ( ( (lv_general_3_0= ruleGeneralCauseStatement ) ) | ( (lv_specific_4_0= ruleSpecificCauseStatement ) ) | ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) ) ) otherlv_8= RightCurlyBracket ) ;
    public final EObject ruleCausesStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_general_3_0 = null;

        EObject lv_specific_4_0 = null;

        EObject lv_general_5_0 = null;

        EObject lv_specific_7_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:1585:28: ( (otherlv_0= Causes_1 otherlv_1= EqualsSignGreaterThanSign otherlv_2= LeftCurlyBracket ( ( (lv_general_3_0= ruleGeneralCauseStatement ) ) | ( (lv_specific_4_0= ruleSpecificCauseStatement ) ) | ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) ) ) otherlv_8= RightCurlyBracket ) )
            // InternalAsamParser.g:1586:1: (otherlv_0= Causes_1 otherlv_1= EqualsSignGreaterThanSign otherlv_2= LeftCurlyBracket ( ( (lv_general_3_0= ruleGeneralCauseStatement ) ) | ( (lv_specific_4_0= ruleSpecificCauseStatement ) ) | ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) ) ) otherlv_8= RightCurlyBracket )
            {
            // InternalAsamParser.g:1586:1: (otherlv_0= Causes_1 otherlv_1= EqualsSignGreaterThanSign otherlv_2= LeftCurlyBracket ( ( (lv_general_3_0= ruleGeneralCauseStatement ) ) | ( (lv_specific_4_0= ruleSpecificCauseStatement ) ) | ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) ) ) otherlv_8= RightCurlyBracket )
            // InternalAsamParser.g:1587:2: otherlv_0= Causes_1 otherlv_1= EqualsSignGreaterThanSign otherlv_2= LeftCurlyBracket ( ( (lv_general_3_0= ruleGeneralCauseStatement ) ) | ( (lv_specific_4_0= ruleSpecificCauseStatement ) ) | ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) ) ) otherlv_8= RightCurlyBracket
            {
            otherlv_0=(Token)match(input,Causes_1,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getCausesStatementAccess().getCausesKeyword_0());
                
            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_22); 

                	newLeafNode(otherlv_1, grammarAccess.getCausesStatementAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            otherlv_2=(Token)match(input,LeftCurlyBracket,FollowSets000.FOLLOW_29); 

                	newLeafNode(otherlv_2, grammarAccess.getCausesStatementAccess().getLeftCurlyBracketKeyword_2());
                
            // InternalAsamParser.g:1601:1: ( ( (lv_general_3_0= ruleGeneralCauseStatement ) ) | ( (lv_specific_4_0= ruleSpecificCauseStatement ) ) | ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) ) )
            int alt8=3;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==General) ) {
                int LA8_1 = input.LA(2);

                if ( (LA8_1==EqualsSignGreaterThanSign) ) {
                    int LA8_3 = input.LA(3);

                    if ( (LA8_3==RULE_STRING) ) {
                        int LA8_4 = input.LA(4);

                        if ( (LA8_4==RightCurlyBracket) ) {
                            alt8=1;
                        }
                        else if ( (LA8_4==Comma) ) {
                            alt8=3;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 8, 4, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 3, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA8_0==Specific) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalAsamParser.g:1601:2: ( (lv_general_3_0= ruleGeneralCauseStatement ) )
                    {
                    // InternalAsamParser.g:1601:2: ( (lv_general_3_0= ruleGeneralCauseStatement ) )
                    // InternalAsamParser.g:1602:1: (lv_general_3_0= ruleGeneralCauseStatement )
                    {
                    // InternalAsamParser.g:1602:1: (lv_general_3_0= ruleGeneralCauseStatement )
                    // InternalAsamParser.g:1603:3: lv_general_3_0= ruleGeneralCauseStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getCausesStatementAccess().getGeneralGeneralCauseStatementParserRuleCall_3_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_28);
                    lv_general_3_0=ruleGeneralCauseStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCausesStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"general",
                            		lv_general_3_0, 
                            		"edu.clemson.asam.Asam.GeneralCauseStatement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:1620:6: ( (lv_specific_4_0= ruleSpecificCauseStatement ) )
                    {
                    // InternalAsamParser.g:1620:6: ( (lv_specific_4_0= ruleSpecificCauseStatement ) )
                    // InternalAsamParser.g:1621:1: (lv_specific_4_0= ruleSpecificCauseStatement )
                    {
                    // InternalAsamParser.g:1621:1: (lv_specific_4_0= ruleSpecificCauseStatement )
                    // InternalAsamParser.g:1622:3: lv_specific_4_0= ruleSpecificCauseStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getCausesStatementAccess().getSpecificSpecificCauseStatementParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_28);
                    lv_specific_4_0=ruleSpecificCauseStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCausesStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"specific",
                            		lv_specific_4_0, 
                            		"edu.clemson.asam.Asam.SpecificCauseStatement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:1639:6: ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) )
                    {
                    // InternalAsamParser.g:1639:6: ( ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) ) )
                    // InternalAsamParser.g:1639:7: ( (lv_general_5_0= ruleGeneralCauseStatement ) ) otherlv_6= Comma ( (lv_specific_7_0= ruleSpecificCauseStatement ) )
                    {
                    // InternalAsamParser.g:1639:7: ( (lv_general_5_0= ruleGeneralCauseStatement ) )
                    // InternalAsamParser.g:1640:1: (lv_general_5_0= ruleGeneralCauseStatement )
                    {
                    // InternalAsamParser.g:1640:1: (lv_general_5_0= ruleGeneralCauseStatement )
                    // InternalAsamParser.g:1641:3: lv_general_5_0= ruleGeneralCauseStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getCausesStatementAccess().getGeneralGeneralCauseStatementParserRuleCall_3_2_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_25);
                    lv_general_5_0=ruleGeneralCauseStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCausesStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"general",
                            		lv_general_5_0, 
                            		"edu.clemson.asam.Asam.GeneralCauseStatement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_6=(Token)match(input,Comma,FollowSets000.FOLLOW_30); 

                        	newLeafNode(otherlv_6, grammarAccess.getCausesStatementAccess().getCommaKeyword_3_2_1());
                        
                    // InternalAsamParser.g:1662:1: ( (lv_specific_7_0= ruleSpecificCauseStatement ) )
                    // InternalAsamParser.g:1663:1: (lv_specific_7_0= ruleSpecificCauseStatement )
                    {
                    // InternalAsamParser.g:1663:1: (lv_specific_7_0= ruleSpecificCauseStatement )
                    // InternalAsamParser.g:1664:3: lv_specific_7_0= ruleSpecificCauseStatement
                    {
                     
                    	        newCompositeNode(grammarAccess.getCausesStatementAccess().getSpecificSpecificCauseStatementParserRuleCall_3_2_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_28);
                    lv_specific_7_0=ruleSpecificCauseStatement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCausesStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"specific",
                            		lv_specific_7_0, 
                            		"edu.clemson.asam.Asam.SpecificCauseStatement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,RightCurlyBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_8, grammarAccess.getCausesStatementAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCausesStatement"


    // $ANTLR start "entryRuleGeneralCauseStatement"
    // InternalAsamParser.g:1693:1: entryRuleGeneralCauseStatement returns [EObject current=null] : iv_ruleGeneralCauseStatement= ruleGeneralCauseStatement EOF ;
    public final EObject entryRuleGeneralCauseStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGeneralCauseStatement = null;


        try {
            // InternalAsamParser.g:1694:2: (iv_ruleGeneralCauseStatement= ruleGeneralCauseStatement EOF )
            // InternalAsamParser.g:1695:2: iv_ruleGeneralCauseStatement= ruleGeneralCauseStatement EOF
            {
             newCompositeNode(grammarAccess.getGeneralCauseStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleGeneralCauseStatement=ruleGeneralCauseStatement();

            state._fsp--;

             current =iv_ruleGeneralCauseStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGeneralCauseStatement"


    // $ANTLR start "ruleGeneralCauseStatement"
    // InternalAsamParser.g:1702:1: ruleGeneralCauseStatement returns [EObject current=null] : (otherlv_0= General otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleGeneralCauseStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_description_2_0=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:1705:28: ( (otherlv_0= General otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) ) )
            // InternalAsamParser.g:1706:1: (otherlv_0= General otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) )
            {
            // InternalAsamParser.g:1706:1: (otherlv_0= General otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) )
            // InternalAsamParser.g:1707:2: otherlv_0= General otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,General,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getGeneralCauseStatementAccess().getGeneralKeyword_0());
                
            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_1, grammarAccess.getGeneralCauseStatementAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalAsamParser.g:1716:1: ( (lv_description_2_0= RULE_STRING ) )
            // InternalAsamParser.g:1717:1: (lv_description_2_0= RULE_STRING )
            {
            // InternalAsamParser.g:1717:1: (lv_description_2_0= RULE_STRING )
            // InternalAsamParser.g:1718:3: lv_description_2_0= RULE_STRING
            {
            lv_description_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_description_2_0, grammarAccess.getGeneralCauseStatementAccess().getDescriptionSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGeneralCauseStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"description",
                    		lv_description_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGeneralCauseStatement"


    // $ANTLR start "entryRuleSpecificCauseStatement"
    // InternalAsamParser.g:1742:1: entryRuleSpecificCauseStatement returns [EObject current=null] : iv_ruleSpecificCauseStatement= ruleSpecificCauseStatement EOF ;
    public final EObject entryRuleSpecificCauseStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSpecificCauseStatement = null;


        try {
            // InternalAsamParser.g:1743:2: (iv_ruleSpecificCauseStatement= ruleSpecificCauseStatement EOF )
            // InternalAsamParser.g:1744:2: iv_ruleSpecificCauseStatement= ruleSpecificCauseStatement EOF
            {
             newCompositeNode(grammarAccess.getSpecificCauseStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSpecificCauseStatement=ruleSpecificCauseStatement();

            state._fsp--;

             current =iv_ruleSpecificCauseStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSpecificCauseStatement"


    // $ANTLR start "ruleSpecificCauseStatement"
    // InternalAsamParser.g:1751:1: ruleSpecificCauseStatement returns [EObject current=null] : (otherlv_0= Specific otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleSpecificCauseStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_description_2_0=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:1754:28: ( (otherlv_0= Specific otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) ) )
            // InternalAsamParser.g:1755:1: (otherlv_0= Specific otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) )
            {
            // InternalAsamParser.g:1755:1: (otherlv_0= Specific otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) ) )
            // InternalAsamParser.g:1756:2: otherlv_0= Specific otherlv_1= EqualsSignGreaterThanSign ( (lv_description_2_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,Specific,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getSpecificCauseStatementAccess().getSpecificKeyword_0());
                
            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_1, grammarAccess.getSpecificCauseStatementAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalAsamParser.g:1765:1: ( (lv_description_2_0= RULE_STRING ) )
            // InternalAsamParser.g:1766:1: (lv_description_2_0= RULE_STRING )
            {
            // InternalAsamParser.g:1766:1: (lv_description_2_0= RULE_STRING )
            // InternalAsamParser.g:1767:3: lv_description_2_0= RULE_STRING
            {
            lv_description_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_description_2_0, grammarAccess.getSpecificCauseStatementAccess().getDescriptionSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSpecificCauseStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"description",
                    		lv_description_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSpecificCauseStatement"


    // $ANTLR start "entryRuleSafetyConstraintStatement"
    // InternalAsamParser.g:1791:1: entryRuleSafetyConstraintStatement returns [EObject current=null] : iv_ruleSafetyConstraintStatement= ruleSafetyConstraintStatement EOF ;
    public final EObject entryRuleSafetyConstraintStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSafetyConstraintStatement = null;


        try {
            // InternalAsamParser.g:1792:2: (iv_ruleSafetyConstraintStatement= ruleSafetyConstraintStatement EOF )
            // InternalAsamParser.g:1793:2: iv_ruleSafetyConstraintStatement= ruleSafetyConstraintStatement EOF
            {
             newCompositeNode(grammarAccess.getSafetyConstraintStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSafetyConstraintStatement=ruleSafetyConstraintStatement();

            state._fsp--;

             current =iv_ruleSafetyConstraintStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSafetyConstraintStatement"


    // $ANTLR start "ruleSafetyConstraintStatement"
    // InternalAsamParser.g:1800:1: ruleSafetyConstraintStatement returns [EObject current=null] : (otherlv_0= SC otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) otherlv_5= Verified otherlv_6= By otherlv_7= LeftSquareBracket ( (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds ) ) otherlv_9= RightSquareBracket ) ;
    public final EObject ruleSafetyConstraintStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_id_2_0=null;
        Token otherlv_3=null;
        Token lv_description_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_matchingXagreeStatements_8_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:1803:28: ( (otherlv_0= SC otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) otherlv_5= Verified otherlv_6= By otherlv_7= LeftSquareBracket ( (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds ) ) otherlv_9= RightSquareBracket ) )
            // InternalAsamParser.g:1804:1: (otherlv_0= SC otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) otherlv_5= Verified otherlv_6= By otherlv_7= LeftSquareBracket ( (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds ) ) otherlv_9= RightSquareBracket )
            {
            // InternalAsamParser.g:1804:1: (otherlv_0= SC otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) otherlv_5= Verified otherlv_6= By otherlv_7= LeftSquareBracket ( (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds ) ) otherlv_9= RightSquareBracket )
            // InternalAsamParser.g:1805:2: otherlv_0= SC otherlv_1= EqualsSignGreaterThanSign ( (lv_id_2_0= RULE_ID ) ) otherlv_3= Colon ( (lv_description_4_0= RULE_STRING ) ) otherlv_5= Verified otherlv_6= By otherlv_7= LeftSquareBracket ( (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds ) ) otherlv_9= RightSquareBracket
            {
            otherlv_0=(Token)match(input,SC,FollowSets000.FOLLOW_19); 

                	newLeafNode(otherlv_0, grammarAccess.getSafetyConstraintStatementAccess().getSCKeyword_0());
                
            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getSafetyConstraintStatementAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalAsamParser.g:1814:1: ( (lv_id_2_0= RULE_ID ) )
            // InternalAsamParser.g:1815:1: (lv_id_2_0= RULE_ID )
            {
            // InternalAsamParser.g:1815:1: (lv_id_2_0= RULE_ID )
            // InternalAsamParser.g:1816:3: lv_id_2_0= RULE_ID
            {
            lv_id_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_6); 

            			newLeafNode(lv_id_2_0, grammarAccess.getSafetyConstraintStatementAccess().getIdIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSafetyConstraintStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,Colon,FollowSets000.FOLLOW_7); 

                	newLeafNode(otherlv_3, grammarAccess.getSafetyConstraintStatementAccess().getColonKeyword_3());
                
            // InternalAsamParser.g:1837:1: ( (lv_description_4_0= RULE_STRING ) )
            // InternalAsamParser.g:1838:1: (lv_description_4_0= RULE_STRING )
            {
            // InternalAsamParser.g:1838:1: (lv_description_4_0= RULE_STRING )
            // InternalAsamParser.g:1839:3: lv_description_4_0= RULE_STRING
            {
            lv_description_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_16); 

            			newLeafNode(lv_description_4_0, grammarAccess.getSafetyConstraintStatementAccess().getDescriptionSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSafetyConstraintStatementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"description",
                    		lv_description_4_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.STRING");
            	    

            }


            }

            otherlv_5=(Token)match(input,Verified,FollowSets000.FOLLOW_17); 

                	newLeafNode(otherlv_5, grammarAccess.getSafetyConstraintStatementAccess().getVerifiedKeyword_5());
                
            otherlv_6=(Token)match(input,By,FollowSets000.FOLLOW_9); 

                	newLeafNode(otherlv_6, grammarAccess.getSafetyConstraintStatementAccess().getByKeyword_6());
                
            otherlv_7=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_7, grammarAccess.getSafetyConstraintStatementAccess().getLeftSquareBracketKeyword_7());
                
            // InternalAsamParser.g:1870:1: ( (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds ) )
            // InternalAsamParser.g:1871:1: (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds )
            {
            // InternalAsamParser.g:1871:1: (lv_matchingXagreeStatements_8_0= ruleGuaranteeIds )
            // InternalAsamParser.g:1872:3: lv_matchingXagreeStatements_8_0= ruleGuaranteeIds
            {
             
            	        newCompositeNode(grammarAccess.getSafetyConstraintStatementAccess().getMatchingXagreeStatementsGuaranteeIdsParserRuleCall_8_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_11);
            lv_matchingXagreeStatements_8_0=ruleGuaranteeIds();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSafetyConstraintStatementRule());
            	        }
                   		set(
                   			current, 
                   			"matchingXagreeStatements",
                    		lv_matchingXagreeStatements_8_0, 
                    		"edu.clemson.asam.Asam.GuaranteeIds");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_9=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_9, grammarAccess.getSafetyConstraintStatementAccess().getRightSquareBracketKeyword_9());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSafetyConstraintStatement"


    // $ANTLR start "entryRuleGuaranteeIds"
    // InternalAsamParser.g:1901:1: entryRuleGuaranteeIds returns [EObject current=null] : iv_ruleGuaranteeIds= ruleGuaranteeIds EOF ;
    public final EObject entryRuleGuaranteeIds() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuaranteeIds = null;


        try {
            // InternalAsamParser.g:1902:2: (iv_ruleGuaranteeIds= ruleGuaranteeIds EOF )
            // InternalAsamParser.g:1903:2: iv_ruleGuaranteeIds= ruleGuaranteeIds EOF
            {
             newCompositeNode(grammarAccess.getGuaranteeIdsRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleGuaranteeIds=ruleGuaranteeIds();

            state._fsp--;

             current =iv_ruleGuaranteeIds; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuaranteeIds"


    // $ANTLR start "ruleGuaranteeIds"
    // InternalAsamParser.g:1910:1: ruleGuaranteeIds returns [EObject current=null] : ( ( (lv_first_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_ID ) ) )* ) ;
    public final EObject ruleGuaranteeIds() throws RecognitionException {
        EObject current = null;

        Token lv_first_0_0=null;
        Token otherlv_1=null;
        Token lv_rest_2_0=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:1913:28: ( ( ( (lv_first_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_ID ) ) )* ) )
            // InternalAsamParser.g:1914:1: ( ( (lv_first_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_ID ) ) )* )
            {
            // InternalAsamParser.g:1914:1: ( ( (lv_first_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_ID ) ) )* )
            // InternalAsamParser.g:1914:2: ( (lv_first_0_0= RULE_ID ) ) (otherlv_1= Comma ( (lv_rest_2_0= RULE_ID ) ) )*
            {
            // InternalAsamParser.g:1914:2: ( (lv_first_0_0= RULE_ID ) )
            // InternalAsamParser.g:1915:1: (lv_first_0_0= RULE_ID )
            {
            // InternalAsamParser.g:1915:1: (lv_first_0_0= RULE_ID )
            // InternalAsamParser.g:1916:3: lv_first_0_0= RULE_ID
            {
            lv_first_0_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); 

            			newLeafNode(lv_first_0_0, grammarAccess.getGuaranteeIdsAccess().getFirstIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGuaranteeIdsRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"first",
                    		lv_first_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            // InternalAsamParser.g:1932:2: (otherlv_1= Comma ( (lv_rest_2_0= RULE_ID ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==Comma) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalAsamParser.g:1933:2: otherlv_1= Comma ( (lv_rest_2_0= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,Comma,FollowSets000.FOLLOW_5); 

            	        	newLeafNode(otherlv_1, grammarAccess.getGuaranteeIdsAccess().getCommaKeyword_1_0());
            	        
            	    // InternalAsamParser.g:1937:1: ( (lv_rest_2_0= RULE_ID ) )
            	    // InternalAsamParser.g:1938:1: (lv_rest_2_0= RULE_ID )
            	    {
            	    // InternalAsamParser.g:1938:1: (lv_rest_2_0= RULE_ID )
            	    // InternalAsamParser.g:1939:3: lv_rest_2_0= RULE_ID
            	    {
            	    lv_rest_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); 

            	    			newLeafNode(lv_rest_2_0, grammarAccess.getGuaranteeIdsAccess().getRestIDTerminalRuleCall_1_1_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getGuaranteeIdsRule());
            	    	        }
            	           		addWithLastConsumed(
            	           			current, 
            	           			"rest",
            	            		lv_rest_2_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuaranteeIds"


    // $ANTLR start "entryRuleError"
    // InternalAsamParser.g:1963:1: entryRuleError returns [EObject current=null] : iv_ruleError= ruleError EOF ;
    public final EObject entryRuleError() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleError = null;


        try {
            // InternalAsamParser.g:1964:2: (iv_ruleError= ruleError EOF )
            // InternalAsamParser.g:1965:2: iv_ruleError= ruleError EOF
            {
             newCompositeNode(grammarAccess.getErrorRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleError=ruleError();

            state._fsp--;

             current =iv_ruleError; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleError"


    // $ANTLR start "ruleError"
    // InternalAsamParser.g:1972:1: ruleError returns [EObject current=null] : ( ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) ) otherlv_1= LeftParenthesis ( ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) ) ) otherlv_3= P ( ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) ) ) otherlv_5= RightParenthesis ) ;
    public final EObject ruleError() throws RecognitionException {
        EObject current = null;

        Token lv_error_0_1=null;
        Token lv_error_0_2=null;
        Token lv_error_0_3=null;
        Token lv_error_0_4=null;
        Token lv_error_0_5=null;
        Token lv_error_0_6=null;
        Token lv_error_0_7=null;
        Token lv_error_0_8=null;
        Token lv_error_0_9=null;
        Token lv_error_0_10=null;
        Token lv_error_0_11=null;
        Token lv_error_0_12=null;
        Token lv_error_0_13=null;
        Token lv_error_0_14=null;
        Token lv_error_0_15=null;
        Token lv_error_0_16=null;
        Token lv_error_0_17=null;
        Token lv_error_0_18=null;
        Token lv_error_0_19=null;
        Token lv_error_0_20=null;
        Token lv_error_0_21=null;
        Token lv_error_0_22=null;
        Token lv_error_0_23=null;
        Token lv_error_0_24=null;
        Token lv_error_0_25=null;
        Token lv_error_0_26=null;
        Token lv_error_0_27=null;
        Token lv_error_0_28=null;
        Token lv_error_0_29=null;
        Token lv_error_0_30=null;
        Token lv_error_0_31=null;
        Token lv_error_0_32=null;
        Token lv_error_0_33=null;
        Token lv_error_0_34=null;
        Token lv_error_0_35=null;
        Token lv_error_0_36=null;
        Token lv_error_0_37=null;
        Token lv_error_0_38=null;
        Token lv_error_0_39=null;
        Token lv_error_0_40=null;
        Token lv_error_0_41=null;
        Token lv_error_0_42=null;
        Token lv_error_0_43=null;
        Token lv_error_0_44=null;
        Token lv_error_0_45=null;
        Token lv_error_0_46=null;
        Token lv_error_0_47=null;
        Token lv_error_0_48=null;
        Token lv_error_0_49=null;
        Token lv_error_0_50=null;
        Token lv_error_0_51=null;
        Token lv_error_0_52=null;
        Token lv_error_0_53=null;
        Token lv_error_0_54=null;
        Token lv_error_0_55=null;
        Token lv_error_0_56=null;
        Token lv_error_0_57=null;
        Token lv_error_0_58=null;
        Token lv_error_0_59=null;
        Token lv_error_0_60=null;
        Token lv_error_0_61=null;
        Token lv_error_0_62=null;
        Token lv_error_0_63=null;
        Token otherlv_1=null;
        Token lv_severityLevel_2_1=null;
        Token lv_severityLevel_2_2=null;
        Token lv_severityLevel_2_3=null;
        Token lv_severityLevel_2_4=null;
        Token otherlv_3=null;
        Token lv_probability_4_1=null;
        Token lv_probability_4_2=null;
        Token lv_probability_4_3=null;
        Token otherlv_5=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:1975:28: ( ( ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) ) otherlv_1= LeftParenthesis ( ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) ) ) otherlv_3= P ( ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) ) ) otherlv_5= RightParenthesis ) )
            // InternalAsamParser.g:1976:1: ( ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) ) otherlv_1= LeftParenthesis ( ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) ) ) otherlv_3= P ( ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) ) ) otherlv_5= RightParenthesis )
            {
            // InternalAsamParser.g:1976:1: ( ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) ) otherlv_1= LeftParenthesis ( ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) ) ) otherlv_3= P ( ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) ) ) otherlv_5= RightParenthesis )
            // InternalAsamParser.g:1976:2: ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) ) otherlv_1= LeftParenthesis ( ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) ) ) otherlv_3= P ( ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) ) ) otherlv_5= RightParenthesis
            {
            // InternalAsamParser.g:1976:2: ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) )
            // InternalAsamParser.g:1977:1: ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) )
            {
            // InternalAsamParser.g:1977:1: ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) )
            // InternalAsamParser.g:1978:1: (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID )
            {
            // InternalAsamParser.g:1978:1: (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID )
            int alt10=63;
            switch ( input.LA(1) ) {
            case ServiceError:
                {
                alt10=1;
                }
                break;
            case ItemOmission:
                {
                alt10=2;
                }
                break;
            case ServiceOmission:
                {
                alt10=3;
                }
                break;
            case SequenceOmission:
                {
                alt10=4;
                }
                break;
            case TransientServiceOmission:
                {
                alt10=5;
                }
                break;
            case LateServiceStart:
                {
                alt10=6;
                }
                break;
            case EarlyServiceTermination:
                {
                alt10=7;
                }
                break;
            case BoundedOmissionInterval:
                {
                alt10=8;
                }
                break;
            case ItemCommission:
                {
                alt10=9;
                }
                break;
            case ServiceCommission:
                {
                alt10=10;
                }
                break;
            case SequenceCommission:
                {
                alt10=11;
                }
                break;
            case EarlyServiceStart:
                {
                alt10=12;
                }
                break;
            case LateServiceTermination:
                {
                alt10=13;
                }
                break;
            case TimingRelatedError:
                {
                alt10=14;
                }
                break;
            case ItemTimingError:
                {
                alt10=15;
                }
                break;
            case EarlyDelivery:
                {
                alt10=16;
                }
                break;
            case LateDelivery:
                {
                alt10=17;
                }
                break;
            case SequenceTimingError:
                {
                alt10=18;
                }
                break;
            case HighRate:
                {
                alt10=19;
                }
                break;
            case LowRate:
                {
                alt10=20;
                }
                break;
            case RateJitter:
                {
                alt10=21;
                }
                break;
            case ServiceTimingError:
                {
                alt10=22;
                }
                break;
            case DelayedService:
                {
                alt10=23;
                }
                break;
            case EarlyService:
                {
                alt10=24;
                }
                break;
            case ValueRelatedError:
                {
                alt10=25;
                }
                break;
            case ItemValueError:
                {
                alt10=26;
                }
                break;
            case UndetectableValueError:
                {
                alt10=27;
                }
                break;
            case DetectableValueError:
                {
                alt10=28;
                }
                break;
            case OutOfRange:
                {
                alt10=29;
                }
                break;
            case BelowRange:
                {
                alt10=30;
                }
                break;
            case AboveRange:
                {
                alt10=31;
                }
                break;
            case OutOfBounds:
                {
                alt10=32;
                }
                break;
            case SequenceValueError:
                {
                alt10=33;
                }
                break;
            case BoundedValueChange:
                {
                alt10=34;
                }
                break;
            case StuckValue:
                {
                alt10=35;
                }
                break;
            case OutOfOrder:
                {
                alt10=36;
                }
                break;
            case ServiceValueError:
                {
                alt10=37;
                }
                break;
            case OutOfCalibration:
                {
                alt10=38;
                }
                break;
            case ReplicationError:
                {
                alt10=39;
                }
                break;
            case AsymmetricReplicatesError:
                {
                alt10=40;
                }
                break;
            case AsymmetricValue:
                {
                alt10=41;
                }
                break;
            case AsymmetricApproximateValue:
                {
                alt10=42;
                }
                break;
            case AsymmetricExactValue:
                {
                alt10=43;
                }
                break;
            case AsymmetricTiming:
                {
                alt10=44;
                }
                break;
            case AsymmetricOmission:
                {
                alt10=45;
                }
                break;
            case AsymmetricItemOmission:
                {
                alt10=46;
                }
                break;
            case AsymmetricServiceOmission:
                {
                alt10=47;
                }
                break;
            case SymmetricReplicatesError:
                {
                alt10=48;
                }
                break;
            case SymmetricValue:
                {
                alt10=49;
                }
                break;
            case SymmetricApproximateValue:
                {
                alt10=50;
                }
                break;
            case SymmetricExactValue:
                {
                alt10=51;
                }
                break;
            case SymmetricTiming:
                {
                alt10=52;
                }
                break;
            case SymmetricOmission:
                {
                alt10=53;
                }
                break;
            case SymmetricItemOmission:
                {
                alt10=54;
                }
                break;
            case SymmetricServiceOmission:
                {
                alt10=55;
                }
                break;
            case ConcurrencyError:
                {
                alt10=56;
                }
                break;
            case RaceCondition:
                {
                alt10=57;
                }
                break;
            case ReadWriteRace:
                {
                alt10=58;
                }
                break;
            case WriteWriteRace:
                {
                alt10=59;
                }
                break;
            case MutExError:
                {
                alt10=60;
                }
                break;
            case Deadlock:
                {
                alt10=61;
                }
                break;
            case Starvation:
                {
                alt10=62;
                }
                break;
            case RULE_ID:
                {
                alt10=63;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalAsamParser.g:1979:3: lv_error_0_1= ServiceError
                    {
                    lv_error_0_1=(Token)match(input,ServiceError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_1, grammarAccess.getErrorAccess().getErrorServiceErrorKeyword_0_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:1992:8: lv_error_0_2= ItemOmission
                    {
                    lv_error_0_2=(Token)match(input,ItemOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_2, grammarAccess.getErrorAccess().getErrorItemOmissionKeyword_0_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_2, null);
                    	    

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:2005:8: lv_error_0_3= ServiceOmission
                    {
                    lv_error_0_3=(Token)match(input,ServiceOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_3, grammarAccess.getErrorAccess().getErrorServiceOmissionKeyword_0_0_2());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_3, null);
                    	    

                    }
                    break;
                case 4 :
                    // InternalAsamParser.g:2018:8: lv_error_0_4= SequenceOmission
                    {
                    lv_error_0_4=(Token)match(input,SequenceOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_4, grammarAccess.getErrorAccess().getErrorSequenceOmissionKeyword_0_0_3());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_4, null);
                    	    

                    }
                    break;
                case 5 :
                    // InternalAsamParser.g:2031:8: lv_error_0_5= TransientServiceOmission
                    {
                    lv_error_0_5=(Token)match(input,TransientServiceOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_5, grammarAccess.getErrorAccess().getErrorTransientServiceOmissionKeyword_0_0_4());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_5, null);
                    	    

                    }
                    break;
                case 6 :
                    // InternalAsamParser.g:2044:8: lv_error_0_6= LateServiceStart
                    {
                    lv_error_0_6=(Token)match(input,LateServiceStart,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_6, grammarAccess.getErrorAccess().getErrorLateServiceStartKeyword_0_0_5());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_6, null);
                    	    

                    }
                    break;
                case 7 :
                    // InternalAsamParser.g:2057:8: lv_error_0_7= EarlyServiceTermination
                    {
                    lv_error_0_7=(Token)match(input,EarlyServiceTermination,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_7, grammarAccess.getErrorAccess().getErrorEarlyServiceTerminationKeyword_0_0_6());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_7, null);
                    	    

                    }
                    break;
                case 8 :
                    // InternalAsamParser.g:2070:8: lv_error_0_8= BoundedOmissionInterval
                    {
                    lv_error_0_8=(Token)match(input,BoundedOmissionInterval,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_8, grammarAccess.getErrorAccess().getErrorBoundedOmissionIntervalKeyword_0_0_7());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_8, null);
                    	    

                    }
                    break;
                case 9 :
                    // InternalAsamParser.g:2083:8: lv_error_0_9= ItemCommission
                    {
                    lv_error_0_9=(Token)match(input,ItemCommission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_9, grammarAccess.getErrorAccess().getErrorItemCommissionKeyword_0_0_8());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_9, null);
                    	    

                    }
                    break;
                case 10 :
                    // InternalAsamParser.g:2096:8: lv_error_0_10= ServiceCommission
                    {
                    lv_error_0_10=(Token)match(input,ServiceCommission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_10, grammarAccess.getErrorAccess().getErrorServiceCommissionKeyword_0_0_9());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_10, null);
                    	    

                    }
                    break;
                case 11 :
                    // InternalAsamParser.g:2109:8: lv_error_0_11= SequenceCommission
                    {
                    lv_error_0_11=(Token)match(input,SequenceCommission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_11, grammarAccess.getErrorAccess().getErrorSequenceCommissionKeyword_0_0_10());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_11, null);
                    	    

                    }
                    break;
                case 12 :
                    // InternalAsamParser.g:2122:8: lv_error_0_12= EarlyServiceStart
                    {
                    lv_error_0_12=(Token)match(input,EarlyServiceStart,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_12, grammarAccess.getErrorAccess().getErrorEarlyServiceStartKeyword_0_0_11());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_12, null);
                    	    

                    }
                    break;
                case 13 :
                    // InternalAsamParser.g:2135:8: lv_error_0_13= LateServiceTermination
                    {
                    lv_error_0_13=(Token)match(input,LateServiceTermination,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_13, grammarAccess.getErrorAccess().getErrorLateServiceTerminationKeyword_0_0_12());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_13, null);
                    	    

                    }
                    break;
                case 14 :
                    // InternalAsamParser.g:2148:8: lv_error_0_14= TimingRelatedError
                    {
                    lv_error_0_14=(Token)match(input,TimingRelatedError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_14, grammarAccess.getErrorAccess().getErrorTimingRelatedErrorKeyword_0_0_13());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_14, null);
                    	    

                    }
                    break;
                case 15 :
                    // InternalAsamParser.g:2161:8: lv_error_0_15= ItemTimingError
                    {
                    lv_error_0_15=(Token)match(input,ItemTimingError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_15, grammarAccess.getErrorAccess().getErrorItemTimingErrorKeyword_0_0_14());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_15, null);
                    	    

                    }
                    break;
                case 16 :
                    // InternalAsamParser.g:2174:8: lv_error_0_16= EarlyDelivery
                    {
                    lv_error_0_16=(Token)match(input,EarlyDelivery,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_16, grammarAccess.getErrorAccess().getErrorEarlyDeliveryKeyword_0_0_15());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_16, null);
                    	    

                    }
                    break;
                case 17 :
                    // InternalAsamParser.g:2187:8: lv_error_0_17= LateDelivery
                    {
                    lv_error_0_17=(Token)match(input,LateDelivery,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_17, grammarAccess.getErrorAccess().getErrorLateDeliveryKeyword_0_0_16());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_17, null);
                    	    

                    }
                    break;
                case 18 :
                    // InternalAsamParser.g:2200:8: lv_error_0_18= SequenceTimingError
                    {
                    lv_error_0_18=(Token)match(input,SequenceTimingError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_18, grammarAccess.getErrorAccess().getErrorSequenceTimingErrorKeyword_0_0_17());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_18, null);
                    	    

                    }
                    break;
                case 19 :
                    // InternalAsamParser.g:2213:8: lv_error_0_19= HighRate
                    {
                    lv_error_0_19=(Token)match(input,HighRate,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_19, grammarAccess.getErrorAccess().getErrorHighRateKeyword_0_0_18());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_19, null);
                    	    

                    }
                    break;
                case 20 :
                    // InternalAsamParser.g:2226:8: lv_error_0_20= LowRate
                    {
                    lv_error_0_20=(Token)match(input,LowRate,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_20, grammarAccess.getErrorAccess().getErrorLowRateKeyword_0_0_19());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_20, null);
                    	    

                    }
                    break;
                case 21 :
                    // InternalAsamParser.g:2239:8: lv_error_0_21= RateJitter
                    {
                    lv_error_0_21=(Token)match(input,RateJitter,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_21, grammarAccess.getErrorAccess().getErrorRateJitterKeyword_0_0_20());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_21, null);
                    	    

                    }
                    break;
                case 22 :
                    // InternalAsamParser.g:2252:8: lv_error_0_22= ServiceTimingError
                    {
                    lv_error_0_22=(Token)match(input,ServiceTimingError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_22, grammarAccess.getErrorAccess().getErrorServiceTimingErrorKeyword_0_0_21());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_22, null);
                    	    

                    }
                    break;
                case 23 :
                    // InternalAsamParser.g:2265:8: lv_error_0_23= DelayedService
                    {
                    lv_error_0_23=(Token)match(input,DelayedService,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_23, grammarAccess.getErrorAccess().getErrorDelayedServiceKeyword_0_0_22());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_23, null);
                    	    

                    }
                    break;
                case 24 :
                    // InternalAsamParser.g:2278:8: lv_error_0_24= EarlyService
                    {
                    lv_error_0_24=(Token)match(input,EarlyService,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_24, grammarAccess.getErrorAccess().getErrorEarlyServiceKeyword_0_0_23());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_24, null);
                    	    

                    }
                    break;
                case 25 :
                    // InternalAsamParser.g:2291:8: lv_error_0_25= ValueRelatedError
                    {
                    lv_error_0_25=(Token)match(input,ValueRelatedError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_25, grammarAccess.getErrorAccess().getErrorValueRelatedErrorKeyword_0_0_24());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_25, null);
                    	    

                    }
                    break;
                case 26 :
                    // InternalAsamParser.g:2304:8: lv_error_0_26= ItemValueError
                    {
                    lv_error_0_26=(Token)match(input,ItemValueError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_26, grammarAccess.getErrorAccess().getErrorItemValueErrorKeyword_0_0_25());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_26, null);
                    	    

                    }
                    break;
                case 27 :
                    // InternalAsamParser.g:2317:8: lv_error_0_27= UndetectableValueError
                    {
                    lv_error_0_27=(Token)match(input,UndetectableValueError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_27, grammarAccess.getErrorAccess().getErrorUndetectableValueErrorKeyword_0_0_26());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_27, null);
                    	    

                    }
                    break;
                case 28 :
                    // InternalAsamParser.g:2330:8: lv_error_0_28= DetectableValueError
                    {
                    lv_error_0_28=(Token)match(input,DetectableValueError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_28, grammarAccess.getErrorAccess().getErrorDetectableValueErrorKeyword_0_0_27());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_28, null);
                    	    

                    }
                    break;
                case 29 :
                    // InternalAsamParser.g:2343:8: lv_error_0_29= OutOfRange
                    {
                    lv_error_0_29=(Token)match(input,OutOfRange,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_29, grammarAccess.getErrorAccess().getErrorOutOfRangeKeyword_0_0_28());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_29, null);
                    	    

                    }
                    break;
                case 30 :
                    // InternalAsamParser.g:2356:8: lv_error_0_30= BelowRange
                    {
                    lv_error_0_30=(Token)match(input,BelowRange,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_30, grammarAccess.getErrorAccess().getErrorBelowRangeKeyword_0_0_29());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_30, null);
                    	    

                    }
                    break;
                case 31 :
                    // InternalAsamParser.g:2369:8: lv_error_0_31= AboveRange
                    {
                    lv_error_0_31=(Token)match(input,AboveRange,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_31, grammarAccess.getErrorAccess().getErrorAboveRangeKeyword_0_0_30());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_31, null);
                    	    

                    }
                    break;
                case 32 :
                    // InternalAsamParser.g:2382:8: lv_error_0_32= OutOfBounds
                    {
                    lv_error_0_32=(Token)match(input,OutOfBounds,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_32, grammarAccess.getErrorAccess().getErrorOutOfBoundsKeyword_0_0_31());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_32, null);
                    	    

                    }
                    break;
                case 33 :
                    // InternalAsamParser.g:2395:8: lv_error_0_33= SequenceValueError
                    {
                    lv_error_0_33=(Token)match(input,SequenceValueError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_33, grammarAccess.getErrorAccess().getErrorSequenceValueErrorKeyword_0_0_32());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_33, null);
                    	    

                    }
                    break;
                case 34 :
                    // InternalAsamParser.g:2408:8: lv_error_0_34= BoundedValueChange
                    {
                    lv_error_0_34=(Token)match(input,BoundedValueChange,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_34, grammarAccess.getErrorAccess().getErrorBoundedValueChangeKeyword_0_0_33());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_34, null);
                    	    

                    }
                    break;
                case 35 :
                    // InternalAsamParser.g:2421:8: lv_error_0_35= StuckValue
                    {
                    lv_error_0_35=(Token)match(input,StuckValue,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_35, grammarAccess.getErrorAccess().getErrorStuckValueKeyword_0_0_34());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_35, null);
                    	    

                    }
                    break;
                case 36 :
                    // InternalAsamParser.g:2434:8: lv_error_0_36= OutOfOrder
                    {
                    lv_error_0_36=(Token)match(input,OutOfOrder,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_36, grammarAccess.getErrorAccess().getErrorOutOfOrderKeyword_0_0_35());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_36, null);
                    	    

                    }
                    break;
                case 37 :
                    // InternalAsamParser.g:2447:8: lv_error_0_37= ServiceValueError
                    {
                    lv_error_0_37=(Token)match(input,ServiceValueError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_37, grammarAccess.getErrorAccess().getErrorServiceValueErrorKeyword_0_0_36());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_37, null);
                    	    

                    }
                    break;
                case 38 :
                    // InternalAsamParser.g:2460:8: lv_error_0_38= OutOfCalibration
                    {
                    lv_error_0_38=(Token)match(input,OutOfCalibration,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_38, grammarAccess.getErrorAccess().getErrorOutOfCalibrationKeyword_0_0_37());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_38, null);
                    	    

                    }
                    break;
                case 39 :
                    // InternalAsamParser.g:2473:8: lv_error_0_39= ReplicationError
                    {
                    lv_error_0_39=(Token)match(input,ReplicationError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_39, grammarAccess.getErrorAccess().getErrorReplicationErrorKeyword_0_0_38());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_39, null);
                    	    

                    }
                    break;
                case 40 :
                    // InternalAsamParser.g:2486:8: lv_error_0_40= AsymmetricReplicatesError
                    {
                    lv_error_0_40=(Token)match(input,AsymmetricReplicatesError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_40, grammarAccess.getErrorAccess().getErrorAsymmetricReplicatesErrorKeyword_0_0_39());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_40, null);
                    	    

                    }
                    break;
                case 41 :
                    // InternalAsamParser.g:2499:8: lv_error_0_41= AsymmetricValue
                    {
                    lv_error_0_41=(Token)match(input,AsymmetricValue,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_41, grammarAccess.getErrorAccess().getErrorAsymmetricValueKeyword_0_0_40());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_41, null);
                    	    

                    }
                    break;
                case 42 :
                    // InternalAsamParser.g:2512:8: lv_error_0_42= AsymmetricApproximateValue
                    {
                    lv_error_0_42=(Token)match(input,AsymmetricApproximateValue,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_42, grammarAccess.getErrorAccess().getErrorAsymmetricApproximateValueKeyword_0_0_41());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_42, null);
                    	    

                    }
                    break;
                case 43 :
                    // InternalAsamParser.g:2525:8: lv_error_0_43= AsymmetricExactValue
                    {
                    lv_error_0_43=(Token)match(input,AsymmetricExactValue,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_43, grammarAccess.getErrorAccess().getErrorAsymmetricExactValueKeyword_0_0_42());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_43, null);
                    	    

                    }
                    break;
                case 44 :
                    // InternalAsamParser.g:2538:8: lv_error_0_44= AsymmetricTiming
                    {
                    lv_error_0_44=(Token)match(input,AsymmetricTiming,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_44, grammarAccess.getErrorAccess().getErrorAsymmetricTimingKeyword_0_0_43());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_44, null);
                    	    

                    }
                    break;
                case 45 :
                    // InternalAsamParser.g:2551:8: lv_error_0_45= AsymmetricOmission
                    {
                    lv_error_0_45=(Token)match(input,AsymmetricOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_45, grammarAccess.getErrorAccess().getErrorAsymmetricOmissionKeyword_0_0_44());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_45, null);
                    	    

                    }
                    break;
                case 46 :
                    // InternalAsamParser.g:2564:8: lv_error_0_46= AsymmetricItemOmission
                    {
                    lv_error_0_46=(Token)match(input,AsymmetricItemOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_46, grammarAccess.getErrorAccess().getErrorAsymmetricItemOmissionKeyword_0_0_45());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_46, null);
                    	    

                    }
                    break;
                case 47 :
                    // InternalAsamParser.g:2577:8: lv_error_0_47= AsymmetricServiceOmission
                    {
                    lv_error_0_47=(Token)match(input,AsymmetricServiceOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_47, grammarAccess.getErrorAccess().getErrorAsymmetricServiceOmissionKeyword_0_0_46());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_47, null);
                    	    

                    }
                    break;
                case 48 :
                    // InternalAsamParser.g:2590:8: lv_error_0_48= SymmetricReplicatesError
                    {
                    lv_error_0_48=(Token)match(input,SymmetricReplicatesError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_48, grammarAccess.getErrorAccess().getErrorSymmetricReplicatesErrorKeyword_0_0_47());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_48, null);
                    	    

                    }
                    break;
                case 49 :
                    // InternalAsamParser.g:2603:8: lv_error_0_49= SymmetricValue
                    {
                    lv_error_0_49=(Token)match(input,SymmetricValue,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_49, grammarAccess.getErrorAccess().getErrorSymmetricValueKeyword_0_0_48());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_49, null);
                    	    

                    }
                    break;
                case 50 :
                    // InternalAsamParser.g:2616:8: lv_error_0_50= SymmetricApproximateValue
                    {
                    lv_error_0_50=(Token)match(input,SymmetricApproximateValue,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_50, grammarAccess.getErrorAccess().getErrorSymmetricApproximateValueKeyword_0_0_49());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_50, null);
                    	    

                    }
                    break;
                case 51 :
                    // InternalAsamParser.g:2629:8: lv_error_0_51= SymmetricExactValue
                    {
                    lv_error_0_51=(Token)match(input,SymmetricExactValue,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_51, grammarAccess.getErrorAccess().getErrorSymmetricExactValueKeyword_0_0_50());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_51, null);
                    	    

                    }
                    break;
                case 52 :
                    // InternalAsamParser.g:2642:8: lv_error_0_52= SymmetricTiming
                    {
                    lv_error_0_52=(Token)match(input,SymmetricTiming,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_52, grammarAccess.getErrorAccess().getErrorSymmetricTimingKeyword_0_0_51());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_52, null);
                    	    

                    }
                    break;
                case 53 :
                    // InternalAsamParser.g:2655:8: lv_error_0_53= SymmetricOmission
                    {
                    lv_error_0_53=(Token)match(input,SymmetricOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_53, grammarAccess.getErrorAccess().getErrorSymmetricOmissionKeyword_0_0_52());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_53, null);
                    	    

                    }
                    break;
                case 54 :
                    // InternalAsamParser.g:2668:8: lv_error_0_54= SymmetricItemOmission
                    {
                    lv_error_0_54=(Token)match(input,SymmetricItemOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_54, grammarAccess.getErrorAccess().getErrorSymmetricItemOmissionKeyword_0_0_53());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_54, null);
                    	    

                    }
                    break;
                case 55 :
                    // InternalAsamParser.g:2681:8: lv_error_0_55= SymmetricServiceOmission
                    {
                    lv_error_0_55=(Token)match(input,SymmetricServiceOmission,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_55, grammarAccess.getErrorAccess().getErrorSymmetricServiceOmissionKeyword_0_0_54());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_55, null);
                    	    

                    }
                    break;
                case 56 :
                    // InternalAsamParser.g:2694:8: lv_error_0_56= ConcurrencyError
                    {
                    lv_error_0_56=(Token)match(input,ConcurrencyError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_56, grammarAccess.getErrorAccess().getErrorConcurrencyErrorKeyword_0_0_55());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_56, null);
                    	    

                    }
                    break;
                case 57 :
                    // InternalAsamParser.g:2707:8: lv_error_0_57= RaceCondition
                    {
                    lv_error_0_57=(Token)match(input,RaceCondition,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_57, grammarAccess.getErrorAccess().getErrorRaceConditionKeyword_0_0_56());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_57, null);
                    	    

                    }
                    break;
                case 58 :
                    // InternalAsamParser.g:2720:8: lv_error_0_58= ReadWriteRace
                    {
                    lv_error_0_58=(Token)match(input,ReadWriteRace,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_58, grammarAccess.getErrorAccess().getErrorReadWriteRaceKeyword_0_0_57());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_58, null);
                    	    

                    }
                    break;
                case 59 :
                    // InternalAsamParser.g:2733:8: lv_error_0_59= WriteWriteRace
                    {
                    lv_error_0_59=(Token)match(input,WriteWriteRace,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_59, grammarAccess.getErrorAccess().getErrorWriteWriteRaceKeyword_0_0_58());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_59, null);
                    	    

                    }
                    break;
                case 60 :
                    // InternalAsamParser.g:2746:8: lv_error_0_60= MutExError
                    {
                    lv_error_0_60=(Token)match(input,MutExError,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_60, grammarAccess.getErrorAccess().getErrorMutExErrorKeyword_0_0_59());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_60, null);
                    	    

                    }
                    break;
                case 61 :
                    // InternalAsamParser.g:2759:8: lv_error_0_61= Deadlock
                    {
                    lv_error_0_61=(Token)match(input,Deadlock,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_61, grammarAccess.getErrorAccess().getErrorDeadlockKeyword_0_0_60());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_61, null);
                    	    

                    }
                    break;
                case 62 :
                    // InternalAsamParser.g:2772:8: lv_error_0_62= Starvation
                    {
                    lv_error_0_62=(Token)match(input,Starvation,FollowSets000.FOLLOW_31); 

                            newLeafNode(lv_error_0_62, grammarAccess.getErrorAccess().getErrorStarvationKeyword_0_0_61());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_62, null);
                    	    

                    }
                    break;
                case 63 :
                    // InternalAsamParser.g:2785:8: lv_error_0_63= RULE_ID
                    {
                    lv_error_0_63=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_31); 

                    			newLeafNode(lv_error_0_63, grammarAccess.getErrorAccess().getErrorIDTerminalRuleCall_0_0_62()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"error",
                            		lv_error_0_63, 
                            		"org.osate.xtext.aadl2.properties.Properties.ID");
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_32); 

                	newLeafNode(otherlv_1, grammarAccess.getErrorAccess().getLeftParenthesisKeyword_1());
                
            // InternalAsamParser.g:2808:1: ( ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) ) )
            // InternalAsamParser.g:2809:1: ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) )
            {
            // InternalAsamParser.g:2809:1: ( (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible ) )
            // InternalAsamParser.g:2810:1: (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible )
            {
            // InternalAsamParser.g:2810:1: (lv_severityLevel_2_1= Catastrophic | lv_severityLevel_2_2= Critical | lv_severityLevel_2_3= Marginal | lv_severityLevel_2_4= Negligible )
            int alt11=4;
            switch ( input.LA(1) ) {
            case Catastrophic:
                {
                alt11=1;
                }
                break;
            case Critical:
                {
                alt11=2;
                }
                break;
            case Marginal:
                {
                alt11=3;
                }
                break;
            case Negligible:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalAsamParser.g:2811:3: lv_severityLevel_2_1= Catastrophic
                    {
                    lv_severityLevel_2_1=(Token)match(input,Catastrophic,FollowSets000.FOLLOW_33); 

                            newLeafNode(lv_severityLevel_2_1, grammarAccess.getErrorAccess().getSeverityLevelCatastrophicKeyword_2_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "severityLevel", lv_severityLevel_2_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:2824:8: lv_severityLevel_2_2= Critical
                    {
                    lv_severityLevel_2_2=(Token)match(input,Critical,FollowSets000.FOLLOW_33); 

                            newLeafNode(lv_severityLevel_2_2, grammarAccess.getErrorAccess().getSeverityLevelCriticalKeyword_2_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "severityLevel", lv_severityLevel_2_2, null);
                    	    

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:2837:8: lv_severityLevel_2_3= Marginal
                    {
                    lv_severityLevel_2_3=(Token)match(input,Marginal,FollowSets000.FOLLOW_33); 

                            newLeafNode(lv_severityLevel_2_3, grammarAccess.getErrorAccess().getSeverityLevelMarginalKeyword_2_0_2());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "severityLevel", lv_severityLevel_2_3, null);
                    	    

                    }
                    break;
                case 4 :
                    // InternalAsamParser.g:2850:8: lv_severityLevel_2_4= Negligible
                    {
                    lv_severityLevel_2_4=(Token)match(input,Negligible,FollowSets000.FOLLOW_33); 

                            newLeafNode(lv_severityLevel_2_4, grammarAccess.getErrorAccess().getSeverityLevelNegligibleKeyword_2_0_3());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "severityLevel", lv_severityLevel_2_4, null);
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_3=(Token)match(input,P,FollowSets000.FOLLOW_34); 

                	newLeafNode(otherlv_3, grammarAccess.getErrorAccess().getPKeyword_3());
                
            // InternalAsamParser.g:2871:1: ( ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) ) )
            // InternalAsamParser.g:2872:1: ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) )
            {
            // InternalAsamParser.g:2872:1: ( (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT ) )
            // InternalAsamParser.g:2873:1: (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT )
            {
            // InternalAsamParser.g:2873:1: (lv_probability_4_1= DigitZero | lv_probability_4_2= DigitOne | lv_probability_4_3= RULE_FLOAT )
            int alt12=3;
            switch ( input.LA(1) ) {
            case DigitZero:
                {
                alt12=1;
                }
                break;
            case DigitOne:
                {
                alt12=2;
                }
                break;
            case RULE_FLOAT:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalAsamParser.g:2874:3: lv_probability_4_1= DigitZero
                    {
                    lv_probability_4_1=(Token)match(input,DigitZero,FollowSets000.FOLLOW_35); 

                            newLeafNode(lv_probability_4_1, grammarAccess.getErrorAccess().getProbability0Keyword_4_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "probability", lv_probability_4_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:2887:8: lv_probability_4_2= DigitOne
                    {
                    lv_probability_4_2=(Token)match(input,DigitOne,FollowSets000.FOLLOW_35); 

                            newLeafNode(lv_probability_4_2, grammarAccess.getErrorAccess().getProbability1Keyword_4_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(current, "probability", lv_probability_4_2, null);
                    	    

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:2900:8: lv_probability_4_3= RULE_FLOAT
                    {
                    lv_probability_4_3=(Token)match(input,RULE_FLOAT,FollowSets000.FOLLOW_35); 

                    			newLeafNode(lv_probability_4_3, grammarAccess.getErrorAccess().getProbabilityFLOATTerminalRuleCall_4_0_2()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"probability",
                            		lv_probability_4_3, 
                            		"edu.clemson.asam.Asam.FLOAT");
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_5=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getErrorAccess().getRightParenthesisKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleError"


    // $ANTLR start "entryRuleErrorNoProbability"
    // InternalAsamParser.g:2931:1: entryRuleErrorNoProbability returns [EObject current=null] : iv_ruleErrorNoProbability= ruleErrorNoProbability EOF ;
    public final EObject entryRuleErrorNoProbability() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleErrorNoProbability = null;


        try {
            // InternalAsamParser.g:2932:2: (iv_ruleErrorNoProbability= ruleErrorNoProbability EOF )
            // InternalAsamParser.g:2933:2: iv_ruleErrorNoProbability= ruleErrorNoProbability EOF
            {
             newCompositeNode(grammarAccess.getErrorNoProbabilityRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleErrorNoProbability=ruleErrorNoProbability();

            state._fsp--;

             current =iv_ruleErrorNoProbability; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleErrorNoProbability"


    // $ANTLR start "ruleErrorNoProbability"
    // InternalAsamParser.g:2940:1: ruleErrorNoProbability returns [EObject current=null] : ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) ) ;
    public final EObject ruleErrorNoProbability() throws RecognitionException {
        EObject current = null;

        Token lv_error_0_1=null;
        Token lv_error_0_2=null;
        Token lv_error_0_3=null;
        Token lv_error_0_4=null;
        Token lv_error_0_5=null;
        Token lv_error_0_6=null;
        Token lv_error_0_7=null;
        Token lv_error_0_8=null;
        Token lv_error_0_9=null;
        Token lv_error_0_10=null;
        Token lv_error_0_11=null;
        Token lv_error_0_12=null;
        Token lv_error_0_13=null;
        Token lv_error_0_14=null;
        Token lv_error_0_15=null;
        Token lv_error_0_16=null;
        Token lv_error_0_17=null;
        Token lv_error_0_18=null;
        Token lv_error_0_19=null;
        Token lv_error_0_20=null;
        Token lv_error_0_21=null;
        Token lv_error_0_22=null;
        Token lv_error_0_23=null;
        Token lv_error_0_24=null;
        Token lv_error_0_25=null;
        Token lv_error_0_26=null;
        Token lv_error_0_27=null;
        Token lv_error_0_28=null;
        Token lv_error_0_29=null;
        Token lv_error_0_30=null;
        Token lv_error_0_31=null;
        Token lv_error_0_32=null;
        Token lv_error_0_33=null;
        Token lv_error_0_34=null;
        Token lv_error_0_35=null;
        Token lv_error_0_36=null;
        Token lv_error_0_37=null;
        Token lv_error_0_38=null;
        Token lv_error_0_39=null;
        Token lv_error_0_40=null;
        Token lv_error_0_41=null;
        Token lv_error_0_42=null;
        Token lv_error_0_43=null;
        Token lv_error_0_44=null;
        Token lv_error_0_45=null;
        Token lv_error_0_46=null;
        Token lv_error_0_47=null;
        Token lv_error_0_48=null;
        Token lv_error_0_49=null;
        Token lv_error_0_50=null;
        Token lv_error_0_51=null;
        Token lv_error_0_52=null;
        Token lv_error_0_53=null;
        Token lv_error_0_54=null;
        Token lv_error_0_55=null;
        Token lv_error_0_56=null;
        Token lv_error_0_57=null;
        Token lv_error_0_58=null;
        Token lv_error_0_59=null;
        Token lv_error_0_60=null;
        Token lv_error_0_61=null;
        Token lv_error_0_62=null;
        Token lv_error_0_63=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:2943:28: ( ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) ) )
            // InternalAsamParser.g:2944:1: ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) )
            {
            // InternalAsamParser.g:2944:1: ( ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) ) )
            // InternalAsamParser.g:2945:1: ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) )
            {
            // InternalAsamParser.g:2945:1: ( (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID ) )
            // InternalAsamParser.g:2946:1: (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID )
            {
            // InternalAsamParser.g:2946:1: (lv_error_0_1= ServiceError | lv_error_0_2= ItemOmission | lv_error_0_3= ServiceOmission | lv_error_0_4= SequenceOmission | lv_error_0_5= TransientServiceOmission | lv_error_0_6= LateServiceStart | lv_error_0_7= EarlyServiceTermination | lv_error_0_8= BoundedOmissionInterval | lv_error_0_9= ItemCommission | lv_error_0_10= ServiceCommission | lv_error_0_11= SequenceCommission | lv_error_0_12= EarlyServiceStart | lv_error_0_13= LateServiceTermination | lv_error_0_14= TimingRelatedError | lv_error_0_15= ItemTimingError | lv_error_0_16= EarlyDelivery | lv_error_0_17= LateDelivery | lv_error_0_18= SequenceTimingError | lv_error_0_19= HighRate | lv_error_0_20= LowRate | lv_error_0_21= RateJitter | lv_error_0_22= ServiceTimingError | lv_error_0_23= DelayedService | lv_error_0_24= EarlyService | lv_error_0_25= ValueRelatedError | lv_error_0_26= ItemValueError | lv_error_0_27= UndetectableValueError | lv_error_0_28= DetectableValueError | lv_error_0_29= OutOfRange | lv_error_0_30= BelowRange | lv_error_0_31= AboveRange | lv_error_0_32= OutOfBounds | lv_error_0_33= SequenceValueError | lv_error_0_34= BoundedValueChange | lv_error_0_35= StuckValue | lv_error_0_36= OutOfOrder | lv_error_0_37= ServiceValueError | lv_error_0_38= OutOfCalibration | lv_error_0_39= ReplicationError | lv_error_0_40= AsymmetricReplicatesError | lv_error_0_41= AsymmetricValue | lv_error_0_42= AsymmetricApproximateValue | lv_error_0_43= AsymmetricExactValue | lv_error_0_44= AsymmetricTiming | lv_error_0_45= AsymmetricOmission | lv_error_0_46= AsymmetricItemOmission | lv_error_0_47= AsymmetricServiceOmission | lv_error_0_48= SymmetricReplicatesError | lv_error_0_49= SymmetricValue | lv_error_0_50= SymmetricApproximateValue | lv_error_0_51= SymmetricExactValue | lv_error_0_52= SymmetricTiming | lv_error_0_53= SymmetricOmission | lv_error_0_54= SymmetricItemOmission | lv_error_0_55= SymmetricServiceOmission | lv_error_0_56= ConcurrencyError | lv_error_0_57= RaceCondition | lv_error_0_58= ReadWriteRace | lv_error_0_59= WriteWriteRace | lv_error_0_60= MutExError | lv_error_0_61= Deadlock | lv_error_0_62= Starvation | lv_error_0_63= RULE_ID )
            int alt13=63;
            switch ( input.LA(1) ) {
            case ServiceError:
                {
                alt13=1;
                }
                break;
            case ItemOmission:
                {
                alt13=2;
                }
                break;
            case ServiceOmission:
                {
                alt13=3;
                }
                break;
            case SequenceOmission:
                {
                alt13=4;
                }
                break;
            case TransientServiceOmission:
                {
                alt13=5;
                }
                break;
            case LateServiceStart:
                {
                alt13=6;
                }
                break;
            case EarlyServiceTermination:
                {
                alt13=7;
                }
                break;
            case BoundedOmissionInterval:
                {
                alt13=8;
                }
                break;
            case ItemCommission:
                {
                alt13=9;
                }
                break;
            case ServiceCommission:
                {
                alt13=10;
                }
                break;
            case SequenceCommission:
                {
                alt13=11;
                }
                break;
            case EarlyServiceStart:
                {
                alt13=12;
                }
                break;
            case LateServiceTermination:
                {
                alt13=13;
                }
                break;
            case TimingRelatedError:
                {
                alt13=14;
                }
                break;
            case ItemTimingError:
                {
                alt13=15;
                }
                break;
            case EarlyDelivery:
                {
                alt13=16;
                }
                break;
            case LateDelivery:
                {
                alt13=17;
                }
                break;
            case SequenceTimingError:
                {
                alt13=18;
                }
                break;
            case HighRate:
                {
                alt13=19;
                }
                break;
            case LowRate:
                {
                alt13=20;
                }
                break;
            case RateJitter:
                {
                alt13=21;
                }
                break;
            case ServiceTimingError:
                {
                alt13=22;
                }
                break;
            case DelayedService:
                {
                alt13=23;
                }
                break;
            case EarlyService:
                {
                alt13=24;
                }
                break;
            case ValueRelatedError:
                {
                alt13=25;
                }
                break;
            case ItemValueError:
                {
                alt13=26;
                }
                break;
            case UndetectableValueError:
                {
                alt13=27;
                }
                break;
            case DetectableValueError:
                {
                alt13=28;
                }
                break;
            case OutOfRange:
                {
                alt13=29;
                }
                break;
            case BelowRange:
                {
                alt13=30;
                }
                break;
            case AboveRange:
                {
                alt13=31;
                }
                break;
            case OutOfBounds:
                {
                alt13=32;
                }
                break;
            case SequenceValueError:
                {
                alt13=33;
                }
                break;
            case BoundedValueChange:
                {
                alt13=34;
                }
                break;
            case StuckValue:
                {
                alt13=35;
                }
                break;
            case OutOfOrder:
                {
                alt13=36;
                }
                break;
            case ServiceValueError:
                {
                alt13=37;
                }
                break;
            case OutOfCalibration:
                {
                alt13=38;
                }
                break;
            case ReplicationError:
                {
                alt13=39;
                }
                break;
            case AsymmetricReplicatesError:
                {
                alt13=40;
                }
                break;
            case AsymmetricValue:
                {
                alt13=41;
                }
                break;
            case AsymmetricApproximateValue:
                {
                alt13=42;
                }
                break;
            case AsymmetricExactValue:
                {
                alt13=43;
                }
                break;
            case AsymmetricTiming:
                {
                alt13=44;
                }
                break;
            case AsymmetricOmission:
                {
                alt13=45;
                }
                break;
            case AsymmetricItemOmission:
                {
                alt13=46;
                }
                break;
            case AsymmetricServiceOmission:
                {
                alt13=47;
                }
                break;
            case SymmetricReplicatesError:
                {
                alt13=48;
                }
                break;
            case SymmetricValue:
                {
                alt13=49;
                }
                break;
            case SymmetricApproximateValue:
                {
                alt13=50;
                }
                break;
            case SymmetricExactValue:
                {
                alt13=51;
                }
                break;
            case SymmetricTiming:
                {
                alt13=52;
                }
                break;
            case SymmetricOmission:
                {
                alt13=53;
                }
                break;
            case SymmetricItemOmission:
                {
                alt13=54;
                }
                break;
            case SymmetricServiceOmission:
                {
                alt13=55;
                }
                break;
            case ConcurrencyError:
                {
                alt13=56;
                }
                break;
            case RaceCondition:
                {
                alt13=57;
                }
                break;
            case ReadWriteRace:
                {
                alt13=58;
                }
                break;
            case WriteWriteRace:
                {
                alt13=59;
                }
                break;
            case MutExError:
                {
                alt13=60;
                }
                break;
            case Deadlock:
                {
                alt13=61;
                }
                break;
            case Starvation:
                {
                alt13=62;
                }
                break;
            case RULE_ID:
                {
                alt13=63;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalAsamParser.g:2947:3: lv_error_0_1= ServiceError
                    {
                    lv_error_0_1=(Token)match(input,ServiceError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_1, grammarAccess.getErrorNoProbabilityAccess().getErrorServiceErrorKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_1, null);
                    	    

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:2960:8: lv_error_0_2= ItemOmission
                    {
                    lv_error_0_2=(Token)match(input,ItemOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_2, grammarAccess.getErrorNoProbabilityAccess().getErrorItemOmissionKeyword_0_1());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_2, null);
                    	    

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:2973:8: lv_error_0_3= ServiceOmission
                    {
                    lv_error_0_3=(Token)match(input,ServiceOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_3, grammarAccess.getErrorNoProbabilityAccess().getErrorServiceOmissionKeyword_0_2());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_3, null);
                    	    

                    }
                    break;
                case 4 :
                    // InternalAsamParser.g:2986:8: lv_error_0_4= SequenceOmission
                    {
                    lv_error_0_4=(Token)match(input,SequenceOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_4, grammarAccess.getErrorNoProbabilityAccess().getErrorSequenceOmissionKeyword_0_3());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_4, null);
                    	    

                    }
                    break;
                case 5 :
                    // InternalAsamParser.g:2999:8: lv_error_0_5= TransientServiceOmission
                    {
                    lv_error_0_5=(Token)match(input,TransientServiceOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_5, grammarAccess.getErrorNoProbabilityAccess().getErrorTransientServiceOmissionKeyword_0_4());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_5, null);
                    	    

                    }
                    break;
                case 6 :
                    // InternalAsamParser.g:3012:8: lv_error_0_6= LateServiceStart
                    {
                    lv_error_0_6=(Token)match(input,LateServiceStart,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_6, grammarAccess.getErrorNoProbabilityAccess().getErrorLateServiceStartKeyword_0_5());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_6, null);
                    	    

                    }
                    break;
                case 7 :
                    // InternalAsamParser.g:3025:8: lv_error_0_7= EarlyServiceTermination
                    {
                    lv_error_0_7=(Token)match(input,EarlyServiceTermination,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_7, grammarAccess.getErrorNoProbabilityAccess().getErrorEarlyServiceTerminationKeyword_0_6());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_7, null);
                    	    

                    }
                    break;
                case 8 :
                    // InternalAsamParser.g:3038:8: lv_error_0_8= BoundedOmissionInterval
                    {
                    lv_error_0_8=(Token)match(input,BoundedOmissionInterval,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_8, grammarAccess.getErrorNoProbabilityAccess().getErrorBoundedOmissionIntervalKeyword_0_7());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_8, null);
                    	    

                    }
                    break;
                case 9 :
                    // InternalAsamParser.g:3051:8: lv_error_0_9= ItemCommission
                    {
                    lv_error_0_9=(Token)match(input,ItemCommission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_9, grammarAccess.getErrorNoProbabilityAccess().getErrorItemCommissionKeyword_0_8());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_9, null);
                    	    

                    }
                    break;
                case 10 :
                    // InternalAsamParser.g:3064:8: lv_error_0_10= ServiceCommission
                    {
                    lv_error_0_10=(Token)match(input,ServiceCommission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_10, grammarAccess.getErrorNoProbabilityAccess().getErrorServiceCommissionKeyword_0_9());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_10, null);
                    	    

                    }
                    break;
                case 11 :
                    // InternalAsamParser.g:3077:8: lv_error_0_11= SequenceCommission
                    {
                    lv_error_0_11=(Token)match(input,SequenceCommission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_11, grammarAccess.getErrorNoProbabilityAccess().getErrorSequenceCommissionKeyword_0_10());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_11, null);
                    	    

                    }
                    break;
                case 12 :
                    // InternalAsamParser.g:3090:8: lv_error_0_12= EarlyServiceStart
                    {
                    lv_error_0_12=(Token)match(input,EarlyServiceStart,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_12, grammarAccess.getErrorNoProbabilityAccess().getErrorEarlyServiceStartKeyword_0_11());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_12, null);
                    	    

                    }
                    break;
                case 13 :
                    // InternalAsamParser.g:3103:8: lv_error_0_13= LateServiceTermination
                    {
                    lv_error_0_13=(Token)match(input,LateServiceTermination,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_13, grammarAccess.getErrorNoProbabilityAccess().getErrorLateServiceTerminationKeyword_0_12());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_13, null);
                    	    

                    }
                    break;
                case 14 :
                    // InternalAsamParser.g:3116:8: lv_error_0_14= TimingRelatedError
                    {
                    lv_error_0_14=(Token)match(input,TimingRelatedError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_14, grammarAccess.getErrorNoProbabilityAccess().getErrorTimingRelatedErrorKeyword_0_13());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_14, null);
                    	    

                    }
                    break;
                case 15 :
                    // InternalAsamParser.g:3129:8: lv_error_0_15= ItemTimingError
                    {
                    lv_error_0_15=(Token)match(input,ItemTimingError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_15, grammarAccess.getErrorNoProbabilityAccess().getErrorItemTimingErrorKeyword_0_14());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_15, null);
                    	    

                    }
                    break;
                case 16 :
                    // InternalAsamParser.g:3142:8: lv_error_0_16= EarlyDelivery
                    {
                    lv_error_0_16=(Token)match(input,EarlyDelivery,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_16, grammarAccess.getErrorNoProbabilityAccess().getErrorEarlyDeliveryKeyword_0_15());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_16, null);
                    	    

                    }
                    break;
                case 17 :
                    // InternalAsamParser.g:3155:8: lv_error_0_17= LateDelivery
                    {
                    lv_error_0_17=(Token)match(input,LateDelivery,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_17, grammarAccess.getErrorNoProbabilityAccess().getErrorLateDeliveryKeyword_0_16());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_17, null);
                    	    

                    }
                    break;
                case 18 :
                    // InternalAsamParser.g:3168:8: lv_error_0_18= SequenceTimingError
                    {
                    lv_error_0_18=(Token)match(input,SequenceTimingError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_18, grammarAccess.getErrorNoProbabilityAccess().getErrorSequenceTimingErrorKeyword_0_17());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_18, null);
                    	    

                    }
                    break;
                case 19 :
                    // InternalAsamParser.g:3181:8: lv_error_0_19= HighRate
                    {
                    lv_error_0_19=(Token)match(input,HighRate,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_19, grammarAccess.getErrorNoProbabilityAccess().getErrorHighRateKeyword_0_18());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_19, null);
                    	    

                    }
                    break;
                case 20 :
                    // InternalAsamParser.g:3194:8: lv_error_0_20= LowRate
                    {
                    lv_error_0_20=(Token)match(input,LowRate,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_20, grammarAccess.getErrorNoProbabilityAccess().getErrorLowRateKeyword_0_19());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_20, null);
                    	    

                    }
                    break;
                case 21 :
                    // InternalAsamParser.g:3207:8: lv_error_0_21= RateJitter
                    {
                    lv_error_0_21=(Token)match(input,RateJitter,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_21, grammarAccess.getErrorNoProbabilityAccess().getErrorRateJitterKeyword_0_20());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_21, null);
                    	    

                    }
                    break;
                case 22 :
                    // InternalAsamParser.g:3220:8: lv_error_0_22= ServiceTimingError
                    {
                    lv_error_0_22=(Token)match(input,ServiceTimingError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_22, grammarAccess.getErrorNoProbabilityAccess().getErrorServiceTimingErrorKeyword_0_21());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_22, null);
                    	    

                    }
                    break;
                case 23 :
                    // InternalAsamParser.g:3233:8: lv_error_0_23= DelayedService
                    {
                    lv_error_0_23=(Token)match(input,DelayedService,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_23, grammarAccess.getErrorNoProbabilityAccess().getErrorDelayedServiceKeyword_0_22());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_23, null);
                    	    

                    }
                    break;
                case 24 :
                    // InternalAsamParser.g:3246:8: lv_error_0_24= EarlyService
                    {
                    lv_error_0_24=(Token)match(input,EarlyService,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_24, grammarAccess.getErrorNoProbabilityAccess().getErrorEarlyServiceKeyword_0_23());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_24, null);
                    	    

                    }
                    break;
                case 25 :
                    // InternalAsamParser.g:3259:8: lv_error_0_25= ValueRelatedError
                    {
                    lv_error_0_25=(Token)match(input,ValueRelatedError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_25, grammarAccess.getErrorNoProbabilityAccess().getErrorValueRelatedErrorKeyword_0_24());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_25, null);
                    	    

                    }
                    break;
                case 26 :
                    // InternalAsamParser.g:3272:8: lv_error_0_26= ItemValueError
                    {
                    lv_error_0_26=(Token)match(input,ItemValueError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_26, grammarAccess.getErrorNoProbabilityAccess().getErrorItemValueErrorKeyword_0_25());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_26, null);
                    	    

                    }
                    break;
                case 27 :
                    // InternalAsamParser.g:3285:8: lv_error_0_27= UndetectableValueError
                    {
                    lv_error_0_27=(Token)match(input,UndetectableValueError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_27, grammarAccess.getErrorNoProbabilityAccess().getErrorUndetectableValueErrorKeyword_0_26());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_27, null);
                    	    

                    }
                    break;
                case 28 :
                    // InternalAsamParser.g:3298:8: lv_error_0_28= DetectableValueError
                    {
                    lv_error_0_28=(Token)match(input,DetectableValueError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_28, grammarAccess.getErrorNoProbabilityAccess().getErrorDetectableValueErrorKeyword_0_27());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_28, null);
                    	    

                    }
                    break;
                case 29 :
                    // InternalAsamParser.g:3311:8: lv_error_0_29= OutOfRange
                    {
                    lv_error_0_29=(Token)match(input,OutOfRange,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_29, grammarAccess.getErrorNoProbabilityAccess().getErrorOutOfRangeKeyword_0_28());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_29, null);
                    	    

                    }
                    break;
                case 30 :
                    // InternalAsamParser.g:3324:8: lv_error_0_30= BelowRange
                    {
                    lv_error_0_30=(Token)match(input,BelowRange,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_30, grammarAccess.getErrorNoProbabilityAccess().getErrorBelowRangeKeyword_0_29());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_30, null);
                    	    

                    }
                    break;
                case 31 :
                    // InternalAsamParser.g:3337:8: lv_error_0_31= AboveRange
                    {
                    lv_error_0_31=(Token)match(input,AboveRange,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_31, grammarAccess.getErrorNoProbabilityAccess().getErrorAboveRangeKeyword_0_30());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_31, null);
                    	    

                    }
                    break;
                case 32 :
                    // InternalAsamParser.g:3350:8: lv_error_0_32= OutOfBounds
                    {
                    lv_error_0_32=(Token)match(input,OutOfBounds,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_32, grammarAccess.getErrorNoProbabilityAccess().getErrorOutOfBoundsKeyword_0_31());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_32, null);
                    	    

                    }
                    break;
                case 33 :
                    // InternalAsamParser.g:3363:8: lv_error_0_33= SequenceValueError
                    {
                    lv_error_0_33=(Token)match(input,SequenceValueError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_33, grammarAccess.getErrorNoProbabilityAccess().getErrorSequenceValueErrorKeyword_0_32());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_33, null);
                    	    

                    }
                    break;
                case 34 :
                    // InternalAsamParser.g:3376:8: lv_error_0_34= BoundedValueChange
                    {
                    lv_error_0_34=(Token)match(input,BoundedValueChange,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_34, grammarAccess.getErrorNoProbabilityAccess().getErrorBoundedValueChangeKeyword_0_33());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_34, null);
                    	    

                    }
                    break;
                case 35 :
                    // InternalAsamParser.g:3389:8: lv_error_0_35= StuckValue
                    {
                    lv_error_0_35=(Token)match(input,StuckValue,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_35, grammarAccess.getErrorNoProbabilityAccess().getErrorStuckValueKeyword_0_34());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_35, null);
                    	    

                    }
                    break;
                case 36 :
                    // InternalAsamParser.g:3402:8: lv_error_0_36= OutOfOrder
                    {
                    lv_error_0_36=(Token)match(input,OutOfOrder,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_36, grammarAccess.getErrorNoProbabilityAccess().getErrorOutOfOrderKeyword_0_35());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_36, null);
                    	    

                    }
                    break;
                case 37 :
                    // InternalAsamParser.g:3415:8: lv_error_0_37= ServiceValueError
                    {
                    lv_error_0_37=(Token)match(input,ServiceValueError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_37, grammarAccess.getErrorNoProbabilityAccess().getErrorServiceValueErrorKeyword_0_36());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_37, null);
                    	    

                    }
                    break;
                case 38 :
                    // InternalAsamParser.g:3428:8: lv_error_0_38= OutOfCalibration
                    {
                    lv_error_0_38=(Token)match(input,OutOfCalibration,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_38, grammarAccess.getErrorNoProbabilityAccess().getErrorOutOfCalibrationKeyword_0_37());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_38, null);
                    	    

                    }
                    break;
                case 39 :
                    // InternalAsamParser.g:3441:8: lv_error_0_39= ReplicationError
                    {
                    lv_error_0_39=(Token)match(input,ReplicationError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_39, grammarAccess.getErrorNoProbabilityAccess().getErrorReplicationErrorKeyword_0_38());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_39, null);
                    	    

                    }
                    break;
                case 40 :
                    // InternalAsamParser.g:3454:8: lv_error_0_40= AsymmetricReplicatesError
                    {
                    lv_error_0_40=(Token)match(input,AsymmetricReplicatesError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_40, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricReplicatesErrorKeyword_0_39());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_40, null);
                    	    

                    }
                    break;
                case 41 :
                    // InternalAsamParser.g:3467:8: lv_error_0_41= AsymmetricValue
                    {
                    lv_error_0_41=(Token)match(input,AsymmetricValue,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_41, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricValueKeyword_0_40());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_41, null);
                    	    

                    }
                    break;
                case 42 :
                    // InternalAsamParser.g:3480:8: lv_error_0_42= AsymmetricApproximateValue
                    {
                    lv_error_0_42=(Token)match(input,AsymmetricApproximateValue,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_42, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricApproximateValueKeyword_0_41());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_42, null);
                    	    

                    }
                    break;
                case 43 :
                    // InternalAsamParser.g:3493:8: lv_error_0_43= AsymmetricExactValue
                    {
                    lv_error_0_43=(Token)match(input,AsymmetricExactValue,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_43, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricExactValueKeyword_0_42());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_43, null);
                    	    

                    }
                    break;
                case 44 :
                    // InternalAsamParser.g:3506:8: lv_error_0_44= AsymmetricTiming
                    {
                    lv_error_0_44=(Token)match(input,AsymmetricTiming,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_44, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricTimingKeyword_0_43());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_44, null);
                    	    

                    }
                    break;
                case 45 :
                    // InternalAsamParser.g:3519:8: lv_error_0_45= AsymmetricOmission
                    {
                    lv_error_0_45=(Token)match(input,AsymmetricOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_45, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricOmissionKeyword_0_44());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_45, null);
                    	    

                    }
                    break;
                case 46 :
                    // InternalAsamParser.g:3532:8: lv_error_0_46= AsymmetricItemOmission
                    {
                    lv_error_0_46=(Token)match(input,AsymmetricItemOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_46, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricItemOmissionKeyword_0_45());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_46, null);
                    	    

                    }
                    break;
                case 47 :
                    // InternalAsamParser.g:3545:8: lv_error_0_47= AsymmetricServiceOmission
                    {
                    lv_error_0_47=(Token)match(input,AsymmetricServiceOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_47, grammarAccess.getErrorNoProbabilityAccess().getErrorAsymmetricServiceOmissionKeyword_0_46());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_47, null);
                    	    

                    }
                    break;
                case 48 :
                    // InternalAsamParser.g:3558:8: lv_error_0_48= SymmetricReplicatesError
                    {
                    lv_error_0_48=(Token)match(input,SymmetricReplicatesError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_48, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricReplicatesErrorKeyword_0_47());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_48, null);
                    	    

                    }
                    break;
                case 49 :
                    // InternalAsamParser.g:3571:8: lv_error_0_49= SymmetricValue
                    {
                    lv_error_0_49=(Token)match(input,SymmetricValue,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_49, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricValueKeyword_0_48());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_49, null);
                    	    

                    }
                    break;
                case 50 :
                    // InternalAsamParser.g:3584:8: lv_error_0_50= SymmetricApproximateValue
                    {
                    lv_error_0_50=(Token)match(input,SymmetricApproximateValue,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_50, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricApproximateValueKeyword_0_49());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_50, null);
                    	    

                    }
                    break;
                case 51 :
                    // InternalAsamParser.g:3597:8: lv_error_0_51= SymmetricExactValue
                    {
                    lv_error_0_51=(Token)match(input,SymmetricExactValue,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_51, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricExactValueKeyword_0_50());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_51, null);
                    	    

                    }
                    break;
                case 52 :
                    // InternalAsamParser.g:3610:8: lv_error_0_52= SymmetricTiming
                    {
                    lv_error_0_52=(Token)match(input,SymmetricTiming,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_52, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricTimingKeyword_0_51());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_52, null);
                    	    

                    }
                    break;
                case 53 :
                    // InternalAsamParser.g:3623:8: lv_error_0_53= SymmetricOmission
                    {
                    lv_error_0_53=(Token)match(input,SymmetricOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_53, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricOmissionKeyword_0_52());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_53, null);
                    	    

                    }
                    break;
                case 54 :
                    // InternalAsamParser.g:3636:8: lv_error_0_54= SymmetricItemOmission
                    {
                    lv_error_0_54=(Token)match(input,SymmetricItemOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_54, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricItemOmissionKeyword_0_53());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_54, null);
                    	    

                    }
                    break;
                case 55 :
                    // InternalAsamParser.g:3649:8: lv_error_0_55= SymmetricServiceOmission
                    {
                    lv_error_0_55=(Token)match(input,SymmetricServiceOmission,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_55, grammarAccess.getErrorNoProbabilityAccess().getErrorSymmetricServiceOmissionKeyword_0_54());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_55, null);
                    	    

                    }
                    break;
                case 56 :
                    // InternalAsamParser.g:3662:8: lv_error_0_56= ConcurrencyError
                    {
                    lv_error_0_56=(Token)match(input,ConcurrencyError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_56, grammarAccess.getErrorNoProbabilityAccess().getErrorConcurrencyErrorKeyword_0_55());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_56, null);
                    	    

                    }
                    break;
                case 57 :
                    // InternalAsamParser.g:3675:8: lv_error_0_57= RaceCondition
                    {
                    lv_error_0_57=(Token)match(input,RaceCondition,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_57, grammarAccess.getErrorNoProbabilityAccess().getErrorRaceConditionKeyword_0_56());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_57, null);
                    	    

                    }
                    break;
                case 58 :
                    // InternalAsamParser.g:3688:8: lv_error_0_58= ReadWriteRace
                    {
                    lv_error_0_58=(Token)match(input,ReadWriteRace,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_58, grammarAccess.getErrorNoProbabilityAccess().getErrorReadWriteRaceKeyword_0_57());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_58, null);
                    	    

                    }
                    break;
                case 59 :
                    // InternalAsamParser.g:3701:8: lv_error_0_59= WriteWriteRace
                    {
                    lv_error_0_59=(Token)match(input,WriteWriteRace,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_59, grammarAccess.getErrorNoProbabilityAccess().getErrorWriteWriteRaceKeyword_0_58());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_59, null);
                    	    

                    }
                    break;
                case 60 :
                    // InternalAsamParser.g:3714:8: lv_error_0_60= MutExError
                    {
                    lv_error_0_60=(Token)match(input,MutExError,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_60, grammarAccess.getErrorNoProbabilityAccess().getErrorMutExErrorKeyword_0_59());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_60, null);
                    	    

                    }
                    break;
                case 61 :
                    // InternalAsamParser.g:3727:8: lv_error_0_61= Deadlock
                    {
                    lv_error_0_61=(Token)match(input,Deadlock,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_61, grammarAccess.getErrorNoProbabilityAccess().getErrorDeadlockKeyword_0_60());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_61, null);
                    	    

                    }
                    break;
                case 62 :
                    // InternalAsamParser.g:3740:8: lv_error_0_62= Starvation
                    {
                    lv_error_0_62=(Token)match(input,Starvation,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_error_0_62, grammarAccess.getErrorNoProbabilityAccess().getErrorStarvationKeyword_0_61());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(current, "error", lv_error_0_62, null);
                    	    

                    }
                    break;
                case 63 :
                    // InternalAsamParser.g:3753:8: lv_error_0_63= RULE_ID
                    {
                    lv_error_0_63=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    			newLeafNode(lv_error_0_63, grammarAccess.getErrorNoProbabilityAccess().getErrorIDTerminalRuleCall_0_62()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getErrorNoProbabilityRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"error",
                            		lv_error_0_63, 
                            		"org.osate.xtext.aadl2.properties.Properties.ID");
                    	    

                    }
                    break;

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleErrorNoProbability"


    // $ANTLR start "entryRuleContainedPropertyAssociation"
    // InternalAsamParser.g:3781:1: entryRuleContainedPropertyAssociation returns [EObject current=null] : iv_ruleContainedPropertyAssociation= ruleContainedPropertyAssociation EOF ;
    public final EObject entryRuleContainedPropertyAssociation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContainedPropertyAssociation = null;


        try {
            // InternalAsamParser.g:3782:2: (iv_ruleContainedPropertyAssociation= ruleContainedPropertyAssociation EOF )
            // InternalAsamParser.g:3783:2: iv_ruleContainedPropertyAssociation= ruleContainedPropertyAssociation EOF
            {
             newCompositeNode(grammarAccess.getContainedPropertyAssociationRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleContainedPropertyAssociation=ruleContainedPropertyAssociation();

            state._fsp--;

             current =iv_ruleContainedPropertyAssociation; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContainedPropertyAssociation"


    // $ANTLR start "ruleContainedPropertyAssociation"
    // InternalAsamParser.g:3790:1: ruleContainedPropertyAssociation returns [EObject current=null] : ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) (otherlv_7= Applies otherlv_8= To ( (lv_appliesTo_9_0= ruleContainmentPath ) ) (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )* )? (otherlv_12= In otherlv_13= Binding otherlv_14= LeftParenthesis ( ( ruleQCREF ) ) otherlv_16= RightParenthesis )? otherlv_17= Semicolon ) ;
    public final EObject ruleContainedPropertyAssociation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_append_2_0=null;
        Token lv_constant_3_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        EObject lv_ownedValue_4_0 = null;

        EObject lv_ownedValue_6_0 = null;

        EObject lv_appliesTo_9_0 = null;

        EObject lv_appliesTo_11_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:3793:28: ( ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) (otherlv_7= Applies otherlv_8= To ( (lv_appliesTo_9_0= ruleContainmentPath ) ) (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )* )? (otherlv_12= In otherlv_13= Binding otherlv_14= LeftParenthesis ( ( ruleQCREF ) ) otherlv_16= RightParenthesis )? otherlv_17= Semicolon ) )
            // InternalAsamParser.g:3794:1: ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) (otherlv_7= Applies otherlv_8= To ( (lv_appliesTo_9_0= ruleContainmentPath ) ) (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )* )? (otherlv_12= In otherlv_13= Binding otherlv_14= LeftParenthesis ( ( ruleQCREF ) ) otherlv_16= RightParenthesis )? otherlv_17= Semicolon )
            {
            // InternalAsamParser.g:3794:1: ( ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) (otherlv_7= Applies otherlv_8= To ( (lv_appliesTo_9_0= ruleContainmentPath ) ) (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )* )? (otherlv_12= In otherlv_13= Binding otherlv_14= LeftParenthesis ( ( ruleQCREF ) ) otherlv_16= RightParenthesis )? otherlv_17= Semicolon )
            // InternalAsamParser.g:3794:2: ( ( ruleQPREF ) ) (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) ) ( (lv_constant_3_0= Constant ) )? ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* ) (otherlv_7= Applies otherlv_8= To ( (lv_appliesTo_9_0= ruleContainmentPath ) ) (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )* )? (otherlv_12= In otherlv_13= Binding otherlv_14= LeftParenthesis ( ( ruleQCREF ) ) otherlv_16= RightParenthesis )? otherlv_17= Semicolon
            {
            // InternalAsamParser.g:3794:2: ( ( ruleQPREF ) )
            // InternalAsamParser.g:3795:1: ( ruleQPREF )
            {
            // InternalAsamParser.g:3795:1: ( ruleQPREF )
            // InternalAsamParser.g:3796:3: ruleQPREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getPropertyPropertyCrossReference_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_36);
            ruleQPREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:3810:2: (otherlv_1= EqualsSignGreaterThanSign | ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==EqualsSignGreaterThanSign) ) {
                alt14=1;
            }
            else if ( (LA14_0==PlusSignEqualsSignGreaterThanSign) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalAsamParser.g:3811:2: otherlv_1= EqualsSignGreaterThanSign
                    {
                    otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_37); 

                        	newLeafNode(otherlv_1, grammarAccess.getContainedPropertyAssociationAccess().getEqualsSignGreaterThanSignKeyword_1_0());
                        

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:3816:6: ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) )
                    {
                    // InternalAsamParser.g:3816:6: ( (lv_append_2_0= PlusSignEqualsSignGreaterThanSign ) )
                    // InternalAsamParser.g:3817:1: (lv_append_2_0= PlusSignEqualsSignGreaterThanSign )
                    {
                    // InternalAsamParser.g:3817:1: (lv_append_2_0= PlusSignEqualsSignGreaterThanSign )
                    // InternalAsamParser.g:3818:3: lv_append_2_0= PlusSignEqualsSignGreaterThanSign
                    {
                    lv_append_2_0=(Token)match(input,PlusSignEqualsSignGreaterThanSign,FollowSets000.FOLLOW_37); 

                            newLeafNode(lv_append_2_0, grammarAccess.getContainedPropertyAssociationAccess().getAppendPlusSignEqualsSignGreaterThanSignKeyword_1_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                           		setWithLastConsumed(current, "append", true, "+=>");
                    	    

                    }


                    }


                    }
                    break;

            }

            // InternalAsamParser.g:3832:3: ( (lv_constant_3_0= Constant ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==Constant) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalAsamParser.g:3833:1: (lv_constant_3_0= Constant )
                    {
                    // InternalAsamParser.g:3833:1: (lv_constant_3_0= Constant )
                    // InternalAsamParser.g:3834:3: lv_constant_3_0= Constant
                    {
                    lv_constant_3_0=(Token)match(input,Constant,FollowSets000.FOLLOW_37); 

                            newLeafNode(lv_constant_3_0, grammarAccess.getContainedPropertyAssociationAccess().getConstantConstantKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                           		setWithLastConsumed(current, "constant", true, "constant");
                    	    

                    }


                    }
                    break;

            }

            // InternalAsamParser.g:3848:3: ( ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )* )
            // InternalAsamParser.g:3848:4: ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) ) (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )*
            {
            // InternalAsamParser.g:3848:4: ( (lv_ownedValue_4_0= ruleOptionalModalPropertyValue ) )
            // InternalAsamParser.g:3849:1: (lv_ownedValue_4_0= ruleOptionalModalPropertyValue )
            {
            // InternalAsamParser.g:3849:1: (lv_ownedValue_4_0= ruleOptionalModalPropertyValue )
            // InternalAsamParser.g:3850:3: lv_ownedValue_4_0= ruleOptionalModalPropertyValue
            {
             
            	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getOwnedValueOptionalModalPropertyValueParserRuleCall_3_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_38);
            lv_ownedValue_4_0=ruleOptionalModalPropertyValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
            	        }
                   		add(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_4_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.OptionalModalPropertyValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:3866:2: (otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==Comma) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalAsamParser.g:3867:2: otherlv_5= Comma ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) )
            	    {
            	    otherlv_5=(Token)match(input,Comma,FollowSets000.FOLLOW_37); 

            	        	newLeafNode(otherlv_5, grammarAccess.getContainedPropertyAssociationAccess().getCommaKeyword_3_1_0());
            	        
            	    // InternalAsamParser.g:3871:1: ( (lv_ownedValue_6_0= ruleOptionalModalPropertyValue ) )
            	    // InternalAsamParser.g:3872:1: (lv_ownedValue_6_0= ruleOptionalModalPropertyValue )
            	    {
            	    // InternalAsamParser.g:3872:1: (lv_ownedValue_6_0= ruleOptionalModalPropertyValue )
            	    // InternalAsamParser.g:3873:3: lv_ownedValue_6_0= ruleOptionalModalPropertyValue
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getOwnedValueOptionalModalPropertyValueParserRuleCall_3_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_38);
            	    lv_ownedValue_6_0=ruleOptionalModalPropertyValue();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedValue",
            	            		lv_ownedValue_6_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.OptionalModalPropertyValue");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }

            // InternalAsamParser.g:3889:5: (otherlv_7= Applies otherlv_8= To ( (lv_appliesTo_9_0= ruleContainmentPath ) ) (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )* )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==Applies) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalAsamParser.g:3890:2: otherlv_7= Applies otherlv_8= To ( (lv_appliesTo_9_0= ruleContainmentPath ) ) (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )*
                    {
                    otherlv_7=(Token)match(input,Applies,FollowSets000.FOLLOW_39); 

                        	newLeafNode(otherlv_7, grammarAccess.getContainedPropertyAssociationAccess().getAppliesKeyword_4_0());
                        
                    otherlv_8=(Token)match(input,To,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_8, grammarAccess.getContainedPropertyAssociationAccess().getToKeyword_4_1());
                        
                    // InternalAsamParser.g:3899:1: ( (lv_appliesTo_9_0= ruleContainmentPath ) )
                    // InternalAsamParser.g:3900:1: (lv_appliesTo_9_0= ruleContainmentPath )
                    {
                    // InternalAsamParser.g:3900:1: (lv_appliesTo_9_0= ruleContainmentPath )
                    // InternalAsamParser.g:3901:3: lv_appliesTo_9_0= ruleContainmentPath
                    {
                     
                    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getAppliesToContainmentPathParserRuleCall_4_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_40);
                    lv_appliesTo_9_0=ruleContainmentPath();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                           		add(
                           			current, 
                           			"appliesTo",
                            		lv_appliesTo_9_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.ContainmentPath");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalAsamParser.g:3917:2: (otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) ) )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==Comma) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // InternalAsamParser.g:3918:2: otherlv_10= Comma ( (lv_appliesTo_11_0= ruleContainmentPath ) )
                    	    {
                    	    otherlv_10=(Token)match(input,Comma,FollowSets000.FOLLOW_5); 

                    	        	newLeafNode(otherlv_10, grammarAccess.getContainedPropertyAssociationAccess().getCommaKeyword_4_3_0());
                    	        
                    	    // InternalAsamParser.g:3922:1: ( (lv_appliesTo_11_0= ruleContainmentPath ) )
                    	    // InternalAsamParser.g:3923:1: (lv_appliesTo_11_0= ruleContainmentPath )
                    	    {
                    	    // InternalAsamParser.g:3923:1: (lv_appliesTo_11_0= ruleContainmentPath )
                    	    // InternalAsamParser.g:3924:3: lv_appliesTo_11_0= ruleContainmentPath
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getAppliesToContainmentPathParserRuleCall_4_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_40);
                    	    lv_appliesTo_11_0=ruleContainmentPath();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getContainedPropertyAssociationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"appliesTo",
                    	            		lv_appliesTo_11_0, 
                    	            		"org.osate.xtext.aadl2.properties.Properties.ContainmentPath");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalAsamParser.g:3940:6: (otherlv_12= In otherlv_13= Binding otherlv_14= LeftParenthesis ( ( ruleQCREF ) ) otherlv_16= RightParenthesis )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==In) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalAsamParser.g:3941:2: otherlv_12= In otherlv_13= Binding otherlv_14= LeftParenthesis ( ( ruleQCREF ) ) otherlv_16= RightParenthesis
                    {
                    otherlv_12=(Token)match(input,In,FollowSets000.FOLLOW_41); 

                        	newLeafNode(otherlv_12, grammarAccess.getContainedPropertyAssociationAccess().getInKeyword_5_0());
                        
                    otherlv_13=(Token)match(input,Binding,FollowSets000.FOLLOW_31); 

                        	newLeafNode(otherlv_13, grammarAccess.getContainedPropertyAssociationAccess().getBindingKeyword_5_1());
                        
                    otherlv_14=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_14, grammarAccess.getContainedPropertyAssociationAccess().getLeftParenthesisKeyword_5_2());
                        
                    // InternalAsamParser.g:3955:1: ( ( ruleQCREF ) )
                    // InternalAsamParser.g:3956:1: ( ruleQCREF )
                    {
                    // InternalAsamParser.g:3956:1: ( ruleQCREF )
                    // InternalAsamParser.g:3957:3: ruleQCREF
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getContainedPropertyAssociationRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getContainedPropertyAssociationAccess().getInBindingClassifierCrossReference_5_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_35);
                    ruleQCREF();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_16=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_13); 

                        	newLeafNode(otherlv_16, grammarAccess.getContainedPropertyAssociationAccess().getRightParenthesisKeyword_5_4());
                        

                    }
                    break;

            }

            otherlv_17=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_17, grammarAccess.getContainedPropertyAssociationAccess().getSemicolonKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContainedPropertyAssociation"


    // $ANTLR start "entryRuleContainmentPath"
    // InternalAsamParser.g:3993:1: entryRuleContainmentPath returns [EObject current=null] : iv_ruleContainmentPath= ruleContainmentPath EOF ;
    public final EObject entryRuleContainmentPath() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContainmentPath = null;


        try {
            // InternalAsamParser.g:3994:2: (iv_ruleContainmentPath= ruleContainmentPath EOF )
            // InternalAsamParser.g:3995:2: iv_ruleContainmentPath= ruleContainmentPath EOF
            {
             newCompositeNode(grammarAccess.getContainmentPathRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleContainmentPath=ruleContainmentPath();

            state._fsp--;

             current =iv_ruleContainmentPath; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContainmentPath"


    // $ANTLR start "ruleContainmentPath"
    // InternalAsamParser.g:4002:1: ruleContainmentPath returns [EObject current=null] : ( (lv_path_0_0= ruleContainmentPathElement ) ) ;
    public final EObject ruleContainmentPath() throws RecognitionException {
        EObject current = null;

        EObject lv_path_0_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4005:28: ( ( (lv_path_0_0= ruleContainmentPathElement ) ) )
            // InternalAsamParser.g:4006:1: ( (lv_path_0_0= ruleContainmentPathElement ) )
            {
            // InternalAsamParser.g:4006:1: ( (lv_path_0_0= ruleContainmentPathElement ) )
            // InternalAsamParser.g:4007:1: (lv_path_0_0= ruleContainmentPathElement )
            {
            // InternalAsamParser.g:4007:1: (lv_path_0_0= ruleContainmentPathElement )
            // InternalAsamParser.g:4008:3: lv_path_0_0= ruleContainmentPathElement
            {
             
            	        newCompositeNode(grammarAccess.getContainmentPathAccess().getPathContainmentPathElementParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_path_0_0=ruleContainmentPathElement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getContainmentPathRule());
            	        }
                   		set(
                   			current, 
                   			"path",
                    		lv_path_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ContainmentPathElement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContainmentPath"


    // $ANTLR start "entryRuleOptionalModalPropertyValue"
    // InternalAsamParser.g:4034:1: entryRuleOptionalModalPropertyValue returns [EObject current=null] : iv_ruleOptionalModalPropertyValue= ruleOptionalModalPropertyValue EOF ;
    public final EObject entryRuleOptionalModalPropertyValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptionalModalPropertyValue = null;


        try {
            // InternalAsamParser.g:4035:2: (iv_ruleOptionalModalPropertyValue= ruleOptionalModalPropertyValue EOF )
            // InternalAsamParser.g:4036:2: iv_ruleOptionalModalPropertyValue= ruleOptionalModalPropertyValue EOF
            {
             newCompositeNode(grammarAccess.getOptionalModalPropertyValueRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleOptionalModalPropertyValue=ruleOptionalModalPropertyValue();

            state._fsp--;

             current =iv_ruleOptionalModalPropertyValue; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptionalModalPropertyValue"


    // $ANTLR start "ruleOptionalModalPropertyValue"
    // InternalAsamParser.g:4043:1: ruleOptionalModalPropertyValue returns [EObject current=null] : ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) (otherlv_1= In otherlv_2= Modes otherlv_3= LeftParenthesis ( (otherlv_4= RULE_ID ) ) (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )* otherlv_7= RightParenthesis )? ) ;
    public final EObject ruleOptionalModalPropertyValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_ownedValue_0_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4046:28: ( ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) (otherlv_1= In otherlv_2= Modes otherlv_3= LeftParenthesis ( (otherlv_4= RULE_ID ) ) (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )* otherlv_7= RightParenthesis )? ) )
            // InternalAsamParser.g:4047:1: ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) (otherlv_1= In otherlv_2= Modes otherlv_3= LeftParenthesis ( (otherlv_4= RULE_ID ) ) (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )* otherlv_7= RightParenthesis )? )
            {
            // InternalAsamParser.g:4047:1: ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) (otherlv_1= In otherlv_2= Modes otherlv_3= LeftParenthesis ( (otherlv_4= RULE_ID ) ) (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )* otherlv_7= RightParenthesis )? )
            // InternalAsamParser.g:4047:2: ( (lv_ownedValue_0_0= rulePropertyExpression ) ) (otherlv_1= In otherlv_2= Modes otherlv_3= LeftParenthesis ( (otherlv_4= RULE_ID ) ) (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )* otherlv_7= RightParenthesis )?
            {
            // InternalAsamParser.g:4047:2: ( (lv_ownedValue_0_0= rulePropertyExpression ) )
            // InternalAsamParser.g:4048:1: (lv_ownedValue_0_0= rulePropertyExpression )
            {
            // InternalAsamParser.g:4048:1: (lv_ownedValue_0_0= rulePropertyExpression )
            // InternalAsamParser.g:4049:3: lv_ownedValue_0_0= rulePropertyExpression
            {
             
            	        newCompositeNode(grammarAccess.getOptionalModalPropertyValueAccess().getOwnedValuePropertyExpressionParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_42);
            lv_ownedValue_0_0=rulePropertyExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOptionalModalPropertyValueRule());
            	        }
                   		set(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:4065:2: (otherlv_1= In otherlv_2= Modes otherlv_3= LeftParenthesis ( (otherlv_4= RULE_ID ) ) (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )* otherlv_7= RightParenthesis )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==In) ) {
                int LA21_1 = input.LA(2);

                if ( (LA21_1==Modes) ) {
                    alt21=1;
                }
            }
            switch (alt21) {
                case 1 :
                    // InternalAsamParser.g:4066:2: otherlv_1= In otherlv_2= Modes otherlv_3= LeftParenthesis ( (otherlv_4= RULE_ID ) ) (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )* otherlv_7= RightParenthesis
                    {
                    otherlv_1=(Token)match(input,In,FollowSets000.FOLLOW_43); 

                        	newLeafNode(otherlv_1, grammarAccess.getOptionalModalPropertyValueAccess().getInKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,Modes,FollowSets000.FOLLOW_31); 

                        	newLeafNode(otherlv_2, grammarAccess.getOptionalModalPropertyValueAccess().getModesKeyword_1_1());
                        
                    otherlv_3=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_3, grammarAccess.getOptionalModalPropertyValueAccess().getLeftParenthesisKeyword_1_2());
                        
                    // InternalAsamParser.g:4080:1: ( (otherlv_4= RULE_ID ) )
                    // InternalAsamParser.g:4081:1: (otherlv_4= RULE_ID )
                    {
                    // InternalAsamParser.g:4081:1: (otherlv_4= RULE_ID )
                    // InternalAsamParser.g:4082:3: otherlv_4= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getOptionalModalPropertyValueRule());
                    	        }
                            
                    otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_44); 

                    		newLeafNode(otherlv_4, grammarAccess.getOptionalModalPropertyValueAccess().getInModeModeCrossReference_1_3_0()); 
                    	

                    }


                    }

                    // InternalAsamParser.g:4093:2: (otherlv_5= Comma ( (otherlv_6= RULE_ID ) ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==Comma) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalAsamParser.g:4094:2: otherlv_5= Comma ( (otherlv_6= RULE_ID ) )
                    	    {
                    	    otherlv_5=(Token)match(input,Comma,FollowSets000.FOLLOW_5); 

                    	        	newLeafNode(otherlv_5, grammarAccess.getOptionalModalPropertyValueAccess().getCommaKeyword_1_4_0());
                    	        
                    	    // InternalAsamParser.g:4098:1: ( (otherlv_6= RULE_ID ) )
                    	    // InternalAsamParser.g:4099:1: (otherlv_6= RULE_ID )
                    	    {
                    	    // InternalAsamParser.g:4099:1: (otherlv_6= RULE_ID )
                    	    // InternalAsamParser.g:4100:3: otherlv_6= RULE_ID
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getOptionalModalPropertyValueRule());
                    	    	        }
                    	            
                    	    otherlv_6=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_44); 

                    	    		newLeafNode(otherlv_6, grammarAccess.getOptionalModalPropertyValueAccess().getInModeModeCrossReference_1_4_1_0()); 
                    	    	

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_7, grammarAccess.getOptionalModalPropertyValueAccess().getRightParenthesisKeyword_1_5());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptionalModalPropertyValue"


    // $ANTLR start "entryRulePropertyValue"
    // InternalAsamParser.g:4124:1: entryRulePropertyValue returns [EObject current=null] : iv_rulePropertyValue= rulePropertyValue EOF ;
    public final EObject entryRulePropertyValue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyValue = null;


        try {
            // InternalAsamParser.g:4125:2: (iv_rulePropertyValue= rulePropertyValue EOF )
            // InternalAsamParser.g:4126:2: iv_rulePropertyValue= rulePropertyValue EOF
            {
             newCompositeNode(grammarAccess.getPropertyValueRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePropertyValue=rulePropertyValue();

            state._fsp--;

             current =iv_rulePropertyValue; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyValue"


    // $ANTLR start "rulePropertyValue"
    // InternalAsamParser.g:4133:1: rulePropertyValue returns [EObject current=null] : ( (lv_ownedValue_0_0= rulePropertyExpression ) ) ;
    public final EObject rulePropertyValue() throws RecognitionException {
        EObject current = null;

        EObject lv_ownedValue_0_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4136:28: ( ( (lv_ownedValue_0_0= rulePropertyExpression ) ) )
            // InternalAsamParser.g:4137:1: ( (lv_ownedValue_0_0= rulePropertyExpression ) )
            {
            // InternalAsamParser.g:4137:1: ( (lv_ownedValue_0_0= rulePropertyExpression ) )
            // InternalAsamParser.g:4138:1: (lv_ownedValue_0_0= rulePropertyExpression )
            {
            // InternalAsamParser.g:4138:1: (lv_ownedValue_0_0= rulePropertyExpression )
            // InternalAsamParser.g:4139:3: lv_ownedValue_0_0= rulePropertyExpression
            {
             
            	        newCompositeNode(grammarAccess.getPropertyValueAccess().getOwnedValuePropertyExpressionParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedValue_0_0=rulePropertyExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPropertyValueRule());
            	        }
                   		set(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyValue"


    // $ANTLR start "entryRulePropertyExpression"
    // InternalAsamParser.g:4163:1: entryRulePropertyExpression returns [EObject current=null] : iv_rulePropertyExpression= rulePropertyExpression EOF ;
    public final EObject entryRulePropertyExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyExpression = null;


        try {
            // InternalAsamParser.g:4164:2: (iv_rulePropertyExpression= rulePropertyExpression EOF )
            // InternalAsamParser.g:4165:2: iv_rulePropertyExpression= rulePropertyExpression EOF
            {
             newCompositeNode(grammarAccess.getPropertyExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePropertyExpression=rulePropertyExpression();

            state._fsp--;

             current =iv_rulePropertyExpression; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyExpression"


    // $ANTLR start "rulePropertyExpression"
    // InternalAsamParser.g:4172:1: rulePropertyExpression returns [EObject current=null] : (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm ) ;
    public final EObject rulePropertyExpression() throws RecognitionException {
        EObject current = null;

        EObject this_RecordTerm_0 = null;

        EObject this_ReferenceTerm_1 = null;

        EObject this_ComponentClassifierTerm_2 = null;

        EObject this_ComputedTerm_3 = null;

        EObject this_StringTerm_4 = null;

        EObject this_NumericRangeTerm_5 = null;

        EObject this_RealTerm_6 = null;

        EObject this_IntegerTerm_7 = null;

        EObject this_ListTerm_8 = null;

        EObject this_BooleanLiteral_9 = null;

        EObject this_LiteralorReferenceTerm_10 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4175:28: ( (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm ) )
            // InternalAsamParser.g:4176:1: (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm )
            {
            // InternalAsamParser.g:4176:1: (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm )
            int alt22=11;
            alt22 = dfa22.predict(input);
            switch (alt22) {
                case 1 :
                    // InternalAsamParser.g:4177:5: this_RecordTerm_0= ruleRecordTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getRecordTermParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RecordTerm_0=ruleRecordTerm();

                    state._fsp--;


                            current = this_RecordTerm_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:4187:5: this_ReferenceTerm_1= ruleReferenceTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getReferenceTermParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ReferenceTerm_1=ruleReferenceTerm();

                    state._fsp--;


                            current = this_ReferenceTerm_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:4197:5: this_ComponentClassifierTerm_2= ruleComponentClassifierTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getComponentClassifierTermParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ComponentClassifierTerm_2=ruleComponentClassifierTerm();

                    state._fsp--;


                            current = this_ComponentClassifierTerm_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalAsamParser.g:4207:5: this_ComputedTerm_3= ruleComputedTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getComputedTermParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ComputedTerm_3=ruleComputedTerm();

                    state._fsp--;


                            current = this_ComputedTerm_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalAsamParser.g:4217:5: this_StringTerm_4= ruleStringTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getStringTermParserRuleCall_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringTerm_4=ruleStringTerm();

                    state._fsp--;


                            current = this_StringTerm_4;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalAsamParser.g:4227:5: this_NumericRangeTerm_5= ruleNumericRangeTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getNumericRangeTermParserRuleCall_5()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NumericRangeTerm_5=ruleNumericRangeTerm();

                    state._fsp--;


                            current = this_NumericRangeTerm_5;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalAsamParser.g:4237:5: this_RealTerm_6= ruleRealTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getRealTermParserRuleCall_6()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealTerm_6=ruleRealTerm();

                    state._fsp--;


                            current = this_RealTerm_6;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // InternalAsamParser.g:4247:5: this_IntegerTerm_7= ruleIntegerTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getIntegerTermParserRuleCall_7()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerTerm_7=ruleIntegerTerm();

                    state._fsp--;


                            current = this_IntegerTerm_7;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // InternalAsamParser.g:4257:5: this_ListTerm_8= ruleListTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getListTermParserRuleCall_8()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ListTerm_8=ruleListTerm();

                    state._fsp--;


                            current = this_ListTerm_8;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 10 :
                    // InternalAsamParser.g:4267:5: this_BooleanLiteral_9= ruleBooleanLiteral
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getBooleanLiteralParserRuleCall_9()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanLiteral_9=ruleBooleanLiteral();

                    state._fsp--;


                            current = this_BooleanLiteral_9;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 11 :
                    // InternalAsamParser.g:4277:5: this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm
                    {
                     
                            newCompositeNode(grammarAccess.getPropertyExpressionAccess().getLiteralorReferenceTermParserRuleCall_10()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_LiteralorReferenceTerm_10=ruleLiteralorReferenceTerm();

                    state._fsp--;


                            current = this_LiteralorReferenceTerm_10;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyExpression"


    // $ANTLR start "entryRuleLiteralorReferenceTerm"
    // InternalAsamParser.g:4293:1: entryRuleLiteralorReferenceTerm returns [EObject current=null] : iv_ruleLiteralorReferenceTerm= ruleLiteralorReferenceTerm EOF ;
    public final EObject entryRuleLiteralorReferenceTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralorReferenceTerm = null;


        try {
            // InternalAsamParser.g:4294:2: (iv_ruleLiteralorReferenceTerm= ruleLiteralorReferenceTerm EOF )
            // InternalAsamParser.g:4295:2: iv_ruleLiteralorReferenceTerm= ruleLiteralorReferenceTerm EOF
            {
             newCompositeNode(grammarAccess.getLiteralorReferenceTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLiteralorReferenceTerm=ruleLiteralorReferenceTerm();

            state._fsp--;

             current =iv_ruleLiteralorReferenceTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralorReferenceTerm"


    // $ANTLR start "ruleLiteralorReferenceTerm"
    // InternalAsamParser.g:4302:1: ruleLiteralorReferenceTerm returns [EObject current=null] : ( ( ruleQPREF ) ) ;
    public final EObject ruleLiteralorReferenceTerm() throws RecognitionException {
        EObject current = null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:4305:28: ( ( ( ruleQPREF ) ) )
            // InternalAsamParser.g:4306:1: ( ( ruleQPREF ) )
            {
            // InternalAsamParser.g:4306:1: ( ( ruleQPREF ) )
            // InternalAsamParser.g:4307:1: ( ruleQPREF )
            {
            // InternalAsamParser.g:4307:1: ( ruleQPREF )
            // InternalAsamParser.g:4308:3: ruleQPREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLiteralorReferenceTermRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getLiteralorReferenceTermAccess().getNamedValueAbstractNamedValueCrossReference_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            ruleQPREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralorReferenceTerm"


    // $ANTLR start "entryRuleBooleanLiteral"
    // InternalAsamParser.g:4330:1: entryRuleBooleanLiteral returns [EObject current=null] : iv_ruleBooleanLiteral= ruleBooleanLiteral EOF ;
    public final EObject entryRuleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanLiteral = null;


        try {
            // InternalAsamParser.g:4331:2: (iv_ruleBooleanLiteral= ruleBooleanLiteral EOF )
            // InternalAsamParser.g:4332:2: iv_ruleBooleanLiteral= ruleBooleanLiteral EOF
            {
             newCompositeNode(grammarAccess.getBooleanLiteralRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanLiteral=ruleBooleanLiteral();

            state._fsp--;

             current =iv_ruleBooleanLiteral; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanLiteral"


    // $ANTLR start "ruleBooleanLiteral"
    // InternalAsamParser.g:4339:1: ruleBooleanLiteral returns [EObject current=null] : ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) ) ;
    public final EObject ruleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:4342:28: ( ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) ) )
            // InternalAsamParser.g:4343:1: ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) )
            {
            // InternalAsamParser.g:4343:1: ( () ( ( (lv_value_1_0= True ) ) | otherlv_2= False ) )
            // InternalAsamParser.g:4343:2: () ( ( (lv_value_1_0= True ) ) | otherlv_2= False )
            {
            // InternalAsamParser.g:4343:2: ()
            // InternalAsamParser.g:4344:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_0(),
                        current);
                

            }

            // InternalAsamParser.g:4349:2: ( ( (lv_value_1_0= True ) ) | otherlv_2= False )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==True) ) {
                alt23=1;
            }
            else if ( (LA23_0==False) ) {
                alt23=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalAsamParser.g:4349:3: ( (lv_value_1_0= True ) )
                    {
                    // InternalAsamParser.g:4349:3: ( (lv_value_1_0= True ) )
                    // InternalAsamParser.g:4350:1: (lv_value_1_0= True )
                    {
                    // InternalAsamParser.g:4350:1: (lv_value_1_0= True )
                    // InternalAsamParser.g:4351:3: lv_value_1_0= True
                    {
                    lv_value_1_0=(Token)match(input,True,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_value_1_0, grammarAccess.getBooleanLiteralAccess().getValueTrueKeyword_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBooleanLiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", true, "true");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:4367:2: otherlv_2= False
                    {
                    otherlv_2=(Token)match(input,False,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_2, grammarAccess.getBooleanLiteralAccess().getFalseKeyword_1_1());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanLiteral"


    // $ANTLR start "entryRuleConstantValue"
    // InternalAsamParser.g:4379:1: entryRuleConstantValue returns [EObject current=null] : iv_ruleConstantValue= ruleConstantValue EOF ;
    public final EObject entryRuleConstantValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstantValue = null;


        try {
            // InternalAsamParser.g:4380:2: (iv_ruleConstantValue= ruleConstantValue EOF )
            // InternalAsamParser.g:4381:2: iv_ruleConstantValue= ruleConstantValue EOF
            {
             newCompositeNode(grammarAccess.getConstantValueRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstantValue=ruleConstantValue();

            state._fsp--;

             current =iv_ruleConstantValue; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstantValue"


    // $ANTLR start "ruleConstantValue"
    // InternalAsamParser.g:4388:1: ruleConstantValue returns [EObject current=null] : ( ( ruleQPREF ) ) ;
    public final EObject ruleConstantValue() throws RecognitionException {
        EObject current = null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:4391:28: ( ( ( ruleQPREF ) ) )
            // InternalAsamParser.g:4392:1: ( ( ruleQPREF ) )
            {
            // InternalAsamParser.g:4392:1: ( ( ruleQPREF ) )
            // InternalAsamParser.g:4393:1: ( ruleQPREF )
            {
            // InternalAsamParser.g:4393:1: ( ruleQPREF )
            // InternalAsamParser.g:4394:3: ruleQPREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getConstantValueRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getConstantValueAccess().getNamedValuePropertyConstantCrossReference_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            ruleQPREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstantValue"


    // $ANTLR start "entryRuleReferenceTerm"
    // InternalAsamParser.g:4416:1: entryRuleReferenceTerm returns [EObject current=null] : iv_ruleReferenceTerm= ruleReferenceTerm EOF ;
    public final EObject entryRuleReferenceTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceTerm = null;


        try {
            // InternalAsamParser.g:4417:2: (iv_ruleReferenceTerm= ruleReferenceTerm EOF )
            // InternalAsamParser.g:4418:2: iv_ruleReferenceTerm= ruleReferenceTerm EOF
            {
             newCompositeNode(grammarAccess.getReferenceTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleReferenceTerm=ruleReferenceTerm();

            state._fsp--;

             current =iv_ruleReferenceTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceTerm"


    // $ANTLR start "ruleReferenceTerm"
    // InternalAsamParser.g:4425:1: ruleReferenceTerm returns [EObject current=null] : (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis ) ;
    public final EObject ruleReferenceTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_path_2_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4428:28: ( (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis ) )
            // InternalAsamParser.g:4429:1: (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis )
            {
            // InternalAsamParser.g:4429:1: (otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis )
            // InternalAsamParser.g:4430:2: otherlv_0= Reference otherlv_1= LeftParenthesis ( (lv_path_2_0= ruleContainmentPathElement ) ) otherlv_3= RightParenthesis
            {
            otherlv_0=(Token)match(input,Reference,FollowSets000.FOLLOW_31); 

                	newLeafNode(otherlv_0, grammarAccess.getReferenceTermAccess().getReferenceKeyword_0());
                
            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getReferenceTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalAsamParser.g:4439:1: ( (lv_path_2_0= ruleContainmentPathElement ) )
            // InternalAsamParser.g:4440:1: (lv_path_2_0= ruleContainmentPathElement )
            {
            // InternalAsamParser.g:4440:1: (lv_path_2_0= ruleContainmentPathElement )
            // InternalAsamParser.g:4441:3: lv_path_2_0= ruleContainmentPathElement
            {
             
            	        newCompositeNode(grammarAccess.getReferenceTermAccess().getPathContainmentPathElementParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_35);
            lv_path_2_0=ruleContainmentPathElement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReferenceTermRule());
            	        }
                   		set(
                   			current, 
                   			"path",
                    		lv_path_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ContainmentPathElement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getReferenceTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceTerm"


    // $ANTLR start "entryRuleRecordTerm"
    // InternalAsamParser.g:4470:1: entryRuleRecordTerm returns [EObject current=null] : iv_ruleRecordTerm= ruleRecordTerm EOF ;
    public final EObject entryRuleRecordTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRecordTerm = null;


        try {
            // InternalAsamParser.g:4471:2: (iv_ruleRecordTerm= ruleRecordTerm EOF )
            // InternalAsamParser.g:4472:2: iv_ruleRecordTerm= ruleRecordTerm EOF
            {
             newCompositeNode(grammarAccess.getRecordTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRecordTerm=ruleRecordTerm();

            state._fsp--;

             current =iv_ruleRecordTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRecordTerm"


    // $ANTLR start "ruleRecordTerm"
    // InternalAsamParser.g:4479:1: ruleRecordTerm returns [EObject current=null] : (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket ) ;
    public final EObject ruleRecordTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_ownedFieldValue_1_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4482:28: ( (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket ) )
            // InternalAsamParser.g:4483:1: (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket )
            {
            // InternalAsamParser.g:4483:1: (otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket )
            // InternalAsamParser.g:4484:2: otherlv_0= LeftSquareBracket ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+ otherlv_2= RightSquareBracket
            {
            otherlv_0=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getRecordTermAccess().getLeftSquareBracketKeyword_0());
                
            // InternalAsamParser.g:4488:1: ( (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation ) )+
            int cnt24=0;
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_ID) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalAsamParser.g:4489:1: (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation )
            	    {
            	    // InternalAsamParser.g:4489:1: (lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation )
            	    // InternalAsamParser.g:4490:3: lv_ownedFieldValue_1_0= ruleFieldPropertyAssociation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRecordTermAccess().getOwnedFieldValueFieldPropertyAssociationParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_45);
            	    lv_ownedFieldValue_1_0=ruleFieldPropertyAssociation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRecordTermRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ownedFieldValue",
            	            		lv_ownedFieldValue_1_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.FieldPropertyAssociation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt24 >= 1 ) break loop24;
                        EarlyExitException eee =
                            new EarlyExitException(24, input);
                        throw eee;
                }
                cnt24++;
            } while (true);

            otherlv_2=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getRecordTermAccess().getRightSquareBracketKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRecordTerm"


    // $ANTLR start "entryRuleComputedTerm"
    // InternalAsamParser.g:4521:1: entryRuleComputedTerm returns [EObject current=null] : iv_ruleComputedTerm= ruleComputedTerm EOF ;
    public final EObject entryRuleComputedTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComputedTerm = null;


        try {
            // InternalAsamParser.g:4522:2: (iv_ruleComputedTerm= ruleComputedTerm EOF )
            // InternalAsamParser.g:4523:2: iv_ruleComputedTerm= ruleComputedTerm EOF
            {
             newCompositeNode(grammarAccess.getComputedTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleComputedTerm=ruleComputedTerm();

            state._fsp--;

             current =iv_ruleComputedTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComputedTerm"


    // $ANTLR start "ruleComputedTerm"
    // InternalAsamParser.g:4530:1: ruleComputedTerm returns [EObject current=null] : (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis ) ;
    public final EObject ruleComputedTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_function_2_0=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:4533:28: ( (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis ) )
            // InternalAsamParser.g:4534:1: (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis )
            {
            // InternalAsamParser.g:4534:1: (otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis )
            // InternalAsamParser.g:4535:2: otherlv_0= Compute otherlv_1= LeftParenthesis ( (lv_function_2_0= RULE_ID ) ) otherlv_3= RightParenthesis
            {
            otherlv_0=(Token)match(input,Compute,FollowSets000.FOLLOW_31); 

                	newLeafNode(otherlv_0, grammarAccess.getComputedTermAccess().getComputeKeyword_0());
                
            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getComputedTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalAsamParser.g:4544:1: ( (lv_function_2_0= RULE_ID ) )
            // InternalAsamParser.g:4545:1: (lv_function_2_0= RULE_ID )
            {
            // InternalAsamParser.g:4545:1: (lv_function_2_0= RULE_ID )
            // InternalAsamParser.g:4546:3: lv_function_2_0= RULE_ID
            {
            lv_function_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_35); 

            			newLeafNode(lv_function_2_0, grammarAccess.getComputedTermAccess().getFunctionIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getComputedTermRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"function",
                    		lv_function_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getComputedTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComputedTerm"


    // $ANTLR start "entryRuleComponentClassifierTerm"
    // InternalAsamParser.g:4575:1: entryRuleComponentClassifierTerm returns [EObject current=null] : iv_ruleComponentClassifierTerm= ruleComponentClassifierTerm EOF ;
    public final EObject entryRuleComponentClassifierTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponentClassifierTerm = null;


        try {
            // InternalAsamParser.g:4576:2: (iv_ruleComponentClassifierTerm= ruleComponentClassifierTerm EOF )
            // InternalAsamParser.g:4577:2: iv_ruleComponentClassifierTerm= ruleComponentClassifierTerm EOF
            {
             newCompositeNode(grammarAccess.getComponentClassifierTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleComponentClassifierTerm=ruleComponentClassifierTerm();

            state._fsp--;

             current =iv_ruleComponentClassifierTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponentClassifierTerm"


    // $ANTLR start "ruleComponentClassifierTerm"
    // InternalAsamParser.g:4584:1: ruleComponentClassifierTerm returns [EObject current=null] : (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis ) ;
    public final EObject ruleComponentClassifierTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:4587:28: ( (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis ) )
            // InternalAsamParser.g:4588:1: (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis )
            {
            // InternalAsamParser.g:4588:1: (otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis )
            // InternalAsamParser.g:4589:2: otherlv_0= Classifier otherlv_1= LeftParenthesis ( ( ruleQCREF ) ) otherlv_3= RightParenthesis
            {
            otherlv_0=(Token)match(input,Classifier,FollowSets000.FOLLOW_31); 

                	newLeafNode(otherlv_0, grammarAccess.getComponentClassifierTermAccess().getClassifierKeyword_0());
                
            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getComponentClassifierTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalAsamParser.g:4598:1: ( ( ruleQCREF ) )
            // InternalAsamParser.g:4599:1: ( ruleQCREF )
            {
            // InternalAsamParser.g:4599:1: ( ruleQCREF )
            // InternalAsamParser.g:4600:3: ruleQCREF
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getComponentClassifierTermRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getComponentClassifierTermAccess().getClassifierComponentClassifierCrossReference_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_35);
            ruleQCREF();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getComponentClassifierTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponentClassifierTerm"


    // $ANTLR start "entryRuleListTerm"
    // InternalAsamParser.g:4627:1: entryRuleListTerm returns [EObject current=null] : iv_ruleListTerm= ruleListTerm EOF ;
    public final EObject entryRuleListTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleListTerm = null;


        try {
            // InternalAsamParser.g:4628:2: (iv_ruleListTerm= ruleListTerm EOF )
            // InternalAsamParser.g:4629:2: iv_ruleListTerm= ruleListTerm EOF
            {
             newCompositeNode(grammarAccess.getListTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleListTerm=ruleListTerm();

            state._fsp--;

             current =iv_ruleListTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleListTerm"


    // $ANTLR start "ruleListTerm"
    // InternalAsamParser.g:4636:1: ruleListTerm returns [EObject current=null] : ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis ) ;
    public final EObject ruleListTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_ownedListElement_2_0 = null;

        EObject lv_ownedListElement_4_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4639:28: ( ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis ) )
            // InternalAsamParser.g:4640:1: ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis )
            {
            // InternalAsamParser.g:4640:1: ( () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis )
            // InternalAsamParser.g:4640:2: () otherlv_1= LeftParenthesis ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )? otherlv_5= RightParenthesis
            {
            // InternalAsamParser.g:4640:2: ()
            // InternalAsamParser.g:4641:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getListTermAccess().getListValueAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,LeftParenthesis,FollowSets000.FOLLOW_46); 

                	newLeafNode(otherlv_1, grammarAccess.getListTermAccess().getLeftParenthesisKeyword_1());
                
            // InternalAsamParser.g:4651:1: ( ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )* )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==Classifier||LA26_0==Reference||LA26_0==Compute||LA26_0==False||LA26_0==True||LA26_0==LeftParenthesis||LA26_0==PlusSign||LA26_0==HyphenMinus||LA26_0==LeftSquareBracket||LA26_0==RULE_INTEGER_LIT||LA26_0==RULE_REAL_LIT||(LA26_0>=RULE_STRING && LA26_0<=RULE_ID)) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalAsamParser.g:4651:2: ( (lv_ownedListElement_2_0= rulePropertyExpression ) ) (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )*
                    {
                    // InternalAsamParser.g:4651:2: ( (lv_ownedListElement_2_0= rulePropertyExpression ) )
                    // InternalAsamParser.g:4652:1: (lv_ownedListElement_2_0= rulePropertyExpression )
                    {
                    // InternalAsamParser.g:4652:1: (lv_ownedListElement_2_0= rulePropertyExpression )
                    // InternalAsamParser.g:4653:3: lv_ownedListElement_2_0= rulePropertyExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getListTermAccess().getOwnedListElementPropertyExpressionParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_44);
                    lv_ownedListElement_2_0=rulePropertyExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getListTermRule());
                    	        }
                           		add(
                           			current, 
                           			"ownedListElement",
                            		lv_ownedListElement_2_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalAsamParser.g:4669:2: (otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) ) )*
                    loop25:
                    do {
                        int alt25=2;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==Comma) ) {
                            alt25=1;
                        }


                        switch (alt25) {
                    	case 1 :
                    	    // InternalAsamParser.g:4670:2: otherlv_3= Comma ( (lv_ownedListElement_4_0= rulePropertyExpression ) )
                    	    {
                    	    otherlv_3=(Token)match(input,Comma,FollowSets000.FOLLOW_37); 

                    	        	newLeafNode(otherlv_3, grammarAccess.getListTermAccess().getCommaKeyword_2_1_0());
                    	        
                    	    // InternalAsamParser.g:4674:1: ( (lv_ownedListElement_4_0= rulePropertyExpression ) )
                    	    // InternalAsamParser.g:4675:1: (lv_ownedListElement_4_0= rulePropertyExpression )
                    	    {
                    	    // InternalAsamParser.g:4675:1: (lv_ownedListElement_4_0= rulePropertyExpression )
                    	    // InternalAsamParser.g:4676:3: lv_ownedListElement_4_0= rulePropertyExpression
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getListTermAccess().getOwnedListElementPropertyExpressionParserRuleCall_2_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_44);
                    	    lv_ownedListElement_4_0=rulePropertyExpression();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getListTermRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ownedListElement",
                    	            		lv_ownedListElement_4_0, 
                    	            		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop25;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,RightParenthesis,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getListTermAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleListTerm"


    // $ANTLR start "entryRuleFieldPropertyAssociation"
    // InternalAsamParser.g:4705:1: entryRuleFieldPropertyAssociation returns [EObject current=null] : iv_ruleFieldPropertyAssociation= ruleFieldPropertyAssociation EOF ;
    public final EObject entryRuleFieldPropertyAssociation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFieldPropertyAssociation = null;


        try {
            // InternalAsamParser.g:4706:2: (iv_ruleFieldPropertyAssociation= ruleFieldPropertyAssociation EOF )
            // InternalAsamParser.g:4707:2: iv_ruleFieldPropertyAssociation= ruleFieldPropertyAssociation EOF
            {
             newCompositeNode(grammarAccess.getFieldPropertyAssociationRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFieldPropertyAssociation=ruleFieldPropertyAssociation();

            state._fsp--;

             current =iv_ruleFieldPropertyAssociation; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFieldPropertyAssociation"


    // $ANTLR start "ruleFieldPropertyAssociation"
    // InternalAsamParser.g:4714:1: ruleFieldPropertyAssociation returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon ) ;
    public final EObject ruleFieldPropertyAssociation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_ownedValue_2_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4717:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon ) )
            // InternalAsamParser.g:4718:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon )
            {
            // InternalAsamParser.g:4718:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon )
            // InternalAsamParser.g:4718:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= EqualsSignGreaterThanSign ( (lv_ownedValue_2_0= rulePropertyExpression ) ) otherlv_3= Semicolon
            {
            // InternalAsamParser.g:4718:2: ( (otherlv_0= RULE_ID ) )
            // InternalAsamParser.g:4719:1: (otherlv_0= RULE_ID )
            {
            // InternalAsamParser.g:4719:1: (otherlv_0= RULE_ID )
            // InternalAsamParser.g:4720:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFieldPropertyAssociationRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_19); 

            		newLeafNode(otherlv_0, grammarAccess.getFieldPropertyAssociationAccess().getPropertyBasicPropertyCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,EqualsSignGreaterThanSign,FollowSets000.FOLLOW_37); 

                	newLeafNode(otherlv_1, grammarAccess.getFieldPropertyAssociationAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // InternalAsamParser.g:4736:1: ( (lv_ownedValue_2_0= rulePropertyExpression ) )
            // InternalAsamParser.g:4737:1: (lv_ownedValue_2_0= rulePropertyExpression )
            {
            // InternalAsamParser.g:4737:1: (lv_ownedValue_2_0= rulePropertyExpression )
            // InternalAsamParser.g:4738:3: lv_ownedValue_2_0= rulePropertyExpression
            {
             
            	        newCompositeNode(grammarAccess.getFieldPropertyAssociationAccess().getOwnedValuePropertyExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_13);
            lv_ownedValue_2_0=rulePropertyExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFieldPropertyAssociationRule());
            	        }
                   		set(
                   			current, 
                   			"ownedValue",
                    		lv_ownedValue_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PropertyExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,Semicolon,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getFieldPropertyAssociationAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFieldPropertyAssociation"


    // $ANTLR start "entryRuleContainmentPathElement"
    // InternalAsamParser.g:4767:1: entryRuleContainmentPathElement returns [EObject current=null] : iv_ruleContainmentPathElement= ruleContainmentPathElement EOF ;
    public final EObject entryRuleContainmentPathElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContainmentPathElement = null;


        try {
            // InternalAsamParser.g:4768:2: (iv_ruleContainmentPathElement= ruleContainmentPathElement EOF )
            // InternalAsamParser.g:4769:2: iv_ruleContainmentPathElement= ruleContainmentPathElement EOF
            {
             newCompositeNode(grammarAccess.getContainmentPathElementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleContainmentPathElement=ruleContainmentPathElement();

            state._fsp--;

             current =iv_ruleContainmentPathElement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContainmentPathElement"


    // $ANTLR start "ruleContainmentPathElement"
    // InternalAsamParser.g:4776:1: ruleContainmentPathElement returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? ) ;
    public final EObject ruleContainmentPathElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_arrayRange_1_0 = null;

        EObject lv_path_3_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4779:28: ( ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? ) )
            // InternalAsamParser.g:4780:1: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? )
            {
            // InternalAsamParser.g:4780:1: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )? )
            // InternalAsamParser.g:4780:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* ) (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )?
            {
            // InternalAsamParser.g:4780:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )* )
            // InternalAsamParser.g:4780:3: ( (otherlv_0= RULE_ID ) ) ( (lv_arrayRange_1_0= ruleArrayRange ) )*
            {
            // InternalAsamParser.g:4780:3: ( (otherlv_0= RULE_ID ) )
            // InternalAsamParser.g:4781:1: (otherlv_0= RULE_ID )
            {
            // InternalAsamParser.g:4781:1: (otherlv_0= RULE_ID )
            // InternalAsamParser.g:4782:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getContainmentPathElementRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_47); 

            		newLeafNode(otherlv_0, grammarAccess.getContainmentPathElementAccess().getNamedElementNamedElementCrossReference_0_0_0()); 
            	

            }


            }

            // InternalAsamParser.g:4793:2: ( (lv_arrayRange_1_0= ruleArrayRange ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==LeftSquareBracket) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalAsamParser.g:4794:1: (lv_arrayRange_1_0= ruleArrayRange )
            	    {
            	    // InternalAsamParser.g:4794:1: (lv_arrayRange_1_0= ruleArrayRange )
            	    // InternalAsamParser.g:4795:3: lv_arrayRange_1_0= ruleArrayRange
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getContainmentPathElementAccess().getArrayRangeArrayRangeParserRuleCall_0_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_47);
            	    lv_arrayRange_1_0=ruleArrayRange();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getContainmentPathElementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"arrayRange",
            	            		lv_arrayRange_1_0, 
            	            		"org.osate.xtext.aadl2.properties.Properties.ArrayRange");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }

            // InternalAsamParser.g:4811:4: (otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==FullStop) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalAsamParser.g:4812:2: otherlv_2= FullStop ( (lv_path_3_0= ruleContainmentPathElement ) )
                    {
                    otherlv_2=(Token)match(input,FullStop,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_2, grammarAccess.getContainmentPathElementAccess().getFullStopKeyword_1_0());
                        
                    // InternalAsamParser.g:4816:1: ( (lv_path_3_0= ruleContainmentPathElement ) )
                    // InternalAsamParser.g:4817:1: (lv_path_3_0= ruleContainmentPathElement )
                    {
                    // InternalAsamParser.g:4817:1: (lv_path_3_0= ruleContainmentPathElement )
                    // InternalAsamParser.g:4818:3: lv_path_3_0= ruleContainmentPathElement
                    {
                     
                    	        newCompositeNode(grammarAccess.getContainmentPathElementAccess().getPathContainmentPathElementParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_path_3_0=ruleContainmentPathElement();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContainmentPathElementRule());
                    	        }
                           		set(
                           			current, 
                           			"path",
                            		lv_path_3_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.ContainmentPathElement");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContainmentPathElement"


    // $ANTLR start "entryRulePlusMinus"
    // InternalAsamParser.g:4844:1: entryRulePlusMinus returns [String current=null] : iv_rulePlusMinus= rulePlusMinus EOF ;
    public final String entryRulePlusMinus() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulePlusMinus = null;


        try {
            // InternalAsamParser.g:4845:1: (iv_rulePlusMinus= rulePlusMinus EOF )
            // InternalAsamParser.g:4846:2: iv_rulePlusMinus= rulePlusMinus EOF
            {
             newCompositeNode(grammarAccess.getPlusMinusRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePlusMinus=rulePlusMinus();

            state._fsp--;

             current =iv_rulePlusMinus.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlusMinus"


    // $ANTLR start "rulePlusMinus"
    // InternalAsamParser.g:4853:1: rulePlusMinus returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= PlusSign | kw= HyphenMinus ) ;
    public final AntlrDatatypeRuleToken rulePlusMinus() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:4857:6: ( (kw= PlusSign | kw= HyphenMinus ) )
            // InternalAsamParser.g:4858:1: (kw= PlusSign | kw= HyphenMinus )
            {
            // InternalAsamParser.g:4858:1: (kw= PlusSign | kw= HyphenMinus )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==PlusSign) ) {
                alt29=1;
            }
            else if ( (LA29_0==HyphenMinus) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalAsamParser.g:4859:2: kw= PlusSign
                    {
                    kw=(Token)match(input,PlusSign,FollowSets000.FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getPlusMinusAccess().getPlusSignKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:4866:2: kw= HyphenMinus
                    {
                    kw=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_2); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getPlusMinusAccess().getHyphenMinusKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlusMinus"


    // $ANTLR start "entryRuleStringTerm"
    // InternalAsamParser.g:4879:1: entryRuleStringTerm returns [EObject current=null] : iv_ruleStringTerm= ruleStringTerm EOF ;
    public final EObject entryRuleStringTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringTerm = null;


        try {
            // InternalAsamParser.g:4880:2: (iv_ruleStringTerm= ruleStringTerm EOF )
            // InternalAsamParser.g:4881:2: iv_ruleStringTerm= ruleStringTerm EOF
            {
             newCompositeNode(grammarAccess.getStringTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringTerm=ruleStringTerm();

            state._fsp--;

             current =iv_ruleStringTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringTerm"


    // $ANTLR start "ruleStringTerm"
    // InternalAsamParser.g:4888:1: ruleStringTerm returns [EObject current=null] : ( (lv_value_0_0= ruleNoQuoteString ) ) ;
    public final EObject ruleStringTerm() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4891:28: ( ( (lv_value_0_0= ruleNoQuoteString ) ) )
            // InternalAsamParser.g:4892:1: ( (lv_value_0_0= ruleNoQuoteString ) )
            {
            // InternalAsamParser.g:4892:1: ( (lv_value_0_0= ruleNoQuoteString ) )
            // InternalAsamParser.g:4893:1: (lv_value_0_0= ruleNoQuoteString )
            {
            // InternalAsamParser.g:4893:1: (lv_value_0_0= ruleNoQuoteString )
            // InternalAsamParser.g:4894:3: lv_value_0_0= ruleNoQuoteString
            {
             
            	        newCompositeNode(grammarAccess.getStringTermAccess().getValueNoQuoteStringParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_value_0_0=ruleNoQuoteString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStringTermRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.NoQuoteString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringTerm"


    // $ANTLR start "entryRuleNoQuoteString"
    // InternalAsamParser.g:4918:1: entryRuleNoQuoteString returns [String current=null] : iv_ruleNoQuoteString= ruleNoQuoteString EOF ;
    public final String entryRuleNoQuoteString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNoQuoteString = null;


        try {
            // InternalAsamParser.g:4919:1: (iv_ruleNoQuoteString= ruleNoQuoteString EOF )
            // InternalAsamParser.g:4920:2: iv_ruleNoQuoteString= ruleNoQuoteString EOF
            {
             newCompositeNode(grammarAccess.getNoQuoteStringRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNoQuoteString=ruleNoQuoteString();

            state._fsp--;

             current =iv_ruleNoQuoteString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNoQuoteString"


    // $ANTLR start "ruleNoQuoteString"
    // InternalAsamParser.g:4927:1: ruleNoQuoteString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRING_0= RULE_STRING ;
    public final AntlrDatatypeRuleToken ruleNoQuoteString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:4931:6: (this_STRING_0= RULE_STRING )
            // InternalAsamParser.g:4932:5: this_STRING_0= RULE_STRING
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); 

            		current.merge(this_STRING_0);
                
             
                newLeafNode(this_STRING_0, grammarAccess.getNoQuoteStringAccess().getSTRINGTerminalRuleCall()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNoQuoteString"


    // $ANTLR start "entryRuleArrayRange"
    // InternalAsamParser.g:4947:1: entryRuleArrayRange returns [EObject current=null] : iv_ruleArrayRange= ruleArrayRange EOF ;
    public final EObject entryRuleArrayRange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArrayRange = null;


        try {
            // InternalAsamParser.g:4948:2: (iv_ruleArrayRange= ruleArrayRange EOF )
            // InternalAsamParser.g:4949:2: iv_ruleArrayRange= ruleArrayRange EOF
            {
             newCompositeNode(grammarAccess.getArrayRangeRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleArrayRange=ruleArrayRange();

            state._fsp--;

             current =iv_ruleArrayRange; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayRange"


    // $ANTLR start "ruleArrayRange"
    // InternalAsamParser.g:4956:1: ruleArrayRange returns [EObject current=null] : ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket ) ;
    public final EObject ruleArrayRange() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_lowerBound_2_0 = null;

        AntlrDatatypeRuleToken lv_upperBound_4_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:4959:28: ( ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket ) )
            // InternalAsamParser.g:4960:1: ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket )
            {
            // InternalAsamParser.g:4960:1: ( () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket )
            // InternalAsamParser.g:4960:2: () otherlv_1= LeftSquareBracket ( (lv_lowerBound_2_0= ruleINTVALUE ) ) (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )? otherlv_5= RightSquareBracket
            {
            // InternalAsamParser.g:4960:2: ()
            // InternalAsamParser.g:4961:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getArrayRangeAccess().getArrayRangeAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,LeftSquareBracket,FollowSets000.FOLLOW_48); 

                	newLeafNode(otherlv_1, grammarAccess.getArrayRangeAccess().getLeftSquareBracketKeyword_1());
                
            // InternalAsamParser.g:4971:1: ( (lv_lowerBound_2_0= ruleINTVALUE ) )
            // InternalAsamParser.g:4972:1: (lv_lowerBound_2_0= ruleINTVALUE )
            {
            // InternalAsamParser.g:4972:1: (lv_lowerBound_2_0= ruleINTVALUE )
            // InternalAsamParser.g:4973:3: lv_lowerBound_2_0= ruleINTVALUE
            {
             
            	        newCompositeNode(grammarAccess.getArrayRangeAccess().getLowerBoundINTVALUEParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_49);
            lv_lowerBound_2_0=ruleINTVALUE();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArrayRangeRule());
            	        }
                   		set(
                   			current, 
                   			"lowerBound",
                    		lv_lowerBound_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.INTVALUE");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:4989:2: (otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==FullStopFullStop) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalAsamParser.g:4990:2: otherlv_3= FullStopFullStop ( (lv_upperBound_4_0= ruleINTVALUE ) )
                    {
                    otherlv_3=(Token)match(input,FullStopFullStop,FollowSets000.FOLLOW_48); 

                        	newLeafNode(otherlv_3, grammarAccess.getArrayRangeAccess().getFullStopFullStopKeyword_3_0());
                        
                    // InternalAsamParser.g:4994:1: ( (lv_upperBound_4_0= ruleINTVALUE ) )
                    // InternalAsamParser.g:4995:1: (lv_upperBound_4_0= ruleINTVALUE )
                    {
                    // InternalAsamParser.g:4995:1: (lv_upperBound_4_0= ruleINTVALUE )
                    // InternalAsamParser.g:4996:3: lv_upperBound_4_0= ruleINTVALUE
                    {
                     
                    	        newCompositeNode(grammarAccess.getArrayRangeAccess().getUpperBoundINTVALUEParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_11);
                    lv_upperBound_4_0=ruleINTVALUE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getArrayRangeRule());
                    	        }
                           		set(
                           			current, 
                           			"upperBound",
                            		lv_upperBound_4_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.INTVALUE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,RightSquareBracket,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getArrayRangeAccess().getRightSquareBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayRange"


    // $ANTLR start "entryRuleSignedConstant"
    // InternalAsamParser.g:5025:1: entryRuleSignedConstant returns [EObject current=null] : iv_ruleSignedConstant= ruleSignedConstant EOF ;
    public final EObject entryRuleSignedConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSignedConstant = null;


        try {
            // InternalAsamParser.g:5026:2: (iv_ruleSignedConstant= ruleSignedConstant EOF )
            // InternalAsamParser.g:5027:2: iv_ruleSignedConstant= ruleSignedConstant EOF
            {
             newCompositeNode(grammarAccess.getSignedConstantRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSignedConstant=ruleSignedConstant();

            state._fsp--;

             current =iv_ruleSignedConstant; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignedConstant"


    // $ANTLR start "ruleSignedConstant"
    // InternalAsamParser.g:5034:1: ruleSignedConstant returns [EObject current=null] : ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) ) ;
    public final EObject ruleSignedConstant() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_op_0_0 = null;

        EObject lv_ownedPropertyExpression_1_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:5037:28: ( ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) ) )
            // InternalAsamParser.g:5038:1: ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) )
            {
            // InternalAsamParser.g:5038:1: ( ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) ) )
            // InternalAsamParser.g:5038:2: ( (lv_op_0_0= rulePlusMinus ) ) ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) )
            {
            // InternalAsamParser.g:5038:2: ( (lv_op_0_0= rulePlusMinus ) )
            // InternalAsamParser.g:5039:1: (lv_op_0_0= rulePlusMinus )
            {
            // InternalAsamParser.g:5039:1: (lv_op_0_0= rulePlusMinus )
            // InternalAsamParser.g:5040:3: lv_op_0_0= rulePlusMinus
            {
             
            	        newCompositeNode(grammarAccess.getSignedConstantAccess().getOpPlusMinusParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_50);
            lv_op_0_0=rulePlusMinus();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSignedConstantRule());
            	        }
                   		set(
                   			current, 
                   			"op",
                    		lv_op_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.PlusMinus");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:5056:2: ( (lv_ownedPropertyExpression_1_0= ruleConstantValue ) )
            // InternalAsamParser.g:5057:1: (lv_ownedPropertyExpression_1_0= ruleConstantValue )
            {
            // InternalAsamParser.g:5057:1: (lv_ownedPropertyExpression_1_0= ruleConstantValue )
            // InternalAsamParser.g:5058:3: lv_ownedPropertyExpression_1_0= ruleConstantValue
            {
             
            	        newCompositeNode(grammarAccess.getSignedConstantAccess().getOwnedPropertyExpressionConstantValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedPropertyExpression_1_0=ruleConstantValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSignedConstantRule());
            	        }
                   		add(
                   			current, 
                   			"ownedPropertyExpression",
                    		lv_ownedPropertyExpression_1_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.ConstantValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignedConstant"


    // $ANTLR start "entryRuleIntegerTerm"
    // InternalAsamParser.g:5082:1: entryRuleIntegerTerm returns [EObject current=null] : iv_ruleIntegerTerm= ruleIntegerTerm EOF ;
    public final EObject entryRuleIntegerTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerTerm = null;


        try {
            // InternalAsamParser.g:5083:2: (iv_ruleIntegerTerm= ruleIntegerTerm EOF )
            // InternalAsamParser.g:5084:2: iv_ruleIntegerTerm= ruleIntegerTerm EOF
            {
             newCompositeNode(grammarAccess.getIntegerTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerTerm=ruleIntegerTerm();

            state._fsp--;

             current =iv_ruleIntegerTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerTerm"


    // $ANTLR start "ruleIntegerTerm"
    // InternalAsamParser.g:5091:1: ruleIntegerTerm returns [EObject current=null] : ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? ) ;
    public final EObject ruleIntegerTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:5094:28: ( ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? ) )
            // InternalAsamParser.g:5095:1: ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? )
            {
            // InternalAsamParser.g:5095:1: ( ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )? )
            // InternalAsamParser.g:5095:2: ( (lv_value_0_0= ruleSignedInt ) ) ( (otherlv_1= RULE_ID ) )?
            {
            // InternalAsamParser.g:5095:2: ( (lv_value_0_0= ruleSignedInt ) )
            // InternalAsamParser.g:5096:1: (lv_value_0_0= ruleSignedInt )
            {
            // InternalAsamParser.g:5096:1: (lv_value_0_0= ruleSignedInt )
            // InternalAsamParser.g:5097:3: lv_value_0_0= ruleSignedInt
            {
             
            	        newCompositeNode(grammarAccess.getIntegerTermAccess().getValueSignedIntParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_51);
            lv_value_0_0=ruleSignedInt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerTermRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.SignedInt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:5113:2: ( (otherlv_1= RULE_ID ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==RULE_ID) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalAsamParser.g:5114:1: (otherlv_1= RULE_ID )
                    {
                    // InternalAsamParser.g:5114:1: (otherlv_1= RULE_ID )
                    // InternalAsamParser.g:5115:3: otherlv_1= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerTermRule());
                    	        }
                            
                    otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		newLeafNode(otherlv_1, grammarAccess.getIntegerTermAccess().getUnitUnitLiteralCrossReference_1_0()); 
                    	

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerTerm"


    // $ANTLR start "entryRuleSignedInt"
    // InternalAsamParser.g:5134:1: entryRuleSignedInt returns [String current=null] : iv_ruleSignedInt= ruleSignedInt EOF ;
    public final String entryRuleSignedInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSignedInt = null;


        try {
            // InternalAsamParser.g:5135:1: (iv_ruleSignedInt= ruleSignedInt EOF )
            // InternalAsamParser.g:5136:2: iv_ruleSignedInt= ruleSignedInt EOF
            {
             newCompositeNode(grammarAccess.getSignedIntRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSignedInt=ruleSignedInt();

            state._fsp--;

             current =iv_ruleSignedInt.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignedInt"


    // $ANTLR start "ruleSignedInt"
    // InternalAsamParser.g:5143:1: ruleSignedInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT ) ;
    public final AntlrDatatypeRuleToken ruleSignedInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INTEGER_LIT_2=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:5147:6: ( ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT ) )
            // InternalAsamParser.g:5148:1: ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT )
            {
            // InternalAsamParser.g:5148:1: ( (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT )
            // InternalAsamParser.g:5148:2: (kw= PlusSign | kw= HyphenMinus )? this_INTEGER_LIT_2= RULE_INTEGER_LIT
            {
            // InternalAsamParser.g:5148:2: (kw= PlusSign | kw= HyphenMinus )?
            int alt32=3;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==PlusSign) ) {
                alt32=1;
            }
            else if ( (LA32_0==HyphenMinus) ) {
                alt32=2;
            }
            switch (alt32) {
                case 1 :
                    // InternalAsamParser.g:5149:2: kw= PlusSign
                    {
                    kw=(Token)match(input,PlusSign,FollowSets000.FOLLOW_48); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedIntAccess().getPlusSignKeyword_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:5156:2: kw= HyphenMinus
                    {
                    kw=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_48); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedIntAccess().getHyphenMinusKeyword_0_1()); 
                        

                    }
                    break;

            }

            this_INTEGER_LIT_2=(Token)match(input,RULE_INTEGER_LIT,FollowSets000.FOLLOW_2); 

            		current.merge(this_INTEGER_LIT_2);
                
             
                newLeafNode(this_INTEGER_LIT_2, grammarAccess.getSignedIntAccess().getINTEGER_LITTerminalRuleCall_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignedInt"


    // $ANTLR start "entryRuleRealTerm"
    // InternalAsamParser.g:5176:1: entryRuleRealTerm returns [EObject current=null] : iv_ruleRealTerm= ruleRealTerm EOF ;
    public final EObject entryRuleRealTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealTerm = null;


        try {
            // InternalAsamParser.g:5177:2: (iv_ruleRealTerm= ruleRealTerm EOF )
            // InternalAsamParser.g:5178:2: iv_ruleRealTerm= ruleRealTerm EOF
            {
             newCompositeNode(grammarAccess.getRealTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealTerm=ruleRealTerm();

            state._fsp--;

             current =iv_ruleRealTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealTerm"


    // $ANTLR start "ruleRealTerm"
    // InternalAsamParser.g:5185:1: ruleRealTerm returns [EObject current=null] : ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? ) ;
    public final EObject ruleRealTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:5188:28: ( ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? ) )
            // InternalAsamParser.g:5189:1: ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? )
            {
            // InternalAsamParser.g:5189:1: ( ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )? )
            // InternalAsamParser.g:5189:2: ( (lv_value_0_0= ruleSignedReal ) ) ( (otherlv_1= RULE_ID ) )?
            {
            // InternalAsamParser.g:5189:2: ( (lv_value_0_0= ruleSignedReal ) )
            // InternalAsamParser.g:5190:1: (lv_value_0_0= ruleSignedReal )
            {
            // InternalAsamParser.g:5190:1: (lv_value_0_0= ruleSignedReal )
            // InternalAsamParser.g:5191:3: lv_value_0_0= ruleSignedReal
            {
             
            	        newCompositeNode(grammarAccess.getRealTermAccess().getValueSignedRealParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_51);
            lv_value_0_0=ruleSignedReal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRealTermRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.SignedReal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:5207:2: ( (otherlv_1= RULE_ID ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_ID) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalAsamParser.g:5208:1: (otherlv_1= RULE_ID )
                    {
                    // InternalAsamParser.g:5208:1: (otherlv_1= RULE_ID )
                    // InternalAsamParser.g:5209:3: otherlv_1= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getRealTermRule());
                    	        }
                            
                    otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		newLeafNode(otherlv_1, grammarAccess.getRealTermAccess().getUnitUnitLiteralCrossReference_1_0()); 
                    	

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealTerm"


    // $ANTLR start "entryRuleSignedReal"
    // InternalAsamParser.g:5228:1: entryRuleSignedReal returns [String current=null] : iv_ruleSignedReal= ruleSignedReal EOF ;
    public final String entryRuleSignedReal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSignedReal = null;


        try {
            // InternalAsamParser.g:5229:1: (iv_ruleSignedReal= ruleSignedReal EOF )
            // InternalAsamParser.g:5230:2: iv_ruleSignedReal= ruleSignedReal EOF
            {
             newCompositeNode(grammarAccess.getSignedRealRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSignedReal=ruleSignedReal();

            state._fsp--;

             current =iv_ruleSignedReal.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignedReal"


    // $ANTLR start "ruleSignedReal"
    // InternalAsamParser.g:5237:1: ruleSignedReal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT ) ;
    public final AntlrDatatypeRuleToken ruleSignedReal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_REAL_LIT_2=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:5241:6: ( ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT ) )
            // InternalAsamParser.g:5242:1: ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT )
            {
            // InternalAsamParser.g:5242:1: ( (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT )
            // InternalAsamParser.g:5242:2: (kw= PlusSign | kw= HyphenMinus )? this_REAL_LIT_2= RULE_REAL_LIT
            {
            // InternalAsamParser.g:5242:2: (kw= PlusSign | kw= HyphenMinus )?
            int alt34=3;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==PlusSign) ) {
                alt34=1;
            }
            else if ( (LA34_0==HyphenMinus) ) {
                alt34=2;
            }
            switch (alt34) {
                case 1 :
                    // InternalAsamParser.g:5243:2: kw= PlusSign
                    {
                    kw=(Token)match(input,PlusSign,FollowSets000.FOLLOW_52); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedRealAccess().getPlusSignKeyword_0_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:5250:2: kw= HyphenMinus
                    {
                    kw=(Token)match(input,HyphenMinus,FollowSets000.FOLLOW_52); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getSignedRealAccess().getHyphenMinusKeyword_0_1()); 
                        

                    }
                    break;

            }

            this_REAL_LIT_2=(Token)match(input,RULE_REAL_LIT,FollowSets000.FOLLOW_2); 

            		current.merge(this_REAL_LIT_2);
                
             
                newLeafNode(this_REAL_LIT_2, grammarAccess.getSignedRealAccess().getREAL_LITTerminalRuleCall_1()); 
                

            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignedReal"


    // $ANTLR start "entryRuleNumericRangeTerm"
    // InternalAsamParser.g:5270:1: entryRuleNumericRangeTerm returns [EObject current=null] : iv_ruleNumericRangeTerm= ruleNumericRangeTerm EOF ;
    public final EObject entryRuleNumericRangeTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumericRangeTerm = null;


        try {
            // InternalAsamParser.g:5271:2: (iv_ruleNumericRangeTerm= ruleNumericRangeTerm EOF )
            // InternalAsamParser.g:5272:2: iv_ruleNumericRangeTerm= ruleNumericRangeTerm EOF
            {
             newCompositeNode(grammarAccess.getNumericRangeTermRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNumericRangeTerm=ruleNumericRangeTerm();

            state._fsp--;

             current =iv_ruleNumericRangeTerm; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumericRangeTerm"


    // $ANTLR start "ruleNumericRangeTerm"
    // InternalAsamParser.g:5279:1: ruleNumericRangeTerm returns [EObject current=null] : ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? ) ;
    public final EObject ruleNumericRangeTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_minimum_0_0 = null;

        EObject lv_maximum_2_0 = null;

        EObject lv_delta_4_0 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:5282:28: ( ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? ) )
            // InternalAsamParser.g:5283:1: ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? )
            {
            // InternalAsamParser.g:5283:1: ( ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )? )
            // InternalAsamParser.g:5283:2: ( (lv_minimum_0_0= ruleNumAlt ) ) otherlv_1= FullStopFullStop ( (lv_maximum_2_0= ruleNumAlt ) ) (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )?
            {
            // InternalAsamParser.g:5283:2: ( (lv_minimum_0_0= ruleNumAlt ) )
            // InternalAsamParser.g:5284:1: (lv_minimum_0_0= ruleNumAlt )
            {
            // InternalAsamParser.g:5284:1: (lv_minimum_0_0= ruleNumAlt )
            // InternalAsamParser.g:5285:3: lv_minimum_0_0= ruleNumAlt
            {
             
            	        newCompositeNode(grammarAccess.getNumericRangeTermAccess().getMinimumNumAltParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_53);
            lv_minimum_0_0=ruleNumAlt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNumericRangeTermRule());
            	        }
                   		set(
                   			current, 
                   			"minimum",
                    		lv_minimum_0_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.NumAlt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,FullStopFullStop,FollowSets000.FOLLOW_50); 

                	newLeafNode(otherlv_1, grammarAccess.getNumericRangeTermAccess().getFullStopFullStopKeyword_1());
                
            // InternalAsamParser.g:5306:1: ( (lv_maximum_2_0= ruleNumAlt ) )
            // InternalAsamParser.g:5307:1: (lv_maximum_2_0= ruleNumAlt )
            {
            // InternalAsamParser.g:5307:1: (lv_maximum_2_0= ruleNumAlt )
            // InternalAsamParser.g:5308:3: lv_maximum_2_0= ruleNumAlt
            {
             
            	        newCompositeNode(grammarAccess.getNumericRangeTermAccess().getMaximumNumAltParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_54);
            lv_maximum_2_0=ruleNumAlt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNumericRangeTermRule());
            	        }
                   		set(
                   			current, 
                   			"maximum",
                    		lv_maximum_2_0, 
                    		"org.osate.xtext.aadl2.properties.Properties.NumAlt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalAsamParser.g:5324:2: (otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==Delta) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalAsamParser.g:5325:2: otherlv_3= Delta ( (lv_delta_4_0= ruleNumAlt ) )
                    {
                    otherlv_3=(Token)match(input,Delta,FollowSets000.FOLLOW_50); 

                        	newLeafNode(otherlv_3, grammarAccess.getNumericRangeTermAccess().getDeltaKeyword_3_0());
                        
                    // InternalAsamParser.g:5329:1: ( (lv_delta_4_0= ruleNumAlt ) )
                    // InternalAsamParser.g:5330:1: (lv_delta_4_0= ruleNumAlt )
                    {
                    // InternalAsamParser.g:5330:1: (lv_delta_4_0= ruleNumAlt )
                    // InternalAsamParser.g:5331:3: lv_delta_4_0= ruleNumAlt
                    {
                     
                    	        newCompositeNode(grammarAccess.getNumericRangeTermAccess().getDeltaNumAltParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_delta_4_0=ruleNumAlt();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getNumericRangeTermRule());
                    	        }
                           		set(
                           			current, 
                           			"delta",
                            		lv_delta_4_0, 
                            		"org.osate.xtext.aadl2.properties.Properties.NumAlt");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumericRangeTerm"


    // $ANTLR start "entryRuleNumAlt"
    // InternalAsamParser.g:5355:1: entryRuleNumAlt returns [EObject current=null] : iv_ruleNumAlt= ruleNumAlt EOF ;
    public final EObject entryRuleNumAlt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumAlt = null;


        try {
            // InternalAsamParser.g:5356:2: (iv_ruleNumAlt= ruleNumAlt EOF )
            // InternalAsamParser.g:5357:2: iv_ruleNumAlt= ruleNumAlt EOF
            {
             newCompositeNode(grammarAccess.getNumAltRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNumAlt=ruleNumAlt();

            state._fsp--;

             current =iv_ruleNumAlt; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumAlt"


    // $ANTLR start "ruleNumAlt"
    // InternalAsamParser.g:5364:1: ruleNumAlt returns [EObject current=null] : (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue ) ;
    public final EObject ruleNumAlt() throws RecognitionException {
        EObject current = null;

        EObject this_RealTerm_0 = null;

        EObject this_IntegerTerm_1 = null;

        EObject this_SignedConstant_2 = null;

        EObject this_ConstantValue_3 = null;


         enterRule(); 
            
        try {
            // InternalAsamParser.g:5367:28: ( (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue ) )
            // InternalAsamParser.g:5368:1: (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue )
            {
            // InternalAsamParser.g:5368:1: (this_RealTerm_0= ruleRealTerm | this_IntegerTerm_1= ruleIntegerTerm | this_SignedConstant_2= ruleSignedConstant | this_ConstantValue_3= ruleConstantValue )
            int alt36=4;
            switch ( input.LA(1) ) {
            case PlusSign:
                {
                switch ( input.LA(2) ) {
                case RULE_REAL_LIT:
                    {
                    alt36=1;
                    }
                    break;
                case RULE_INTEGER_LIT:
                    {
                    alt36=2;
                    }
                    break;
                case RULE_ID:
                    {
                    alt36=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 1, input);

                    throw nvae;
                }

                }
                break;
            case HyphenMinus:
                {
                switch ( input.LA(2) ) {
                case RULE_ID:
                    {
                    alt36=3;
                    }
                    break;
                case RULE_REAL_LIT:
                    {
                    alt36=1;
                    }
                    break;
                case RULE_INTEGER_LIT:
                    {
                    alt36=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 2, input);

                    throw nvae;
                }

                }
                break;
            case RULE_REAL_LIT:
                {
                alt36=1;
                }
                break;
            case RULE_INTEGER_LIT:
                {
                alt36=2;
                }
                break;
            case RULE_ID:
                {
                alt36=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // InternalAsamParser.g:5369:5: this_RealTerm_0= ruleRealTerm
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getRealTermParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealTerm_0=ruleRealTerm();

                    state._fsp--;


                            current = this_RealTerm_0;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalAsamParser.g:5379:5: this_IntegerTerm_1= ruleIntegerTerm
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getIntegerTermParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerTerm_1=ruleIntegerTerm();

                    state._fsp--;


                            current = this_IntegerTerm_1;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalAsamParser.g:5389:5: this_SignedConstant_2= ruleSignedConstant
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getSignedConstantParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SignedConstant_2=ruleSignedConstant();

                    state._fsp--;


                            current = this_SignedConstant_2;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalAsamParser.g:5399:5: this_ConstantValue_3= ruleConstantValue
                    {
                     
                            newCompositeNode(grammarAccess.getNumAltAccess().getConstantValueParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ConstantValue_3=ruleConstantValue();

                    state._fsp--;


                            current = this_ConstantValue_3;
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumAlt"


    // $ANTLR start "entryRuleINTVALUE"
    // InternalAsamParser.g:5415:1: entryRuleINTVALUE returns [String current=null] : iv_ruleINTVALUE= ruleINTVALUE EOF ;
    public final String entryRuleINTVALUE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleINTVALUE = null;


        try {
            // InternalAsamParser.g:5416:1: (iv_ruleINTVALUE= ruleINTVALUE EOF )
            // InternalAsamParser.g:5417:2: iv_ruleINTVALUE= ruleINTVALUE EOF
            {
             newCompositeNode(grammarAccess.getINTVALUERule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleINTVALUE=ruleINTVALUE();

            state._fsp--;

             current =iv_ruleINTVALUE.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleINTVALUE"


    // $ANTLR start "ruleINTVALUE"
    // InternalAsamParser.g:5424:1: ruleINTVALUE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INTEGER_LIT_0= RULE_INTEGER_LIT ;
    public final AntlrDatatypeRuleToken ruleINTVALUE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INTEGER_LIT_0=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:5428:6: (this_INTEGER_LIT_0= RULE_INTEGER_LIT )
            // InternalAsamParser.g:5429:5: this_INTEGER_LIT_0= RULE_INTEGER_LIT
            {
            this_INTEGER_LIT_0=(Token)match(input,RULE_INTEGER_LIT,FollowSets000.FOLLOW_2); 

            		current.merge(this_INTEGER_LIT_0);
                
             
                newLeafNode(this_INTEGER_LIT_0, grammarAccess.getINTVALUEAccess().getINTEGER_LITTerminalRuleCall()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleINTVALUE"


    // $ANTLR start "entryRuleQPREF"
    // InternalAsamParser.g:5446:1: entryRuleQPREF returns [String current=null] : iv_ruleQPREF= ruleQPREF EOF ;
    public final String entryRuleQPREF() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQPREF = null;


        try {
            // InternalAsamParser.g:5447:1: (iv_ruleQPREF= ruleQPREF EOF )
            // InternalAsamParser.g:5448:2: iv_ruleQPREF= ruleQPREF EOF
            {
             newCompositeNode(grammarAccess.getQPREFRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQPREF=ruleQPREF();

            state._fsp--;

             current =iv_ruleQPREF.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQPREF"


    // $ANTLR start "ruleQPREF"
    // InternalAsamParser.g:5455:1: ruleQPREF returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? ) ;
    public final AntlrDatatypeRuleToken ruleQPREF() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:5459:6: ( (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? ) )
            // InternalAsamParser.g:5460:1: (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? )
            {
            // InternalAsamParser.g:5460:1: (this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )? )
            // InternalAsamParser.g:5460:6: this_ID_0= RULE_ID (kw= ColonColon this_ID_2= RULE_ID )?
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_55); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQPREFAccess().getIDTerminalRuleCall_0()); 
                
            // InternalAsamParser.g:5467:1: (kw= ColonColon this_ID_2= RULE_ID )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==ColonColon) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalAsamParser.g:5468:2: kw= ColonColon this_ID_2= RULE_ID
                    {
                    kw=(Token)match(input,ColonColon,FollowSets000.FOLLOW_5); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQPREFAccess().getColonColonKeyword_1_0()); 
                        
                    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		current.merge(this_ID_2);
                        
                     
                        newLeafNode(this_ID_2, grammarAccess.getQPREFAccess().getIDTerminalRuleCall_1_1()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQPREF"


    // $ANTLR start "entryRuleQCREF"
    // InternalAsamParser.g:5488:1: entryRuleQCREF returns [String current=null] : iv_ruleQCREF= ruleQCREF EOF ;
    public final String entryRuleQCREF() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQCREF = null;


        try {
            // InternalAsamParser.g:5489:1: (iv_ruleQCREF= ruleQCREF EOF )
            // InternalAsamParser.g:5490:2: iv_ruleQCREF= ruleQCREF EOF
            {
             newCompositeNode(grammarAccess.getQCREFRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQCREF=ruleQCREF();

            state._fsp--;

             current =iv_ruleQCREF.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQCREF"


    // $ANTLR start "ruleQCREF"
    // InternalAsamParser.g:5497:1: ruleQCREF returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? ) ;
    public final AntlrDatatypeRuleToken ruleQCREF() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;
        Token this_ID_4=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:5501:6: ( ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? ) )
            // InternalAsamParser.g:5502:1: ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? )
            {
            // InternalAsamParser.g:5502:1: ( (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )? )
            // InternalAsamParser.g:5502:2: (this_ID_0= RULE_ID kw= ColonColon )* this_ID_2= RULE_ID (kw= FullStop this_ID_4= RULE_ID )?
            {
            // InternalAsamParser.g:5502:2: (this_ID_0= RULE_ID kw= ColonColon )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==RULE_ID) ) {
                    int LA38_1 = input.LA(2);

                    if ( (LA38_1==ColonColon) ) {
                        alt38=1;
                    }


                }


                switch (alt38) {
            	case 1 :
            	    // InternalAsamParser.g:5502:7: this_ID_0= RULE_ID kw= ColonColon
            	    {
            	    this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_56); 

            	    		current.merge(this_ID_0);
            	        
            	     
            	        newLeafNode(this_ID_0, grammarAccess.getQCREFAccess().getIDTerminalRuleCall_0_0()); 
            	        
            	    kw=(Token)match(input,ColonColon,FollowSets000.FOLLOW_5); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQCREFAccess().getColonColonKeyword_0_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

            this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_57); 

            		current.merge(this_ID_2);
                
             
                newLeafNode(this_ID_2, grammarAccess.getQCREFAccess().getIDTerminalRuleCall_1()); 
                
            // InternalAsamParser.g:5522:1: (kw= FullStop this_ID_4= RULE_ID )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==FullStop) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalAsamParser.g:5523:2: kw= FullStop this_ID_4= RULE_ID
                    {
                    kw=(Token)match(input,FullStop,FollowSets000.FOLLOW_5); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQCREFAccess().getFullStopKeyword_2_0()); 
                        
                    this_ID_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		current.merge(this_ID_4);
                        
                     
                        newLeafNode(this_ID_4, grammarAccess.getQCREFAccess().getIDTerminalRuleCall_2_1()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQCREF"


    // $ANTLR start "entryRuleSTAR"
    // InternalAsamParser.g:5543:1: entryRuleSTAR returns [String current=null] : iv_ruleSTAR= ruleSTAR EOF ;
    public final String entryRuleSTAR() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSTAR = null;


        try {
            // InternalAsamParser.g:5544:1: (iv_ruleSTAR= ruleSTAR EOF )
            // InternalAsamParser.g:5545:2: iv_ruleSTAR= ruleSTAR EOF
            {
             newCompositeNode(grammarAccess.getSTARRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSTAR=ruleSTAR();

            state._fsp--;

             current =iv_ruleSTAR.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSTAR"


    // $ANTLR start "ruleSTAR"
    // InternalAsamParser.g:5552:1: ruleSTAR returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= Asterisk ;
    public final AntlrDatatypeRuleToken ruleSTAR() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // InternalAsamParser.g:5556:6: (kw= Asterisk )
            // InternalAsamParser.g:5558:2: kw= Asterisk
            {
            kw=(Token)match(input,Asterisk,FollowSets000.FOLLOW_2); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getSTARAccess().getAsteriskKeyword()); 
                

            }

             leaveRule();
                
        }
         
        	catch (RecognitionException re) { 
        	    recover(input,re); 
        	    appendSkippedTokens();
        	}
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSTAR"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA22 dfa22 = new DFA22(this);
    static final String dfa_1s = "\14\uffff";
    static final String dfa_2s = "\1\115\2\uffff\1\103\2\uffff\1\u0084\1\164\1\u0083\1\116\2\uffff";
    static final String dfa_3s = "\1\166\2\uffff\1\103\2\uffff\1\u0084\1\164\1\u0083\1\126\2\uffff";
    static final String dfa_4s = "\1\uffff\1\1\1\2\1\uffff\1\5\1\6\4\uffff\1\3\1\4";
    static final String dfa_5s = "\14\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\12\uffff\1\3\4\uffff\1\1\1\uffff\1\5\26\uffff\1\4",
            "",
            "",
            "\1\6",
            "",
            "",
            "\1\7",
            "\1\10",
            "\1\11",
            "\1\13\7\uffff\1\12",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "241:1: (this_ErrsStatement_0= ruleErrsStatement | this_InternalFailureStatement_1= ruleInternalFailureStatement | this_SafetyConstraintHandlesStatement_2= ruleSafetyConstraintHandlesStatement | this_SafetyConstraintPreventsStatement_3= ruleSafetyConstraintPreventsStatement | this_ErrorPropagationRuleStatement_4= ruleErrorPropagationRuleStatement | this_TypeStatement_5= ruleTypeStatement )";
        }
    }
    static final String dfa_7s = "\25\uffff";
    static final String dfa_8s = "\10\uffff\1\17\1\21\1\23\3\uffff\1\17\1\uffff\1\21\3\uffff\1\23";
    static final String dfa_9s = "\1\102\5\uffff\2\172\3\122\3\uffff\1\122\1\uffff\1\122\1\uffff\1\u0084\1\uffff\1\122";
    static final String dfa_10s = "\1\u0084\5\uffff\4\u0084\1\165\3\uffff\1\165\1\uffff\1\165\1\uffff\1\u0084\1\uffff\1\165";
    static final String dfa_11s = "\1\uffff\1\1\1\2\1\3\1\4\1\5\5\uffff\1\11\1\12\1\6\1\uffff\1\7\1\uffff\1\10\1\uffff\1\13\1\uffff";
    static final String dfa_12s = "\25\uffff}>";
    static final String[] dfa_13s = {
            "\1\3\2\uffff\1\2\16\uffff\1\4\6\uffff\1\14\2\uffff\1\14\14\uffff\1\13\2\uffff\1\6\1\uffff\1\7\5\uffff\1\1\3\uffff\1\11\5\uffff\1\10\2\uffff\1\5\1\12",
            "",
            "",
            "",
            "",
            "",
            "\1\11\5\uffff\1\10\3\uffff\1\15",
            "\1\11\5\uffff\1\10\3\uffff\1\15",
            "\1\17\20\uffff\1\15\4\uffff\1\17\3\uffff\1\17\2\uffff\1\17\5\uffff\1\17\16\uffff\1\16",
            "\1\21\20\uffff\1\15\4\uffff\1\21\3\uffff\1\21\2\uffff\1\21\5\uffff\1\21\16\uffff\1\20",
            "\1\23\20\uffff\1\15\1\22\3\uffff\1\23\3\uffff\1\23\2\uffff\1\23\5\uffff\1\23",
            "",
            "",
            "",
            "\1\17\20\uffff\1\15\4\uffff\1\17\3\uffff\1\17\2\uffff\1\17\5\uffff\1\17",
            "",
            "\1\21\20\uffff\1\15\4\uffff\1\21\3\uffff\1\21\2\uffff\1\21\5\uffff\1\21",
            "",
            "\1\24",
            "",
            "\1\23\20\uffff\1\15\4\uffff\1\23\3\uffff\1\23\2\uffff\1\23\5\uffff\1\23"
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA22 extends DFA {

        public DFA22(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 22;
            this.eot = dfa_7;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "4176:1: (this_RecordTerm_0= ruleRecordTerm | this_ReferenceTerm_1= ruleReferenceTerm | this_ComponentClassifierTerm_2= ruleComponentClassifierTerm | this_ComputedTerm_3= ruleComputedTerm | this_StringTerm_4= ruleStringTerm | this_NumericRangeTerm_5= ruleNumericRangeTerm | this_RealTerm_6= ruleRealTerm | this_IntegerTerm_7= ruleIntegerTerm | this_ListTerm_8= ruleListTerm | this_BooleanLiteral_9= ruleBooleanLiteral | this_LiteralorReferenceTerm_10= ruleLiteralorReferenceTerm )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000002L,0x00400000A1002000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000010L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000000L,0x0010000000000000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000000L,0x0000000000800000L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0xEFF7FFFFF7FFFFF0L,0x0000000000020183L,0x0000000000000010L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000000L,0x0080000000000000L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000000L,0x0000008000000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000000L,0x0000002000000000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000008000000L,0x0000000002000810L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000002L,0x0000800000000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000000L,0x0080800000000000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000000000L,0x0000004000000000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000000L,0x0000000000010400L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x1008000000000000L,0x0000000000000240L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000000000L,0x080C000000000000L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000000L,0x0000002100000000L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000000000L,0x0441480048101024L,0x0000000000000019L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000000L,0x0020810000040000L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000000000L,0x0020810000000000L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000002L,0x0000010000000000L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000000L,0x0000900000000000L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000000000L,0x0080000000000000L,0x0000000000000010L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000000000L,0x0441580048101024L,0x0000000000000019L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000002L,0x0042000000000000L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000000L,0x0080000800000000L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000000L,0x0401400000000000L,0x0000000000000011L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000010L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000001L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000000000002L,0x0000000004000000L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000000000002L,0x0000001000000000L});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000000000000000L,0x0000001000000000L});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000002L,0x0002000000000000L});
    }


}