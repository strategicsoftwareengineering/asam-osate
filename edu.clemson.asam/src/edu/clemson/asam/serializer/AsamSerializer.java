package edu.clemson.asam.serializer;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.impl.Serializer;

//import com.google.inject.Guice;
//import com.google.inject.Injector;
import com.google.inject.Inject;
//import com.google.inject.Provider;

import edu.clemson.asam.services.AsamGrammarAccess;
import edu.clemson.asam.asam.AsamContractLibrary;
import edu.clemson.asam.asam.AsamContractSubclause;;

@SuppressWarnings("restriction")
public class AsamSerializer extends Serializer {
	@Inject
	private AsamGrammarAccess grammarAccess;

    @Override
    protected EObject getContext(EObject semanticObject) {
    	EObject result = null;
    	if (semanticObject instanceof AsamContractLibrary) {
    		result = grammarAccess.getAsamLibraryRule();
    	} else if (semanticObject instanceof AsamContractSubclause) {
    		result = grammarAccess.getAsamContractRule();
    	} else {
    		result = super.getContext(semanticObject);
    	}
    	return result;
    }
}
