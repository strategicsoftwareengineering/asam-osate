package edu.clemson.asam.analysis.views;


import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.part.ViewPart;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.WriteToFile;

import edu.clemson.asam.analysis.handlers.PortError;
import edu.clemson.asam.analysis.handlers.AsamReport;

public class AsamResultsView extends ViewPart {
    public static final String ID = "edu.clemson.asam.analysis.views.asamResultsView";

    TableViewer viewer;
    
	@Override
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		createColumns(viewer);
		
		// make lines and header visible
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
        getSite().setSelectionProvider(viewer);

        GridData gridData = new GridData();
        gridData.verticalAlignment = GridData.FILL;
        gridData.horizontalSpan = 2;
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.horizontalAlignment = GridData.FILL;
        viewer.getControl().setLayoutData(gridData);
	}
	
	String[] columns = {"Internal Failure", "Error Caused", "Probability Value", "Probability Threshold", "Hazard Level", "Component Name", "Component Type", "Propogation Path", "Error Type Propogation", "Mitigated by Safety Constraint", "Mitigated in Component Name", "Mitigated by Component Type", "Verifed"};
	
	//Define head of the table and its size in the array
	void createColumns(TableViewer viewer) {
		String[] titles = columns;
		Integer[] bounds = {100,100,100,100,100,100,100,100,100,100,100,100,100};
		
		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
		
		col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
        		AsamReport err = (AsamReport)element;
                return err.getInternalFailureId() + ": " + err.getInternalFailureDescription();
            }
        });
        col = createTableViewerColumn(titles[1], bounds[1], 1);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
        		AsamReport err = (AsamReport)element;
                return err.getErrorCaused();
            }
        });
        col = createTableViewerColumn(titles[2], bounds[2], 2);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
        		AsamReport err = (AsamReport)element;
                return err.getProbabilityOfOccurrence().toString();
            }
        });
        col = createTableViewerColumn(titles[3], bounds[3], 3);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
        		AsamReport err = (AsamReport)element;
                return err.getProbabilityString();
            }
        });
        col = createTableViewerColumn(titles[4], bounds[4], 4);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
        		AsamReport err = (AsamReport)element;
                return err.getHazardLevel();
            }
        });
        col = createTableViewerColumn(titles[5], bounds[5], 5);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
        		AsamReport err = (AsamReport)element;
                return err.getSourceComponentName();
            }
        });
        col = createTableViewerColumn(titles[6], bounds[6], 6);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
            		AsamReport err = (AsamReport)element;
            		return err.getSourceComponentType();
            }
        });
        col = createTableViewerColumn(titles[7], bounds[7], 7);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
            		AsamReport err = (AsamReport)element;
            		return err.getPropogationPath();
            }
        });
        col = createTableViewerColumn(titles[8], bounds[8], 8);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
            		AsamReport err = (AsamReport)element;
            		String s = "";
            		boolean first = true;
            		for(PortError e : err.getErrorTypePropogation()) {
            			if(!first) s += "->";
            			first = false;
            			s += e.getError() + "{" + e.getPort() + "(" + e.getHazardLevel() + ",p=" + e.getProbabilityOfOccurrence() + ")" + "}";
            		}
            		return s;
            }
        });
        col = createTableViewerColumn(titles[9], bounds[9], 9);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
            		AsamReport err = (AsamReport)element;
            		if(err.getMitigatingSafetyConstraintId() != null) {
            			return err.getMitigatingSafetyConstraintId() + ": " + err.getMitigatingSafetyConstraintDescription();
            		}
            		return "";
            }
        });
        col = createTableViewerColumn(titles[10], bounds[10], 10);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
            		AsamReport err = (AsamReport)element;
                return err.getMitigatingComponentName();
            }
        });
        col = createTableViewerColumn(titles[11], bounds[11], 11);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
            		AsamReport err = (AsamReport)element;
                return err.getMitigatingComponentType();
            }
        });
        col = createTableViewerColumn(titles[12], bounds[12], 12);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
        		AsamReport err = (AsamReport)element;
                return err.getVerified();
            }
        });
	}
	
	private TableViewerColumn createTableViewerColumn(String title, int bound, final int colNumber) {
        final TableViewerColumn viewerColumn = new TableViewerColumn(viewer, SWT.NONE);
        final org.eclipse.swt.widgets.TableColumn column = viewerColumn.getColumn();
        column.setText(title);
        column.setWidth(bound);
        column.setResizable(true);
        column.setMoveable(true);
        return viewerColumn;
    }
	
	private void writeReportToCSV(List<AsamReport> reportRows, SystemInstance si) {
		WriteToFile writeToFile = new WriteToFile("ASAM-ANALYSIS", si);
		writeToFile.setFileExtension("csv");
		String s = "";
		// first row
		boolean first = true;
		for(String column : columns) {
			s += (first ? "" : ",") + String.format("\"%s\"", column);
			first = false;
		}
		s += "\n";
		writeToFile.addOutput(s);
		for(AsamReport reportRow : reportRows) {
			s = String.format("\"%s\"", reportRow.getInternalFailureId() + ": " + reportRow.getInternalFailureDescription()) + ",";
			s += String.format("\"%s\"", reportRow.getErrorCaused()) + ",";
			s += String.format("\"%s\"", reportRow.getProbabilityOfOccurrence()) + ",";
			s += String.format("\"%s\"", reportRow.getProbabilityString()) + ",";
			s += String.format("\"%s\"", reportRow.getHazardLevel()) + ",";
			s += String.format("\"%s\"", reportRow.getSourceComponentName()) + ",";
			s += String.format("\"%s\"", reportRow.getSourceComponentType()) + ",";
			s += String.format("\"%s\"", reportRow.getPropogationPath()) + ",";
			String g = "";
			first = true;
    		for(PortError e : reportRow.getErrorTypePropogation()) {
    			if(!first) g += "->";
    			first = false;
    			g += e.getError() + "{" + e.getPort() + "(" + e.getHazardLevel() + ",p=" + e.getProbabilityOfOccurrence() + ")" + "}";
    		}
			s += String.format("\"%s\"", g) + ",";
			if(reportRow.getMitigatingSafetyConstraintId() != null) {
				s += String.format("\"%s\"", reportRow.getMitigatingSafetyConstraintId() + ": " + reportRow.getMitigatingSafetyConstraintDescription()) + ",";
    		} else {
    			s += ",";
    		}
			
			s += String.format("\"%s\"", reportRow.getMitigatingComponentName()) + ",";
			s += String.format("\"%s\"", reportRow.getMitigatingComponentType());
			s += String.format("\"%s\"", reportRow.getVerified());
			s += "\n";
			writeToFile.addOutput(s);
		}
		writeToFile.saveToFile();
	}
	
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	public void setInput(List<AsamReport> rows, SystemInstance si) {
		viewer.setContentProvider(new ArrayContentProvider());
		if(rows == null) {
			viewer.setInput(new ArrayList<AsamReport>());
		} else {
			viewer.setInput(rows);
			writeReportToCSV(rows, si);
		}
	}
}
