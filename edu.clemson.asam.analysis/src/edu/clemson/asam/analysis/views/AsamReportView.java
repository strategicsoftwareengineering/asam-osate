package edu.clemson.asam.analysis.views;


import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.part.ViewPart;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.WriteToFile;

import edu.clemson.asam.analysis.handlers.ComponentError;
import edu.clemson.asam.analysis.handlers.PortError;

public class AsamReportView extends ViewPart {
    public static final String ID = "edu.clemson.asam.analysis.views.asamReportView";

    TableViewer viewer;
    
	@Override
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		createColumns(viewer);
		
		// make lines and header visible
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
        getSite().setSelectionProvider(viewer);

        GridData gridData = new GridData();
        gridData.verticalAlignment = GridData.FILL;
        gridData.horizontalSpan = 2;
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.horizontalAlignment = GridData.FILL;
        viewer.getControl().setLayoutData(gridData);
	}
	
	String columns[] = {"Component Name", "Error Ontology", "Probability Value", "Probability Threshold", "Hazard Level", "Unsafe Control Action", "Causes of UCA", "Safety Constraints", "Verified"};
	
	//Define head of the table and its size in the array
	void createColumns(TableViewer viewer) {
		String[] titles = columns;
		Integer[] bounds = {100,100,100,100,100,300,400,400,100};
		
		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
		
		//Get a column for Component Name
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getComponentName();
            }
        });
        //Get a column for Error Ontology
        col = createTableViewerColumn(titles[1], bounds[1], 1);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getErrorOntologyType();
            }
        });
        col = createTableViewerColumn(titles[2], bounds[2], 2);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getProbabilityOfOccurence().toString();
            }
        });
        col = createTableViewerColumn(titles[3], bounds[3], 3);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getProbabilityString();
            }
        });
        col = createTableViewerColumn(titles[4], bounds[4], 4);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getHazardLevel();
            }
        });
        //Get a column for UCA id and its description
        col = createTableViewerColumn(titles[5], bounds[5], 5);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getUnsafeControlActionId()+": "+err.getUnsafeControlActionDescription();
            }
        });
        //Get a column for Causes of UCA like general cause and specific cause.
        col = createTableViewerColumn(titles[6], bounds[6], 6);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return "General: " + (err.getGeneralCause() != null ? err.getGeneralCause() : "") + " Specific: " + (err.getSpecificCause() != null ? err.getSpecificCause() : "");
            }
        });
        //get a column for safety constraints
        col = createTableViewerColumn(titles[7], bounds[7], 7);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getSafetyConstraintId()+": "+err.getSafetyConstraintDescription();
            }
        });
        col = createTableViewerColumn(titles[8], bounds[8], 8);
        col.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object element) {
                ComponentError err = (ComponentError)element;
                return err.getVerified();
            }
        });
        
	}
	
	private TableViewerColumn createTableViewerColumn(String title, int bound, final int colNumber) {
        final TableViewerColumn viewerColumn = new TableViewerColumn(viewer, SWT.NONE);
        final org.eclipse.swt.widgets.TableColumn column = viewerColumn.getColumn();
        column.setText(title);
        column.setWidth(bound);
        column.setResizable(true);
        column.setMoveable(true);
        return viewerColumn;
    }
	
	private void writeReportToCSV(List<ComponentError> reportRows, SystemInstance si) {
		WriteToFile writeToFile = new WriteToFile("ASAM-REPORT", si);
		writeToFile.setFileExtension("csv");
		
		String s = "";
		// first row
		boolean first = true;
		for(String column : columns) {
			s += (first ? "" : ",") + String.format("\"%s\"", column);
			first = false;
		}
		s += "\n";
		writeToFile.addOutput(s);
		for(ComponentError reportRow : reportRows) {
			s = String.format("\"%s\"", reportRow.getComponentName()) + ",";
			s += String.format("\"%s\"", reportRow.getErrorOntologyType()) + ",";
			s += String.format("\"%s\"", reportRow.getProbabilityOfOccurence()) + ",";
			s += String.format("\"%s\"", reportRow.getProbabilityString()) + ",";
			s += String.format("\"%s\"", reportRow.getHazardLevel()) + ",";
			s += String.format("\"%s\"", reportRow.getUnsafeControlActionId() + ": " + reportRow.getUnsafeControlActionDescription()) + ",";
			s += String.format("\"%s\"", "General: " + (reportRow.getGeneralCause() != null ? reportRow.getGeneralCause() : "") + " Specific: " + (reportRow.getSpecificCause() != null ? reportRow.getSpecificCause() : "")) + ",";
			s += String.format("\"%s\"", reportRow.getSafetyConstraintId() + ": " + reportRow.getSafetyConstraintDescription());
			s += String.format("\"%s\"", reportRow.getVerified());
			s += "\n";
			writeToFile.addOutput(s);
		}
		
		writeToFile.saveToFile();
	}
	
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	public void setInput(List<ComponentError> rows, SystemInstance si) {
		viewer.setContentProvider(new ArrayContentProvider());
		if(rows == null) {
			viewer.setInput(new ArrayList<ComponentError>());
		} else {
			viewer.setInput(rows);
			writeReportToCSV(rows, si);
		}
	}
}