package edu.clemson.asam.analysis.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

import edu.clemson.asam.analysis.Activator;
import edu.clemson.asam.analysis.views.AsamReportView;
import edu.clemson.xagree.analysis.handlers.VerifyAllHandler;
import jkind.api.results.JKindResult;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instantiation.InstantiateModel;
import org.osate.aadl2.modelsupport.util.AadlUtil;
import org.eclipse.core.resources.ResourcesPlugin;
import org.osate.aadl2.Element;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.impl.ComponentImplementationImpl;

public class ReportHandler extends AbstractHandler {
	private IWorkbenchWindow window;

	@Override
	public Object execute(ExecutionEvent event) {
		URI uri = getSelectionURI(HandlerUtil.getCurrentSelection(event));
		if (uri == null) {
			return null;
		}
		
		window = HandlerUtil.getActiveWorkbenchWindow(event);
		if (window == null) {
			return null;
		}

		clearView();
		return executeURI(uri);
	}
	//Create list for ComponentError.java
	private List<ComponentError> errors = new ArrayList<ComponentError>();
	private SystemInstance systemInstance;
	
	protected void showView() {
		getWindow().getShell().getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    AsamReportView page = (AsamReportView) getWindow().getActivePage().showView(AsamReportView.ID);
                    page.setInput(errors, systemInstance); //Set errors as an input object
                } catch (PartInitException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected void clearView() {
    	getWindow().getShell().getDisplay().syncExec(new Runnable() {
            @Override
            public void run() {
                try {
                		AsamReportView page = (AsamReportView) getWindow().getActivePage().showView(AsamReportView.ID);
                		page.setInput(null, systemInstance); //Set ComponentError 
                } catch (PartInitException e) {
                    e.printStackTrace();
                }
            }
        });
    } 
    
    private SystemInstance instantiateComponent(ComponentImplementation ci) {
    	try {
    		return InstantiateModel.buildInstanceModelFile(ci);
		} catch (Exception e) {
			throw new RuntimeException("Error Instantiating model");
		}
    }
    
    public static boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(children[i]);
                if (!success) {
                    return false;
                }
            }
        }

        // either file or an empty directory
        System.out.println("removing file or directory : " + dir.getName());
        return dir.delete();
    }

	protected IWorkbenchWindow getWindow() {
		return window;
	}
	
	public Object executeURI(final URI uri) {
		final XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
		if (xtextEditor == null) {
			return null;
		}

		WorkspaceJob job = getWorkspaceJob(xtextEditor, uri);
		job.setRule(ResourcesPlugin.getWorkspace().getRoot());
		job.schedule();
		return null;
	}
	
	protected WorkspaceJob getWorkspaceJob(XtextEditor xtextEditor, URI uri) {
		return new WorkspaceJob("ASAM_CALCUATE") {
			@Override
			public IStatus runInWorkspace(IProgressMonitor monitor) {
				return xtextEditor.getDocument().readOnly(getUnitOfWork(uri, monitor));
			}
		};
	}

	protected IUnitOfWork<IStatus, XtextResource> getUnitOfWork(URI uri, IProgressMonitor monitor) {
		return new IUnitOfWork<IStatus, XtextResource>() {
			@Override
			public IStatus exec(XtextResource resource) throws Exception {
				deleteDirectory(new File("instances"));
				
				EObject eobj = resource.getResourceSet().getEObject(uri, true);
				SystemInstance si = CalculateHandler.getSysInstance((Element) eobj); //create object si
				systemInstance = si;
				
				errors.clear(); //Clear object
				//get componentImpl as an object
				ComponentImplementationImpl componentImpl = (ComponentImplementationImpl)si.getComponentImplementation();
				if(componentImpl != null) { 
					analyzer(componentImpl, monitor); //Call componentImpl analyzer
					showView();
					try {
						return new Status(IStatus.OK, Activator.PLUGIN_ID, null);
			        } catch (Throwable e) {
			            return new Status(IStatus.ERROR, Activator.PLUGIN_ID, null);
			        }
				}
				return new Status(IStatus.ERROR, Activator.PLUGIN_ID, null);
			}
		};
	}
	//define ASAM for .impl part of the component
	private void analyzer (ComponentImplementation impl, IProgressMonitor monitor){
		if(containsAsamAnnex(impl)) {
			AsamEvaluator evaluator = new AsamEvaluator();
			List<ComponentError> componentErrors = evaluator.evaluateAsamSubclauseForErrors(getAsamAnnex(impl));
			for(ComponentError error : componentErrors) {
				error.setComponentName(impl.getFullName());
			}
			
			VerifyAllHandler handler = new VerifyAllHandler();
			try {
				handler.verifyComponent(impl, monitor, true, false);
				List<JKindResult> verificationResults = handler.getResults();
				for(ComponentError error : componentErrors) {
					boolean allPass = true;
					for(String id : error.getVerifiedBy()) {
						allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(impl), verificationResults, id);
					}
					error.setVerified(allPass ? "Y" : "N");
				}
			} catch (Exception e) {
				// handle generated exception
				e.printStackTrace();
			}
			
			errors.addAll(componentErrors);
		}
		
		if(impl != null && impl.getChildren() != null) {
			for (Element component: impl.getChildren()){
				if(component instanceof Subcomponent){
					ComponentImplementation compImpl = ((Subcomponent)component).getComponentImplementation();
					analyzer(compImpl, monitor);
				}
			}
		}
		
	}
	
	private boolean containsAsamAnnex(ComponentImplementation componentImpl) {
		if(componentImpl != null && componentImpl.getOwnedAnnexSubclauses() != null) {
			for(AnnexSubclause subclause : componentImpl.getOwnedAnnexSubclauses()) {
				if(subclause.getName().equals("asam")) {
					instantiateComponent(componentImpl);
					return true;
				}
			}
		}
		return false;
	}
	
	private AnnexSubclause getAsamAnnex(ComponentImplementation componentImpl) {
		for(AnnexSubclause subclause : componentImpl.getOwnedAnnexSubclauses()) {
			if(subclause.getName().equals("asam")) {
				instantiateComponent(componentImpl);
				return subclause;
			}
		}
		return null;
	}
	
	private URI getSelectionURI(ISelection currentSelection) {
		if (currentSelection instanceof IStructuredSelection) {
			IStructuredSelection iss = (IStructuredSelection) currentSelection;
			if (iss.size() == 1) {
				EObjectNode node = (EObjectNode) iss.getFirstElement();
				return node.getEObjectURI();
			}
		} else if (currentSelection instanceof TextSelection) {
			XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
			TextSelection ts = (TextSelection) xtextEditor.getSelectionProvider().getSelection();
			return xtextEditor.getDocument().readOnly(resource -> {
				EObject e = new EObjectAtOffsetHelper().resolveContainedElementAt(resource, ts.getOffset());
				return EcoreUtil2.getURI(e);
			});
		}
		return null;
	}
	
	protected static SystemInstance getSysInstance(Element root) {
		ComponentImplementation ci = getComponentImplementation(root);
		try {
			return InstantiateModel.buildInstanceModelFile(ci);
		} catch (Exception e) {
			return null;
		}
	}
	
	private static ComponentImplementation getComponentImplementation(Element root) {
		Classifier classifier = getOutermostClassifier(root);
		if (classifier instanceof ComponentImplementation) {
			return (ComponentImplementation) classifier;
		}
		if (!(classifier instanceof ComponentType)) {
			throw new RuntimeException("Must select an AADL Component Type or Implementation");
		}
		ComponentType ct = (ComponentType) classifier;
		List<ComponentImplementation> cis = getComponentImplementations(ct);
		if (cis.size() == 0) {
			throw new RuntimeException("AADL Component Type has no implementation to calculate");
		} else if (cis.size() == 1) {
			ComponentImplementation ci = cis.get(0);
			return ci;
		} else {
			throw new RuntimeException("AADL Component Type has multiple implementations to calculate: please select just one");
		}
	}
	
	private static List<ComponentImplementation> getComponentImplementations(ComponentType ct) {
		List<ComponentImplementation> result = new ArrayList<>();
		AadlPackage pkg = AadlUtil.getContainingPackage(ct);
		for (ComponentImplementation ci : EcoreUtil2.getAllContentsOfType(pkg, ComponentImplementation.class)) {
			if (ci.getType().equals(ct)) {
				result.add(ci);
			}
		}
		return result;
	}
	
	protected static Classifier getOutermostClassifier(Element element) {
		List<EObject> containers = new ArrayList<>();
		EObject curr = element;
		while (curr != null) {
			containers.add(curr);
			curr = curr.eContainer();
		}
		Collections.reverse(containers);
		for (EObject container : containers) {
			if (container instanceof Classifier) {
				System.out.println(container);
				return (Classifier) container;
			}
		}
		return null;
	}
}
