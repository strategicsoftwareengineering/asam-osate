package edu.clemson.asam.analysis.handlers;

import edu.clemson.asam.asam.AsamContract;
import edu.clemson.asam.asam.AsamStatement;
import edu.clemson.asam.asam.CausesStatement;
import edu.clemson.asam.asam.ErrorPropagationRuleStatement;
import edu.clemson.asam.asam.ErrorStatement;
import edu.clemson.asam.asam.ErrorTypeStatement;
import edu.clemson.asam.asam.ErrorsList;
import edu.clemson.asam.asam.ErrorsListNoProbability;
import edu.clemson.asam.asam.ErrsStatement;
import edu.clemson.asam.asam.GeneralCauseStatement;
import edu.clemson.asam.asam.InternalFailureStatement;
import edu.clemson.asam.asam.PortsList;
import edu.clemson.asam.asam.SafetyConstraintHandlesStatement;
import edu.clemson.asam.asam.SafetyConstraintPreventsStatement;
import edu.clemson.asam.asam.SafetyConstraintStatement;
import edu.clemson.asam.asam.SpecificCauseStatement;
import edu.clemson.asam.asam.TypeStatement;
import edu.clemson.asam.asam.UnsafeControlActionStatement;
import edu.clemson.asam.asam.impl.AsamContractSubclauseImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;

public class AsamEvaluator {

	public AsamError evaluateAsamLibrary(AnnexLibrary object) {
		if(!object.eContents().isEmpty()) {
			for(EObject obj : object.eContents()) {
				if(obj instanceof AsamContractSubclauseImpl) {
					AsamContract contract = (AsamContract)((AsamContractSubclauseImpl)obj).getContract();
					return evaluateAsamContract(contract);
				}
			}
		}
		return null;
	}

	public AsamError evaluateAsamSubclause(AnnexSubclause object) {
		if(!object.eContents().isEmpty()) {
			for(EObject obj : object.eContents()) {
				if(obj instanceof AsamContractSubclauseImpl) {
					AsamContract contract = (AsamContract)((AsamContractSubclauseImpl)obj).getContract();
					return evaluateAsamContract(contract);
				}
			}
		}
		return null;
	}
    
	public List<ComponentError> evaluateAsamSubclauseForErrors(AnnexSubclause object) {
		if(!object.eContents().isEmpty()) {
			for(EObject obj : object.eContents()) {
				if(obj instanceof AsamContractSubclauseImpl) {
					AsamContract contract = (AsamContract)((AsamContractSubclauseImpl)obj).getContract();
					return evaluateAsamContractForErrors(contract);
				}
			}
		}
		return null;
	}
	
	private List<ComponentError> evaluateAsamContractForErrors(AsamContract object) {
		List<ComponentError> returnValue = new ArrayList<ComponentError>();
		for(AsamStatement statement : object.getStatement()) {
			if(statement instanceof ErrsStatement) {
				returnValue.addAll(evaluateErrsStatement((ErrsStatement)statement));
			}
		}
		return returnValue;
	}
	
	private List<ComponentError> evaluateErrsStatement(ErrsStatement statement) {
		List<ComponentError> returnValue = new ArrayList<ComponentError>();
		returnValue.add(evaluateErrorStatement(statement.getFirstError()));
		if(statement.getRestErrors() != null) {
			for(ErrorStatement errorStatement : statement.getRestErrors()) {
				returnValue.add(evaluateErrorStatement(errorStatement));
			}
		}
		return returnValue;
	}
	
	private ComponentError evaluateErrorStatement(ErrorStatement object) {
		ComponentError returnValue = new ComponentError();
		returnValue = evaluateErrorTypeStatement(object.getType(), returnValue);
		returnValue = evaluateUnsafeControlActionStatement(object.getUca(), returnValue);
		returnValue = evaluateCausesStatement(object.getCause(), returnValue);
		returnValue = evaluateSafetyConstraintStatement(object.getSc(), returnValue);
		
		return returnValue;
	}
	

	//Evaluate sc (set and get) for id and description for SC
	private ComponentError evaluateSafetyConstraintStatement(SafetyConstraintStatement sc, ComponentError returnValue) {
		returnValue.setSafetyConstraintId(sc.getId());
		returnValue.setSafetyConstraintDescription(sc.getDescription());
		returnValue.getVerifiedBy().add(sc.getMatchingXagreeStatements().getFirst());
		if(sc.getMatchingXagreeStatements().getRest() != null) {
			for(String id : sc.getMatchingXagreeStatements().getRest()) {
				returnValue.getVerifiedBy().add(id);
			}
		}
		return returnValue;
	}

	private ComponentError evaluateCausesStatement(CausesStatement cause, ComponentError returnValue) {
		if(cause.getGeneral() != null) {
			returnValue = evaluateGeneralCauseStatement(cause.getGeneral(), returnValue);
		}
		if(cause.getSpecific() != null) {
			returnValue = evaluateSpecificCauseStatement(cause.getSpecific(), returnValue);
		}
		return returnValue;
	}

	private ComponentError evaluateSpecificCauseStatement(SpecificCauseStatement specific, ComponentError returnValue) {
		returnValue.setSpecificCause(specific.getDescription());
		return returnValue;
	}

	private ComponentError evaluateGeneralCauseStatement(GeneralCauseStatement general, ComponentError returnValue) {
		returnValue.setGeneralCause(general.getDescription());
		return returnValue;
	}
	//Evaluate UCA (set and get) for id and description of UCA
	private ComponentError evaluateUnsafeControlActionStatement(UnsafeControlActionStatement uca,
			ComponentError returnValue) {
		returnValue.setUnsafeControlActionId(uca.getId());
		returnValue.setUnsafeControlActionDescription(uca.getDescription());
		return returnValue;
	}
	//Evaluate error type (set and get) for error type.
	private ComponentError evaluateErrorTypeStatement(ErrorTypeStatement type, ComponentError returnValue) {
		returnValue.setErrorOntologyType(type.getErrorType().getError());
		returnValue.setProbabilityOfOccurence(new BigDecimal(type.getErrorType().getProbability()));
		returnValue.setHazardLevel(type.getErrorType().getSeverityLevel());
		return returnValue;
	}
	
	private AsamError evaluateAsamContract(AsamContract object) {
		AsamError returnValue = new AsamError();
		for(AsamStatement statement : object.getStatement()) {
			if(statement instanceof TypeStatement) {
				evaluateTypeStatement((TypeStatement)statement, returnValue);
			} else if (statement instanceof InternalFailureStatement) {
				evaluateInternalFailureStatement((InternalFailureStatement)statement, returnValue);
			} else if (statement instanceof SafetyConstraintHandlesStatement) {
				evaluateSafetyConstraintHandlesStatement((SafetyConstraintHandlesStatement)statement, returnValue);
			} else if (statement instanceof SafetyConstraintPreventsStatement) {
				evaluateSafetyConstraintPreventsStatement((SafetyConstraintPreventsStatement)statement, returnValue);
			} else if (statement instanceof ErrorPropagationRuleStatement) {
				evaluateErrorPropagationRuleStatement((ErrorPropagationRuleStatement)statement, returnValue);
			}
		}
		return returnValue;
	}
	
	private void evaluateTypeStatement(TypeStatement statement, AsamError returnValue) {
		returnValue.setType(statement.getType());
	}
	
	private class TempError {
		public String errorType;
		public BigDecimal probabilityOfOccurrence;
		public String hazardLevel;
	}
	
	private void evaluateInternalFailureStatement(InternalFailureStatement statement, AsamError returnValue) {
		InternalFailure intf = new InternalFailure();
		intf.setId(statement.getInternalFailureId());
		intf.setDescription(statement.getInternalFailureDescription());
		List<TempError> errors = evaluateErrorsList(statement.getErrors());
		List<String> ports = evaluatePortsList(statement.getPorts());
		Set<PortError> portErrors = new HashSet<PortError>();
		for(TempError error : errors) {
			for(String port : ports) {
				PortError portError = new PortError();
				portError.setError(error.errorType);
				portError.setProbabilityOfOccurrence(error.probabilityOfOccurrence);
				portError.setHazardLevel(error.hazardLevel);
				portError.setPort(port);
				portErrors.add(portError);
			}
		}
		returnValue.getInternalFailuresToErrorsCaused().put(intf, portErrors);
	}
	
	private void evaluateSafetyConstraintHandlesStatement(SafetyConstraintHandlesStatement statement, AsamError returnValue) {
		SafetyConstraint sc = new SafetyConstraint();
		sc.setId(statement.getSafetyConstraintId());
		sc.setDescription(statement.getSafetyConstraintDescription());
		sc.getVerifiedBy().add(statement.getMatchingXagreeStatements().getFirst());
		if(statement.getMatchingXagreeStatements().getRest() != null) {
			for(String id : statement.getMatchingXagreeStatements().getRest()) {
				sc.getVerifiedBy().add(id);
			}
		}
		List<TempError> errors = evaluateErrorsListNoProbability(statement.getErrors());
		List<String> ports = evaluatePortsList(statement.getPorts());
		Set<PortError> portErrors = new HashSet<PortError>();
		for(TempError error : errors) {
			for(String port : ports) {
				PortError portError = new PortError();
				portError.setError(error.errorType);
				//portError.setProbabilityOfOccurrence(error.probabilityOfOccurrence);
				//portError.setHazardLevel(error.hazardLevel);
				portError.setPort(port);
				portErrors.add(portError);
			}
		}
		returnValue.getIncomingErrorsHandled().put(sc, portErrors);
	}
	
	private void evaluateSafetyConstraintPreventsStatement(SafetyConstraintPreventsStatement statement, AsamError returnValue) {
		SafetyConstraint sc = new SafetyConstraint();
		sc.setId(statement.getSafetyConstraintId());
		sc.setDescription(statement.getSafetyConstraintDescription());
		sc.getVerifiedBy().add(statement.getMatchingXagreeStatements().getFirst());
		if(statement.getMatchingXagreeStatements().getRest() != null) {
			for(String id : statement.getMatchingXagreeStatements().getRest()) {
				sc.getVerifiedBy().add(id);
			}
		}
		List<TempError> errors = evaluateErrorsListNoProbability(statement.getErrors());
		List<String> ports = evaluatePortsList(statement.getPorts());
		Set<PortError> portErrors = new HashSet<PortError>();
		for(TempError error : errors) {
			for(String port : ports) {
				PortError portError = new PortError();
				portError.setError(error.errorType);
				//portError.setProbabilityOfOccurrence(error.probabilityOfOccurrence);
				//portError.setHazardLevel(error.hazardLevel);
				portError.setPort(port);
				portErrors.add(portError);
			}
		}
		returnValue.getOutgoingErrorsPrevented().put(sc, portErrors);
	}
	
	private void evaluateErrorPropagationRuleStatement(ErrorPropagationRuleStatement statement, AsamError returnValue) {
		List<TempError> inErrors = evaluateErrorsListNoProbability(statement.getInErrorsList());
		List<String> inPorts = evaluatePortsList(statement.getInPortsLists());
		
		List<TempError> outErrors = evaluateErrorsList(statement.getOutErrorsList());
		List<String> outPorts = evaluatePortsList(statement.getOutPortsLists());
		Set<PortError> outPortErrors = new HashSet<PortError>();
		for(TempError error : outErrors) {
			for(String port : outPorts) {
				PortError portError = new PortError();
				portError.setError(error.errorType);
				portError.setProbabilityOfOccurrence(error.probabilityOfOccurrence);
				portError.setHazardLevel(error.hazardLevel);
				portError.setPort(port);
				outPortErrors.add(portError);
			}
		}
		
		for(TempError error : inErrors) {
			for(String port : inPorts) {
				PortError portError = new PortError();
				portError.setError(error.errorType);
				//portError.setProbabilityOfOccurrence(error.probabilityOfOccurrence);
				//portError.setHazardLevel(error.hazardLevel);
				portError.setPort(port);
				returnValue.getErrorPropagationMap().put(portError, outPortErrors);
			}
		}
	}
	
	private List<TempError> evaluateErrorsList(ErrorsList errors) {
		List<TempError> strs = new ArrayList<TempError>();
		TempError err1 = new TempError();
		err1.errorType = errors.getFirstError().getError();
		err1.probabilityOfOccurrence = new BigDecimal(errors.getFirstError().getProbability());
		err1.hazardLevel = errors.getFirstError().getSeverityLevel();
		strs.add(err1);
		if(errors.getRestErrors() != null) {
			for(edu.clemson.asam.asam.Error str : errors.getRestErrors()) {
				TempError err = new TempError();
				err.errorType = str.getError();
				err.probabilityOfOccurrence = new BigDecimal(str.getProbability());
				err.hazardLevel = str.getSeverityLevel();
				strs.add(err);
			}
		}
		return strs;
	}
	
	private List<TempError> evaluateErrorsListNoProbability(ErrorsListNoProbability errors) {
		List<TempError> strs = new ArrayList<TempError>();
		TempError err1 = new TempError();
		err1.errorType = errors.getFirstError().getError();
		//err1.probabilityOfOccurrence = new BigDecimal(errors.getFirstError().getProbability());
		//err1.hazardLevel = errors.getFirstError().getSeverityLevel();
		strs.add(err1);
		if(errors.getRestErrors() != null) {
			for(edu.clemson.asam.asam.ErrorNoProbability str : errors.getRestErrors()) {
				TempError err = new TempError();
				err.errorType = str.getError();
				//err.probabilityOfOccurrence = new BigDecimal(str.getProbability());
				//err.hazardLevel = str.getSeverityLevel();
				strs.add(err);
			}
		}
		return strs;
	}
	
	private List<String> evaluatePortsList(PortsList ports) {
		List<String> strs = new ArrayList<String>();
		strs.add(ports.getFirstPort());
		if(ports.getRestPorts() != null) {
			for(String str : ports.getRestPorts()) {
				strs.add(str);
			}
		}
		return strs;
	}
}
