package edu.clemson.asam.analysis.handlers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AsamReport implements Serializable {
	
	private static final long serialVersionUID = 1557381997966019717L;
	
	private String internalFailureId;
	private String internalFailureDescription;
	private String errorCaused;
	private BigDecimal probabilityOfOccurrence;
	private String hazardLevel;
	private String sourceComponentName;
	private String sourceComponentType;
	private String propogationPath;
	private List<PortError> errorTypePropogation;
	private String mitigatingSafetyConstraintId;
	private String mitigatingSafetyConstraintDescription;
	private String mitigatingComponentName;
	private String mitigatingComponentType;
	private String verified;
	
	public String getVerified() {
		return verified;
	}
	public void setVerified(String verified) {
		this.verified = verified;
	}
	public String getInternalFailureId() {
		return internalFailureId;
	}
	public void setInternalFailureId(String internalFailureId) {
		this.internalFailureId = internalFailureId;
	}
	public String getInternalFailureDescription() {
		return internalFailureDescription;
	}
	public void setInternalFailureDescription(String internalFailureDescription) {
		this.internalFailureDescription = internalFailureDescription;
	}
	public String getErrorCaused() {
		return errorCaused;
	}
	public void setErrorCaused(String errorCaused) {
		this.errorCaused = errorCaused;
	}
	public String getSourceComponentName() {
		return sourceComponentName;
	}
	public void setSourceComponentName(String sourceComponentName) {
		this.sourceComponentName = sourceComponentName;
	}
	public String getSourceComponentType() {
		return sourceComponentType;
	}
	public void setSourceComponentType(String sourceComponentType) {
		this.sourceComponentType = sourceComponentType;
	}
	public String getMitigatingSafetyConstraintId() {
		return mitigatingSafetyConstraintId;
	}
	public void setMitigatingSafetyConstraintId(String mitigatingSafetyConstraintId) {
		this.mitigatingSafetyConstraintId = mitigatingSafetyConstraintId;
	}
	public String getMitigatingSafetyConstraintDescription() {
		return mitigatingSafetyConstraintDescription;
	}
	public void setMitigatingSafetyConstraintDescription(String mitigatingSafetyConstraintDescription) {
		this.mitigatingSafetyConstraintDescription = mitigatingSafetyConstraintDescription;
	}
	public String getMitigatingComponentName() {
		return mitigatingComponentName;
	}
	public void setMitigatingComponentName(String mitigatingComponentName) {
		this.mitigatingComponentName = mitigatingComponentName;
	}
	public String getMitigatingComponentType() {
		return mitigatingComponentType;
	}
	public void setMitigatingComponentType(String mitigatingComponentType) {
		this.mitigatingComponentType = mitigatingComponentType;
	}
	public String getPropogationPath() {
		return propogationPath;
	}
	public void setPropogationPath(String propogationPath) {
		this.propogationPath = propogationPath;
	}
	public List<PortError> getErrorTypePropogation() {
		if(errorTypePropogation == null) {
			errorTypePropogation = new ArrayList<PortError>();
		}
		return errorTypePropogation;
	}
	public void setErrorTypePropogation(List<PortError> errorTypePropogation) {
		this.errorTypePropogation = errorTypePropogation;
	}
	public BigDecimal getProbabilityOfOccurrence() {
		return probabilityOfOccurrence;
	}
	public void setProbabilityOfOccurrence(BigDecimal probabilityOfOccurrence) {
		this.probabilityOfOccurrence = probabilityOfOccurrence;
	}
	public String getHazardLevel() {
		return hazardLevel;
	}
	public void setHazardLevel(String hazardLevel) {
		this.hazardLevel = hazardLevel;
	}
	public String getProbabilityString() {
		if(this.probabilityOfOccurrence != null) {
			return ProbabilityLevelTranslator.getProbabilityLevelString(probabilityOfOccurrence);
		}
		return "";
	}
}
