package edu.clemson.asam.analysis.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.impl.EObjectNode;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

import edu.clemson.asam.analysis.Activator;
import edu.clemson.asam.analysis.handlers.InternalFailure;
import edu.clemson.asam.analysis.handlers.PortError;
import edu.clemson.asam.analysis.handlers.SafetyConstraint;
import edu.clemson.asam.analysis.views.AsamResultsView;
import edu.clemson.xagree.analysis.handlers.VerifyAllHandler;
import jkind.api.results.JKindResult;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.ConnectedElement;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ConnectionEnd;
import org.osate.aadl2.Context;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instantiation.InstantiateModel;
import org.osate.aadl2.modelsupport.util.AadlUtil;

import org.eclipse.core.resources.ResourcesPlugin;
import org.osate.aadl2.Element;
import org.osate.aadl2.Feature;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.impl.ComponentImplementationImpl;
import org.osate.aadl2.impl.SubcomponentImpl;

public class CalculateHandler extends AbstractHandler {
	private IWorkbenchWindow window;

	@Override
	public Object execute(ExecutionEvent event) {
		URI uri = getSelectionURI(HandlerUtil.getCurrentSelection(event));
		if (uri == null) {
			return null;
		}
		
		window = HandlerUtil.getActiveWorkbenchWindow(event);
		if (window == null) {
			return null;
		}

		clearView();
		return executeURI(uri);
	}
	//Create list for ComponentError.java
	private List<AsamReport> errors = new ArrayList<AsamReport>();
	private SystemInstance systemInstance;
	
	protected void showView() {
		getWindow().getShell().getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    AsamResultsView page = (AsamResultsView) getWindow().getActivePage().showView(AsamResultsView.ID);
                    page.setInput(errors, systemInstance); //Set errors as an input object
                } catch (PartInitException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected void clearView() {
    	getWindow().getShell().getDisplay().syncExec(new Runnable() {
            @Override
            public void run() {
                try {
                	AsamResultsView page = (AsamResultsView) getWindow().getActivePage().showView(AsamResultsView.ID);
                	page.setInput(null, systemInstance); //Set ComponentError 
                } catch (PartInitException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private SystemInstance instantiateComponent(ComponentImplementation ci) {
    	try {
    		return InstantiateModel.buildInstanceModelFile(ci);
		} catch (Exception e) {
			throw new RuntimeException("Error Instantiating model");
		}
    }
    
    public static boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(children[i]);
                if (!success) {
                    return false;
                }
            }
        }

        // either file or an empty directory
        System.out.println("removing file or directory : " + dir.getName());
        return dir.delete();
    }

	protected IWorkbenchWindow getWindow() {
		return window;
	}
	
	public Object executeURI(final URI uri) {
		final XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
		if (xtextEditor == null) {
			return null;
		}

		WorkspaceJob job = getWorkspaceJob(xtextEditor, uri);
		job.setRule(ResourcesPlugin.getWorkspace().getRoot());
		job.schedule();
		return null;
	}
	
	protected WorkspaceJob getWorkspaceJob(XtextEditor xtextEditor, URI uri) {
		return new WorkspaceJob("ASAM_CALCUATE") {
			@Override
			public IStatus runInWorkspace(IProgressMonitor monitor) {
				return xtextEditor.getDocument().readOnly(getUnitOfWork(uri, monitor));
			}
		};
	}

	private XtextResource myResource;
	//behind the button in the menu
	protected IUnitOfWork<IStatus, XtextResource> getUnitOfWork(URI uri, IProgressMonitor monitor) {
		return new IUnitOfWork<IStatus, XtextResource>() {
			@Override
			public IStatus exec(XtextResource resource) throws Exception {
				deleteDirectory(new File("instances"));
				
				myResource = resource;
				EObject eobj = resource.getResourceSet().getEObject(uri, true);
				SystemInstance si = CalculateHandler.getSysInstance((Element) eobj); //create object si
				systemInstance = si;
				
				errors.clear(); //Clear object
				//get componentImpl as an object
				ComponentImplementationImpl componentImpl = (ComponentImplementationImpl)si.getComponentImplementation();
				if(componentImpl != null) {
					Map<ComponentImplementation, List<ComponentImplementation>> controllers = getControllers(componentImpl);
					for(Map.Entry<ComponentImplementation, List<ComponentImplementation>> entry : controllers.entrySet()) {
						for(ComponentImplementation controller : entry.getValue()) {
							analyzeFlows(entry.getKey(), controller, monitor);
						}
					}
					showView();
					try {
						return new Status(IStatus.OK, Activator.PLUGIN_ID, null);
			        } catch (Throwable e) {
			            return new Status(IStatus.ERROR, Activator.PLUGIN_ID, null);
			        }
				}
				return new Status(IStatus.ERROR, Activator.PLUGIN_ID, null);
			}
		};
	}
	
	private Map<ComponentImplementation, List<ComponentImplementation>> getControllers(ComponentImplementation impl) {
		Map<ComponentImplementation, List<ComponentImplementation>> componentToControllerList = new HashMap<ComponentImplementation,List<ComponentImplementation>>();
		
		List<ComponentImplementation> controllers = new ArrayList<ComponentImplementation>();
		if(impl != null && impl.getChildren() != null) {
			for (Element component: impl.getChildren()){
				if(component instanceof Subcomponent){
					ComponentImplementation compImpl = ((Subcomponent)component).getComponentImplementation();
					if(containsAsamAnnex(compImpl)) {
						AsamEvaluator evaluator = new AsamEvaluator();
						AsamError asamError = evaluator.evaluateAsamSubclause(getAsamAnnex(compImpl));
						if(asamError.getType().equals("controller")) {
							controllers.add(compImpl);
						}
					}
				}
			}
		}
		if(controllers.size() > 0) {
			componentToControllerList.put(impl, controllers);
		}
		
		if(impl != null && impl.getChildren() != null) {
			for (Element component: impl.getChildren()){
				if(component instanceof Subcomponent){
					ComponentImplementation compImpl = ((Subcomponent)component).getComponentImplementation();
					Map<ComponentImplementation, List<ComponentImplementation>> subcontrollers = getControllers(compImpl);
					for(Map.Entry<ComponentImplementation, List<ComponentImplementation>> entry : subcontrollers.entrySet()) {
						componentToControllerList.put(entry.getKey(), entry.getValue());
					}
				}
			}
		}
		
		return componentToControllerList;
	}
	
	private void analyzeFlows(ComponentImplementation parent, ComponentImplementation controller, IProgressMonitor monitor){
		Map<String, String> portRenamings = getPortRenamings(parent);
		Set<ComponentImplementation> sensors = getSensorsToControllerList(parent, controller);
		Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> tailFlows = getActuatorToControlledProcessToSensorMap(parent, controller);
		generateReportItemsForSensorControllerActuatorFlow(portRenamings, sensors, controller, tailFlows, monitor);
		generateReportItemsForControllerActuatorSensorFlow(portRenamings, controller, tailFlows, monitor);
		generateReportItemsForActuatorSensorFlow(portRenamings, tailFlows, monitor);
	}
	
	private List<AsamReport> doSensorPartOfSCAFlow(Set<ComponentImplementation> sensors, IProgressMonitor monitor) {
		List<AsamReport> sensorGeneratedErrors = new ArrayList<AsamReport>();
		for(ComponentImplementation sensorImpl : sensors) {
			AsamEvaluator sensorEvaluator = new AsamEvaluator();
			AsamError sensorAsam = sensorEvaluator.evaluateAsamSubclause(getAsamAnnex(sensorImpl));
			for(Map.Entry<InternalFailure, Set<PortError>> internalFailures : sensorAsam.getInternalFailuresToErrorsCaused().entrySet()) {
				for(PortError internalFailureError : internalFailures.getValue()) {
					AsamReport report = new AsamReport();
					report.setInternalFailureId(internalFailures.getKey().getId());
					report.setInternalFailureDescription(internalFailures.getKey().getDescription());
					report.setErrorCaused(internalFailureError.getError() + "{" + internalFailureError.getPort() + "}");
					report.setProbabilityOfOccurrence(internalFailureError.getProbabilityOfOccurrence());
					report.setHazardLevel(internalFailureError.getHazardLevel());
					report.setSourceComponentName(sensorImpl.getFullName());
					report.setSourceComponentType("S");
					report.setPropogationPath(sensorImpl.getFullName());
					report.getErrorTypePropogation().add(internalFailureError);
					//outbound check for the sensor
					SafetyConstraint mitigatingConstraint = null;
					for(Map.Entry<SafetyConstraint, Set<PortError>> scs : sensorAsam.getOutgoingErrorsPrevented().entrySet()) {
						for(PortError preventedError : scs.getValue()) {
							if(preventedError.equals(internalFailureError)) {
								mitigatingConstraint = scs.getKey();
							}
						}
					}
					if(mitigatingConstraint == null) {
						sensorGeneratedErrors.add(report);
					} else {
						report.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
						report.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
						report.setMitigatingComponentName(sensorImpl.getFullName());
						report.setMitigatingComponentType("S");
						//link btween asam and xagree
						VerifyAllHandler handler = new VerifyAllHandler();
						try {
							handler.verifyComponent(sensorImpl, monitor, true, false);
							List<JKindResult> verificationResults = handler.getResults();
							boolean allPass = true;
							for(String id : mitigatingConstraint.getVerifiedBy()) {
								allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(sensorImpl), verificationResults, id);
							}
							report.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
						} catch (Exception e) {
							// handle generated exception
							e.printStackTrace();
						}
						errors.add(report);
					}
				}
			}
		}
		return sensorGeneratedErrors;
	}
	
	private List<AsamReport> doControllerPartofSCAFlow(List<AsamReport> sensorGeneratedErrors, Map<String, String> portRenamings, ComponentImplementation controller, IProgressMonitor monitor) {
		Cloner cloner = new Cloner();
		List<AsamReport> controllerPropogatedErrors = new ArrayList<AsamReport>();
		AsamEvaluator controllerEvaluator = new AsamEvaluator();
		AsamError controllerAsam = controllerEvaluator.evaluateAsamSubclause(getAsamAnnex(controller));
		for(AsamReport report : sensorGeneratedErrors) {
			PortError lastError = (PortError)cloner.deepClone(report.getErrorTypePropogation().get(report.getErrorTypePropogation().size() - 1));
			String newPort = portRenamings.get(report.getSourceComponentName() + "->" + lastError.getPort());
			if(newPort != null && newPort.contains("->")) {
				lastError.setPort(newPort.split("->")[1]);
			}
			report.setPropogationPath(report.getPropogationPath() + "->" + controller.getFullName());
			//inbound check for the controller
			SafetyConstraint mitigatingConstraint = null;
			for(Map.Entry<SafetyConstraint, Set<PortError>> scs : controllerAsam.getIncomingErrorsHandled().entrySet()) {
				for(PortError preventedError : scs.getValue()) {
					if(preventedError.equals(lastError)) {
						mitigatingConstraint = scs.getKey();
					}
				}
			}
			if(mitigatingConstraint != null) {
				report.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
				report.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
				report.setMitigatingComponentName(controller.getFullName());
				report.setMitigatingComponentType("C");
				VerifyAllHandler handler = new VerifyAllHandler();
				try {
					handler.verifyComponent(controller, monitor, true, false);
					List<JKindResult> verificationResults = handler.getResults();
					boolean allPass = true;
					for(String id : mitigatingConstraint.getVerifiedBy()) {
						allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(controller), verificationResults, id);
					}
					report.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
				} catch (Exception e) {
					// handle generated exception
					e.printStackTrace();
				}
				errors.add(report);
			} else {
				if(controllerAsam.getErrorPropagationMap().containsKey(lastError)) {
					//translate error type for the controller
					List<PortError> newPortErrorTypes = new ArrayList<>(controllerAsam.getErrorPropagationMap().get(lastError));
					for(PortError newPortErrorType : newPortErrorTypes) {
						AsamReport newReport = (AsamReport)cloner.deepClone(report);
						newReport.getErrorTypePropogation().add(newPortErrorType);
						//check outbound for the controller
						SafetyConstraint newMitigatingConstraint = null;
						for(Map.Entry<SafetyConstraint, Set<PortError>> scs : controllerAsam.getOutgoingErrorsPrevented().entrySet()) {
							for(PortError preventedError : scs.getValue()) {
								if(preventedError.equals(newPortErrorType)) {
									newMitigatingConstraint = scs.getKey();
								}
							}
						}
						if(newMitigatingConstraint != null) {
							newReport.setMitigatingSafetyConstraintId(newMitigatingConstraint.getId());
							newReport.setMitigatingSafetyConstraintDescription(newMitigatingConstraint.getDescription());
							newReport.setMitigatingComponentName(controller.getFullName());
							newReport.setMitigatingComponentType("C");
							VerifyAllHandler handler = new VerifyAllHandler();
							try {
								handler.verifyComponent(controller, monitor, true, false);
								List<JKindResult> verificationResults = handler.getResults();
								boolean allPass = true;
								for(String id : mitigatingConstraint.getVerifiedBy()) {
									allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(controller), verificationResults, id);
								}
								newReport.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
							} catch (Exception e) {
								// handle generated exception
								e.printStackTrace();
							}
							errors.add(newReport);
						} else {
							controllerPropogatedErrors.add(newReport);
						}
					}
				} else {
					errors.add(report);
				}
			}
		}
		return controllerPropogatedErrors;
	}
	
	private void doActuatorPartOfSCAFlow(List<AsamReport> controllerPropogatedErrors, Map<String, String> portRenamings, ComponentImplementation controller, Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> tailFlows, IProgressMonitor monitor) { 
		Cloner cloner = new Cloner();
		for(Map.Entry<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> actuator : tailFlows.entrySet()) {
			for(AsamReport report : controllerPropogatedErrors) {
				AsamReport actuatorReport = (AsamReport)cloner.deepClone(report);
				PortError lastError = (PortError)cloner.deepClone(actuatorReport.getErrorTypePropogation().get(actuatorReport.getErrorTypePropogation().size() - 1));
				String newPort = portRenamings.get(controller.getFullName() + "->" + lastError.getPort());
				if(newPort != null && newPort.contains("->")) {
					lastError.setPort(newPort.split("->")[1]);
				}
				actuatorReport.setPropogationPath(actuatorReport.getPropogationPath() + "->" + actuator.getKey().getFullName());
				//checking incoming error
				AsamEvaluator actuatorEvaluator = new AsamEvaluator();
				AsamError actuatorAsam = actuatorEvaluator.evaluateAsamSubclause(getAsamAnnex(actuator.getKey()));
				SafetyConstraint mitigatingConstraint = null;
				for(Map.Entry<SafetyConstraint, Set<PortError>> scs : actuatorAsam.getIncomingErrorsHandled().entrySet()) {
					for(PortError preventedError : scs.getValue()) {
						if(preventedError.equals(lastError)) {
							mitigatingConstraint = scs.getKey();
						}
					}
				}
				
				if(mitigatingConstraint != null) {
					actuatorReport.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
					actuatorReport.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
					actuatorReport.setMitigatingComponentName(actuator.getKey().getFullName());
					actuatorReport.setMitigatingComponentType("A");
					VerifyAllHandler handler = new VerifyAllHandler();
					try {
						handler.verifyComponent(actuator.getKey(), monitor, true, false);
						List<JKindResult> verificationResults = handler.getResults();
						boolean allPass = true;
						for(String id : mitigatingConstraint.getVerifiedBy()) {
							allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(actuator.getKey()), verificationResults, id);
						}
						actuatorReport.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
					} catch (Exception e) {
						// handle generated exception
						e.printStackTrace();
					}
					errors.add(actuatorReport);
				} else {
					errors.add(actuatorReport);
				}
			}
		}
	}
	private void generateReportItemsForSensorControllerActuatorFlow(Map<String, String> portRenamings, Set<ComponentImplementation> sensors, ComponentImplementation controller, Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> tailFlows, IProgressMonitor monitor) {
		//sensors -> controllers -> actuators
		List<AsamReport> sensorErrors = doSensorPartOfSCAFlow(sensors, monitor);
		List<AsamReport> controllerPropogatedErrors = doControllerPartofSCAFlow(sensorErrors, portRenamings, controller, monitor);
		doActuatorPartOfSCAFlow(controllerPropogatedErrors, portRenamings, controller, tailFlows, monitor);
	}
	//controller->actutor->sensor
	private List<AsamReport> doControllerPartOfCASFlow(ComponentImplementation controller, IProgressMonitor monitor) {
		List<AsamReport> controllerGeneratedReports = new ArrayList<AsamReport>();
		
		AsamEvaluator controllerEvaluator = new AsamEvaluator();
		AsamError controllerAsam = controllerEvaluator.evaluateAsamSubclause(getAsamAnnex(controller));
		for(Map.Entry<InternalFailure, Set<PortError>> internalFailures : controllerAsam.getInternalFailuresToErrorsCaused().entrySet()) {
			for(PortError internalFailureError : internalFailures.getValue()) {
				AsamReport report = new AsamReport();
				report.setInternalFailureId(internalFailures.getKey().getId());
				report.setInternalFailureDescription(internalFailures.getKey().getDescription());
				report.setErrorCaused(internalFailureError.getError() + "{" + internalFailureError.getPort() + "}");
				report.setProbabilityOfOccurrence(internalFailureError.getProbabilityOfOccurrence());
				report.setHazardLevel(internalFailureError.getHazardLevel());
				report.setSourceComponentName(controller.getFullName());
				report.setSourceComponentType("C");
				report.setPropogationPath(controller.getFullName());
				report.getErrorTypePropogation().add(internalFailureError);
				//outbound checking error for controller
				SafetyConstraint mitigatingConstraint = null;
				for(Map.Entry<SafetyConstraint, Set<PortError>> scs : controllerAsam.getOutgoingErrorsPrevented().entrySet()) {
					for(PortError preventedError : scs.getValue()) {
						if(preventedError.equals(internalFailureError)) {
							mitigatingConstraint = scs.getKey();
						}
					}
				}
				if(mitigatingConstraint == null) {
					controllerGeneratedReports.add(report);
				} else {
					report.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
					report.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
					report.setMitigatingComponentName(controller.getFullName());
					report.setMitigatingComponentType("C");
					VerifyAllHandler handler = new VerifyAllHandler();
					try {
						handler.verifyComponent(controller, monitor, true, false);
						List<JKindResult> verificationResults = handler.getResults();
						boolean allPass = true;
						for(String id : mitigatingConstraint.getVerifiedBy()) {
							allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(controller), verificationResults, id);
						}
						report.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
					} catch (Exception e) {
						// handle generated exception
						e.printStackTrace();
					}
					errors.add(report);
				}
			}
		}
		return controllerGeneratedReports;
	}
	
	private void doActuatorPartOfCASFlow(List<AsamReport> controllerGeneratedReports, Map<String, String> portRenamings, ComponentImplementation controller, Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> tailFlows, IProgressMonitor monitor) {
		Cloner cloner = new Cloner();
		
		for(Map.Entry<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> actuator : tailFlows.entrySet()) {
			for(AsamReport report : controllerGeneratedReports) {
				AsamReport actuatorReport = (AsamReport)cloner.deepClone(report);
				PortError lastError = (PortError)cloner.deepClone(actuatorReport.getErrorTypePropogation().get(actuatorReport.getErrorTypePropogation().size() - 1));
				String newPort = portRenamings.get(controller.getFullName() + "->" + lastError.getPort());
				if(newPort != null && newPort.contains("->")) {
					lastError.setPort(newPort.split("->")[1]);
				}
				actuatorReport.setPropogationPath(actuatorReport.getPropogationPath() + "->" + actuator.getKey().getFullName());
				
				AsamEvaluator actuatorEvaluator = new AsamEvaluator();
				AsamError actuatorAsam = actuatorEvaluator.evaluateAsamSubclause(getAsamAnnex(actuator.getKey()));
				//checking inbound constraint
				SafetyConstraint mitigatingConstraint = null;
				for(Map.Entry<SafetyConstraint, Set<PortError>> scs : actuatorAsam.getIncomingErrorsHandled().entrySet()) {
					for(PortError preventedError : scs.getValue()) {
						if(preventedError.equals(lastError)) {
							mitigatingConstraint = scs.getKey();
						}
					}
				}
				
				if(mitigatingConstraint != null) {
					actuatorReport.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
					actuatorReport.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
					actuatorReport.setMitigatingComponentName(actuator.getKey().getFullName());
					actuatorReport.setMitigatingComponentType("A");
					VerifyAllHandler handler = new VerifyAllHandler();
					try {
						handler.verifyComponent(actuator.getKey(), monitor, true, false);
						List<JKindResult> verificationResults = handler.getResults();
						boolean allPass = true;
						for(String id : mitigatingConstraint.getVerifiedBy()) {
							allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(actuator.getKey()), verificationResults, id);
						}
						actuatorReport.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
					} catch (Exception e) {
						// handle generated exception
						e.printStackTrace();
					}
					errors.add(actuatorReport);
				} else {
					if(actuatorAsam.getErrorPropagationMap().containsKey(lastError)) {
						//translate inbound error to outbound error
						List<PortError> newPortErrorTypes = new ArrayList<>(actuatorAsam.getErrorPropagationMap().get(lastError));
						for(PortError newPortErrorType : newPortErrorTypes) {
							AsamReport newActuatorReport = (AsamReport)cloner.deepClone(actuatorReport);
							newActuatorReport.getErrorTypePropogation().add(newPortErrorType);
							//checking outbound constraint
							SafetyConstraint newMitigatingConstraint = null;
							for(Map.Entry<SafetyConstraint, Set<PortError>> scs : actuatorAsam.getOutgoingErrorsPrevented().entrySet()) {
								for(PortError preventedError : scs.getValue()) {
									if(preventedError.equals(newPortErrorType)) {
										newMitigatingConstraint = scs.getKey();
									}
								}
							}
							if(newMitigatingConstraint != null) {
								newActuatorReport.setMitigatingSafetyConstraintId(newMitigatingConstraint.getId());
								newActuatorReport.setMitigatingSafetyConstraintDescription(newMitigatingConstraint.getDescription());
								newActuatorReport.setMitigatingComponentName(controller.getFullName());
								newActuatorReport.setMitigatingComponentType("A");
								VerifyAllHandler handler = new VerifyAllHandler();
								try {
									handler.verifyComponent(actuator.getKey(), monitor, true, false);
									List<JKindResult> verificationResults = handler.getResults();
									boolean allPass = true;
									for(String id : mitigatingConstraint.getVerifiedBy()) {
										allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(actuator.getKey()), verificationResults, id);
									}
									newActuatorReport.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
								} catch (Exception e) {
									// handle generated exception
									e.printStackTrace();
								}
								errors.add(newActuatorReport);
							} else {
								for(Map.Entry<ComponentImplementation, Set<ComponentImplementation>> controlledProcess : actuator.getValue().entrySet()) {
									doControlledProcessPartOfCASFlow(controlledProcess.getKey(), portRenamings, actuator.getKey(), controlledProcess.getValue(), newActuatorReport, monitor);
								}
							}
						}
					} else {
						errors.add(actuatorReport);
					}
				}
			}
		}
	}
	
	private void doControlledProcessPartOfCASFlow(ComponentImplementation controlledProcess, Map<String, String> portRenamings, ComponentImplementation actuator, Set<ComponentImplementation> controlledProcessSensors, AsamReport actuatorReport, IProgressMonitor monitor) {
		Cloner cloner = new Cloner();
		AsamReport controlledProcessReport = (AsamReport)cloner.deepClone(actuatorReport);
		//translate actutor port to controlled process port
		PortError lastError = (PortError)cloner.deepClone(controlledProcessReport.getErrorTypePropogation().get(controlledProcessReport.getErrorTypePropogation().size() - 1));
		String newPort = portRenamings.get(actuator.getFullName() + "->" + lastError.getPort());
		if(newPort != null && newPort.contains("->")) {
			lastError.setPort(newPort.split("->")[1]);
		}
		controlledProcessReport.setPropogationPath(controlledProcessReport.getPropogationPath() + "->" + controlledProcess.getFullName());
		
		AsamEvaluator controlledProcessEvaluator = new AsamEvaluator();
		AsamError controlledProcessAsam = controlledProcessEvaluator.evaluateAsamSubclause(getAsamAnnex(controlledProcess));
		if(controlledProcessAsam.getErrorPropagationMap().containsKey(lastError)) {
			//translate inbound error type to outbound error type no checking in the controlled process
			List<PortError> newNewPortErrorTypes = new ArrayList<>(controlledProcessAsam.getErrorPropagationMap().get(lastError));
			for(PortError newNewPortErrorType : newNewPortErrorTypes) {
				AsamReport newControlledProcessReport = (AsamReport)cloner.deepClone(controlledProcessReport);
				newControlledProcessReport.getErrorTypePropogation().add(newNewPortErrorType);
				
				for(ComponentImplementation sensor : controlledProcessSensors) {
					doSensorPartOfCASFlow(sensor, portRenamings, controlledProcess, newControlledProcessReport, monitor);
				}
			}
		} else {
			errors.add(controlledProcessReport);
		}
	}
	
	private void doSensorPartOfCASFlow(ComponentImplementation sensor, Map<String, String> portRenamings, ComponentImplementation controlledProcess, AsamReport controlledProcessReport, IProgressMonitor monitor) {
		Cloner cloner = new Cloner();
		//translate from CP port to sensor port
		AsamReport sensorReport = (AsamReport)cloner.deepClone(controlledProcessReport);
		PortError lastError = (PortError)cloner.deepClone(sensorReport.getErrorTypePropogation().get(sensorReport.getErrorTypePropogation().size() - 1));
		String newPort = portRenamings.get(controlledProcess.getFullName() + "->" + lastError.getPort());
		if(newPort != null && newPort.contains("->")) {
			lastError.setPort(newPort.split("->")[1]);
		}
		sensorReport.setPropogationPath(sensorReport.getPropogationPath() + "->" + sensor.getFullName());
		//check inbound safety constraint in the sensor
		AsamEvaluator sensorEvaluator = new AsamEvaluator();
		AsamError sensorAsam = sensorEvaluator.evaluateAsamSubclause(getAsamAnnex(sensor));
		SafetyConstraint mitigatingConstraint = null;
		for(Map.Entry<SafetyConstraint, Set<PortError>> scs : sensorAsam.getIncomingErrorsHandled().entrySet()) {
			for(PortError preventedError : scs.getValue()) {
				if(preventedError.equals(lastError)) {
					mitigatingConstraint = scs.getKey();
				}
			}
		}
		
		if(mitigatingConstraint != null) {
			sensorReport.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
			sensorReport.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
			sensorReport.setMitigatingComponentName(sensor.getFullName());
			sensorReport.setMitigatingComponentType("S");
			VerifyAllHandler handler = new VerifyAllHandler();
			try {
				handler.verifyComponent(sensor, monitor, true, false);
				List<JKindResult> verificationResults = handler.getResults();
				boolean allPass = true;
				for(String id : mitigatingConstraint.getVerifiedBy()) {
					allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(sensor), verificationResults, id);
				}
				sensorReport.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
			} catch (Exception e) {
				// handle generated exception
				e.printStackTrace();
			}
			errors.add(sensorReport);
		} else {
			errors.add(sensorReport);
		}
	}
	//building internal failures and sc table
	private void generateReportItemsForControllerActuatorSensorFlow(Map<String, String> portRenamings, ComponentImplementation controller, Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> tailFlows, IProgressMonitor monitor) {
		List<AsamReport> controllerGeneratedErrors = doControllerPartOfCASFlow(controller, monitor);
		doActuatorPartOfCASFlow(controllerGeneratedErrors, portRenamings, controller, tailFlows, monitor);
	}
	
	private void doActuatorPartOfASFlow(ComponentImplementation actuator, Map<String, String> portRenamings, Map<ComponentImplementation, Set<ComponentImplementation>> controlledProcessesAndSensors, IProgressMonitor monitor) {
		AsamEvaluator actuatorEvaluator = new AsamEvaluator();
		AsamError actuatorAsam = actuatorEvaluator.evaluateAsamSubclause(getAsamAnnex(actuator));
		for(Map.Entry<InternalFailure, Set<PortError>> internalFailures : actuatorAsam.getInternalFailuresToErrorsCaused().entrySet()) {
			for(PortError internalFailureError : internalFailures.getValue()) {
				AsamReport report = new AsamReport();
				report.setInternalFailureId(internalFailures.getKey().getId());
				report.setInternalFailureDescription(internalFailures.getKey().getDescription());
				report.setErrorCaused(internalFailureError.getError() + "{" + internalFailureError.getPort() + "}");
				report.setProbabilityOfOccurrence(internalFailureError.getProbabilityOfOccurrence());
				report.setHazardLevel(internalFailureError.getHazardLevel());
				report.setSourceComponentName(actuator.getFullName());
				report.setSourceComponentType("A");
				report.setPropogationPath(actuator.getFullName());
				report.getErrorTypePropogation().add(internalFailureError);
				//cheking outbound sc for actutor
				SafetyConstraint mitigatingConstraint = null;
				for(Map.Entry<SafetyConstraint, Set<PortError>> scs : actuatorAsam.getOutgoingErrorsPrevented().entrySet()) {
					for(PortError preventedError : scs.getValue()) {
						if(preventedError.equals(internalFailureError)) {
							mitigatingConstraint = scs.getKey();
						}
					}
				}
				if(mitigatingConstraint == null) {
					for(Map.Entry<ComponentImplementation, Set<ComponentImplementation>> controlledProcess : controlledProcessesAndSensors.entrySet()) {
						doControlledProcessPartOfASFlow(controlledProcess.getKey(), portRenamings, actuator, controlledProcess.getValue(), report, monitor);
					}
				} else {
					report.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
					report.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
					report.setMitigatingComponentName(actuator.getFullName());
					report.setMitigatingComponentType("A");
					VerifyAllHandler handler = new VerifyAllHandler();
					try {
						handler.verifyComponent(actuator, monitor, true, false);
						List<JKindResult> verificationResults = handler.getResults();
						boolean allPass = true;
						for(String id : mitigatingConstraint.getVerifiedBy()) {
							allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(actuator), verificationResults, id);
						}
						report.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
					} catch (Exception e) {
						// handle generated exception
						e.printStackTrace();
					}
					errors.add(report);
				}
			}
		}
	}
	
	private void doControlledProcessPartOfASFlow(ComponentImplementation controlledProcess, Map<String, String> portRenamings, ComponentImplementation actuator, Set<ComponentImplementation> sensors, AsamReport actuatorReport, IProgressMonitor monitor) {
		Cloner cloner = new Cloner();
		AsamReport controlledProcessReport = (AsamReport)cloner.deepClone(actuatorReport);
		PortError lastError = (PortError)cloner.deepClone(controlledProcessReport.getErrorTypePropogation().get(controlledProcessReport.getErrorTypePropogation().size() - 1));
		String newPort = portRenamings.get(actuator.getFullName() + "->" + lastError.getPort());
		if(newPort != null && newPort.contains("->")) {
			lastError.setPort(newPort.split("->")[1]);
		}
		controlledProcessReport.setPropogationPath(controlledProcessReport.getPropogationPath() + "->" + controlledProcess.getFullName());
		
		AsamEvaluator controlledProcessEvaluator = new AsamEvaluator();
		AsamError controlledProcessAsam = controlledProcessEvaluator.evaluateAsamSubclause(getAsamAnnex(controlledProcess));
		if(controlledProcessAsam.getErrorPropagationMap().containsKey(lastError)) {
			List<PortError> newNewPortErrorTypes = new ArrayList<>(controlledProcessAsam.getErrorPropagationMap().get(lastError));
			for(PortError newNewPortErrorType : newNewPortErrorTypes) {
				AsamReport newControlledProcessReport = (AsamReport)cloner.deepClone(controlledProcessReport);
				newControlledProcessReport.getErrorTypePropogation().add(newNewPortErrorType);
				
				for(ComponentImplementation sensor : sensors) {
					doSensorPartOfASFlow(sensor, portRenamings, controlledProcess, newControlledProcessReport, monitor);
				}
			}
		} else {
			errors.add(controlledProcessReport);
		}
	}
	
	private void doSensorPartOfASFlow(ComponentImplementation sensor, Map<String, String> portRenamings, ComponentImplementation controlledProcess, AsamReport controlledProcessReport, IProgressMonitor monitor) {
		Cloner cloner = new Cloner();
		AsamReport sensorReport = (AsamReport)cloner.deepClone(controlledProcessReport);
		PortError lastError = (PortError)cloner.deepClone(sensorReport.getErrorTypePropogation().get(sensorReport.getErrorTypePropogation().size() - 1));
		String newPort = portRenamings.get(controlledProcess.getFullName() + "->" + lastError.getPort());
		if(newPort != null && newPort.contains("->")) {
			lastError.setPort(newPort.split("->")[1]);
		}
		sensorReport.setPropogationPath(sensorReport.getPropogationPath() + "->" + sensor.getFullName());
		
		AsamEvaluator sensorEvaluator = new AsamEvaluator();
		AsamError sensorAsam = sensorEvaluator.evaluateAsamSubclause(getAsamAnnex(sensor));
		SafetyConstraint mitigatingConstraint = null;
		for(Map.Entry<SafetyConstraint, Set<PortError>> scs : sensorAsam.getIncomingErrorsHandled().entrySet()) {
			for(PortError preventedError : scs.getValue()) {
				if(preventedError.equals(lastError)) {
					mitigatingConstraint = scs.getKey();
				}
			}
		}
		
		if(mitigatingConstraint != null) {
			sensorReport.setMitigatingSafetyConstraintId(mitigatingConstraint.getId());
			sensorReport.setMitigatingSafetyConstraintDescription(mitigatingConstraint.getDescription());
			sensorReport.setMitigatingComponentName(sensor.getFullName());
			sensorReport.setMitigatingComponentType("S");
			VerifyAllHandler handler = new VerifyAllHandler();
			try {
				handler.verifyComponent(sensor, monitor, true, false);
				List<JKindResult> verificationResults = handler.getResults();
				boolean allPass = true;
				for(String id : mitigatingConstraint.getVerifiedBy()) {
					allPass = allPass & JKindResultProcessingUtil.didGuaranteePass(handler.getContractForImpl(sensor), verificationResults, id);
				}
				sensorReport.setVerified(verificationResults.size() > 0 && allPass ? "Y" : "N");
			} catch (Exception e) {
				// handle generated exception
				e.printStackTrace();
			}
			errors.add(sensorReport);
		} else {
			errors.add(sensorReport);
		}
	}
	
	private void generateReportItemsForActuatorSensorFlow(Map<String, String> portRenamings, Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> tailFlows, IProgressMonitor monitor) {
		for(Map.Entry<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> actuator : tailFlows.entrySet()) {
			doActuatorPartOfASFlow(actuator.getKey(), portRenamings, actuator.getValue(), monitor);
		}
	}
	
	private Map<String, String> getPortRenamings(ComponentImplementation parent) {
		Map<String, String> portRenamings = new HashMap<String, String>();
		for(Connection conn : parent.getAllConnections()) {
			ConnectionEnd destPort = conn.getDestination().getConnectionEnd();
			
			ConnectionEnd srcPort = conn.getSource().getConnectionEnd();
			
			if(conn.getDestination().getContext() instanceof Subcomponent && conn.getSource().getContext() instanceof Subcomponent) {
				Subcomponent dest = (Subcomponent)conn.getDestination().getContext();
				ComponentImplementation destImpl = dest.getComponentImplementation();
				
				Subcomponent src = (Subcomponent)conn.getSource().getContext();
				ComponentImplementation srcImpl = src.getComponentImplementation();
				
				if(destImpl != null && srcImpl != null && containsAsamAnnex(destImpl) && containsAsamAnnex(srcImpl)) {
					AsamEvaluator evaluator1 = new AsamEvaluator();
					AsamError asamError1 = evaluator1.evaluateAsamSubclause(getAsamAnnex(destImpl));
					
					AsamEvaluator evaluator2 = new AsamEvaluator();
					AsamError asamError2 = evaluator2.evaluateAsamSubclause(getAsamAnnex(srcImpl));
					
					if((asamError2.getType().equals("sensor") && asamError1.getType().equals("controller")) ||
							(asamError2.getType().equals("controller") && asamError1.getType().equals("actuator")) ||
							(asamError2.getType().equals("actuator") && asamError1.getType().equals("controlled_process")) ||
							(asamError2.getType().equals("controlled_process") && asamError1.getType().equals("sensor"))) {
						portRenamings.put(srcImpl.getFullName() + "->" + srcPort.getFullName(), destImpl.getFullName() + "->" + destPort.getFullName());
					}
				}
			}
		}
		return portRenamings;
	}
	
	private Set<ComponentImplementation> getSensorsToControllerList(ComponentImplementation parent, ComponentImplementation controller) {
		Set<ComponentImplementation> sensors = new HashSet<ComponentImplementation>();
		
		for(Connection conn : parent.getAllConnections()) {
			if(conn.getDestination().getContext() instanceof Subcomponent) {
				Subcomponent dest = (Subcomponent)conn.getDestination().getContext();
				boolean isController = dest.getComponentImplementation() != null && dest.getComponentImplementation().equals(controller);
				
				if(isController && conn.getSource().getContext() instanceof Subcomponent) {
					Subcomponent src = (Subcomponent)conn.getSource().getContext();
					ComponentImplementation srcComp = src.getComponentImplementation();
					
					if(srcComp != null && containsAsamAnnex(srcComp)) {
						AsamEvaluator evaluator = new AsamEvaluator();
						AsamError asamError = evaluator.evaluateAsamSubclause(getAsamAnnex(srcComp));
						if(asamError.getType().equals("sensor")) {
							sensors.add(srcComp);
						}
					}
				}
			}
		}
		
		return sensors;
	}
	
	private Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> getActuatorToControlledProcessToSensorMap(ComponentImplementation parent, ComponentImplementation controller) {
		Map<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>> tailFlows = new HashMap<ComponentImplementation, Map<ComponentImplementation, Set<ComponentImplementation>>>();
		
		for(Connection conn : parent.getAllConnections()) {
			if(conn.getSource().getContext() instanceof Subcomponent) {
				Subcomponent src = (Subcomponent)conn.getSource().getContext();
				boolean isController = src.getComponentImplementation() != null && src.getComponentImplementation().equals(controller);
				
				if(isController && conn.getDestination().getContext() instanceof Subcomponent) {
					Subcomponent controllerDestination = (Subcomponent)conn.getDestination().getContext();
					ComponentImplementation controllerDestinationComp = controllerDestination.getComponentImplementation();
					
					if(controllerDestinationComp != null && containsAsamAnnex(controllerDestinationComp)) {
						AsamEvaluator evaluator = new AsamEvaluator();
						AsamError asamError = evaluator.evaluateAsamSubclause(getAsamAnnex(controllerDestinationComp));
						if(asamError.getType().equals("actuator")) {
							ComponentImplementation actuator = controllerDestinationComp;
							Map<ComponentImplementation, Set<ComponentImplementation>> actuatorConnections = getControlledProcessesForActuator(parent, actuator);
							if(actuatorConnections.size() > 0) {
								tailFlows.put(actuator, actuatorConnections);
							}
						}
					}
				}
			}
		}
		
		return tailFlows;
	}
	
	private Map<ComponentImplementation, Set<ComponentImplementation>> getControlledProcessesForActuator(ComponentImplementation parent, ComponentImplementation actuator) {
		Map<ComponentImplementation, Set<ComponentImplementation>> tailFlows = new HashMap<ComponentImplementation, Set<ComponentImplementation>>();
		
		for(Connection conn : parent.getAllConnections()) {
			if(conn.getSource().getContext() instanceof Subcomponent) {
				Subcomponent src = (Subcomponent)conn.getSource().getContext();
				boolean isActuator = src.getComponentImplementation() != null && src.getComponentImplementation().equals(actuator);
				
				if(isActuator && conn.getDestination().getContext() instanceof Subcomponent) {
					Subcomponent actuatorDestination = (Subcomponent)conn.getDestination().getContext();
					ComponentImplementation actuatorDestinationComp = actuatorDestination.getComponentImplementation();
					
					if(actuatorDestinationComp != null && containsAsamAnnex(actuatorDestinationComp)) {
						AsamEvaluator evaluator = new AsamEvaluator();
						AsamError asamError = evaluator.evaluateAsamSubclause(getAsamAnnex(actuatorDestinationComp));
						if(asamError.getType().equals("controlled_process")) {
							ComponentImplementation controlledProcess = actuatorDestinationComp;
							Set<ComponentImplementation> controlledProcessConnections = getSensorsForControlledProcess(parent, controlledProcess);
							if(controlledProcessConnections.size() > 0) {
								tailFlows.put(controlledProcess, controlledProcessConnections);
							}
						}
					}
				}
			}
		}
		
		return tailFlows;
	}
	
	private Set<ComponentImplementation> getSensorsForControlledProcess(ComponentImplementation parent, ComponentImplementation controlledProcess) {
		Set<ComponentImplementation> tailFlows = new HashSet<ComponentImplementation>();
		
		for(Connection conn : parent.getAllConnections()) {
			if(conn.getSource().getContext() instanceof Subcomponent) {
				Subcomponent src = (Subcomponent)conn.getSource().getContext();
				boolean isControlledProcess = src.getComponentImplementation() != null && src.getComponentImplementation().equals(controlledProcess);
				
				if(isControlledProcess && conn.getDestination().getContext() instanceof Subcomponent) {
					Subcomponent controlledProcessDestination = (Subcomponent)conn.getDestination().getContext();
					ComponentImplementation controlledProcessDestinationComp = controlledProcessDestination.getComponentImplementation();
					
					if(controlledProcessDestinationComp != null && containsAsamAnnex(controlledProcessDestinationComp)) {
						AsamEvaluator evaluator = new AsamEvaluator();
						AsamError asamError = evaluator.evaluateAsamSubclause(getAsamAnnex(controlledProcessDestinationComp));
						if(asamError.getType().equals("sensor")) {
							ComponentImplementation sensor = controlledProcessDestinationComp;
							tailFlows.add(sensor);
						}
					}
				}
			}
		}
		
		return tailFlows;
	}
	
	private boolean containsAsamAnnex(ComponentImplementation componentImpl) {
		if(componentImpl != null && componentImpl.getOwnedAnnexSubclauses() != null) {
			for(AnnexSubclause subclause : componentImpl.getOwnedAnnexSubclauses()) {
				if(subclause.getName().equals("asam")) {
					instantiateComponent(componentImpl);
					return true;
				}
			}
		}
		return false;
	}
	
	private AnnexSubclause getAsamAnnex(ComponentImplementation componentImpl) {
		for(AnnexSubclause subclause : componentImpl.getOwnedAnnexSubclauses()) {
			if(subclause.getName().equals("asam")) {
				instantiateComponent(componentImpl);
				return subclause;
			}
		}
		return null;
	}
	
	private URI getSelectionURI(ISelection currentSelection) {
		if (currentSelection instanceof IStructuredSelection) {
			IStructuredSelection iss = (IStructuredSelection) currentSelection;
			if (iss.size() == 1) {
				EObjectNode node = (EObjectNode) iss.getFirstElement();
				return node.getEObjectURI();
			}
		} else if (currentSelection instanceof TextSelection) {
			XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
			TextSelection ts = (TextSelection) xtextEditor.getSelectionProvider().getSelection();
			return xtextEditor.getDocument().readOnly(resource -> {
				EObject e = new EObjectAtOffsetHelper().resolveContainedElementAt(resource, ts.getOffset());
				return EcoreUtil2.getURI(e);
			});
		}
		return null;
	}
	
	protected static SystemInstance getSysInstance(Element root) {
		ComponentImplementation ci = getComponentImplementation(root);
		try {
			return InstantiateModel.buildInstanceModelFile(ci);
		} catch (Exception e) {
			return null;
		}
	}
	
	private static ComponentImplementation getComponentImplementation(Element root) {
		Classifier classifier = getOutermostClassifier(root);
		if (classifier instanceof ComponentImplementation) {
			return (ComponentImplementation) classifier;
		}
		if (!(classifier instanceof ComponentType)) {
			throw new RuntimeException("Must select an AADL Component Type or Implementation");
		}
		ComponentType ct = (ComponentType) classifier;
		List<ComponentImplementation> cis = getComponentImplementations(ct);
		if (cis.size() == 0) {
			throw new RuntimeException("AADL Component Type has no implementation to calculate");
		} else if (cis.size() == 1) {
			ComponentImplementation ci = cis.get(0);
			return ci;
		} else {
			throw new RuntimeException("AADL Component Type has multiple implementations to calculate: please select just one");
		}
	}
	
	private static List<ComponentImplementation> getComponentImplementations(ComponentType ct) {
		List<ComponentImplementation> result = new ArrayList<>();
		AadlPackage pkg = AadlUtil.getContainingPackage(ct);
		for (ComponentImplementation ci : EcoreUtil2.getAllContentsOfType(pkg, ComponentImplementation.class)) {
			if (ci.getType().equals(ct)) {
				result.add(ci);
			}
		}
		return result;
	}
	
	protected static Classifier getOutermostClassifier(Element element) {
		List<EObject> containers = new ArrayList<>();
		EObject curr = element;
		while (curr != null) {
			containers.add(curr);
			curr = curr.eContainer();
		}
		Collections.reverse(containers);
		for (EObject container : containers) {
			if (container instanceof Classifier) {
				System.out.println(container);
				return (Classifier) container;
			}
		}
		return null;
	}
}
