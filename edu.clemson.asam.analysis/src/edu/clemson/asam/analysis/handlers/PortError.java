package edu.clemson.asam.analysis.handlers;

import java.io.Serializable;
import java.math.BigDecimal;

public class PortError implements Serializable {
	
	private static final long serialVersionUID = 422593733882071284L;
	
	private String port;
	private String error;
	private BigDecimal probabilityOfOccurrence;
	private String hazardLevel;
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public BigDecimal getProbabilityOfOccurrence() {
		return probabilityOfOccurrence;
	}
	public void setProbabilityOfOccurrence(BigDecimal probabilityOfOccurrence) {
		this.probabilityOfOccurrence = probabilityOfOccurrence;
	}
	public String getHazardLevel() {
		return hazardLevel;
	}
	public void setHazardLevel(String hazardLevel) {
		this.hazardLevel = hazardLevel;
	}
	public String getProbabilityString() {
		if(this.probabilityOfOccurrence != null) {
			return ProbabilityLevelTranslator.getProbabilityLevelString(probabilityOfOccurrence);
		}
		return "";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((error == null) ? 0 : error.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PortError other = (PortError) obj;
		if (error == null) {
			if (other.error != null)
				return false;
		} else if (!error.equals(other.error))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		return true;
	}
}
