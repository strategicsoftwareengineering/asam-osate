package edu.clemson.asam.analysis.handlers;

import java.math.BigDecimal;

public class ProbabilityLevelTranslator {
	private static final BigDecimal ten_to_minus_6 = new BigDecimal(0.000001);
	private static final BigDecimal ten_to_minus_3 = new BigDecimal(0.001);
	private static final BigDecimal ten_to_minus_2 = new BigDecimal(0.01);
	private static final BigDecimal ten_to_minus_1 = new BigDecimal(0.1);
	
	private static final String level5 = "Improbable";
	private static final String level4 = "Remote";
	private static final String level3 = "Occasional";
	private static final String level2 = "Probable";
	private static final String level1 = "Frequent";
	
	public static String getProbabilityLevelString(BigDecimal value) {
		if(value.compareTo(ten_to_minus_6) <= 0) {
			return level5;
		} else if (value.compareTo(ten_to_minus_3) <= 0) {
			return level4;
		} else if (value.compareTo(ten_to_minus_2) <= 0) {
			return level3;
		} else if (value.compareTo(ten_to_minus_1) <= 0) {
			return level2;
		}
		return level1;
	}
}
