package edu.clemson.asam.analysis.handlers;

import java.util.List;

import edu.clemson.xagree.xagree.AgreeContract;
import edu.clemson.xagree.xagree.AgreeContractSubclause;
import edu.clemson.xagree.xagree.GuaranteeStatement;
import edu.clemson.xagree.xagree.SpecStatement;
import jkind.api.results.JKindResult;
import jkind.api.results.PropertyResult;
import jkind.api.results.Status;

public class JKindResultProcessingUtil {
	public static boolean didGuaranteePass(AgreeContractSubclause contract, List<JKindResult> results, String guaranteeId) {
		String descriptorToSearchFor = null;
		
		if(contract.getContract() instanceof AgreeContract) {
			AgreeContract assumeGuarantees = (AgreeContract) contract.getContract();
			for(SpecStatement statement : assumeGuarantees.getSpecs()) {
				if(statement instanceof GuaranteeStatement) {
					GuaranteeStatement guarantee = (GuaranteeStatement) statement;
					if(guarantee.getId().equals(guaranteeId)) {
						descriptorToSearchFor = guarantee.getStr();
					}
				}
			}
		}
		
		if(descriptorToSearchFor != null) {
			for(JKindResult result : results) {
				for(PropertyResult propertyResult : result.getPropertyResults()) {
					if(propertyResult.getName().equals(descriptorToSearchFor)) {
						if(propertyResult.getStatus().equals(Status.VALID)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
