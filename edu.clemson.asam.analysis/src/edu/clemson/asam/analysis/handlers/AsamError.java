package edu.clemson.asam.analysis.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//import java.util.Set;

public class AsamError implements Serializable {
	
	private static final long serialVersionUID = 3691142253282599828L;

	private String type;
	private Map<InternalFailure, Set<PortError>> internalFailuresToErrorsCaused = new HashMap<InternalFailure, Set<PortError>>();
	private Map<SafetyConstraint, Set<PortError>> incomingErrorsHandled = new HashMap<SafetyConstraint, Set<PortError>>();
	private Map<SafetyConstraint, Set<PortError>> outgoingErrorsPrevented = new HashMap<SafetyConstraint, Set<PortError>>();
	private Map<PortError, Set<PortError>> errorPropagationMap = new HashMap<PortError, Set<PortError>>();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Map<InternalFailure, Set<PortError>> getInternalFailuresToErrorsCaused() {
		return internalFailuresToErrorsCaused;
	}
	public void setInternalFailuresToErrorsCaused(Map<InternalFailure, Set<PortError>> internalFailuresToErrorsCaused) {
		this.internalFailuresToErrorsCaused = internalFailuresToErrorsCaused;
	}
	public Map<SafetyConstraint, Set<PortError>> getIncomingErrorsHandled() {
		return incomingErrorsHandled;
	}
	public void setIncomingErrorsHandled(Map<SafetyConstraint, Set<PortError>> incomingErrorsHandled) {
		this.incomingErrorsHandled = incomingErrorsHandled;
	}
	public Map<SafetyConstraint, Set<PortError>> getOutgoingErrorsPrevented() {
		return outgoingErrorsPrevented;
	}
	public void setOutgoingErrorsPrevented(Map<SafetyConstraint, Set<PortError>> outgoingErrorsPrevented) {
		this.outgoingErrorsPrevented = outgoingErrorsPrevented;
	}
	public Map<PortError, Set<PortError>> getErrorPropagationMap() {
		return errorPropagationMap;
	}
	public void setErrorPropagationMap(Map<PortError, Set<PortError>> errorPropagationMap) {
		this.errorPropagationMap = errorPropagationMap;
	}
}
