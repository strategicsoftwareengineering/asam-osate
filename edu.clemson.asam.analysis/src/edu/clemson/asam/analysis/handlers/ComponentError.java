package edu.clemson.asam.analysis.handlers;

//import java.util.Set;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ComponentError {
	//Define data members
	private String componentName;
	private String errorOntologyType;
	private BigDecimal probabilityOfOccurence;
	private String hazardLevel;
	private String unsafeControlActionId;
	private String unsafeControlActionDescription;
	private String generalCause;
	private String specificCause;
	private String safetyConstraintId;
	private String safetyConstraintDescription;
	private String verified;
	private List<String> verifiedBy;
	
	//Define getter and setter for each data member
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getErrorOntologyType() {
		return errorOntologyType;
	}
	public void setErrorOntologyType(String errorOntologyType) {
		this.errorOntologyType = errorOntologyType;
	}
	public String getUnsafeControlActionId() {
		return unsafeControlActionId;
	}
	public void setUnsafeControlActionId(String unsafeControlActionId) {
		this.unsafeControlActionId = unsafeControlActionId;
	}
	public String getUnsafeControlActionDescription() {
		return unsafeControlActionDescription;
	}
	public void setUnsafeControlActionDescription(String unsafeControlActionDescription) {
		this.unsafeControlActionDescription = unsafeControlActionDescription;
	}
	public String getGeneralCause() {
		return generalCause;
	}
	public void setGeneralCause(String generalCause) {
		this.generalCause = generalCause;
	}
	public String getSpecificCause() {
		return specificCause;
	}
	public void setSpecificCause(String specificCause) {
		this.specificCause = specificCause;
	}
	public String getSafetyConstraintId() {
		return safetyConstraintId;
	}
	public void setSafetyConstraintId(String safetyConstraintId) {
		this.safetyConstraintId = safetyConstraintId;
	}
	public String getSafetyConstraintDescription() {
		return safetyConstraintDescription;
	}
	public void setSafetyConstraintDescription(String safetyConstraintDescription) {
		this.safetyConstraintDescription = safetyConstraintDescription;
	}
	public BigDecimal getProbabilityOfOccurence() {
		return probabilityOfOccurence;
	}
	public void setProbabilityOfOccurence(BigDecimal probabilityOfOccurence) {
		this.probabilityOfOccurence = probabilityOfOccurence;
	}
	public String getHazardLevel() {
		return hazardLevel;
	}
	public void setHazardLevel(String hazardLevel) {
		this.hazardLevel = hazardLevel;
	}
	public String getVerified() {
		return verified;
	}
	public void setVerified(String verified) {
		this.verified = verified;
	}
	public List<String> getVerifiedBy() {
		if(verifiedBy == null) {
			verifiedBy = new ArrayList<String>();
		}
		return verifiedBy;
	}
	public void setVerifiedBy(List<String> verifiedBy) {
		this.verifiedBy = verifiedBy;
	}
	public String getProbabilityString() {
		if(this.probabilityOfOccurence != null) {
			return ProbabilityLevelTranslator.getProbabilityLevelString(probabilityOfOccurence);
		}
		return "";
	}
}
