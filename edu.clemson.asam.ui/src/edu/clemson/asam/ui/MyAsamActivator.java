package edu.clemson.asam.ui;

import org.apache.log4j.Logger;
import org.osate.core.OsateCorePlugin;
import org.osgi.framework.BundleContext;

import com.google.inject.Injector;

import edu.clemson.asam.ui.internal.AsamActivator;

public class MyAsamActivator extends AsamActivator {
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		try {
			registerInjectorFor(EDU_CLEMSON_ASAM_ASAM);

		} catch (Exception e) {
			Logger.getLogger(getClass()).error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public Injector getInjector(String languageName) {
		return OsateCorePlugin.getDefault().getInjector(languageName);
	}

	protected void registerInjectorFor(String language) throws Exception {
		OsateCorePlugin.getDefault().registerInjectorFor(language, createInjector(language));
	}
}
