package edu.clemson.asam.parsing;

import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.modelsupport.errorreporting.ParseErrorReporter;
import org.osate.annexsupport.AnnexParseUtil;
import org.osate.annexsupport.AnnexParser;

import com.google.inject.Injector;
import edu.clemson.asam.parser.antlr.AsamParser;
import edu.clemson.asam.services.AsamGrammarAccess;
import edu.clemson.asam.ui.internal.AsamActivator;

// Based on EMV2AnnexParser from Error Model annex
public class AsamAnnexParser implements AnnexParser {
    private AsamParser parser;

    protected AsamParser getParser() {
        if (parser == null) {
            Injector injector = AsamActivator.getInstance().getInjector(
            		AsamActivator.EDU_CLEMSON_ASAM_ASAM);
            parser = injector.getInstance(AsamParser.class);
        }
        return parser;
    }

    protected AsamGrammarAccess getGrammarAccess() {
        return getParser().getGrammarAccess();
    }

    public AnnexLibrary parseAnnexLibrary(String annexName, String source, String filename,
            int line, int column, ParseErrorReporter errReporter) {
        return (AnnexLibrary) AnnexParseUtil.parse(getParser(),source, getGrammarAccess().getAsamLibraryRule(), filename, line,
                column, errReporter);
    }

    public AnnexSubclause parseAnnexSubclause(String annexName, String source, String filename,
            int line, int column, ParseErrorReporter errReporter) {
    	return (AnnexSubclause) AnnexParseUtil.parse(getParser(),source,getGrammarAccess().getAsamSubclauseRule(),filename,line,column, errReporter);
    }


}