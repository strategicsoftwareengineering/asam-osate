package edu.clemson.asam.unparsing;

import org.eclipse.xtext.serializer.ISerializer;
import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.annexsupport.AnnexUnparser;

import com.google.inject.Inject;
import com.google.inject.Injector;

import edu.clemson.asam.serializer.AsamSerializer;
import edu.clemson.asam.ui.internal.AsamActivator;

public class AsamAnnexUnparser implements AnnexUnparser {
    @Inject private ISerializer serializer;

    protected ISerializer getSerializer() {
        if (serializer == null) {
            Injector injector = AsamActivator.getInstance().getInjector(
            		AsamActivator.EDU_CLEMSON_ASAM_ASAM);
            serializer = injector.getInstance(AsamSerializer.class);
        }
        return serializer;
    }

	@Override
	public String unparseAnnexLibrary(AnnexLibrary library, String indent) {
		return indent + getSerializer().serialize(library);
	}

	@Override
	public String unparseAnnexSubclause(AnnexSubclause subclause, String indent) {
		return indent + getSerializer().serialize(subclause);
	}
}